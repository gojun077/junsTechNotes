Creating Win11 VM for KVM/QEMU Hypervisor
=============================================

# Summary

- Created on: Jan 31 2023
- Created by: gojun077@gmail.com
- Last Updated: Jan 12 2024

My notes on tips and tricks for getting a Win11 VM to run properly on
KVM.

# Topics

## References

The guides I referred to for the installation are as follows:

- https://getlabsdone.com/how-to-install-windows-11-on-kvm/
- https://dev.to/christianbueno1/install-windows-11-virtual-machine-on-kvm-libvirt-qemu-virt-manager-in-linux-fedora-3637-rpm-based-distribution-1nkd

The first link is more comprehensive and gets most things right, but it
contains outdated information regarding the UEFI ovmf firmware image that
should be used. In the guide, it recommends selecting `OVMF_CODE.fd`
firmware, but after the Win11 22H2 update, the firmware image needs to be
the 4MB version `/usr/share/edk2/ovmf-4m/OVMF_CODE.secboot.fd`.
This path is valid on Fedora 37.

Also, you will have to copy `/usr/share/edk2/ovmf-4m/OVMF_VARS.fd`
to `/var/lib/libvirt/qemu/nvram/myVMname_VARS.fd`, overwriting
the existing file. Then you may need to add an `<nvram>` within the `<os>`
block (if it doesn't exist already) that contains the following:

```
<nvram>/var/lib/libvirt/qemu/nvram/myVMname_VARS.fd</nvram>
```

The second link uses the correct UEFI firmware image `OVMF_CODE.secboot.fd`
but it doesn't use the correct TPM settings.

## Fixing the libvirt xml for Win11 VM version < 22H2

Old xml snippet from win11 VM running on KVM; this config uses `libvirt`'s
firmware autodetection, which fails to load the proper UEFI ovmf
firmware for secure boot on Win11 VM's. In the below examples, assume that
the VM is named `win11`

```xml
  <os firmware="efi">
    <type arch="x86_64" machine="pc-q35-6.2">hvm</type>
    <loader secure="yes"/>
    <bootmenu enable="yes"/>
  </os>
  <features>
    <acpi/>
    <apic/>
    <hyperv mode="custom">
      <relaxed state="on"/>
      <vapic state="on"/>
      <spinlocks state="on" retries="8191"/>
    </hyperv>
    <vmport state="off"/>
  </features>
```

The workaround is listed [here](https://github.com/tianocore/edk2/discussions/3221#discussioncomment-3896063)
and also requires the following edits to the `<os>` and `<features>` xml
blocks:

```xml
  <os>
    <type arch="x86_64" machine="pc-q35-7.0">hvm</type>
    <loader readonly="yes" secure="yes" type="pflash">/usr/share/edk2/ovmf-4m/OVMF_CODE.secboot.fd</loader>
    <nvram>/var/lib/libvirt/qemu/nvram/win11_VARS.fd</nvram>
    <bootmenu enable="yes"/>
  </os>
  <features>
    <acpi/>
    <apic/>
    <hyperv mode="custom">
      <relaxed state="on"/>
      <vapic state="on"/>
      <spinlocks state="on" retries="8191"/>
    </hyperv>
    <vmport state="off"/>
    <smm state="on"/>
  </features>
```

Note that in the `<os>` block, `firmware` is no longer specified, which
disables firmware auto select. Also within this block, I added parameters in
the `<loader>` sub-block:

- `readonly="yes"`
- `type="pflash"`
- `/usr/share/edk2/ovmf-4m/OVMF_CODE.secboot.fd`
  + path to the secure boot loader that works with MS Windows

Next, I added `<nvram>` block within the `<os>` block. This contains
variables used by the secure boot loader and has been renamed to include
the name of the VM.

```xml
<nvram>/var/lib/libvirt/qemu/nvram/win11_VARS.fd</nvram>
```

This file has been renamed and copied from `/usr/share/edk2/ovmf-4m/OVMF_VARS.fd`
on Fedora / RHEL-based distros.

This OVMF firmware blob issue only appeared after the Win11 22H2 version
was released. Previous Win11 versions installed fine on KVM/QEMU VM's even
without the `ovmf-4m` secure boot firmware images, but with the new Win11
patches released in Aug/Sept 2022, existing Win11 VM's running with the
wrong UEFI firmware were not able to install Win11 updates, specifically
`KB5012170`, emitting error `0x800f0922`. This also blocked subsequent
updates.

## Changed edk2 ovmf firmware filenames in Fedora 39


## Problems installing Redhat Drivers due to digital signature

I recently ran into a strange issue in one of my Win11 VM's on KVM
using TPM 2.0 Secure Boot + UEFI. The Redhat `qxldod.sys` driver
refuses to load because the digital signature is not recognized.

On one of my Win11 VM's with Secure Boot enabled, the `qxldod.sys`
driver is signed by `Microsoft Windows Hardware Compatibility`,
but in the other Win11 VM, the same driver is signed by
`Red Hat Inc`. I think the difference is that my other Win11 VM used
a `virtio-win` package containing WHQL signed drivers (which is only
available to Redhat subscribers).

You can also use a test cert provided by the Fedora `virtio-win.iso`
but you have to follow the process below:

From https://github.com/virtio-win/virtio-win-pkg-scripts/blob/master/README.md#virtio-win-driver-signatures

> Warning: Due to the signing requirements of the Windows Driver Signing
> Policy, drivers which are not signed by Microsoft will not be loaded by
> some versions of Windows when Secure Boot is enabled in the virtual
> machine. See bug #1844726. The test signed drivers require enabling to
> load the test signed drivers.

https://learn.microsoft.com/en-us/windows-hardware/drivers/install/the-testsigning-boot-configuration-option

https://learn.microsoft.com/en-us/windows-hardware/drivers/install/installing-test-certificates

But it is supposedly still possible to download the WHQL signed Redhat
virtio-win drivers from CentOS and Rocky Linux package archives.

- https://dl.rockylinux.org/pub/rocky/9.3/AppStream/x86_64/os/Packages/v/
- http://mirror.centos.org/centos/8-stream/AppStream/x86_64/os/Packages/

After downloading the `.rpm` file, you can extract the contents using
`rpm2cpio` and `cpio` utils:

```sh
rpm2cpio myfile.rpm | cpio -idmv
```

After mounting the `.iso` you can install all Windows guest drivers
including `virtio` (Storage and I/O) and `qxldod` (video adapter) by
running the `.exe` or you can install drivers individually by going to
Device Manager and clicking on add hardware and pointing to specific
folders in the `.iso`.


## Improving Performance of Windows VM's

Compared to Linux VM's, Windows VM's often feel sluggish. This can happen
for several reasons, such as:

- Automatic updates
  + the update backlog can get huge if you don't use your VM often
  + many of the updates are mandatory and can't be disabled
- Windows File Indexing Service
  + indexing files takes up lots of CPU, esp. when first indexing
- Windows Edge startup processes
  + news, weather, and other MS Edge processes startup at boot

By disabling these features, you can get a speed boost in your Windows VM

### Disable Windows File Indexing Service

I mainly use my Windows VM's to do one-off tasks that require Windows OS
(some Korean gov't websites and Korean financial websites), so I don't
need to have a file indexer always running in the background. Disabling
the file indexing service will free up quite a bit of CPU resources.

Open the File Explorer and right-click on your hard drive (usually `C:\`),
uncheck the box *Allow files on this drive to have contents indexed...*
(it will also ask you if you want to turn off indexing recursively for
sub-folders on `C:\`), and click *OK*. It will then un-index previously
indexed files, which could take a few minutes or much longer depending on
how many files were already indexed.

### Disable Unused Startup Services

Press `Ctrl-Alt-Del` or search for the system monitor in the Start menu and
in select the *Startup* tab. Here you can disable specific startup
services.
