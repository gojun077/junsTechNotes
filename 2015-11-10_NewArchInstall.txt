## New Arch installation on DELL XPS M1330

# get internet connection (the wifi switch was turned off)
iwctl


# activate wired interface so we can install remotely via GNU Screen
# change address and iface name for your particular HW
ip a add 192.168.10.58/24 broadcast 192.168.10.255 dev enp9s0
# activate sshd.socket (instead of sshd.service)
systemctl start sshd.socket
# change root PW so we can remotely connect through ssh & screen
passwd
...
# find name of local disk
lsblk
# create partition table (gdisk for gpt, fdisk for dos)
# for new GPT partition, press 'o' which will erase
# all existing partition info (and will not convert MBR
# to GPT)
# also change gdisk type to 'ef00' EFI System Partition
gdisk
n, 1, default, +512M
ef00

# Note EFI System Partition must be formatted as FAT32
# for UEFI systems this partition will be mounted as /boot!
mkfs.fat -F32 /dev/sdxy

# create partitions (GPT: BIOS boot part, /boot and LVM part)
gdisk
n,1 +2M type ef02 (MBR BIOS boot for GPT)
n,2,+300M type 8300 (Linux Part) #unencrypted /boot
n,3,(default, default) type 8E00 (LVM)
# create LUKS container for LVM volumes
cryptsetup luksFormat /dev/sda3
# open LUKS container and use mountpoint 'lvm'
cryptsetup open --type luks /dev/sda3 lvm
# create PV in the open LUKS container
pvcreate /dev/mapper/lvm
# create VG in the PV
vgcreate ARCH /dev/mapper/lvm
# create all logical volumes in VG
lvcreate -L 2G ARCH -n swapvol
lvcreate -L 25G ARCH -n rootvol
lvcreate -L 20G ARCH -n varvol
lvcreate -L 40G ARCH -n homevol
lvcreate -l +100%FREE ARCH -n multivol
# create swap
mkswap /dev/ARCH/swapvol

#==========================================================================
# make file system on LV's and regular partitions (MBR /boot)
for i in {rootvol,varvol,homevol,multivol}; do mkfs.ext4 /dev/ARCH/$i; done
mkfs.ext4 /dev/sda2 (sda1 is the MBR BIOS boot for GPT)
# create mount points and mount system partitions (MBR)
mount /dev/ARCH/rootvol /mnt
mkdir /mnt/{boot,var,home,MULTIMEDIA}
mount /dev/sda1 /mnt/boot
mount /dev/ARCH/varvol /mnt/var
mount /dev/ARCH/homevol /mnt/home
mount /dev/ARCH/multivol /mnt/MULTIMEDIA
#==========================================================================
# make file systems on LV's (GPT, EFI System partition will be /boot;
# already created fat32 on this partition)
for i in {rootvol,varvol,homevol}; do mkfs.ext4 /dev/ARCH/$i; done
mkfs.ext4 /dev/sda{1,2}
# create mount points and mount system partitions (GPT)
mkdir /mnt/{boot,var,home,MULTIMEDIA}
mount /dev/ARCH/rootvol /mnt
mount /dev/sdb1 /mnt/boot
mount /dev/ARCH/varvol /mnt/var
mount /dev/ARCH/homevol /mnt/home
mount /dev/sda1 /mnt/MULTIMEDIA
#==========================================================================

# activate swap
swapon /dev/ARCH/swapvol

# edit mirrorlist with fast mirrors
vim /etc/pacman.d/mirrorlist
# build packages
pacstrap /mnt base linux linux-firmware
# generate fstab file
genfstab -U /mnt >> /mnt/etc/fstab
# change root into new system
arch-chroot /mnt /bin/bash
# install add'l packages (these packages should not require X11)
# for UEFI GPT don't install grub
pacman -Syyu --noconfirm iwd screen mc vim cronie dialog wpa_supplicant libnl \
  git cmus quodlibet alsa-utils alsa-tools emacs shellcheck bitlbee ncdu \
  android-tools android-udev htop irssi dhclient ethtool openssh firewalld \
  nfs-utils traceroute iptraf-ng rfkill docker intel-ucode wget grub syslinux \
  gptfdisk lvm2 sudo


# set hostname
echo someHostname > /etc/hostname
# add hostname to /etc/hosts
someHostname.localdomain someHostname
# set time zone
ln -s /usr/share/zoneinfo/Asia/Seoul /etc/localtime
# set time and date (if you haven't done so already)
date -s "XXXX-XX-XX YY:YY"
hwclock -w
# enable locales in /etc/locale.gen (en_US)
vim /etc/locale.gen
# generate locales
locale-gen
# set locale preferences in /etc/locale.conf
echo "LANG=$LANG" > /etc/locale.conf

# add LUKS and LVM modules to kernel config /etc/mkinitcpio.conf
vim /etc/mkinitcpio.conf
# HOOKS=(base udev autodetect keyboard keymap consolefont modconf block encrypt lvm2 filesystems fsck)
# regenerate initramfs image
mkinitcpio -P


# install bootloader to drive OS was installed to
# Note: all should go well, without errors
# if you get an error about an encrypted, /boot, check that
# /boot is, in fact, mounted! For some reason we had to remount
# boot after chrooting...
grub-install --recheck /dev/sda
# generate grub.cfg
# Note that installing GRUB on LVM in a chroot env may cause warnings
# like 'WARNING: failed to connect to lvmetad' this is because /run
# is not available inside the chroot

# add cryptdevice info to /etc/default/grub
# note that the device UUID should be that of the raw crypto device
vim /etc/default/grub
GRUB_CMDLINE_LINUX_DEFAULT="cryptdevice=UUID=5a2527d8-8972-43f6-a32d-2c50f734f7a9:ARCH root=/dev/mapper/ARCH-rootvol"
GRUB_CMDLINE_LINUX=""
# Note: if /boot is also inside a LUKS container, you would have to add
# 'cryptodisk luks' to GRUB_PRELOAD_MODULES

# include CPU microcode in GRUB
grub-mkconfig -o /boot/grub/grub.cfg
# Note that grub-mkconfig will detect intel-ucode and make
# /boot/intel-ucode.img the first initrd in the bootloader

# Change root passwd
passwd
# exit from chroot
exit
# unmount chroot partitions
umount -R /mnt
# reboot
reboot

## AFTER REBOOT
#create new user
useradd -m archjun
#add user to group wheel
usermod -a -G wheel archjun
# edit /etc/sudoers to enable wheel
visudo
#enable sshd
systemctl enable sshd
systemctl start sshd
# enable iwd
systemctl enable iwd
systemctl start iwd

##  download PKGBUILD's from AUR (download to ~/Downloads)
mkdir $HOME/Downloads
# Dropbox
wget https://aur.archlinux.org/cgit/aur.git/snapshot/dropbox.tar.gz
# dropbox-cli
wget https://aur.archlinux.org/cgit/aur.git/snapshot/dropbox-cli.tar.gz
# Spideroak One
wget https://aur.archlinux.org/cgit/aur.git/snapshot/spideroak-one.tar.gz
# smtp-cli
wget https://aur.archlinux.org/cgit/aur.git/snapshot/smtp-cli.tar.gz

# extract AUR pkg's (extract to ~/Downloads/AUR/)
cd ~/Downloads
mkdir -p $HOME/Downloads/AUR
tar -xvf dropbox.tar.gz -C ./AUR/
tar -xvf dropbox-cli.tar.gz -C ./AUR/
tar -xvf spideroak-one.tar.gz -C ./AUR/
tar -xvf smtp-cli.tar.gz -C ./AUR/

# setup Spideroak
# https://spideroak.com/faq/how-do-i-set-up-a-new-device-from-the-command-line
SpiderOakONE --setup=-
ln -s $HOME/"SpiderOak Hive" $HOME/SpiderOak_Hive
# setup Dropbox
# http://www.dropboxwiki.com/tips-and-tricks/install-dropbox-in-an-entirely-text-based-linux-environment

# setup ALSA - all channels are muted by default; go to MASTER and press 'm'
alsmixer
# setup cmus
# copy all the files from ~/.config/cmus/ from another machine
# to the new machine; o/w cmus sometimes refuses to even start
# https://wiki.archlinux.org/index.php/Cmus
# https://bbs.archlinux.org/viewtopic.php?id=203305 - extra settings for ALSA

scp * archjun@192.168.10.58:~/.config/cmus/

# setup bitlbee (refer to separate tut)
chown -R bitlbee:bitlbee /var/lib/bitlbee

# clone github repo 'dotfiles'
git clone https://github.com/gojun077/jun-dotfiles.git $HOME/dotfiles
# clone bitbucket repo 'bin'
git clone https://gojun077@bitbucket.org/gojun077/bin-scripts.git $HOME/bin
# run dotfiles symlink script
bash $HOME/dotfiles/create_symlinks.sh

# setup pomo2beeminder
mkdir $HOME/Documents

git clone https://github.com/gojun077/pomo2beeminder.git $HOME/Documents/pomo2beeminder
