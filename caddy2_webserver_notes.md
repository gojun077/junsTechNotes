Caddy 2 Webserver
=====================

# Summary

- Created on: May 10 2023
- Created by: gopeterjun@naver.com
- Last Updated: May 10 2023

Caddy is a webserver written in Go that has a simple configuration syntax
and automatically uses HTTPS with Let's Encrypt certs even for internal web
apps. This open source project was created by Matt Holt and its performance
compares favorably with `nginx` and `apache`, but with much less admin
overhead, as the config syntax for both nginx and apache is quite
complicated.


# Topics

## Install Caddy web server packages

### Debian-based Linux distros

https://caddyserver.com/docs/install#debian-ubuntu-raspbian

```sh
sudo apt install -y debian-keyring debian-archive-keyring apt-transport-https curl
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/gpg.key' | sudo gpg --dearmor -o /usr/share/keyrings/caddy-stable-archive-keyring.gpg
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt' | sudo tee /etc/apt/sources.list.d/caddy-stable.list
sudo apt update
sudo apt install caddy
```

### RHEL-based Linux distros

https://caddyserver.com/docs/install#fedora-redhat-centos

https://copr.fedorainfracloud.org/coprs/g/caddy/caddy/

```sh
dnf install 'dnf-command(copr)'
dnf copr enable @caddy/caddy
dnf install caddy
```

## Use Caddy to manage Tailscale HTTPS certs

https://tailscale.com/blog/caddy/

> With the beta release of Caddy 2.5, Caddy automatically recognizes and
> uses certificates for your Tailscale network (`*.ts.net`), and can use
> Tailscale’s HTTPS certificate provisioning when spinning up a new
> service.

The version of Caddy I'm running on my Raspberry Pi 3B running RPi OS 64-bit
is 2.6.4 as of May 2023, so Caddy support for Tailscale is built-in.

> To use Caddy with your Tailscale network, first make sure you have HTTPS
> certificates enabled on your tailnet. Then you will either need to run
> Caddy as root, or configure the Caddy user to have access to Tailscale’s
> socket.

https://tailscale.com/kb/1190/caddy-certificates/#provide-non-root-users-with-access-to-fetch-certificate

> If Caddy is running as a non-root user, such as when it runs on Debian as
> caddy, you need to modify `/etc/default/tailscaled` to grant the user
> access to fetch the certificate. In `/etc/default/tailscaled`, set the
> `TS_PERMIT_CERT_UID` environment variable to the name or ID of the
> non-root user:

```
TS_PERMIT_CERT_UID=caddy
```

Note that `/etc/default/tailscaled` has default permissions `444` so
you will have to change this before editing the file:

Run the following as root user:

```sh
cd /etc/default
chmod 644 tailscaled
vim tailscaled
chmod 444 tailscaled
```
