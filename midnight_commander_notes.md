Midnight Commander HOWTO
============================================================

# Summary

- Created on: 21 Mar 2023
- Created by: gojun077@gmail.com
- Last Updated: Fri 04 Oct 2024

Midnight Commander `mc` is a TUI file and directory viewer inspired by
Norton Commander for DOS. The project was started in 1994 and uses the GNU
GPL open source license. It contains its own FTP and FISH client for
connecting to remote machines via FTP and SSH, respectively, and also
includes its own editor `mcedit` for viewing and editing files from within
`mc`. It also supports mouse input.

`mc` is really handy for bulk file transfers and bulk file renaming, and
since version 4.70, it supports Unicode. It is easily customizable and
users can add custom menus, scripts, and color themes (*skins*). This file
contains snippets and other notes I have found useful while using `mc`.

# Topics

## Main menu

`F9` to access main menu.

## Select/de-select files

With the cursor located on a file in either Left or Right pane, press `C-t`
(Ctrl-t) to select a file. You can also select a file by holding down
`<Shift> up/down arrow`. You can de-select by pressing `C-t` once more or
`<Shift> up/down arrow` on previously selected files.

## Copy files

Once you have selected files in one of the panes, press `<F5>` (or open the
*File* menu and select *Copy*) and you will be prompted by a dialog box to
copy the selected files to the opposite pane. Note that you do not need to
move to the opposite pane with `<TAB>` before copying!

Note that you cannot use `<F5>` to copy symlinked files.

## Create new folder

`F7`

## Connect to remote machine via ssh

You can connect one pane to a remote machine via `ssh`. In `mc`, this is
called a `Shell Link` and can be accessed via `<F9>-r-h` for the right
pane and `<F9>-l-h` for the left pane.

## Change color theme / skin

To use skins, you must first make sure that the following directory exists
and create it if it is absent:

```sh
~/.local/share/mc/skins
```

You must then copy your `.ini` skin/theme files into the above path.

Once your skin/theme `.ini` files are in the proper path, you can apply new
skins via the Midnight Commander menu via `<F9>-o-a` or *Options* ->
*Appearance*. Press `<SPACE>` to get a drop-down menu of all available
skins. There are over a dozen default skins you can choose from, but custom
themes are also available on Github. After copying or symlinking these
`.ini` theme files into `~/.local/share/mc/skins/` you should be able to
see them in the drop-down menu. You can also set the skin declaratively in
`~/.config/mc/ini` via key=keyval `skin=<skinName>`.

## Open file from within `mc`

Select the file and press `<F3>` to view the contents of a file. Press
`<F10>` to exit the file view.

## Note about `Meta-` keychords on MacOS

The `M-!` (filtered view, run command on file), `M-?` (find file) commands
keychords might not work on MacOS depending on your terminal, because MacOS
does not interpret the `Option` key as `Meta` unless you set a particular
option in your terminal. `iterm2` supports this option, but terminals like
`alacritty` do not. There are various workarounds, such as manually
specifying `Meta` keychord bindings for your terminal, but these might
not work perfectly.

## Keyboard shortcuts

- `TAB` to toggle between split panes
