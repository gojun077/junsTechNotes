Install LineageOS 14.1 (Nougat 7.1) on HTC Sensation
=====================================================

# References
- Get apps without GAPPS
    - https://android.gadgethacks.com/how-to/install-apps-from-play-store-without-gapps-google-services-0176241/
- FDroid OSS App Store (accesses Google Play)
    - https://f-droid.org
