Setup guide for `systemd_networkd`, `wpa_supplicant` and `dhcpcd`
=======================================================================

# Summary

- Created on: Sep 22 2021
- Created by: gojun077@gmail.com
- Last Updated: Jun 4 2024

For a Linux desktop user, you would probably just use `NetworkManager`
which will handle all the networking and `systemd` details for you, but if
you are on a resource contrained system like a RPi or on a headless machine
that doesn't run Xorg or Wayland, this guide is for you!

In this guide, all network interfaces and settings are defined in
`/etc/systemd/network/` and follow the common `.ini` file format for
`.network` files that is understood by any machine using `systemd` and
`systemd-networkd`. The latter is a big help for managing networking across
heterogenous Linux distributions. In the past, Ubuntu, RHEL, and other
distros each had their own way of defining network ifaces that was mutually
incompatible. With the advent of `systemd-networkd`, it doesn't matter which
Linux distro you are on, network configuration now uses a common syntax if you
decide to use the `systemd` default network mgmt service.

There still are some idiosyncrasies that you have to keep in mind across
distro's, however. While rolling update distros like Archlinux offer cutting
edge `systemd` features like per-interface service templates, these features
are not provided OOTB in Ubuntu, and you will have to manually create these
service template files in `/etc/systemd/system/`.

Setting up LAN interfaces is straightforward in `systemd-networkd`, but
getting wireless interfaces in a working state without any user intervention
takes a bit more work. Once `systemd-networkd` has brought up the wireless
network interface, `wpa_supplicant` must be manually executed on the iface
at least once. This involves running the `wpa_cli` utility, scanning for
wifi Access Points (AP), choosing an AP, and associating with it by submitting
a Pre-Shared Key (psk). Once the connection succeeds, this information will
then be saved into a your `wpa_supplicant-<iface>.conf` file located in
`/etc/wpa_supplicant/`.

# Topics

## `systemd-networkd`

All interfaces that need to automatically come up on boot must be defined
in `/etc/systemd/network/`. You can view the status of all interfaces
being managed by systemd-networkd by invoking **`networkctl`**.

Each interface file has the extension `.network`, while misc settings
are stored in `.netdev` files (i.e. `br0.network`, `br0.netdev`).


## `wpa_supplicant`

`wpa_supplicant` is the most popular and long-lived low-level wireless
connection daemon now that `wext` has been deprecated. Although there are
newer wireless daemons like `iwd`, at the time of writing in Sep 2021 it
is not as stable as `wpa_supplicant` and its performance leaves alot to be
desired even though it might have cleaner code and be easier to maintain.

`wpa_supplicant` can be run manually with the following options:

- `-B` run in background daemon mode
- `-i` the wireless network interface to connect to
- `-c` absolute path to the config file
  + distinct files per iface, i.e. `wpa_supplicant-wlan{0,1}.conf`
  + naming convention is `wpa_supplicant-<ifaceName>.conf`

These conf files go under the `/etc/wpa_supplicant/` directory. A sample
`wpa_supplicant-wlan0.conf` will look something like the following:

```
ctrl_interface=/run/wpa_supplicant
update_config=1

network={
        ssid="mySSID"
        psk="yourWiFiPW"
}
```

### Ubuntu

There is no need to run `wpa_supplicant` manually, however. By default
there will already be a `wpa_supplicant.service` running when you first
login. We will replace this with an interface-specific `wpa_supplicant`, so
do the following:

```sh
systemctl stop wpa_supplicant
systemctl disable wpa_supplicant
systemctl enable wpa_supplicant@wlan0
```

This will create a symlink named `wpa_supplicant@wlan0.service` inside
`/etc/systemd/system/multi-user.target.wants/` which is linked from
the template `/lib/systemd/system/wpa_supplicant@.service`.

Now we need to setup `dhclient` to get a DHCP lease from the
wireless router.


## `dhcp`

### Archlinux

You need to have some kind of dhcp client daemon running on the network
interfaces which need a dhcp lease to get an IP from a dhcp server.
In a SOHO setup, the dhcp server will probably be running on your integrated
router/wireless AP and the network interface requiring a dhcp lease will
be your wireless interface, i.e. something like `wlan0` or `wlp2s0`.

On a Linux distro with a recent version of `systemd`, you should be able to
create a `@<iface>.service` from systemd templates in
`/usr/lib/systemd/system/`. Templates exist for `dhclient@.service`,
`dhcpd@.service`, `wpa_supplicant@.service` etc. You can create a custom
`dhclient` service for a single network iface simply by invoking `systemctl enable
dhclient@wlan0`, for example.

Doing so will create `dhclient@wlan0.service` in
`/etc/systemd/system/multi-user.target.wants/` which will be executed on startup
in runlevel 5 (multi-user).

Note that the path to `dhclient` is `/usr/bin/dhclient` on Archlinux.

### Ubuntu

Ubuntu doesn't have a systemd template for `dhclient` so you will need to
create `dhclient@.service` in `/lib/systemd/system`. It should look
something like:

```
[Unit]
Description=dhclient on %I
Wants=network.target
Before=network.target

[Service]
ExecStart=/usr/sbin/dhclient -pf /var/run/dhclient@%i/dhclient.pid -d %I
RuntimeDirectory=dhclient@%i
ProtectSystem=on
ProtectHome=on

[Install]
WantedBy=multi-user.target
```

On Ubuntu, `dhclient` is located in `/usr/sbin/dhclient`, whereas
it is in `/usr/bin/dhclient` on Archlinux.

Enable the service with `systemctl enable dhclient@wlan0`.

You will also need to edit the apparmor profile for `dhclient` in
`/etc/apparmor.d/sbin.dhclient` and after the line
`/{,var/}run/dhclient*.pid lrw,` add the new lines:

```
/{,var/}run/dhclient*/dhclient.pid lrw,
/{,var/}run/dhclient*/dhclient*.lease* lrw,
```

And then reload this apparmor config with `apparmor_parser -r
/etc/apparmor.d/sbin.dhclient`. Now you can start the service with
`systemctl start dhclient@wlan0`. If you don't edit the apparmor profile
for dhclient, you will see the following error in the systemd logs for
`dhclient@wlan0.service`:


```sh
Jan 18 17:46:10 pj0 dhclient[5325]: Can't create /var/run/dhclient@wlan0/dhclient.pid: Permission denied
```

If you haven't already started `wpa_supplicant@wlan0.service` do so now.

**Note**: `dhclient` is deprecated (I think it was deprecated in 2022?)
so you shouldn't use it. Use `dhcpcd` instead, or better yet, just use
`nmcli` to create persistent configs for your network interfaces. It is
much easier than creating custom systemd service files!

## `systemd-resolved`


## Check your work

Go to `/etc/systemd/system/multi-user.target.wants` and list the contents
of the directory. You should at least have the 4 following systemd
`.service` files:

- `dhcpcd@wlan0.service` or `dhclient@wlan0.service`
- `systemd-networkd.service`
- `systemd-resolved.service`
- `wpa_supplicant@wlan0.service`
