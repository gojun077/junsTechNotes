Notes on Setting Up Slack Webhooks
=====================================

# Summary

- Created on: Dec 30 2021
- Created by: gojun077@gmail.com
- Last Updated: Jan 19 2022

Slack webhooks are endpoints of the form `https://hooks/slack.com/<secret>`
that don't require any authentication but allow external clients to send
limited-size JSON text payloads to specific Slack channels that have
webhooks enabled. Before 2021, it was possible to generate webhooks without
registering a Slack app, but at the time of me writing this in Dec 2021,
you are only allowed to generate webhooks for a channel if you've created
a Slack app with privileges to create a webhook in a certain channel.


# Topics

## Setup incoming webhooks

https://slack.com/help/articles/115005265063-Incoming-webhooks-for-Slack#set-up-incoming-webhooks

### Create a new Slack app

https://api.slack.com/apps/new

Set the App Name, pick the workspace your app will be able to work in. If
your org has Slack admins, they will have to approve your new app.

You will then be taken to a settings screen for your new app. On the left
hand menu click on *Incoming Webhooks*. Click the *Off* button to *On*.
At the bottom click on the button *Request to Add New Webhook*.

Once you are approved, from the lefthand menu select *Install to Workspace*.
You will then be prompted to select a channel to post to as an app.

Finally you will be shown your webhook address. Make sure it is not revealed
publically, i.e. in a Github public repo, as anyone from the Internet will
then be able to spam your Slack channel.

## Test your webhook

Here is a quick-and-dirty way to test that your webhook works via `curl`:


```sh
curl -X POST -H 'Content-type: application/json' \
--data '{"text":"Hello, World!"}' \
https://hooks.slack.com/services/your/secret/webhookserial
```
