#+TITLE: Asahi Linux setup HOWTO
#+SUBTITLE: Asahilinux Fedora Remix for Apple Silicon
#+AUTHOR: Peter Jun Koh
#+EMAIL: gopeterjun@naver.com
#+DESCRIPTION: Notes and snippets related to setting up Asahi Linux on Apple Mac
#+KEYWORDS:  linux, arm64, sysadmin, desktop
#+LANGUAGE:  en

* Summary

- Created on: [2024-05-02 Thu]
- Last Updated: [2024-12-12 Thu 22:57]

As of Apr 2024, Asahilinux Fedora Remix (based on Fedora 39) can be used as
a daily driver for Macbooks with native HDMI ports because external display
support has been added. Macbooks that lack HDMI ports are not yet supported
because that will require drivers for virtual Display Port (DP), but
according to the Asahilinux core dev team this should be completed by the
end of 2024.

I installed the Asahilinux minimal version which comes without KDE or GNOME
desktops and then installed Sway BSP tiling Window Manager (WM).

This guide contains tips and tricks for getting Asahilinux minimal install
into a usable state with Sway WM on Wayland desktop.

*Note 2024.09.16*: I am now using Asahilinux based on Fedora 40. After the
official Fedora release, the Asahilinux SIG usually releases an Asahilinux
rebase from the official release within 1 month.

* Topics

** Bluetooth

- ~bluetoothctl~
  - provided by the ~bluez~ package on most distros

- =bluetuith= I highly recommend this!
  - https://github.com/darkhz/bluetuith
  - written in Go, binaries available for download from GH

- https://major.io/p/bluetooth-automatic-switch/
  #+begin_src sh
    pactl load-module module-switch-on-connect
  #+end_src

  I have yet to confirm whether the above snippet works or not on my
  Asahilinux F40 system.

** Change power button behavior

By default, if you even briefly press your power button, Asahilinux will
initiate an immediate shutdown. This is normally not the behavior you want;
Since most modern distros use ~systemd-logind~ daemon to manage logins, to
disable the default /powerdown/ behavior you can do the following:

As root, open =/etc/systemd/logind.conf= in an editor. Uncomment the
following two lines:

#+begin_src conf
HandlePowerKey=poweroff
HandlePowerKeyLongPress=ignore
#+end_src

And I edited their values to the following:

#+begin_src conf
HandlePowerKey=ignore
HandlePowerKeyLongPress=poweroff
#+end_src

Next you must apply these config changes and restart =systemd-logind=

#+begin_src sh
sudo systemctl daemon-reload
sudo systemctl restart systemd-logind
#+end_src

Note that restarting =systemd-logind= will log you out of your current GUI
session. When you log into a graphical desktop session again, you will find
the short powerbutton presses will no longer shut down your machine:

#+begin_src sh
jundora@asahilinux ~$ systemctl status systemd-logind
● systemd-logind.service - User Login Management
     Loaded: loaded (/usr/lib/systemd/system/systemd-logind.service; static)
    Drop-In: /usr/lib/systemd/system/systemd-logind.service.d
             └─10-grub2-logind-service.conf
             /usr/lib/systemd/system/service.d
             └─10-timeout-abort.conf
     Active: active (running) since Tue 2024-07-09 18:24:36 KST; 2h 22min ago
       Docs: man:sd-login(3)
             man:systemd-logind.service(8)
             man:logind.conf(5)
             man:org.freedesktop.login1(5)
   Main PID: 2791 (systemd-logind)
     Status: "Processing requests..."
      Tasks: 1 (limit: 19042)
   FD Store: 6 (limit: 512)
     Memory: 7.6M (peak: 11.0M)
        CPU: 64ms
     CGroup: /system.slice/systemd-logind.service
             └─2791 /usr/lib/systemd/systemd-logind

Jul 09 18:24:36 asahilinux systemd-logind[2791]: Watching system buttons on /dev/input/event2 (Apple SMC >
Jul 09 18:24:36 asahilinux systemd-logind[2791]: Watching system buttons on /dev/input/event1 (Apple Inte>
Jul 09 18:24:36 asahilinux systemd-logind[2791]: Watching system buttons on /dev/input/event4 (Logi K855 >
Jul 09 18:24:36 asahilinux systemd-logind[2791]: New session 1 of user jundora.
Jul 09 18:24:36 asahilinux systemd[1]: Started systemd-logind.service - User Login Management.
Jul 09 18:25:03 asahilinux systemd-logind[2791]: Power key pressed short.
#+end_src

This is a must for Asahilinux, because most people who use Macbooks have
muscle memory from MacOS, where touching the power button is needed to
wake up the system from sleep and fingerprint reading for login and as an
alternative to entering your password.

As of July 2024, fingerprint reader support is still not available on
Asahilinux.

** Input Method Editor ~fcitx5~ and Hangul input

You need to change the input switcher hotkey from the default ~C-SPC~
because the latter conflicts with ~set mark~ key chord in Emacs.

** Sway Mouse settings

First run ~swaymsg -t get_inputs~ which will give you the device ID for
your mouse. At the office I use an Apple Magic Mouse, but at home I use a
Logitech MX3 Anywhere. Settings for each mouse need to be tweaked.

Here is an example of my Apple Magic Mouse 2 settings:

#+begin_src text
# settings for Apple Magic Mouse on Asahi Linux
input "76:617:peter.koh___s_Mouse" {
    accel_profile flat
    pointer_accel -0.1
    natural_scroll enabled
    scroll_factor 1.2
    scroll_method two_finger
    middle_emulation disabled
    click_method clickfinger
    tap_button_map lrm
}
#+end_src

And here is an example of my settings for Logitech MX3 Anywhere:

#+begin_src text
input "1133:45111:MX_Anywhere_3S_Mouse" {
    accel_profile adaptive
    pointer_accel 0
    natural_scroll enabled  # scroll up moves screen down
    scroll_factor 1.0
}
#+end_src

** Sway keyboard custom keymap

At home I use a Logitech K855 mechanical Bluetooth keyboard which supports
MacOS and Windows and has dual-use key labels. For example,
~OPT/Start(Win)~, ~CMD/alt~. On MacOS, things /Just Work/, but on Linux
and Windows, sometimes the actual key actions differ from the key labels!

I have muscle memory from using an Apple Magic Keyboard at work, and am
able to use the ~CMD~ key as a ~Super/Start/Win~ key in Linux with Sway.
However, at home, I noticed that my ~CMD~ key was detected as ~L_Alt~ and
my ~Option~ key was detected as ~Super_L~ on my Logitech K855 Bluetooth
mechanical keyboard. I verified this with the ~wev~ keycode detection
utility.

Wayland supports the legacy ~xkeyboard-config~ syntax from X11, although it
does not depend on X11 for keycode remapping. ~man xkeyboard-config~ shows
tons of xkb options, and for simple key remapping, you can just use the
pre-defined remaps. I wanted to swap /Left Windows Key/ with /Left Alt/,
and this option is ~altwin:swap_lalt_lwin~. Translated to sway libinput
format:

#+begin_src text
input "1133:45938:Logi_K855_Keyboard" {
    # Left Alt is swapped with Left Win
    xkb_options altwin:swap_lalt_lwin
}
#+end_src

The ~xkeyboard-config~ manpage contains dozens of of these options and key
swaps. It is even possible to define your own custom xkb keymap file for
Dvorak or other layouts, but that is a subject for a separate HOWTO.

On MacOS, ~Hyper~ key is ~Shift+Ctrl+Opt+Cmd~, on Windows
~Shift+Ctrl+Alt+Windows~, and on Linux you can map 4 keys to a single
key. Originally the /Space Cadet keyboard/ for Lisp machines had a physical
Hyper key. This is a good key to use for custom shortcuts because most apps
don't use this key combination.

** brightness control (screens, keyboard backlights, etc)

In my Asahilinux Fedora Sway session, the brightness keys F1 and F2 do not
work OOTB, but there is a utility, ~brightnessctl~ which can list devices
with leds and control their brightness. It is available from the default
repos.

#+begin_src sh
$ brightnessctl -l
Available devices:
Device 'apple-panel-bl' of class 'backlight':
	Current brightness: 28 (6%)
	Max brightness: 500

Device 'input1::numlock' of class 'leds':
	Current brightness: 0 (0%)
	Max brightness: 1

Device 'input1::compose' of class 'leds':
	Current brightness: 0 (0%)
	Max brightness: 1

Device 'kbd_backlight' of class 'leds':
	Current brightness: 5 (2%)
	Max brightness: 255

Device 'input1::kana' of class 'leds':
	Current brightness: 0 (0%)
	Max brightness: 1

Device 'input1::scrolllock' of class 'leds':
	Current brightness: 0 (0%)
	Max brightness: 1

Device 'input1::capslock' of class 'leds':
	Current brightness: 0 (0%)
	Max brightness: 1
#+end_src

The built-in LED screen backlight on my Macbook is ~apple-panel-bl~
but as you can see above, there is also ~kbd_backlight~ for the LED's
under the chiclet keyboard on the Mac, and there are also LED's for
~capslock~ and ~numlock~.

#+begin_src sh
$ brightnessctl set 50% apple-panel-bl
Updated device 'apple-panel-bl':
Device 'apple-panel-bl' of class 'backlight':
        Current brightness: 250 (50%)
        Max brightness: 500
#+end_src

The above command sets the panel brightness to 50 of max. You can also
increment the brightness from the current brightness up or down with
~brightnessctl set +10~ or ~brightnessctl set 10-~

#+begin_src sh
$ brightnessctl get -d apple-panel-bl
270
#+end_src

In my sway config, I added the following =bindsym= directives to bind F1
and F2 to =brightnessctl=:

#+begin_src text
    bindsym XF86MonBrightnessDown exec brightnessctl set 10-
    bindsym XF86MonBrightnessUp exec brightnessctl set +10
#+end_src

Now pressing F1 and F2 from the Macbook keyboard (not the external
keyboard) increments brightness in steps of 10.

To set the brightness for ~kbd_backlight~, you can execute the following:

#+begin_src sh
$ brightnessctl -d kbd_backlight set +10
Updated device 'kbd_backlight':
Device 'kbd_backlight' of class 'leds':
	Current brightness: 15 (6%)
	Max brightness: 255
$ brightnessctl -d kbd_backlight set 10-
Updated device 'kbd_backlight':
Device 'kbd_backlight' of class 'leds':
	Current brightness: 5 (2%)
	Max brightness: 255
#+end_src

** Document Scanner

I use the Canon Pixma MG2400 (multi-function inkjet + scanner) for scanning
and Linux support for this device has been in the kernel since 2013, IIRC.
On Asahilinux F40 I have to access the scanner as root user, and because
you cannot normally launch graphical applications as root in wayland, I
had to preface the command with ~sudo -EH~

Also, ~sane~ keeps detecting the MBP webcam as the first scanning device,
so I could only get simple-scan to work with the following:

#+begin_src sh
sudo -EH simple-scan pixma
#+end_src

You can find the =sane= recognized name of your scanning device by running
=scanimage -L=. Note that this command is provided by the ~sane-backends~
package from the default Fedora repos. It should be installed automatically
as a dependency when you install ~simple-scan~.

~simple-scan~ is a Wayland-native frontend to ~sane~ (Scanner Access Made
Easy) although it has fewer options than the X11-native ~xsane~.

** Set hardware clock to use UTC instead of local time

When you run =sudo systemctl status=, you may see something like:

#+begin_src sh
$ sudo systemctl status
● asahilinux
    State: running
    Units: 511 loaded (incl. loaded aliases)
     Jobs: 0 queued
   Failed: 0 units
    Since: Wed 2024-07-24 09:13:47 KST; 55min ago
  systemd: 255.8-1.fc40
  Tainted: local-hwclock
#+end_src

The =local-hwclock= shows up as /Tainted/ because =hwclock= should ideally
use ~UTC~ which doesn't require any daylight savings adjustments.

#+begin_src sh
$ sudo hwclock -v
hwclock from util-linux 2.40.1
System Time: 1721783415.904301
Trying to open: /dev/rtc0
Using the rtc interface to the clock.
Last drift adjustment done at 1711437997 seconds after 1969
Last calibration done at 1711437997 seconds after 1969
Hardware clock is on local time
Assuming hardware clock is kept in local time.
Waiting for clock tick...
ioctl(4, RTC_UIE_ON, 0): Invalid argument
Waiting in loop for time from /dev/rtc0 to change
...got clock tick
Time read from Hardware Clock: 2024/07/24 10:10:16
Hw clock time : 2024/07/24 10:10:16 = 1721783416 seconds since 1969
Time since last adjustment is 10345419 seconds
Calculated Hardware Clock drift is 0.000000 seconds
2024-07-24 10:10:15.803657+09:00
#+end_src

=hwclock= confirms that the Realtime Clock (RTC) is using local time
instead of UTC.

#+begin_src sh
$ timedatectl status
               Local time: Wed 2024-07-24 10:15:11 KST
           Universal time: Wed 2024-07-24 01:15:11 UTC
                 RTC time: Wed 2024-07-24 10:15:11
                Time zone: Asia/Seoul (KST, +0900)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: yes

Warning: The system is configured to read the RTC time in the local time zone.
         This mode cannot be fully supported. It will create various problems
         with time zone changes and daylight saving time adjustments. The RTC
         time is never updated, it relies on external facilities to maintain it.
         If at all possible, use RTC in UTC by calling
         'timedatectl set-local-rtc 0'.
#+end_src

=timedatectl= gives the most dire warning, and recommends changing the
RealTime Clock s.t. it uses UTC. I thus edited the RTC settings as below:

#+begin_src sh
$ timedatectl set-local-rtc 0
==== AUTHENTICATING FOR org.freedesktop.timedate1.set-local-rtc ====
Authentication is required to control whether the RTC stores the local or UTC time.
Authenticating as: jundora
Password:
==== AUTHENTICATION COMPLETE ====
$ sudo hwclock -v
[sudo] password for jundora:
hwclock from util-linux 2.40.1
System Time: 1721783750.142316
Trying to open: /dev/rtc0
Using the rtc interface to the clock.
Last drift adjustment done at 1711437997 seconds after 1969
Last calibration done at 1711437997 seconds after 1969
Hardware clock is on UTC time
Assuming hardware clock is kept in UTC time.
Waiting for clock tick...
ioctl(4, RTC_UIE_ON, 0): Invalid argument
Waiting in loop for time from /dev/rtc0 to change
...got clock tick
Time read from Hardware Clock: 2024/07/24 10:15:51
Hw clock time : 2024/07/24 10:15:51 = 1721816151 seconds since 1969
Time since last adjustment is 10378154 seconds
Calculated Hardware Clock drift is 0.000000 seconds
2024-07-24 19:15:50.027787+09:00
$ timedatectl status
               Local time: Wed 2024-07-24 10:16:11 KST
           Universal time: Wed 2024-07-24 01:16:11 UTC
                 RTC time: Wed 2024-07-24 10:16:11
                Time zone: Asia/Seoul (KST, +0900)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
#+end_src

Now =systemctl status= no longer shows the RTC as /Tainted/:

#+begin_src sh
$ sudo systemctl status
● asahilinux
    State: running
    Units: 513 loaded (incl. loaded aliases)
     Jobs: 0 queued
   Failed: 0 units
    Since: Wed 2024-07-24 09:13:47 KST; 1h 2min ago
  systemd: 255.8-1.fc40
#+end_src

*References*

- https://superuser.com/questions/1427414/how-can-i-set-osx-to-use-local-time-instead-of-utc


** Setting the locale

Nowadays thanks to =systemd= you don't have to worry about manually
exporting a bunch of =LC= environment variables to your =.bashrc= or
=.bash_profile= in order to set your language. Now you can use =localectl=
to handle these settings and =/etc/locale.conf= on your behalf.

=sudo localectl status= will show your currently set locale and keyboard
layout. When I installed Asahilinux KDE Plasma version on a Macbook Air M2,
for some reason =LC_TIME= was set to ~ko_KR.UTF-8~ which caused the =date=
to show up in Korean. You can edit env vars in your locale by using
=localectl set-locale <envvar>=

#+begin_src sh
sudo localectl set-locale LC_TIME=en_US.UTF-8
#+end_src

The above changed LC_TIME from ~ko_KR.UTF-8~ to ~en_US.UTF-8~ so that the
date would show up in US format.


** Audio

In the default KDE Plasma version that is shipped with Asahilinux, audio
just works OOTB. Most of the necessary libraries and configs are already
setup for audio even on a minimal install, but you will want to setup
an audio applet in your system tray or perhaps setup the /audio/ section
in your status bar like =waybar=. In any case, you will want to install
the old =pavucontrol= utility back from the Pulseaudio era. Modern Linux
audio with =pipewire= is compatible with Pulseaudio tooling.

Within =pavucontrol= you can select different audio profiles and even
select which device to use for input and output streams. This is very handy
when you want to play certain audio streams from your Bluetooth headphones
but play other streams from the built-in speakers.

*Note*: the built-in Macbook microphone is not supported circa July 2024,
so you will have to rely on the mic in external devices like Bluetooth
headsets or even a 3.5mm jack TRS (Tip Ring Sleeve) or TRRS (Tip Ring Ring
Sleeve) microphone.

On my Macbook Pro M1 with 32GB DDR4 RAM, I was hearing pops and stutters
from Youtube and Internet Radio under the /Default/ profile, but when I
switch to the /Pro Audio/ (Real Time Audio) profile, these hisses and pops
disappeared.


** Enable native Wayland support for certain apps

Install ~xlsclients~, terminal util that will show all apps that are
running on XWayland instead of native Wayland display server.

*** Emacs 29.x

*Note*: Since Fedora 40, the default version of =emacs= is compiled with
~pgtk~ support for Wayland.

In Fedora 39, the default version of Emacs did not support native Wayland
mode, so I had to use the following Fedora COPR package:

https://copr.fedorainfracloud.org/coprs/enigm-a/emacs-pgtk-nativecomp/


#+begin_src sh
sudo dnf copr enable enigm-a/emacs-pgtk-nativecomp
#+end_src

https://discussion.fedoraproject.org/t/wayland-native-emacs/99994/11

I already had the =emacs= package installed from the default repos,
which was at a higher version, so I actually had to manually downgrade
=emacs= to the version contained in the downloaded ~.rpm~ file.

#+begin_src sh
wget https://download.copr.fedorainfracloud.org/results/enigm-a/emacs-pgtk-nativecomp/fedora-39-aarch64/07057875-emacs/emacs-29.2.50-1.20240129.git77f5d4d.fc39.aarch64.rpm
sudo dnf downgrade emacs-29.2.50-1.20240129.git77f5d4d.fc39.aarch64.rpm
#+end_src

Note that as of Fedora 40, ~emacs~ runs in native Wayland mode by
default. However, if you ever find yourself on an old X11 machine or VM,
keep in mind that instead of installing the =pgtk= version of =emacs=, you
must install the vanilla =gtk= version.

*** chromium-browser

Supposedly you can enable Wayland in ~chromium-browser~ by setting
certain flags in ~chrome://flags~, but I couldn't get this to work on
Asahilinux!

Instead you must launch with the following options in Wayland:

```sh
chromium-browser --enable-features=UseOzonePlatform \
  --ozone-platform=wayland --gtk-version=4
```

The options above are necessary to make chromium run as a native Wayland
app and enable partial support for East Asian character input IME with
~fcitx5~. Note that IME support is not yet perfect as of Jun 2024; Hanja
inpute does not work and character input rendering is also broken after you
type the char '가' (but you can workaround this issue by typing in English
after this character).

Fortunately, ~firefox-wayland~ works fine

*** firefox

Just use package ~firefox-wayland~

*** Keybase GUI

Make sure that you launch the Keybase Electron GUI app with the
following:

#+begin_src sh
/opt/keybase/Keybase --ozone-platform=wayland
#+end_src

This ensures that Electron will run in native Wayland mode. Otherwise
the app will run in XWayland. Note that the option above works for most
Electron apps in 2024 such as ~chromium-browser~, ~vscode~, ~vscodium~,
etc.


** Package list

From default Fedora repos:

#+begin_src text
awscli2
chromium-browser
clipman
diceware
dvipng  # to preview LaTeX rendered to PDF in AUCTeX
emacs
fcitx5
fcitx5-hangul
google-noto-sans-bengali-fonts
google-noto-sans-devanagari-fonts
hadolint  # Dockerfile linter
imv  # wayland native image viewer
lnav  # logfile navigator
mc
mupdf  # light PDF viewer
myrepos
neofetch  # custom motd ASCII banners
nethogs  # top-like bandwidth monitoring
network-manager-applet
nm-connection-editor
openssh-askpass
pavucontrol  # audio settings compatible util; works w/ pipewire
powerline-go  # powerline rewritten in Go
python3-pip  # pip package manager for python3
ruff  # python linter written in rust
ShellCheck  # shell script linter
simple-scan  # wayland native frontend to SANE (Scanner)
sipcalc
swappy  # Wayland native image editor
texlive-chktex  # LaTeX linter
texlive-cite
texlive-isodate
texlive-mathfont
texlive-nanumtype1
texlive-tex
texlive-textpos
texlive-xetex
texlive-xetexko
thunar
tigervnc  # provides vncviewer (native Wayland)
vim-ale  # Asynchronous Vim Lint Engine (replaces Syntastic)
wev  # keycode detector - replaces 'xev'
wf-recorder
wofi
wl-clipboard
xlsclients  # Wayland util showing XWayland processes
#+end_src

To get the latest packages from package repos you have set up on your
system, use the following command:

#+begin_src sh
sudo dnf --refresh up -y
#+end_src

If you don't use the =--refresh= option, latest packages might not be
downloaded until your package metadata expires. By using this option,
metadata will be set as expired so that you can re-download the latest
package metadata, ensuring you can download packages recently uploaded to
repos.

** 3rd party packages

Some 3rd party packages will be installed manually by downloading a =.rpm=
file and installing with =dnf install mypkg.rpm=, but some 3rd party
packages have =.repo= files that will be downloaded into =/etc/yum.repos.d=
so that everytime you update your system new versions of these packages can
be installed automatically. Note that =/etc/dnf= contains settings for the
dnf package manager only and that package repository settings are still
stored in the =/etc/yum.repos.d= path historically used by =yum= which has
now been replaced by =dnf=.

*** vscode / vscodium

~vscodium~ is the OSS version of ~vscode~ - the biggest difference is that
the former has removed all telemetry that comes with the latter.

You can launch the native wayland version for both with

~{code/codium} --ozone-platform=wayland~

https://code.visualstudio.com/docs/setup/linux

#+begin_src sh
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" \
    | sudo tee /etc/yum.repos.d/vscode.repo > /dev/null
dnf --refresh up -y
sudo dnf install code
#+end_src

*** starship PS1 customization

#+begin_src sh
sudo dnf copr enable atim/starship
sudo dnf install starship
#+end_src

*** docker community edition setup

#+begin_src sh
sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
sudo dnf install docker-ce docker-ce-cli containerd.io
#+end_src

Start the ~docker~ daemon and then try to run the ~hello-world~
container to make sure the installation is working.

#+begin_src sh
$ sudo systemctl start docker
$ sudo docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
478afc919002: Pull complete
Digest: sha256:a26bff933ddc26d5cdf7faa98b4ae1e3ec20c4985e6f87ac0973052224d24302
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.
...
#+end_src

https://developer.fedoraproject.org/tools/docker/docker-installation.html

*** tfenv

This is a must if you use terraform within a team and have dependencies
on particular TF versions.

*** Proprietary audio/video codecs

Fedora does not allow packages with proprietary, non-opensource licensed
bits in their repositories. Therefore the best proprietary codecs for mp4,
mp3, etc are not available by default. You can, however, enable the
3rd-party /rpmfusion/ or /terra/ repositories which /do/ offer these ~.rpm~
packages for download.

- https://rpmfusion.org/Configuration/
- https://terra.fyralabs.com/

*Note*: /rpmfusion/ has been around for over 15 years IIRC, but in 2024 I
noticed that getting repo metadata has gotten slower and slower to the
point that the bottleneck when I run =dnf --refresh up= is rpmfusion. I
recently tried the Terra 3rd-party package repository for Fedora and was
pleasantly surprised by how fast it is and how many packages it hosts!
However, /Terra/ is not an rpmfusion replacement - nonfree audio codecs and
=ffmpeg-devel= are only available from rpmfusion!

Setup rpmfusion repo:

#+begin_src sh
sudo dnf install \
  https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
  https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
#+end_src

Note that the above command installs rpm packages that will install a =.repo=
files for rpmfusion within =/etc/yum.repos.d=. When you want to remove the
rpmfusion repos, do not just delete the =rpmfusion-*.repo= files from that
folder; instead =dnf remove rpmfusion-{free,nonfree}-*= the packages from
your system and this will automatically cleanup the ~.repo~ files.

Setup Terra repo for Fedora:

#+begin_src sh
  sudo dnf install --repofrompath \
    'terra,https://repos.fyralabs.com/terra$releasever' \
    --setopt='terra.gpgkey=https://repos.fyralabs.com/terra$releasever/key.asc'
    \ terra-release
#+end_src

*** cmus Ncurses music player

=cmus= is not available from the default Fedora repos any longer, but
there is a [[https://copr.fedorainfracloud.org/coprs/pgaskin/cmus-testing][COPR repo]] that offers binaries of =cmus-testing= for
multiple architectures including ~aarch64~. You can install this user
repo with the following:

#+begin_src sh
$ wget https://copr.fedorainfracloud.org/coprs/pgaskin/cmus-testing/repo/fedora-40/pgaskin-cmus-testing-fedora-40.repo
$ sudo dnf config-manager --add-repo ~/Downloads/pgaskin-cmus-testing-fedora-40.repo
Adding repo from: file:///home/jundora/Downloads/pgaskin-cmus-testing-fedora-40.repo
#+end_src

*Note*, =cmus-testing= is /not/ built with proprietary codec versions of
=ffmpeg= and does not have =mp4= or =aac= support. ~pgaskin~, the owner of
the above COPR repo and contributor to =cmus= upstream on Github offers a
separate rpm =cmus-testing-plugins-rpmfusion= available only on Github (not
COPR) but builds are only available for architecture ~x86-64~ and not
~aarch64~.

I also tried using the version of =cmus= built by the OpenSUSE Tumbleweed
build system for [[https://download.opensuse.org/ports/aarch64/tumbleweed/repo/oss/aarch64/][aarch64 ports]] but the OpenSUSE C library dependencies
conflicted with those on Asahilinux Fedora, so I couldn't use the SUSE
packages.

I finally built from source and had really good results - no more pops and
audio artifacts when playing streaming Internet radio. See the section
below for details.

**** building from source

https://github.com/cmus/cmus

After cloning the repo, try running =./configure= inside the cloned folder
and the script will tell you if you have the necessary toolchain and C
libraries installed or not.

#+begin_src sh
./configure
checking for program gcc... /usr/bin/gcc
checking for program gcc... /usr/bin/gcc
checking for C11 (with atomics support)... yes
checking for CFLAGS -pipe -Wall -Wshadow -Wcast-align -Wpointer-arith -Wwrite-strings -Wundef -Wmissing-prototypes -Wredundant-decls -Wextra -Wno-sign-compare -Wformat-security... yes
checking for CFLAGS -Wold-style-definition... yes
checking for CFLAGS -Wno-pointer-sign... yes
checking for CFLAGS -Werror-implicit-function-declaration... yes
checking for CFLAGS -Wno-unused-parameter... yes
checking for CFLAGS -Wno-missing-field-initializers... yes
checking if CC can generate dependency information... yes
checking byte order... little-endian
checking for DL_LIBS (-ldl -Wl,--export-dynamic)... yes
checking for PTHREAD_LIBS (-lpthread)... yes
checking for realtime scheduling... yes
checking for program pkg-config... /usr/bin/pkg-config
checking for NCURSES_LIBS (pkg-config)... no
checking for NCURSES_LIBS (-lncursesw)... no
checking for NCURSES_LIBS (pkg-config)... no
checking for NCURSES_LIBS (-lncurses)... no
checking for NCURSES_LIBS (pkg-config)... no
checking for NCURSES_LIBS (-lcurses)... no
configure failed.
#+end_src

As Asahilinux is based on Fedora, at a bare minimum, you must install
=ncurses-devel= . You also need to have =ffmpeg-devel= installed, but that
package is only available from ~rpmfusion~. If you want support for FLAC
lossless audio codec, CDDB protocol (getting album info from Musicbrainz,
etc), you will also have to install the additional relevant libraries.

- faac-devel  # AAC encoding codecs (rpmfusion free) (not needed)
- faad2-devel  # AAC decoding codecs (rpmfusion free)
- ffmpeg-devel  (rpmfusion free)
- flac-devel
- libcddb-devel
- libmad-devel  # MPEG audio decoders
- libmp4v2-devel
- libvorbis-devel
- ncurses-devel
- opus-devel
- pulseaudio-libs-devel

#+begin_src sh
./configure
make
sudo make install
#+end_src

Once you successfully start =cmus= after building from source, you might
see the error:

#+begin_src text
Error: selecting output plugin ':' no such plugin
#+end_src

Run =cmus --plugins= to check which audio plugins you have installed; in my
case I installed the ~pulseaudio~ C libraries, so I had ~pulse~ available
as an output plugin according to =cmus --plugins=. While =cmus= is running,
press =:= to enter command mode and type =set output_plugin=pulse<ENTER>=
and the error should go away.

Github Issue discussing the /no such plugin/ error: [[https://github.com/cmus/cmus/issues/185][this]] =cmus= issue.

After building =cmus= with all the latest non-free codecs for AAC, FLAC,
mp4 audio, MPEG, etc. I no longer hear audio artifacts during audio
streams. I have installed =cmus= from default Linux package repos, 3rd
party repos like /rpmfusion/ and COPR, but the *version I built from source
on my local machine by far has the best audio quality*.


*** Sway Notification Center

https://github.com/ErikReider/SwayNotificationCenter

#+begin_src sh
sudo dnf copr enable erikreider/SwayNotificationCenter
sudo dnf install SwayNotificationCenter
#+end_src


*** ArgoCD CLI

Download the binary for your platform from the ArgoCD Github Releases
Page https://github.com/argoproj/argo-cd/releases

Once you have downloaded the binary, you can install it as follows:

#+begin_src sh
sudo install -m 555 argocd-linux-arm64 /usr/local/bin/argocd
rm argocd-linux-amd64
#+end_src

*Reference*: [[https://argo-cd.readthedocs.io/en/stable/cli_installation/#download-with-curl][ArgoCD installation docs]]

** Third-party apps which must be built from source for ARM64 support

*** musikcube

https://github.com/clangen/musikcube/wiki/building

Needed to edit the package name for ~ffmpeg~ because the default Fedora
repos don't contain ~ffmpeg-devel~... Also ~asio-devel~ was missing
from the list!

#+begin_src sh
sudo dnf install gcc-c++ make cmake libogg-devel libvorbis-devel \
  ffmpeg-free-devel ncurses-devel zlib-devel alsa-lib-devel \
  pulseaudio-libs-devel libcurl-devel libmicrohttpd-devel lame-devel \
  libev-devel taglib-devel openssl-devel libopenmpt-devel asio-devel
#+end_src

#+begin_src sh
# from the cloned directory run the following
cmake -G "Unix Makefiles" .
make
sudo make install
#+end_src

If you later enable the ~rpmfusion~ repos and decide to install the
proprietary ~ffmpeg-devel~ package, you will have to delete
~ffmpeg-free-devel~ and ~ffmpeg-free~. or invoke ~dnf~ with
~--allowerasing~.

*** nerd-fonts

Custom fonts repo containing thousands of True Type Fonts
https://github.com/ryanoasis/nerd-fonts

After downloading the tarball of TrueType files for /Monofur/, ~tar
xvf~ the tarball and then either install the fonts for the user or
system-wide. In my case, I am only interested in the Monofur font so I
go to https://github.com/ryanoasis/nerd-fonts/releases look for the
Monofur tarball and ~wget~ it.

#+begin_src sh
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.2.1/Monofur.tar.xz
#+end_src
https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/Monofur/Regular

**** Install fonts for current user only

https://fedoramagazine.org/add-fonts-fedora/

**** Install fonts system-wide

https://docs.fedoraproject.org/en-US/quick-docs/fonts/

Create a new directory in the system fonts directory
~/usr/local/share/fonts/~ to accommodate a new font family, and copy
the downloaded fonts in the folder ~monofur~.

#+begin_src sh
sudo chown -R root: /usr/local/share/fonts/monofur
sudo chmod 644 /usr/local/share/fonts/monofur/*
sudo restorecon -vFr /usr/local/share/fonts/monofur
# update the font cache
sudo fc-cache -v
#+end_src

*** tflint - Terraform linter

https://github.com/terraform-linters/tflint

You can download a =.zip= file which contains a single =tflint= binary.
Copy this file to =/usr/local/bin= so that it is available from your
=PATH=. Note that =tflint= cannot be run against individual files but
must be executed against a directory containing =.tf= files:

#+begin_src sh
$ tflint --chdir=dev-instance
12 issue(s) found:

Warning: Module source "git@github.com:ujet/ujet-google//infra/modules/tf-modules/storage/cloud_storage" is not pinned (terraform_module_pinned_source)

  on dev-instance/gcs.tf line 17:
  17:   source = "git@github.com:ujet/ujet-google//infra/modules/tf-modules/storage/cloud_storage"

Reference: https://github.com/terraform-linters/tflint-ruleset-terraform/blob/v0.8.0/docs/rules/terraform_module_pinned_source.md
...
#+end_src


** Troubleshooting

*** Audio degradation when multiple audio streams exist

Asahilinux uses ~pipewire~ as the audio daemon, but pipewire supports
~pulseaudio~ as a frontend. When running ~musikcube~ client streaming audio
over my tailnet from my ~musikcube~ server running at home, quality is
quite good when this is my only audio stream. But if I open a tab in the
browser that plays audio, suddenly the audio quality from musikcube
degrades!

Perhaps this was an artifact of using the free codecs from the default
Fedora repos? I later switched to non-free codecs from ~rpmfusion~ and
downloaded packages containing proprietary, non-free codecs:

#+begin_src text
ffmpeg-devel
ffmpeg-libs
libavdevice
x264-libs
x265-libs
#+end_src


Currently streaming audio playback from ~musikcube~ is very high quality!

*** Zotero 7 beta arm64 support

https://github.com/zotero/zotero/issues/3515

The above issue opened on 1 Dec 2023 is still open as of May 2024 - Zotero
depends on Firefox builds, but the Zotero maintainers would prefer a
prebuilt ARM64 binary from Mozilla; currently the ~ARM64~/~aarch64~ builds
being used by Linux distros are self-built on their own build infra.

Instructions for building Zotero from source:

https://www.zotero.org/support/dev/client_coding/building_the_desktop_app
