Notes on Raspberry Pi 3B
==============================

# Summary

- Created on 18 Apr 2023
- Created by: gopeterjun@naver.com
- Last Updated: 08 Sep 2024

I have been using the RPI3B since 2018, but after purchasing three RPI4B
SBC's the RPI3B sat in a drawer for a long time. The RPI3B only has 1GB of
LPDDR2 memory (900MHz) although it _does_ have 4 ARM Cortex-A53 cores
running @1.2GHz. Its Ethernet is only 100Mbit. I own 4GB and 8GB
LPDDR4-3200 memory RPi4B models. RPi 4B's have 4 ARM Cortex-A72 cores
running @1.8GHz as well as Gigabit Ethernet, so their hardware specs are
much better than those of the RPI3B.

I dusted off my old RPI3B recently because all my RPI4B's are running `k3s`
distro of Kubernetes. I have run Archlinux ARM (both 32 and 64-bit),
Raspberry PiOS 32-bit, and Raspberry PiOS 64-bit on the RPI3B. 2018 and
2024, a lot of things have changed regarding software support for the
RPI3B.


# Topics

## Fedora 37 aarch64 for RPI3B

TL;DR: was never able to get boot from USB working with Fedora 37 with
`arm-image-installer --target=rpi3`. Maybe I should retry with a more
recent Fedora `aarch64` version like F40 or F41?

## RPi OS 64-bit minimal

Works great from microSD

Works great from USB-to-m2-NVME enclosure (AS1153 chipset)

Could not boot from Seagate 750GB SATA3 HDD connected to NEXT 318U3
SATA-to-USB adapter using JMS567 chipset.  JMicron firmware is notoriously
buggy and in many cases does not properly support the USB Attached SCSI
Protocol (UASP) which is required for booting from storage devices
connected over USB. The one good thing about this adapter, however, is that
it comes with a 12V DC plug and adapter for powering external HDD and DVD
drives. If I plan to boot from a HDD using an UASP-compliant USB-to-SATA
adapter in the future, I will have to ensure that it supports an external
power source.

When I initially booted RPi OS 64-bit minimal *Debian Bullseye 11.6*, from
a UGREEN m2-NVMe-to-USB enclosure, I also connected a monitor via HDMI port.

```sh
sudo apt update && sudo apt dist-upgrade -y
```

Connecting a monitor makes the RPI3B draw more power, and as a result I got
lots of *undervoltage detected!* errors. Now I am running the RPI3B off of
a UGREEN USB-to-m2-NVME enclosure headless, so undervoltage shouldn't be an
issue anymore, but the RPI3B won't be able to handle any more peripherals
(Monitor, keyboard, etc).

### Headless setup for RPi3B

Normally when setting up Linux on RPi without an attached monitor, I use a
special serial console cable for the RPi that runs on 3.3V instead of the
normal 5V for other SBC's. For some reason, I was not able to get the
serial console over GPIO working for the RPi3B in April 2023 although I
successfully used the serial console on the same board in 2018-2019 using
older versions of RPi OS 32-bit, Ubuntu ARM64, and Archlinux ARM.

Therefore this time around (Apr 2023) I had to use the `rpi-imager`
advanced settings menu to define a new user (`pi:raspberry` was deprecated
in Spring 2022) and also enable SSH after selecting RPi OS 64-bit as my
installation image. You can also specify the SSID for your wifi AP and the
user/pass for connecting to wireless. Now when you boot the RPi3B it will
automatically connect to the wireless network and get an ipv4 address. If
your RPi3B is also connected to Ethernet, it will also automatically get an
ipv4 address if you have a DHCP server running in your local network, which
is usually the case if you have a consumer router connected to your ISP's
modem.

However, you still won't know the new IP address assigned to the RPi3B
unless your router has a web UI showing the ipv4 addresses allocated to
various hosts; or you could run `nmap` network scanner in *Ping Scan* mode
on your local network to enumerate hosts:

```sh
$ sudo nmap -sn 192.168.21.0/24
[sudo] password for jundora:
Starting Nmap 7.93 ( https://nmap.org ) at 2023-04-20 15:58 KST
Nmap scan report for _gateway (192.168.21.1)
Host is up (0.00030s latency).
MAC Address: 28:EE:52:95:07:48 (Tp-link Technologies)
Nmap scan report for 192.168.21.118
Host is up (0.00023s latency).
MAC Address: E4:5F:01:1A:CD:BF (Raspberry Pi Trading)
...
Nmap done: 256 IP addresses (12 hosts up) scanned in 4.62 seconds
```

Ping Scan disables port scanning and will only send ICMP handshake packets
which is still enough to get IP address and MAC address info. As you can
see above, the MAC address contains info about the device. If you see
*Raspberry Pi Trading* or *Raspberry Pi Foundation*, you've found a RPi
SBC.

Now you can `ssh` to the ipv4 address you found above using the user/pass
you defined in the advanced options of `rpi-imager` before writing the RPi
OS 64-bit image to your install media.

## Enabling boot from USB on the RPI3B

To enable boot from USB, you must enable a setting in the One Time
Programmable (OTP) memory by adding the following line to the end of
`/boot/config.txt`:

```
program_usb_boot_mode=1
```

Once you have booted into RPi OS, you can check if boot from USB has been
enabled with the following command:

```
$ vcgencmd otp_dump | grep 17:
17:3020000a
```

If you see the hex output `17:3020000a`, boot from USB is now set. Make
sure to remove `program_usb_boot_mode=1` from `/boot/config.txt` just in
case you happen to use the RPi OS sdmicro card on another RPI3B SBC.

Now you can use `rpi-imager` to install RPi OS ARM64 directly onto
USB-attached storage like an SSD as long as the USB-to-SSD adapter comes
with drivers for UASP (USB Attached Storage Protocol). After April 2022,
when installing RPi OS to new media, be sure to click on the Advanced
Settings gear icon to set a new username/password combination and enable
SSH because `pi:raspberry` is now deprecated and won't work on first login.


**References**:

- https://www.raspberrypi.com/news/raspberry-pi-bullseye-update-april-2022/
