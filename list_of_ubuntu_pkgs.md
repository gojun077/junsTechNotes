 List of Ubuntu Packages
=========================

# Packages
I will later create an Ansible playbook to install the pkgs below

- ipython3
- ipython2
- htop
- git
- emacs
- wpagui

# Network

## Interfaces
```
auto lo
iface lo inet loopback

auto enp9s0
iface enp9s0 inet static
   address 10.10.10.123
   netmask 255.255.255.0
   network 10.10.10.0
   gateway 10.10.10.97
   dns-nameservers 168.126.63.1
```

## Wireless with wpa_supplicant

- copy `local.cfg` template file from dotfiles repo

```
ctrl_interface=/var/run/wpa_supplicant
ctrl_interface_group=sudo
update_config=1
```
