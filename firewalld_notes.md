Firewalld notes
==================

# Summary

- Created on: Apr 12 2023
- Created by: gojun077@gmail.com
- Last Updated: May 27 2023

`firewalld` is a dynamic firewall daemon that allows rules to be added and
removed in real-time without the needing to unload and reload `netfilter`
and other firewall-related kernel modules like in the past. Now all
firewall changes must go through the `firewalld` daemon to ensure that
daemon state and kernel firewall module state are in sync.

`firewalld` uses *zones* and *services* instead of *chains* and *rules* and
can change firewall state dynamically without breaking existing sessions
and connections.

It is now used in all Redhat-based distributions and integrates with
NetworkManager.


# Topics

## Pre-defined firewalld zones

https://www.redhat.com/sysadmin/firewalld-rules-and-scenarios

On FedoraServer distro, the zones are as follows:

- `FedoraServer`
- `FedoraWorkstation`
- `trusted`
  + all network connections accepted
- `block`
  + block all incoming traffic with `icmp-host-prohibited`
  + only connections initiated from within the system are allowed
- `dmz`
  + accepts only limited internal network connections
- `drop`
  + connections dropped w/o notifications. Outgoing connections possible
- `internal`
- `home`
- `libvirt`
- `nm-shared`
- `public`
  + intended for untrusted public networks
- `work`

On my Debian Bullseye machine, the zones are a bit different:

```
block dmz drop external home internal nm-shared public trusted work
```

## View current firewalld setup

```sh
sudo firewall-cmd --list-all
```

This will show info for your default zone.


## Setup zone *home*

- Add your wired iface to *home* zone
  + in my case, I use bridge interface `br0` for ethernet
  + my ethernet is a slave iface under `br0`
- Add your wifi iface to *home* zone
- add your home network IP range as source
- add `vnc-server` service to *home* zone
  + port 5900/tcp

```sh
sudo firewall-cmd --add-interface br0 --zone home
sudo firewall-cmd --remove-interface wlp3s0 --zone FedoraServer
sudo firewall-cmd --add-interface wlp3s0 --zone home
sudo firewall-cmd --add-source=192.168.21.0/24 --zone home
sudo firewall-cmd --add-port 41641/udp --zone home  # Wireguard
sudo firewall-cmd --add-service vnc-server --zone home
sudo firewall-cmd --runtime-to-permanent
```

Note: `musikcube` streaming audio server uses `7905/tcp` for metadata and
`7906/tcp` for sending audio data over `http`.

You can add a range of high ports which are sometimes used by `p2p` file
sharing programs or by Minio S3-compatible object storage for the web console
with the following:

```sh
sudo firewall-cmd --add-port=40000-60000/tcp --zone home
sudo firewall-cmd --runtime-to-permanent
```

On Fedora, the default `firewalld` zone is `FedoraServer`, but after
setting up the `home` zone above, we should make it our default zone.

```sh
$ sudo firewall-cmd --get-default-zone
FedoraServer
$ sudo firewall-cmd --get-default-zone
home
```

## When to dynamically assign an interface to another zone

Let's say you use `firewalld` on your linux laptop and you take your laptop
to a coffeeshop and connect to wifi on `wlan0`. `wlan0` might normally be
assigned to the `home` zone, but the coffeeshop is obviously
different. When you get an IPv4 address from the coffeeshop router, it will
most probably not be in the same IPv4 address range as your `home`
network. You also don't want all the same ports to be open when you are
connected to the public wifi AP at Starbucks! In this case, remove your
network interface from `--zone home` and add your interface to `--zone
public`:

```sh
sudo firewall-cmd --remove-interface wlan0 --zone home
sudo firewall-cmd --add-interface wlan0 --zone public
```

When you leave the coffee shop and arrive back home, you can simply
reload your `firewalld` saved settings without manually reversing
the above commands.

```sh
sudo firewall-cmd --reload
```

The above command drops all runtime configurations and loads the permanent,
saved config, but keeps the current firewall state, so existing connections
are not interrupted. This is in contrast to `systemctl restart firewalld`
which cause a loss of network state.
