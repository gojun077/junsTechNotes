beets audio tagger notes
===========================

# Summary

- Created on: 22 Feb 2023
- Created by: gojun077@gmail.com
- Last Updated: 11 Dec 2024

`beets` is a command line tool written in Python that automates ID3v2.4
tagging of `mp3` files. While it can decode other audio file types, ID3
metadata is only defined for `mp3` format files, and `beets` can only
reliably add ID3v2 tags (v2.4 by default) to mp3's.


# Topics

## Installation

Can be installed from the Python Package Index (`pip`) but it is also
available from many default repositories of Linux distros. On Fedora 37+,
the base package is `beets` with the optional packages `beets-doc` and
`beets-plugins`.

## Configuration

https://beets.readthedocs.io/en/stable/reference/config.html

`~/.config/beets/` is the default configuration directory on Linux. By
default, `config.yaml` does not exist and must be created. In this file you
can specify which plugins should be enabled and settings for those plugins
in addition to many other settings. See the above link for details.

The beets sqlite3 DB file `library.db` is also stored in `~/.config/beets`


## Plugins

https://beets.readthedocs.io/en/stable/plugins/index.html#using-plugins

### chroma

This plugin enables acoustic fingerprinting from an audio file's sound data
which means that even without any `ID3` tag info at all, a file can be
identified. This plugin requires the `chromaprint` binary to be installed
in addition to the python library `pyacoustid`. In addition, you will
need the GStreamer codecs so that chromaprint can decode audio files. You
will need the `good`, `bad`, and `ugly` GStreamer (`gst`) codecs.

On Fedora 37, the required package names are as follows:

- `beets`
- `beets-plugins`
- `beets-doc`

## import audio file to your beets library

`beet import <audioFile>`

Beets will attempt to identify the file using various methods, i.e. `id3v2`
tags, filename, and even acousticId fingerprint if you have the relevant
configs and plugins setup. In my `~/.config/beets/config` file, I am using
the `chromaprint` acousticId plugin. Once the audio file is identified you
will be prompted whether or not to organize the file in an album folder or
as a Non-Album track. I usually select `Track` because I organize my
collection by ID3 tags, not folder hierarchy. My audio player also allows
filtering by ID3 tag *genre* and *artist*, so having all my audio files in
a single folder is just fine.

`beet import <dir>`

The above command will automatically try to import all the files in a
directory into the same album.

Note that sometimes the beets autotagger will mostly use the correct info
from the MusicBrainz database, but sometimes it will tag the primary
artist field `TPE1` in weird ways that will break your carefully
curated `artist -> album` folder hierarchy. In cases like these,
the `mid3v2` utility for CRUD tasks is a life-saver. Particularly the
option `--delete-frames "name1,name2,name3"`

I had a case in which the artist field `TPE1` was correctly set, but
`TPE2` contained multiple artists, which caused *beets* to create a new
`artist` folder, when instead the album should have been created in the
existing folder for the artist. I was able to fix this by doing something
like:

```sh
OIFS=$IFS
IFS=$'\n'
for f in $(find . -maxdepth 1 -type f); do
  mid3v2 --delete-frame "TPE2" "$f";
done; IFS="$OIFS"

beet update "my query pattern"
```

## remove audio file from beets db

`beet remove <query>`

- `-d` delete audio file in addition to db deletion
- `-a` operate on albums instead of individual tracks
- `-f` force; do not prompt on deletion


## Apply out-of-band ID3 tag changes to the beets DB

```sh
beet update QUERY
```

Sometimes you forget to edit some ID3 tags (like `genre`) before running
`beet import`. You can manually make the changes to the beet-imported files
and then apply these changes back to the DB with `beet update...`:

```sh
$ beet update "The Harp Consort"
The Harp Consort, Andrew Lawrence‐King -  - Concerto for Organ: Allegro
  title: Concerto for Organ: Allegro -> HWV 289 Organ Concerto in G minor
  genre:  -> Baroque
  composer:  -> George Frideric Handel
```

The text after the `->` shows edited/new content.

You can go about this in two different ways; one would be to `mid3v2` edit
the files directly in the beet music directory, then run `beet update`; the
other way would be to edit files existing in another directory and then
copy them into the beet music dir before running `beet update`.

## Using external applications

`beets` contains the `beet` binary which is executed from the command line
with option flags. It uses `id3v2.4` to tag audio files in an automated
way. For manual tagging, however, you will need to use an external
application.

While `id3v2` CLI tool exists, in my experience I have found that `mid3v2`
provided by the `python mutagen` library is much more featureful.

### mid3v2 (python `mutagen` replacement of `id3v2`)

`mid3v2` (from the `python3-mutagen` package on Fedora) enables you to read
and write `id3v{1,2.4}` tags. This is really useful for bulk-tagging `mp3`
files, i.e. adding the `genre` tag/field to ID3 metadata, editing misc
`TXXX` tags, i.e.  `TCOM` for the *composer* tag and so on. There are
simple CLI options for setting tags for common tags like artist, album,
title, year, and genre:

- `-a` / `--artist` frame `TPE1`
- `-A` / `--album` frame `TALB`
- `-g` / `--genre` frame `TCON`
  + to see a list of all ID3v1 genres, use `-L`/`--list-genres`
- `-t` / `--song` set title information `TIT2`
- `-T` / `--track=<NUM/NUM>` set the track number `TRCK`
- `-y` / `--year` `TDRC`

You can modify or add any IDV3 frame by prefixing the name of the frame with
`--`. Here is an example of setting the *composer* tag value:

```sh
$ mid3v2 --TCOM "Jimmy Van Heusen" "Lynne_Arriale_trio_-_But_Beautiful.mp3"
$ mid3v2 -l Lynne_Arriale_trio_-_But_Beautiful.mp3
IDv2 tag info for Lynne_Arriale_trio_-_But_Beautiful.mp3
TALB=Melody
TCOM=Jimmy Van Heusen
TCON=Jazz
TDRC=1999
TIT2=But Beautiful
TLAN=eng
TPE1=Lynne Arriale trio
TSSE=Lavf59.27.100
```

You can see a list of all possible ID3v2.2 frames with

```sh
mid3v2 -f # aka --list-frames
```

For example, `--TPUB` allows you to set the album *Publisher*
field.


When editing tag `COMM`, *comments* tag it is often better to delete the tag
if there is some existing text in the field and to start over because each
time you try to add a comment with `--comment` option, mid3v2 will just
append to the existing comment. You can delete the comment frame with
`--delete-frames "COMM"`.

Refer to the following link for a description of `ID3` tags through
version `2.4`

http://web.mit.edu/graphics/src/Image-ExifTool-6.99/html/TagNames/ID3.html

And below is a list of `ID3` values for the tag `genre`:

https://en.wikipedia.org/wiki/List_of_ID3v1_genres


By default, `beets` organizes audio files into `artist -> album -> track`
hierarchy, and doesn't populate the `genre` field of ID3. This makes it
inconvenient to create playlists. Sometimes I just want to listen to genre
`150` "Baroque" during a listening session, or genre `154` "Chillout".
In `musikcube` it is possible to filter by the `genre` tag.

The following applies the `08` *Jazz* as the value for the `genre` tag
to all files in a directory:

```sh
for f in $(find . -maxdepth 1 -type f); do
  mid3v2 -g 08 "$f"
done
```

If your files have spaces in their filenames (which will happen in most
cases when you download multiple songs via `yt-dlp` from a playlist), you
will have to change the Internal Field Separator (`IFS`) from its default
value of `SPACE`, `TAB` and `NEWLINE` chars to something like newline only:

```sh
OIFS=$IFS  # save default IFS setting to SAVEIFS
IFS=$'\n'  # change IFS to newline char only
for f in $(find . -maxdepth 1 -type f); do
  mid3v2 -g 08 "$f"
done
IFS="$OIFS"  # restore default IFS setting
```

Note that when setting IFS, you must use special syntax `=$''`

Here is another example of using `mid3v2` to tag audio files containing
spaces in filenames. In this case, audio files were downloaded from a YT
Music playlist from the Miles Davis album *Birth Of The Cool*. When
downloading from Album playlists, the files are downloaded in parallel, so
the `mtime` of the first track will not necessarily be earlier than that of
later subsequent tracks. As a result, you will have to view the original
track list and manually `mv` each downloaded file and prefix the
track number followed by underscore, so that the file is named
like `1_myartist mysong.mp3`.

You can use Midnight Commander, `mc`, `<F6> RenMov` dialog to rename the
downloaded files with prefix `tracknumber_`. Note that there is no need to
add double-quotes to escape filenames with spaces, as Midnight Commander
will insert all necessary quotes for you.

![rename](images/mc_f6_renmov.png)

In the `to:` field, simply add your prefix, followed by `*`. In the example
above, `6_*` will rename `Nocturne No. 6 in G Minor, Op. 15 No. 3.mp3` to
`6_Nocturne No. 6 in G Minor, Op. 15 No. 3.mp3`

You can also mark files for selection using `C-t` in Midnight commander and
then execute a bulk action on all selected items.


```sh
OIFS=$IFS
IFS=$'\n'
for f in $(find . -maxdepth 1 -type f); do
  # make clean track name without prefix "num_" \
  bname=$(basename -s .mp3 $f) \
  trkname=$(echo "$bname" | cut -d'_' -f2-) \
  # get track number from basename \
  trknum=$(echo "$bname" | cut -d'_' -f1) ; \
  mid3v2 -g 08 -a "Miles Davis" -A "Birth Of The Cool" -y 1957 \
  -t "$trkname" --TRCK "$trknum" --TCOM "Composer" "$f"; \
done; IFS="$OIFS"
```

Below is an explanation of the `mid3v2` options used above:

- `-g` / `--genre`: `08` is for *Jazz*
- `-a` / `--artist`
- `-A` / `--album`
- `-y` / `--year`
- `-t` / `--title`
  + `basename` returns just filename without path info
    + `-s` / `--suffix`: remove suffix `<string>`

You might want to remove spaces from filenames entirely so you don't
have to bother with changing the Internal Field Separator `IFS` before
and after running a bash for-loop to run `mid3v2` on all files in
a directory.

```sh
find . -maxdepth 1 -type f -name "* *" \
  -exec sh -c 'mv "$1" "${1// /_}"' _ {} \;
```

The above looks for files with at least one space in their names, and
then executes a shell command for each found item that replaces a
space with an underscore.

- `{1// /_}` replace all spaces with underscores
- `_` argument for `mv` destination (unused in this case)
- `{}` substitutes found item's name at this position
- `\;` terminates the `-exec` command

Then you can set the id3v2 tags with `mid3v2` more simply:

```sh
for f in $(find . -maxdepth 1 -type f); do
  bname=$(basename -s .mp3 $f); trkname=$(echo "$bname" | cut -d'_' -f2-); \
  trknum=$(echo "$bname" | cut -d'_' -f1); \
  mid3v2 -a foo -g 154 -t $trkname --TRCK $trknum/11 -y 2012 -A "Cosmic Balance" "$f"; done
```

Note that `cut -d` sets the *delimiter* or Field Separator (FS) character
and `-f2-` means *extract everything from the 2nd field onwards*.

Note that `trknum` assumes that the filename starts with the track number,
i.e. `6_Getting_Away_With_It`.

Although `$trkname` will have underscores in lieu of spaces, most album
data is in Musikbrainz database, so if you have a >90% similarity match,
`beets` will automatically fix the Trackname tag for you!


### `ffmpeg` for audio file format conversion

Another external application that `beets` often relies on is the `convert`
transcoder plugin in `ffmpeg`. It is a toolbox for working with A/V files
and supports lots of different codecs via a plug and play architecture.

The following is an example of using `ffmpeg` to convert an `m4a` format
audio file to `mp3` using Variable Bitrate (VBR) with quality *2* on a
scale of `0-10`:

```sh
ffmpeg -i myfile.m4a -y -q:a 2 -c:a libmp3lame myfile.mp3
```

To convert mp4/m4a style metadata to ID3 format, use the option
`-map_metadata 0`; this pre-supposes that metadata is in mp4 container
format, which is not always true. Some mp4/m4a files actually have ID3
format metadata shoe-horned into the file, although ID3 data in mp4's is
undefined:

```
$ file A.C_Jobim_-_Amor_em_Paz_1963.m4a
A.C_Jobim_-_Amor_em_Paz_1963.m4a: Audio file with ID3 version 2.3.0
```

The `m4a` file above is using ID3v2.3 mp3 metadata tags in an m4a file,
inserted at some unknown byte address by some tagging app. In these cases,
using `-map_metadata 0` won't work and it's better not to try converting
the metadata.

Here is an example of converting from `opus` to `mp3` and converting the
metadata to `id3v2.4` while using VBR encoding level 2 of 10 (lower
is better):

```sh
$ ffmpeg -i myfile.opus -y -c:a libmp3lame -q:a 2\
  -map_metadata 0:s:a:0 -id3v2_version 4 myfile.mp3
```

## `yt-dlp` for downloading audio from Youtube

`yt-dlp` is a fork of `youtube-dl` that is more popular than the original
project. It contains more features and is often able to download content
that `youtube-dl` chokes on.

Example of saving *best audio* quality possible from Youtube:

```sh
yt-dlp -f 'ba' https://<youtube-url> -o '%(id)s.%(ext)'s
```

To save best quality audio as an mp3:

```sh
yt-dlp -f 'ba' -x --audio-format mp3 https://<youtube-url> \
  -o '<songname>.%(ext)'s
```


If you have a Youtube Premium account, after logging in you can use a cookie
exporter extension to save your cookies to `cookies.txt` and then use the
option `--cookies /path/to/cookies.txt`. You can also use the option
`--cookies-from-browser <browserName>` where the browser can be
`chrome`, `firefox`, etc.


Save all files from a `music.youtube.com` playlist as mp3 files:

```sh
yt-dlp -f 'ba' -x --audio-format mp3 --cookies-from-browser firefox \
  --yes-playlist https://music.youtube.com/playlist?list=<longID> \
  -o "%(title)s.%(ext)"s
```

In the manpages for `yt-dlp`, search for the section `OUTPUT TEMPLATE`


## Troubleshooting

`beets` sometimes cannot read `.opus` files downloaded from youtube with
`youtube-dl` because the file contains strange metadata.

for example:

```sh
$ file Kerem_Gorsev_-_Hands_and_Lips.opus
Kerem_Gorsev_-_Hands_and_Lips.opus: Audio file with ID3 version 2.3.0, contains:\012- Ogg data, Opus audio, version 0.1,
 stereo, 48000 Hz (Input Sample Rate)
```

You see that the metadata has some extra text, i.e. `contains:\012-`.
A normal `.opus` should show the following output from `file`:

```sh
$ file Tony_Bennet_and_George_Michael_-_How_Do_You_Keep_The_Music_Playing.opus
Tony_Bennet_and_George_Michael_-_How_Do_You_Keep_The_Music_Playing.opus: Ogg data, Opus audio, version 0.1, stereo, 4800
0 Hz (Input Sample Rate)
```

If you convert the `.opus` file -using `ffmpeg` to some other
audio format (like `.m4a`) the conversion will remove non-id3 metadata
that `beets` tends to choke on:

```sh
ffmpeg -i Kerem_Gorsev_-_Hands_and_Lips.opus -y -c:a libfdk_aac -vbr 5 Kerem_Gorsev_-_Hands_and
_Lips.m4a
ffmpeg version 5.1.2 Copyright (c) 2000-2022 the FFmpeg developers
  built with gcc 12 (GCC)
...
  libavutil      57. 28.100 / 57. 28.100
  libavcodec     59. 37.100 / 59. 37.100
  libavformat    59. 27.100 / 59. 27.100
  libavdevice    59.  7.100 / 59.  7.100
  libavfilter     8. 44.100 /  8. 44.100
  libswscale      6.  7.100 /  6.  7.100
  libswresample   4.  7.100 /  4.  7.100
  libpostproc    56.  6.100 / 56.  6.100
[ogg @ 0x55b999ab4840] 11637 bytes of comment header remain
Input #0, ogg, from 'Kerem_Gorsev_-_Hands_and_Lips.opus':
  Metadata:
    artist          : Kerem Gorsev
    genre           : Jazz
  Duration: 00:09:56.03, start: 0.007500, bitrate: 142 kb/s
  Stream #0:0(eng): Audio: opus, 48000 Hz, stereo, fltp
    Metadata:
      ARTIST          : Kerem Görsev
      ARTIST_CREDIT   : Kerem Görsev
      ARTISTSORT      : Görsev, Kerem
      BPM             : 0
      COMPILATION     : 0
      DATE            : 0000
      YEAR            : 0
      disc            : 0
      DISCTOTAL       : 0
      DISCC           : 0
      TOTALDISCS      : 0
      ENCODEDBY       : Lavf58.76.100
      ENCODER         : Lavf58.76.100
      LANGUAGE        : eng
      MUSICBRAINZ_ARTISTID: faf3638c-2a19-469e-b777-5d2ab9b72c88
      MUSICBRAINZ_TRACKID: ef65bf4c-64cf-4a40-b1e3-da16ab8e799d
      ORIGINALDATE    : 0000
      TITLE           : Hands and Lips (Take 1)
      track           : 0
      TRACKTOTAL      : 0
      TRACKC          : 0
      TOTALTRACKS     : 0
Stream mapping:
  Stream #0:0 -> #0:0 (opus (native) -> aac (libfdk_aac))
Press [q] to stop, [?] for help
[libfdk_aac @ 0x55b999ab8e80] Note, the VBR setting is unsupported and only works with some parameter combinations
```

You can also add option flags to `ffmpeg` to convert `.opus` metadata to
`id3v2.4` metadata:

```
-map_metadata 0:s:a:0 -id3v2_version 4
```

Here is an example of converting from `opus` to `mp3` and converting the
metadata to `id3v2.4` while using VBR encoding level 2 of 10 (lower
is better) inside a bash for-loop:

```sh
for f in $(find . -name "*.opus" -maxdepth 1 -type f); do
  ffmpeg -i $f -y -c:a libmp3lame -q:a 2\
  -map_metadata 0:s:a:0 -id3v2_version 4 \
  $(basename -s .opus "$f").mp3
done
```

Note that the `basename --suffix` / `-s` option removes a trailing
*suffix*, i.e. `basename -s .html mypage.html` would return
`mypage`.


### youtube vs youtube music sample rates of downloaded audio

When I download from `music.youtube.com` using `yt-dlp` browser
cookies option for Youtube Premium, it downloads 64kbps encoded audio
sampled at 44.1 KHz:

```sh
$ yt-dlp -f 'ba' --cookies-from-browser firefox -x --audio-format mp3 "https://music.youtube.com/watch?v=EundmZZM9lU" -o 'Elak_-_Remember_Me.%(ext)'s
[Cookies] Extracting cookies from firefox
[Cookies] Extracted 100 cookies from firefox
[youtube] Extracting URL: https://music.youtube.com/watch?v=EundmZZM9lU
[youtube] EundmZZM9lU: Downloading webpage
[youtube] EundmZZM9lU: Downloading android player API JSON
[youtube] EundmZZM9lU: Downloading android music player API JSON
[youtube] EundmZZM9lU: Downloading web music client config
[youtube] EundmZZM9lU: Downloading player 9419f2ea
[youtube] EundmZZM9lU: Downloading web music player API JSON
[info] EundmZZM9lU: Downloading 1 format(s): 141
[download] Destination: Elak_-_Remember_Me.m4a
[download] 100% of    6.12MiB in 00:00:00 at 14.21MiB/s
[FixupM4a] Correcting container of "Elak_-_Remember_Me.m4a"
[ExtractAudio] Destination: Elak_-_Remember_Me.mp3
Deleting original file Elak_-_Remember_Me.m4a (pass -k to keep)
[jundora@argonaut chill2]$ file Elak_-_Remember_Me.mp3
Elak_-_Remember_Me.mp3: Audio file with ID3 version 2.4.0, contains: MPEG ADTS, layer III, v1, 64 kbps, 44.1 kHz, Stereo
```

If I use `yt-dlp` without cookies on a youtube link without the *music*
prefix, the downloaded file has the same bitrate, but is sampled at 48kHz:

```sh
$ yt-dlp -f 'ba' -x --audio-format mp3 "https://www.youtube.com/watch?v=EundmZZM9lU" -o 'Elak_-_Remember_Me_alt.%(ext)'s
[youtube] Extracting URL: https://www.youtube.com/watch?v=EundmZZM9lU
[youtube] EundmZZM9lU: Downloading webpage
[youtube] EundmZZM9lU: Downloading android player API JSON
[info] EundmZZM9lU: Downloading 1 format(s): 251
[download] Destination: Elak_-_Remember_Me_alt.webm
[download] 100% of    3.04MiB in 00:00:00 at 3.24MiB/s
[ExtractAudio] Destination: Elak_-_Remember_Me_alt.mp3
Deleting original file Elak_-_Remember_Me_alt.webm (pass -k to keep)
[jundora@argonaut chill2]$ file Elak_-_Remember_Me_alt.mp3
Elak_-_Remember_Me_alt.mp3: Audio file with ID3 version 2.4.0, contains: MPEG ADTS, layer III, v1, 64 kbps, 48 kHz, Stereo
```

I can't tell any difference in the sound quality, but for audio-only files,
44.1 kHz is still the [industry
standard](https://decibelpeak.com/44-1-khz-or-48-khz). 48 kHz is the
standard for picture (TV, film, etc), which is why audio ripped from
youtube videos is sampled at 48 kHz, because the audio has to be synced to
the video.

## id3v2 genre tag reference

Here is a partial list of the genre codes and values for `id3v2`:

- `02` Country
- `07` Hip-Hop
- `08` Jazz
- `13` Pop
- `32` Classical
- `36` Game
- `52` Electronic
- `66` New Wave
- `77` Musical
- `83` Swing
- `96` Big Band
- `103` Opera
- `150` Baroque
- `153` Breakbeat
- `154` Chillout
- `159` Electro
- `174` Nu-breakz

You can also see the entire list with `mid3v2 -L`. Note that if a genre
you want to use is not listed above, you can define a custom genre.
For example, `mid3v2 -g "Classical Funk" my-song.mp3` or use the frame
tag `--TCON`

You can list all possible frames for ID3v2.3 / ID3v2.4 with `mid3v2
-f`. Here are some frames (tags) which I use occasionally:

- `--TBPM`: Beats per minute
- `--TCOM`: Composer
- `--TOPE`: Original Artist/Performer
- `--TORY`: Original Release Year
- `--TPE2`: Band/Orchestra/Accompaniment
- `--TPE3`: Conductor
- `--TPE4`: Interpreter/Remixer/Modifier
- `--TPUB`: Publisher
- `--TEXT`: Lyricist
- `--TOLY`: Original Lyricist
- `--USLT`: Unsynchronised lyrics/text transcription
- `--TLAN`: Audio language
- `--TRCK`: Track number


## References

- https://beets.readthedocs.io/en/stable/guides/tagger.html
- https://beets.readthedocs.io/en/stable/plugins/index.html#using-plugins
- https://beets.readthedocs.io/en/stable/plugins/chroma.html
