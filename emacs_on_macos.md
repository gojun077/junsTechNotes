Emacs on MacOS - HOWTO
========================

# Summary

- Created on: Jul 16 2023
- Created by: gopeterjun@naver.com
- Last Updated: Mar 5 2024

MacOS is a POSIX environment similar to Linux, so Emacs mostly
runs fine on MacOS, but there are some differences. This HOWTO
contains my notes on setting up and using Emacs in this environment.

# Topics

## Install Emacs from nixos package manager (RECOMMENDED)

```sh
nix-env --install --attr nixpkgs.emacs29-macport
```

In the future you will need to bump the version numbers, but as of
early 2024 the current stable version is Emacs 29.1

The macport version includes elisp native compilation and works much better
and bug-free than installing Emacs from `brew` or manually downloading the
`.dmg` from *emacsformacosx.com*. Note that `brew install emacs` actually
downloads the `.dmg` from *emacsformacosx*.

Note that `nix-env` will install `Emacs.app` into the following PATH:

```
~/.nix-profile/Applications/Emacs.app
```

In addition, it will install `emacsclient` into `~/.nix-profile/bin`.

`~/.nix-profile/bin` should already be in your PATH, so you only need to
create a symlink from `~/.nix-profile/Applications/Emacs.app` to
`~/Applications`:

```sh
ln -s ~/.nix-profile/Applications/Emacs.app /Applications
```

Also, create a symlink for `emacsclient` pointing to `/usr/local/bin`:

```sh
sudo ln -s ~/.nix-profile/bin/emacsclient /usr/local/bin
```

## Install emacs from https://emacsformacosx.com/ (DEPRECATED)

https://emacsformacosx.com/ offers `.dmg` MacOS disk images for the latest
stable Emacs versions. Emacs will be installed into

`/Applications/Emacs.app/` and you can find the binary executables in
`/Applications/Emacs.app/Contents/MacOS/` and the subdirectory
`../bin/`. The binaries you will use most are:

- `Emacs` located in `../Contents/MacOS/`
- `emacsclient` located in `../Contents/MacOS/bin/`

Create a symlink from for `emacsclient`:

```sh
ln -s /Applications/Emacs.app/Contents/MacOS/bin/emacsclient \
  /usr/local/bin
```

In your `.bashrc` file:

```sh
export EDITOR=/usr/bin/local/emacsclient
```


**Note**: the version available from this website is not built with native
compilation enabled; also I experienced some random crashes. As of 2024, I
recommend installing `emacs-macport` from nixos package manager.

## Install emacs from homebrew

`brew install emacs`

The homebrew recipe for emacs should install both `Emacs` and setup
`emacsclient` for you. Emacsclient is used when you have an Emacs server
instance already running and want to create a client session on the server.

## Give Emacs permission to read/write to the File System

Click on the apple icon in the upper-left of the screen. From the left-hand
menu select *Privacy & Security* and in the right-hand menu click on *Full
Disk Access*. Click on the `+` button at the bottom of the Window and
select Emacs and enter your system password or put your finger on the
fingerprint reader to authorize this change.
