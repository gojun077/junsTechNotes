Hexedit HOWTO
================

# Summary

- Created on: Jan 10 2024
- Created by: gopeterjun@naver.com
- Last Updated: Jan 10 2024

Hexedit is available on all Linux distros and id CLI tool for viewing and
editing files in hexadecimal, but it also supports a side by side ASCII
view. Although GUI hex editors with many more features do exist, `hexedit`
is great because it's available virtually everywhere, even on headless
machines or SBC's and is probably sufficient for simple tasks like fixing
corrupted file headers or examining suspicious files.

# Topics

## Commonly used commands

- `TAB`: toggle between hex (left) and ASCII (right) navigation
- `C-SPC`: set mark
- `<`: go to beginning of file
- `>`: go to end of file
- `Home`: jump to beginning of line
- `End`: jump to end of line
- `ESC-W`: copy selection
- `ESC-Y`: paste selection to file
- `C-c`: exit without saving
- `C-x`: save and exit
- `C-s`: search forward
- `C-r`: search backward
- `C-y`: paste
- `F1`: view help (equivalent to `man hexedit`)
- `F2`: save
- `F3`: load file
- `F7`: copy the selected region (`Delete` is also mapped to copy!)

You can navigate throughout the file with the arrow keys as well as `PUp`
and `PDown`. Note that although you can use `Backspace` to delete edits
you have made, you CANNOT delete existing data! Instead, you should select
data, copy the selection, and paste it into a new file.

## Repairing corrupted files with hexedit

File headers can get damaged if you accidentally run a binary editing
tool on a file that has a different file type. For example, I often use
the CLI tool `mid3v2` to bulk tag my mp3 files, especially files grouped
into album folders, by using a `bash` for-loop:

```sh
for f in $(find . -maxdepth 1 -type f); do
  bname=$(basename -s .mp3 $f); trkname=$(echo "$bname" | cut -d'_' -f2-); \
  trknum=$(echo "$bname" | cut -d'_' -f1); \
  mid3v2 -a myartist -g 8 -t $trkname -T $trknum/11 -y 2012 -A "Some Jazz" "$f";
done
```

Unfortunately, the `mid3v2` utility doesn't do any sanity checking before
writing to a file, so you could even add id3v2 tags to a PDF file. The
id3v2 tags are added to the very beginning of the file, so if you run the
`file` utility against a mid3v2 edited file, it will appear as an audio
file!

I stupidly ran the above on my `~/` user home directory, and all the
regular files (mostly dotfiles) there got corrupted due to the `ID3...`
tags inserted into the file header. Below you can see that `file` detects
a `mid3v2` edited PDF as an audio file!

```sh
jundora@argonaut:~$ file pj_resume_en-US.pdf
pj_resume_en-US.pdf: Audio file with ID3 version 2.4.0, contains:PDF document, version 1.5 (zip deflate encoded)
```

Of course, this will prevent me from opening the file with a PDF reader, as
the file header is corrupted. If you open the corrupted file with
`hexedit`, you will see that the first byte in ASCII representation
contains the chars `ID3`, or `49 44 33` in hexadecimal. To repair the file,
first `hexedit pj_resume_en-US.pdf` to open the file in the hex editor.

Press `TAB` to change navigation to the ASCII view on the right-hand
column. Navigate down until you reach the first ASCII character of the
original data. press `C-SPC` to set mark, and then press `>` to jump to the
end of the file. This will select all the original data. Press `ESC-W` to
copy the selection, and finally `ESC-Y` to paste the data to a new file,
say, `pj_resume_en-US.pdf.recovery`. `C-c` to close `hexedit` without
saving and now run `file` against the new file to verify that it is
detected as a PDF instead of an audio file.

```sh
jundora@argonaut:~$ file pj_resume_en-US.pdf.recovery
pj_resume_en-US.pdf.recovery: PDF document, version 1.5 (zip deflate encoded)
```

Looking good! Now just overwrite the old filename with the repaired file:

```sh
mv pj_resume_en-US.pdf.recovery pj_resume_en-US.pdf
```

