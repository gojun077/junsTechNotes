#+TITLE: Tailscale Notes
#+SUBTITLE: tips and tricks, tailscale cli, etc
#+AUTHOR: Peter Jun Koh
#+EMAIL: gopeterjun@naver.com
#+DESCRIPTION: simple, commercial VPN based on Wireguard
#+KEYWORDS: network, sec, wireguard, VPN
#+LANGUAGE: en

* Summary

- Created on: [2022-03-03 Thu]
- Last Updated: [2024-12-17 Tue 16:57]

Tailscale is a VPN service built on top of Wireguard that "just works"
with much lower complexity than legacy VPN's like ~openvpn~.

* Topics

https://tailscale.com/kb/1080/cli/

** Tailscale installation

https://tailscale.com/kb/installation/

*** Tailscale Unstable repo

https://pkgs.tailscale.com/unstable

The unstable repo tracks the =main= branch at [[https://github.com/tailscale/tailscale/tree/main][Tailscale upstream]] and is the
first to get bug fixes.

** Tailscale status

From Windows and Linux, =tailscale= will be in your default path.

#+begin_src sh
ubuntu@pimax:~$ tailscale status
100.118.186.118 pimax                gojun077@    linux   -
100.118.206.3   pj-mbp2019           gojun077@    macOS   active; direct 192.168.21.227:41641
100.68.85.82    pj0                  gojun077@    linux   active; direct 192.168.0.10:41641
100.126.158.68  pj1                  gojun077@    linux   active; direct 192.168.0.11:41641
100.67.245.51   pju36jc              gojun077@    linux   offline
100.81.70.85    win11-mac            gojun077@    windows offline
#+end_src

For MacOS, you must specify the absolute path to Tailscale:

#+begin_src sh
$ /Applications/Tailscale.app/Contents/MacOS/Tailscale status
100.118.206.3   pj-mbp2019           gojun077@    macOS   -
100.118.186.118 pimax                gojun077@    linux   active; direct 192.168.21.229:41641, tx 948 rx 860
100.68.85.82    pj0                  gojun077@    linux   active; direct 192.168.21.218:41641, tx 1204 rx 1116
100.126.158.68  pj1                  gojun077@    linux   active; direct 192.168.21.119:41641, tx 820 rx 732
100.67.245.51   pju36jc              gojun077@    linux   offline
100.81.70.85    win11-mac            gojun077@    windows offline
#+end_src

It is recommended to add an alias to your ~.bashrc~ on macOS so that you
can use the same lowercase =tailscale= command as on other OS'es.

#+begin_src sh
alias tailscale="/Applications/Tailscale.app/Contents/MacOS/Tailscale"
#+end_src

=tailscale status= can show you whether you are directly connected to
another tailscale node via your network or if you are connecting to
another node via DERP relay. If you are connecting to another node via
relay, that means your traffic is going out onto the Internet, passing
through one of Tailscale's regional proxies, and getting routed to your
destination tailnet device.

** Tailscale ping

On Windows and Linux simply run =tailscale ping <deviceName>=

#+begin_src sh
ubuntu@pimax:~$ tailscale ping pj-mbp2019
pong from pj-mbp2019 (100.118.206.3) via 192.168.21.227:41641 in 5ms
ubuntu@pimax:~$ tailscale ping pj0
pong from pj0 (100.68.85.82) via 192.168.0.10:41641 in 3ms
ubuntu@pimax:~$ tailscale ping pj1
pong from pj1 (100.126.158.68) via 192.168.0.11:41641 in 2ms
#+end_src

** Get SSL certs from Let's Encrypt

https://tailscale.com/kb/1153/enabling-https/

To get your unique tailnet name you have to go into the DNS page in the
Tailscale admin console. Apparently you can't get this via the
=tailscale= CLI app.

You must have /MagicDNS/ enabled for the following to work:

#+begin_src sh
sudo tailscale cert <hostname>.<magicDNS-FQDN>
#+end_src

This will generate a ~.crt~ and ~.key~ in the working directory from
which you executed the above command. Also the keys will be stored in
~/var/lib/tailscale/certs/~ and the Let's Encrypt SSL certs will be
regularly updated by =tailscaled= in ~/var/lib/tailscale/~ using the
~acme-account.key.pem~ file for authentication.

For applications running as ~root~, you can simply symlink these keys
from ~/var/lib/tailscale/certs/~ to the appropriate directory, i.e.
~/etc/nginx/..~ etc.

For applications that need to use these SSL certs but don't run as
~root~, you will have to copy the certs from ~/var/lib/tailscale/certs/~
and =chown= them to the appropriate owner and group

*Note*: =tailscale cert= does /not/ renew certs for you! You must
manually run this command before your certs expire. I was only alerted
to the Let's Encrypt cert expiration for Tailscale when my ~musikdroid~
client was no longer able to connect to my ~musikcube~ server which runs
behind an =nginx= reverse proxy for terminating TLS. The nginx
~error.log~ showed the following:

#+begin_example
023/07/14 10:27:44 [error] 2258#2258: OCSP response not successful (6: unauthorized) while requesting certificate status, responder: r3.o.lencr.org, peer: 23.216.159.51:80, certificate: "/var/lib/tailscale/certs/REDACTED.ts.net.crt"
#+end_example

There was an issue in =tailscale= < ~1.44~ in which running =tailscale
cert= would not issue new Let's Encrypt certs unless the cert was less than
14 days to expiration. With tailscale [[https://github.com/tailscale/tailscale/pull/8258][PR 8258]], now =tailscale cert
<tailscale FQDN>= will renew certs when 2/3 of the cert lifetime has
passed.

Related links:

- https://community.letsencrypt.org/t/ocsp-response-not-successful-6-unauthorized/103589
- https://community.letsencrypt.org/t/tailscale-certificates/198895/20
- https://github.com/tailscale/tailscale/issues/8204

In [[https://github.com/tailscale/tailscale/issues/1235#issuecomment-931745783][Tailscale issue 1235]], ~@bradfitz~ says:

#+begin_quote
Also, note that the =tailscaled= daemon does not automatically keep the
certs on disk refreshed. It only keeps its certs in memory refreshed.
It's up to you to write them to disk as needed with =tailscale cert= (or
use the Go library if you use Go)
#+end_quote

Here is a simple systemd service file to run =tailscale cert= and then
reload nginx. It is intended to be triggered by a systemd timer every
week.

#+begin_src systemd
[Unit]
Description=Renew Let's Encrypt cert via "tailscale cert" and reload nginx
After=network.target tailscaled.service
Requires=tailscaled.service

[Service]
Type=oneshot
ExecStart=/usr/bin/tailscale cert --cert-file /var/lib/tailscale/certs/<redacted>.ts.net.crt --key-file /var/lib/tailscale/certs/<redacted>.ts.net.key <redacted>.ts.net
ExecStartPost=/usr/bin/systemctl reload nginx

[Install]
WantedBy=multi-user.target
#+end_src

Another way to do the same thing would be to create two separate systemd
service files, with the dependent task running after the first task as
covered
[[https://trstringer.com/simple-vs-oneshot-systemd-service][here]].

Let's assume the service below is named =tailscale-cert.service=

#+begin_src systemd
[Unit]
Description=Renew Let's Encrypt cert with "tailscale cert"

[Service]
Type=oneshot
ExecStart=/usr/bin/tailscale cert --cert-file /var/lib/tailscale/certs/<redacted>.ts.net.crt --key-file /var/lib/tailscale/certs/<redacted>.ts.net.key

[Install]
WantedBy=multi-user.target
#+end_src

and the dependent service that runs after =tailscale-cert.service=:

#+begin_src systemd
[Unit]
Description=Reload nginx after "tailscale cert" service has run
After=tailscale-cert.service
Requires=tailscale-cert.service

[Service]
ExecStart=/usr/bin/systemctl reload nginx
#+end_src

** UFW and Tailscale

https://tailscale.com/kb/1077/secure-server-ubuntu-18-04/

Allow all traffic from =tailscale0=, deny arbitrary incoming traffic,
allow ssh from internal IP range, allow rsync from internal IP, allow
UDP from 41641 in internal network.

#+begin_src sh
sudo ufw allow in on tailscale0
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow from 192.168.21.0/24 proto tcp to any port 22
sudo ufw allow from 192.168.21.0/24 proto tcp to any port 873
sudo ufw allow from 192.168.21.0/24 proto udp to any port 41641
#+end_src

** Firewalld and Tailscale

In =firewalld=, unlike in =ufw= or =iptables=, using discrete rich rules
is discouraged; instead of specifying source IP ranges for traffic to
certain ports, source IP ranges should be added to particular firewall
/zones/ and network interfaces should be added to these zones.

In the case of tailscale, when you first install or re-install
=tailscaled= the tunnel interface =tailscale0= will be created. You
should add =tailscale0= to the /trusted/ firewalld zone, which allows
all traffic from this interface.

*Note*: if you delete and install =tailscale=, your permanent firewalld
rule for =tailscale0= will need to be recreated! Also allow your
internal devices to initiate/accept UDP from =41641= for Wireguard and
UDP =3478= for STUN.

#+begin_src sh
sudo firewall-cmd --add-interface tailscale0 --zone trusted
sudo firewall-cmd --add-port 41641/udp --zone home
sudo firewall-cmd --add-port 3478/udp --zone home
sudo firewall-cmd --runtime-to-permanent
#+end_src

** Check device-to-device connection state

When your devices are on the same physical network, they should be
communicating directly, without any relay servers required. You can
check this with =sudo tailscale status=.

#+begin_example
1           2         3          4         5
100.1.2.3   device-a  alice@     linux     active; direct <ip-port>, tx 1116 rx 1124
100.4.5.6   device-b  bob@       macOS     active; relay <relay-server>, tx 1351 rx 4262
100.7.8.9   device-c  charlie@   windows   idle; tx 1214 rx 50
100.0.1.2   device-d  diane@     iOS       —
#+end_example

#+begin_quote
If a device is active on the tailnet, for the connection status
(column 5) you'll see "direct" for peer-to-peer connections, along with
the IP address used to connect, or "relay" for connections using DERP,
along with a city code, such as nyc, fra, etc.) for the respective
location.

#+end_quote

** Re-installing Tailscale

If you remove a machine from your tailnet via the Tailnet web console or
if you delete the state file in =/var/lib/tailscale/tailscaled.state=
Tailscale will issue your machine a new IP address. In the latter case,
the Tailscale web console will still have your old hostname registered,
so when you run =tailscale up= to re-authenticate and re-add your
machine, it may get assigned =<oldHostname>-1= as its hostname instead
of just =<oldHostname>=. In this case, you will have to remove both
hosts from the Tailscale web console and try =tailscale up= once more to
get your old hostname assigned to your machine.

Also in such cases, the hash for your machine will change although the
hostname is the same. This can cause connectivity issues between your
re-added node and other nodes in your tailnet. I was able to resolve
this issue by rebooting the re-added machine. Also keep in mind that you
will have to recreate any =firewalld= configs that were attached to the
=tailscale0= interface.

** Tailscale operator setup

https://tailscale.com/kb/1236/kubernetes-operator

** References

- https://tailscale.com/kb/1082/firewall-ports/
