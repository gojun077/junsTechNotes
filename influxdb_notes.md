Notes on InfluxDB
======================

# Summary

- Created on: Feb 15 2022
- Last Updated: May 14 2023
- Created by: gojun077@gmail.com

InfluxDB is a database that specializes in storing time series data.  I
started using it to store Limit Order Book (LOB) data for Korean stocks, as
the LOB is updated many times per second for really active issues. On a
single node, InfluxDB can easily handle up to 5000 datapoints per second,
but I will never be able to stress my InfluxDB instance like this because I
am limited by the update speed of my broker's data feed. Still, I'm sending
33 datapoints per second, per stock including 29 FID's of Kiwoom real-time
event data (which must be subscribed to in advance) including the price and
depth of the nearest 6 bid and asks or BBO (Best Bid/Offer).

This file contains my notes of working with the various programming
language clients for InfluxDB and the `influx` CLI.

These notes are for an InfluxDB instance and CLI running version `v2.x`.

- https://docs.influxdata.com/influxdb/v2.1/reference/key-concepts/data-elements/

# Topics

## Hardware Requirements

https://docs.influxdata.com/influxdb/v1.8/guides/hardware_sizing/#influxdb-oss-guidelines


## Installation

### MacOS

https://docs.influxdata.com/influxdb/v2.1/install/

Start InfluxDB by running the influxd daemon with `influxd`


### Ubuntu

https://docs.influxdata.com/influxdb/v2.7/install/


```sh
wget https://dl.influxdata.com/influxdb/releases/influxdb2-2.7.0-arm64.deb
sudo dpkg -i influxdb2-2.7.0-arm64.deb
```

### Fedora

InfluxDB has repos for RHEL-based distros, but not for Fedora specifically.
This is not a big issue, however. Download the `.repo` file and edit a
few strings to make it compatible with Fedora.

By default, the rpm package installs the influxDB engine into
`/var/lib/influxdb/engine` and generates a config file at
`/etc/influxdb/config.toml`. It also creates a systemd service
`influxd.service`.


## Configuration

https://docs.influxdata.com/influxdb/v2.7/reference/config-options/

You can certainly run influxdb with just command line option flags and env
vars, but for ease of administration and amenability config management with
version control, it is better to use a configuration file to store runtime
settings.

If you installed influxdb from a native OS package (`.deb`, `.rpm`, etc) a
service daemon and config file will be generated for you. As of influxdb
`v2.7` the default config file is in `TOML` format. The path to this file
is `/etc/influxdb/config.toml`.

### Enable https

In `config.toml`, add `tls-key` and `tls-cert` and point to the path to the
key. However, if you are using automatically-renewed certs from Let's
Encrypt, these certs will have owner and group set to `root:root`, which
isn't compatible with InfluxDB, as the daemon runs as user `influxdb`.

One workaround is to copy your certs to, say, `/etc/influxdb-ssl-certs/`
and then change the ownership to `influxdb:influxdb`. Then in `config.toml`
point `tls-key` and `tls-cert` to these copied files. This works, but the
downside is that you will have to manually copy over the updated certs
about every 90 days when Let's Encrypt `certbot` renews your certs. Here is
one [post](https://grodansparadis.com/wordpress/?p=5342) in which someone
created a new group called `newusers`, containing the users
`root,influxdb`. They then apply this new group ownership to the Let's
Encrypt directories and give read & execute rights to the group:

```sh
chgrp newgrp /etc/letsencrypt/archive
chgrp newgrp /etc/letsencrypt/live
chmod g+rx /etc/letsencrypt/archive
chmod g+rx /etc/letsencrypt/live
```

I don't understand why eXecute rights are necessary for the `influxdb` user
to use Let's Encrypt certs; in my case I just copied the SSL cert and key
files to `/etc/influxdb-ssl-certs` and simply changed the owner to
`influxdb:influxdb` and the files have the permissions `644` and `600`,
respectively. No need to set the execute bit.

References:
https://docs.influxdata.com/influxdb/v2.7/security/enable-tls/#configure-influxdb-to-use-tls

**Note about Tailscale SSL certs with Let's Encrypt**: Tailscale now offers
SSL certs for hosts in your tailnet with MagicDNS enabled:

```sh
sudo tailscale cert <hostname>.<magicDNS-FQDN>
```

The above command will create `.crt` and `.key` in your Present Working
Directory. By default the files we be owned by `root:root` and the
permissions will be *644* for `.crt` and *600* for `.key`. For apps that
run as the root user, there will be no problems, but as stated above, this
poses problems for InfluxDB. The workaround I am using is to *copy* these
files to a separate directory and change the ownership to
`influxdb:influxdb` so influxdb can use these files for the Admin UI on
port `8086`. Another solution to this problem is to use a reverse proxy
like nginx in front of InfluxDB.


## nginx reverse-proxy with influxDB on Fedora 38

In Fedora, there are no `/etc/nginx/sites-available/` or
`/etc/nginx/sites-enabled/` folders. On distros like Ubuntu, for example,
first you create a virtual site config file in the `sites-available` folder
and then create a symlink from the config file to the `sites-enabled`
folder.

Instead, you will create virtual host config files in `/etc/nginx/conf.d/`
and after restarting `nginx.service` the new virtual host will be active.

Here is a sample `/etc/nginx/conf.d/influxdb.conf`

```nginx
server {
    listen 8086 ssl http2;
    listen [::]:8086 ssl http2;
    server_name argonaut.finch-blues.ts.net;

    location / {
        proxy_pass http://127.0.0.1:9086;
    }

    ssl_certificate /var/lib/tailscale/certs/argonaut.finch-blues.ts.net.crt
    ssl_certificate_key /var/lib/tailscale/certs/argonaut.finch-blues.ts.net.key
    ssl_session_timeout 1d;
    ssl_session_cache shared:MozSSL:10m;  # about 40000 sessions
    ssl_session_tickets off;

    # curl https://ssl-config.mozilla.org/ffdhe2048.txt > /path/to/dhparam
    ssl_dhparam /etc/nginx/ssl-dhparams.txt;

    # intermediate configuration
    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
    ssl_prefer_server_ciphers off;

    # HSTS (ngx_http_headers_module is required) (63072000 seconds)
    add_header Strict-Transport-Security "max-age=63072000" always;

    # OCSP stapling
    ssl_stapling on;
    ssl_stapling_verify on;

    # verify chain of trust of OCSP response using Root CA and Intermediate certs
    ssl_trusted_certificate /var/lib/tailscale/certs/lets-encrypt-x3-cross-signed.pem;

    # replace with the IP address of your resolver
    resolver 192.168.21.1;
}
```

Note that for `ssl_trusted_certificate` I am using the Let's Encrypt
intermediate CA cert downloaded from https://letsencrypt.org/certs/lets-encrypt-x3-cross-signed.pem

Normally if you are using `certbot` to manage your Let's Encrypt TLS certs,
`/etc/letsencrypt/...` will contain `fullchain.pem` which includes your
FQDN's TLS cert as well as the full chain of CA attestations.

Because I am using Let's Encrypt certs issued via Tailscale, however, in
`/var/lib/tailscale/certs/` only a private `.key` and certificate `.crt`
file exist; there is no `fullchain.pem`. This is no problem; you can `wget`
the intermediate CA chain pem directly from Let's Encrypt at the link above.

Note that by default, `selinux` will not allow nginx to bind port `8086`.
You will see the following error in `/var/log/audit/audit.log`:

```
type=AVC msg=audit(1683626628.789:1237): avc:  denied  { name_bind } for  pid=96838 comm="nginx" src=8086 scontext=system_u:system_r:httpd_t:s0 tcontext=system_u:object_r:unreserved_port_t:s0 tclass=tcp_socket permissive=0
```

By default, selinux only allows the following ports to be used by `nginx`:

```sh
root@argonaut:/etc/nginx/conf.d# semanage port -l | grep http_port_t
http_port_t                    tcp      80, 81, 443, 488, 8008, 8009, 8443, 9000
pegasus_http_port_t            tcp      5988
```

You will have to update the `http_port_t` selinux context label to allow
nginx to bind to TCP 8086:

```sh
semanage port -a -t http_port_t -p tcp 8086
```

Finally, make sure `/etc/influxdb/config.toml` contains the setting to
listen on tcp 9086:

```toml
bolt-path = "/var/lib/influxdb/influxd.bolt"
engine-path = "/var/lib/influxdb/engine"
http-bind-address = ":9086"
log-level = "info"  # debug, info, error
```


Reference:

- https://serverfault.com/questions/566317/nginx-no-permission-to-bind-port-8090-but-it-binds-to-80-and-8080
  + updating selinux context label for nginx


## caddy2 reverse proxy with influxdb on Ubuntu 22.04

Unlike for RHEL-based distros, on Ubuntu we don't have to worry about
SElinux settings for web servers and custom port bindings. Also the `caddy`
webserver configuration syntax is really simple compared to that of
`nginx`.

Caddy will listen on TCP 8086 for HTTPS/TLS traffic and send raw HTTP to
influxdb listening on TCP 9086.

First make sure the following line exists in `/etc/influxdb/config.toml`:

```toml
http-bind-address = ":9086"
```

To enable reverse proxy from tcp 8086 to 9086, add the following block to
`/etc/caddy/Caddyfile`:

```
rpi3b.finch-blues.ts.net:8086 {
        reverse_proxy localhost:9086
}
```

This is 1/10 the size of the `nginx` reverse proxy config ;) Caddy is
awesome!


## Using the Influx CLI

https://docs.influxdata.com/influxdb/v2.7/reference/cli/influx/

Using the Influx CLI is most convenient for admin tasks like checking running
configs, listing users, changing passwords, and generating API tokens.

When you first install InfluxDB you will have to create an organization,
admin user, and API token. This information will be saved to
`~/.influxdb/configs`. To avoid specifying your host, API token, and
organization with command-line flags every time you execute the `influx`
CLI, use `influx config create` with the following flags:

```sh
influx config create --config-name <config-name> \
  --host-url http://argonaut.finch-blues.ts.net:8086 \
  --org <your-org> \
  --token <your-auth-token> \
  --active
```

**Common admin commands**

- `influx user list`
  + show all users' ID and Name
- `influx user password -n <username> -t <user-API-token>`
  + change user password


## Schema Design

- https://blog.zhaw.ch/icclab/influxdb-design-guidelines-to-avoid-performance-issues

> InfluxDB lets you specify fields and tags, both being key/value pairs
> where the difference is that *tags are automatically indexed*.

> Because fields are not being indexed at all, on every query where
> InfluxDB is asked to find a specified field, it needs to sequentially
> scan every value of the field column.

> A rule of thumb would be to persist highly dynamic values as fields


## Generate a Token

https://docs.influxdata.com/influxdb/v2.7/security/tokens/create-token/

Easiest way is to use the InfluxDB web UI, but it is possible to use
the influx CLI and v2 API as well.

## Create a bucket

https://docs.influxdata.com/influxdb/cloud/organizations/buckets/create-bucket/

## Data point

https://docs.influxdata.com/influxdb/v1.8/concepts/glossary/#point

> In InfluxDB, a point represents a single data record, similar to a row in
> a SQL database table. Each point:

> - has a measurement, a tag set, a field key, a field value, and a timestamp;
> - is uniquely identified by its series and timestamp


## Write a Data Point to a Bucket


```python
point = Point("h2o_feet") \
    .tag("location", "pacific") \
    .field("water_level", 5) \
    .field("timestamp",
    datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%m:%S.%fZ'))

sync_result = write_api.write(bucket="pjtest",
                                     record=point, precision='us')
```

From the *Data Explorer* dashboard, to see sub-second time you must click
on *View Raw Data* and then the `_time` will show sub-second resolution.
No reason to worry, however, as InfluxDB stores data points at nanosecond
resolution on disk (you can also specify lower time resolution at write
time).


## Delete Data from a Bucket

```sh
influx delete --bucket stocks_kr --start '2022-02-15T00:00:00Z' \
--stop '2022-02-15T00:39:59Z' --org EPH2 \
-t "superLongHashString"
```

## Export non-data resources (Dashboards, bucket names) as templates

https://docs.influxdata.com/influxdb/v2.7/reference/cli/influx/export/all/

```
influx export all --filter=resourceKind=Bucket \
  --filter=resourceKind=Dashboard > my.template
```

You can then import this text file into another InfluxDB instance and
you will get all your dashboards and buckets setup, although the
new buckets will be empty until you write data into them.

## Import non-data resources via template

https://docs.influxdata.com/influxdb/v2.7/influxdb-templates/use/

Before you try to import an exported template file, first validate the
template:

```sh
influx template validate -f /path/to/template
```

If all is well, apply the template:

```sh
influx apply -o <orgName> -f path/to/templateFile
```

## Export all data from a bucket

There is no separate command to export data from a bucket; instead you must
run a query and pipe this query to a file. Note that the InfluxDB v2 query
API returns results in [annotated CSV
format](https://docs.influxdata.com/influxdb/v2.7/reference/syntax/annotated-csv/). You
can also write data to InfluxDB using this format.

The following query returns annotated CSV results:

```
influx query \
  'from(bucket: "stocks_kr") |> range(start: -365d, stop:-330d)' \
  --raw
```

```csv
,,40,2022-04-12T06:25:19.881444Z,2022-05-17T06:25:19.881444Z,2022-04-27T05:43:22.030621Z,3451,ask01_depth,stock_LOB_data,KOSDAQ,SBI인베스트먼트,019550
,,40,2022-04-12T06:25:19.881444Z,2022-05-17T06:25:19.881444Z,2022-04-27T05:43:22.294377Z,3451,ask01_depth,stock_LOB_data,KOSDAQ,SBI인베스트먼트,019550
,,40,2022-04-12T06:25:19.881444Z,2022-05-17T06:25:19.881444Z,2022-04-27T05:43:22.705048Z,3451,ask01_depth,stock_LOB_data,KOSDAQ,SBI인베스트먼트,019550
,,40,2022-04-12T06:25:19.881444Z,2022-05-17T06:25:19.881444Z,2022-04-27T05:43:24.468315Z,3451,ask01_depth,stock_LOB_data,KOSDAQ,SBI인베스트먼트,019550
```

You can pipe the query results to a file and then use `influx write` from
another machine to read data from the text file into a new InfluxDB instance.

After your query completes you should have a raw text file in annotated csv
format. Depending on how much data was written, the file could be quite large.
InfluxDB natively supports gzipped annotated csv and line protocol data, and
writing such data is actually faster than writing uncompressed data because
InfluxDB internally stores its data in gzipped form.

Note that `gzip` default behavior is to compress `myfile` and replace it
with `myfile.gz`.

```sh
gzip file.txt  # this creates 'file.txt.gz' replacing file.txt
```

To maintain the original and create a compressed copy:

```sh
gzip -c file.txt > file.txt.gz
```

According to 2021
[benchmarks](https://www.adaltas.com/en/2021/03/22/performance-comparison-of-file-formats),
`gzip` can compress CSV and JSON by about 95%. in my own experience
compressing annotated CSV data for Limit Order Book and TAQ for about a
dozen Korean stocks over 7 months, the raw data file was 128GB, but after
compression with gzip default settings, the resulting file was only 5.8GB!


## Import data from file

- https://docs.influxdata.com/influxdb/cloud/write-data/developer-tools/csv/
- https://docs.influxdata.com/influxdb/cloud/reference/cli/influx/write/

Since Flux returns query results in annotated CSV format, you can write
these raw query results back to InfluxDB. In the previous section we
redirected the results of a Flux query to a file. Before using writing this
data to InfluxDB, first we will `gzip` the query results file and then
read it into an existing bucket with `influx write`.

According to InfluxDB [best
practices](https://docs.influxdata.com/influxdb/cloud/write-data/best-practices/optimize-writes/#use-gzip-compression),
gzip compression speeds up writes to InfluxDB up to 5x over writing
uncompressed raw data.

By default, the `influx write` command will use gzip compression for files
with `.gz` extension, but if you have a gzipped file wihout this extension
in the filename, you must specify `--compression gzip` as a CLI
option. There is also a `--format` option flag for `lp` and `csv` files,
but influxdb will use the appropriate format indicated by the file
extension. If the filename does not contain the appropriate extension, you
should specify the format via the option flag. Also when reading raw data
from `stdin`, you must specify the format with this option flag.

```sh
influx write -b stocks_kr.csv.gz --format csv
```

The file above is 5.8GB gzipped, but 128GB uncompressed. Writing this
amount of data is quite slow, (started around 18:00, completed around
21:22, roughly 3.5 hours!)

Using `iostat -dh 5` to show only disk I/O in pretty-print format
every 5 seconds, shows that writes are steadily occurring but the
write speed is generally less than 1M/s. First the fields for the
`-d` Device Utilization report:

- `Device`: self-explanatory
- `tps`: transfers per second
- `kB_read/s`: amount of data read from the device in kB/s
- `kB_wrtn/s`: amount of data written to the device in kB/s
- `kB_dscd/s`: amount of data discarded for the device in kB/s
- `kB_read`: total amount of kB read from device
- `kB_wrtn`: total amount of kB written to device
- `kB_dscd`: total amount of kB discarded from device

On average, I am seeing less than 100 `tps`, and less than 1M written per
second (ranging from 100 - 800 kB/s). In a 5-sec interval I generally
see less than 5M written to my m2 NVMe drive.

Related issue *backup & restore process is slow*:

https://github.com/influxdata/influxdb/issues/9984


## InfluxDB Backup

- https://docs.influxdata.com/influxdb/v2.7/backup-restore/backup/

> The InfluxDB 2.7 influx backup command is not compatible with versions of
> InfluxDB prior to 2.0.0.

> The influx backup command requires:

> - directory path for where to store the backup file set
> - root authorization token

```sh
influx backup \
  path/to/backup_$(date '+%Y-%m-%d_%H-%M') \
  -t xXXXX0xXX0xxX0xx_x0XxXxXXXxxXX0XXX0XXxXxX0XxxxXX0Xx0xx==
```


## InfluxDB Restore

- https://docs.influxdata.com/influxdb/v2.7/backup-restore/restore/


```sh
# restore all time series data
influx restore /backups/2020-01-20_12-00/

# restore data from a specific bucket
influx restore \
  /backups/2020-01-20_12-00/ \
  --bucket example-bucket  # bucket name

# OR
influx restore \
  /backups/2020-01-20_12-00/ \
  --bucket-id 000000000000  # bucket ID
```

### Restore to a *new* InfluxDB server

**I don't recommend using `influx restore`** on remote instances due to the
requirement to use the admin token from the source instance to setup the
new instance; it is much simpler to manually get annotated csv data via
`influx query`, `gzip` the data, transfer the data to a new InfluxDB
server, and `influx write` the gzipped annotated csv data directly to a new
bucket on the new instance.

**Steps**

- Retrieve the admin token from your source InfluxDB instance
- Setup a new InfluxDB instance but use `--token <sourceToken>`

```sh
influx setup --token <tokenFromSourceInstance>
```

- Restore the source backup to the new server

```sh
influx restore \
  /backups/2020-01-20_12-00/ \
  --full
```

> If you do not provide the admin token from your source InfluxDB instance
> as the admin token in your new instance, the restore process and all
> subsequent attempts to authenticate with the new server will fail.


## Explore Data in Web GUI

### Flux Query

Find all data points where the field name is `ask_01_depth_chg` and
where that field's value is l.t. 0:

```
from(bucket: "stocks_kr")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "stock_LOB_data")
  |> filter(fn: (r) => r["secname"] == "휴림로봇")
  |> filter(fn: (r) => r["_field"] == "ask01_depth_chg" and r["_value"] < 0)
  |> filter(fn: (r) => r["market"] == "kosdaq")
  |> aggregateWindow(every: v.windowPeriod, fn: last, createEmpty: false)
  |> yield(name: "last")
```

### Query Builder

Build queries through the GUI


## Calculate Microprice from LOB data

```flux
from(bucket: "stocks_kr")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "stock_LOB_data")
  |> filter(fn: (r) => r["_field"] == "ask01_price" or r["_field"] == "bid01_price" or r["_field"] == "ask01_depth" or r["_field"] == "bid01_depth")
  |> filter(fn: (r) => r["secno"] == "900110")
  |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
|> map(fn: (r) => ({ r with _value:((float(v: r.ask01_depth) * float(v: r.bid01_price))+(float(v: r.bid01_depth) * float(v: r.ask01_price))) / (float(v: r.ask01_depth) + float(v: r.bid01_depth))
                  }))
```

I wish there was an easier way to typecast all the fields to `float` in
one go instead of having to do it one-by-one for each element in the
`map()` statement.


## Define Dashboard Variable

From the *Explore*, menu, you can save a query as type `map`, `query`, or
"" and then refer to the variable with `v.<varName>` within a dashboard.

## InfluxDB 2.x engine - Time Structured Merge (TSM) Tree

https://docs.influxdata.com/influxdb/v2.7/reference/internals/storage-engine/#time-structured-merge-tree-tsm


