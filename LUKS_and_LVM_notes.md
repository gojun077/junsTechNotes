LUKS and LVM Notes
=====================

# Summary

- Created on: Aug 1 2020
- Created by: gojun077@gmail.com
- Last Updated: Aug 1 2022

My notes on working with Linux Full Disk Encryption using LUKS and LVM.

# Topics

## Mount an LVM logical volume that contains a LUKS container

It is quite common to find one large LUKS container which, when opened,
contains an entire LVM volume group with logical volumes.

But it is also possible to have an LVM Volume Group that contains Logical
Volumes that are only visible as LUKS containers. To actually mount the
LV's, you have to first open the LUKS container.

Consider the following output from `lsblk -f`:

```
...
sdd
├─sdd1            vfat             A734-2FFC
├─sdd2            ext4             dc84aadc-79da-48e5-9dfc-18c0cdcc3a0a
├─sdd3            ext4             0ed7ffee-d195-42bc-a4a4-79ae9486294a
├─sdd4            ext4             308e5745-1a46-4eac-96ad-0059eced9d91
├─sdd5            swap             22b7d309-f234-40fd-953f-b2b1f4e0f8bd
├─sdd6            ext4             9af11d7f-f86c-4422-809e-622910d3c379
└─sdd7            LVM2_membe      ElkErh-tKOZ-qmnk-yxWE-C43h-eUG1-817Slf
   ├─fedora_u36jfed23-00
   │               crypto_LUK       7644165e-6c3c-425d-8d7c-0c37d9ae24b5
   ├─fedora_u36jfed23-01
   │               crypto_LUK       43fc4ee9-6c9f-4bc7-b86d-7ef74313db56
   ├─fedora_u36jfed23-02
   │               crypto_LUK       2396cab3-3176-4e0d-8de2-84cf5ae7a958
   └─fedora_u36jfed23-03
                   crypto_LUK       325711e5-8c22-49ad-82d2-42b422b9d3dd
```

`/dev/sdd7` contains an LVM Volume Group (VG) inside of which are 4 Logical
Volumes that are of type `crypto_LUK`, i.e. LUKS crypto container.

The first step is to activate the LVM Volume Group.

```sh
$ sudo vgchange -ay /dev/fedora_u36jfed23
  4 logical volume(s) in volume group "fedora_u36jfed23" now active
```

Second step - create a mapping for a decrypted LUKS container. Here we will
create mapping `c0` for `/dev/fedora_u36jfed23/00`

```sh
$ sudo cryptsetup luksOpen /dev/fedora_u36jfed23/00 c0
Enter passphrase for /dev/fedora_u36jfed23/00:
junbuntu@pjU36JC:~$ lsblk -f
NAME             FSTYPE      LABEL UUID
...
sdd
├─sdd1           vfat              A734-2FFC
├─sdd2           ext4              dc84aadc-79da-48e5-9dfc-18c0cdcc3a0a
├─sdd3           ext4              0ed7ffee-d195-42bc-a4a4-79ae9486294a
├─sdd4           ext4              308e5745-1a46-4eac-96ad-0059eced9d91
├─sdd5           swap              22b7d309-f234-40fd-953f-b2b1f4e0f8bd
├─sdd6           ext4              9af11d7f-f86c-4422-809e-622910d3c379
├─sdd7           LVM2_member       ElkErh-tKOZ-qmnk-yxWE-C43h-eUG1-817Slf
│ ├─fedora_u36jfed23-00
│ │              crypto_LUKS       7644165e-6c3c-425d-8d7c-0c37d9ae24b5
│ │ └─c0         ext4              6c69ddfe-3cc8-46eb-9542-c29c619073f9
...
```

Now you can mount the regular `ext4` partition `c0` that resulted when the
LUKS container was opened. But what is the naming convention for `c0`?

```sh
$ ls -al /dev/mapper
total 0
drwxr-xr-x  2 root root     260  8월  1 03:07 .
drwxr-xr-x 23 root root    5080  8월  1 03:07 ..
lrwxrwxrwx  1 root root       7  8월  1 03:07 c0 -> ../dm-9
...
```

Dev mapper has created a nice, easy-to-remember alias for dm-9; Now you can
simply mount `/dev/mapper/c0`

```sh
$ sudo mount /dev/mapper/c0 /mnt/mount0
```

Once you're done using the encrypted partition/LV, `umount` the partition,
and then invoke `cryptsetup luksClose <alias>`

## Mount LV inside LVM that is inside a LUKS container

```sh
[jundora@argonaut ~]$ lsblk -f
NAME                   FSTYPE      FSVER    LABEL UUID                                   FSAVAIL FSUSE% MOUNTPOINTS
sda
├─sda1                 vfat        FAT32          9C22-1F60
├─sda2                 ext4        1.0            8b44b53b-cf04-4edc-9060-e339257ef8ae
└─sda3                 crypto_LUKS 2              68ffe7b0-d901-417f-899b-e643eec41d0a
...
```

This case is the opposite of that described previously. You can see that
`/dev/sda3` is a LUKS encrypted partition. First you need to create a
mapping for the LUKS container and decrypt it with `luksOpen`.

```sh
[jundora@argonaut ~]$ sudo cryptsetup luksOpen /dev/sda3 c0
Enter passphrase for /dev/sda3:
[jundora@argonaut ~]$ lsblk
NAME                   MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINTS
sda                      8:0    0 931.5G  0 disk
├─sda1                   8:1    0   512M  0 part
├─sda2                   8:2    0   732M  0 part
└─sda3                   8:3    0 930.3G  0 part
  └─c0                 253:1    0 930.3G  0 crypt
    ├─vgubuntu-root    253:2    0 929.3G  0 lvm
    └─vgubuntu-swap_1  253:3    0   976M  0 lvm
```

Now that you have decrypted the LUKS container, you can see that there are
two LVM logical volumes inside. The volume group is named `vgubuntu`. Let's
activate the volume group:

```sh
[jundora@argonaut ~]$ sudo vgchange -a y vgubuntu
  2 logical volume(s) in volume group "vgubuntu" now active
[jundora@argonaut ~]$ lsblk -f
NAME                   FSTYPE      FSVER    LABEL UUID                                   FSAVAIL FSUSE% MOUNTPOINTS
sda
├─sda1                 vfat        FAT32          9C22-1F60
├─sda2                 ext4        1.0            8b44b53b-cf04-4edc-9060-e339257ef8ae
└─sda3                 crypto_LUKS 2              68ffe7b0-d901-417f-899b-e643eec41d0a
  └─c0                 LVM2_member LVM2 001       Wv2AU2-G8hS-sVHP-u9xZ-g0Ep-5kYY-rZkg1c
    ├─vgubuntu-root    ext4        1.0            b7a418d6-2dfe-4119-92be-b8fa609c311c
    └─vgubuntu-swap_1  swap        1              330e4c6e-5357-40d5-b202-2cee4c874b51
```

Now we can finally mount the logical volumes.

```sh
sudo mount /dev/vgubuntu/root /mnt
```

### Close a LUKS container containing LVM volumes and LV's

Before closing the LUKS container, first unmount the mounted Logical
Volume. Then deactivate the volume group. Finally you can close the LUKS
container.

```sh
sudo umount /mnt
sudo vgchange -a n vgubuntu
sudo cryptsetup luksClose c0
```

After you have disabled/deactivated an LVM Volume Group, you should
see a message like:

```
  0 logical volume(s) in volume group "vgubuntu" now active
```

