How to Use i3 WM
====================

# Summary
- Created on: 2018.09.03
- Updated on: 2018.09.03

> My cheatsheet on using i3 WM. Config is in `~/.config/i3/config`
> which is symlinked from my `~/dotfiles` folder which is under
> git version control. I have set my i3 *mod* key to the Windows key


# Cheatsheet
- open terminal: `mod+Enter`
- open dmenu: `mod+d`
- close window: `mod+shift+q`
- move focus: `mod+<arrow keys>`
- move window to workspace: `mod+shift+<number key>`
- reload config: `mod+shift+r`
- goto workspace 'n'(1~9): `mod+n`

## Change between viewing modes
- full-screen: `mod+f`
- stacked: `mod+s`
- tabbed: ` mod+w`
- tiled: `mod+e`

## Move windows around the desktop
- `mod+shift+<arrow keys>`


## Display arrangement
- `arandr` frontend to xrandr

# Environment

> Since i3 is a text-based WM, you will manually have to launch
> userspace daemons such as `dropbox`, `SpiderOakONE`, `clipit`,
> and `ibus-daemon` in your i3 config for them to run at startup.
> But even if you add `exec` or `exec_always` stmts to your config
> file, these settings will not be automatically applied after
> `mod+shift+r` or `mod+shift+c`; these settings will only be
> applied on a restart.

> Although `ssh-agent` launches with `im-launch i3` when you
> start an i3 session from the gdm session login screen, it
> doesn't work properly with `ssh-add`, so you need to relaunch
> `ssh-agent` and add your ssh key so you can work with github
> and log into your servers. I have added this to `~/.bash_profile`;
> settings are available in my [dotfiles repo](https://github.com/gojun077/jun-dotfiles)
> on github.
