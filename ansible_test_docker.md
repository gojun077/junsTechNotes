Testing Ansible with Docker
=============================

# Summary

- Created on: Feb 1 2018
- Created by: gojun077@gmail.com
- Last Updated: Jun 28 2024

This is a quick guide for testing Ansible against *hosts* represented by
docker containers instead of VM's. Containers do not have init systems
enabled so there is generally no `systemd` and daemons are not enabled.
Also by default `sshd` is not enabled within most container images.

If you are using docker you don't need to install `sshd` inside containers
because you can get a shell in the container with either of the following:

- `docker attach <container name or ID>`
  + can only use one shell session
- `docker exec -it <name/ID> /bin/bash`
  + spawns a new shell session


For the purposes of creating an Ansible lab environment, however, I will
create container images that have `sshd` enabled so Ansible can connect
over ssh to the containers to run tasks.

# Topics

## CentOS container setup

- CentOS container from dockerhub
    + https://hub.docker.com/_/centos/
- `docker run -it centos:7.4.1708 /bin/bash`
    + place (version) tags after colon `:`
- install iproute2 (iproute) package
    + `yum install iproute`
- install sshd
    + `yum install openssh-server`
- create host keys for sshd
    + `ssh-keygen -f /etc/ssh/ssh_host_ecdsa_key -N '' -t ecdsa`
    + `ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa`
    + `ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa`
    + `ssh-keygen -f /etc/ssh/ssh_host_ed25519_key -N '' -t ed25519`
- start sshd
    + `/usr/sbin/sshd &`
- verify sshd is running
    + `ps -ef | grep ssh`
        + `root       125     1  0 14:50 ?        00:00:00 /usr/sbin/sshd`
- create passwd for root user
    + `passwd root`

## Create a new image from your customized container

- View all containers
    + `sudo docker ps -a`

- Commit changes you made to running container

```sh
sudo docker commit -m \
"Added openssh-server, iproute, and generated host keys; created PW for root user" -a "gojun077" 1809d206ad7d  docker.io/gojun077/my_cent:7.4.1708
```

- Arguments to `docker commit` are as follows
    + `-m "commit message"`
    + `-a "author name"`
    + `[existing container name/id]`
    + `[name of new image:tag]`
        + to upload to dockerhub, use `docker.io`
        + followed by `/acctname`
        + followed by `/imageName:tagname`

- check that your new image was created
    + `sudo docker image list`

- delete image (if you made a mistake)
    + `sudo docker image rm <image id>`

- log into docker hub so you can upload your new image
    + `sudo docker login`
- `docker push` to docker hub
    + `docker push docker.io/gojun077/my_cent:7.4.1708`
    + *docker.io* signifies `hub.docker.com`
    + *my_cent* will become the remote repo name
    + *7.4.1708* is the tag name and must be preceded with `:`

## Create a customized image from a dockerfile

- start with centos official image dockerfile

## Build a docker image from a Dockerfile

docker build -t my-container:mytag .

## Generating a dockerfile from a container image

## Setup Ansible Environment


## Launch Test Containers

- launch container shell session, then leave session running
    + `docker run -it 425ac58977f1 /bin/bash`
    + `C-p C-p` to exit container while leaving shell running

Note that if you forget to add a command (i.e. `/bin/bash` in the above
example), the container will exit immediately after launching



## Docker Housekeeping Commands

- Show all docker containers, running or stopped
    + `sudo docker ps -a`
- Stop a docker container
    + `sudo docker stop <cntnr ID/name>`
- Remove (delete) a docker container
    + `sudo docker rm <cntnr ID/name>`
- Remove all stopped containers
    + `sudo docker container prune`
- Show docker resource usage
    + `sudo docker system df`


## References
- https://www.mirantis.com/blog/how-do-i-create-a-new-docker-image-for-my-application/
- https://codefresh.io/docker-tutorial/create-docker-containers-command-line-interface/
