#+TITLE: Notes on using Slackdump Slack exporter
#+SUBTITLE: a Slack export util written in Go
#+AUTHOR: Peter Jun Koh
#+EMAIL: peter.koh@ujet.cx
#+DESCRIPTION: useful for exporting threads, channels, workspaces
#+KEYWORDS: productivity, slack, json, export
#+LANGUAGE:  en

* Summary

- Created on: <2024-06-17 Mon>
- Last Updated: <2024-08-25 Sun 00:15>

* Topics

** Setup

- https://github.com/rusq/slackdump
- https://github.com/rusq/slackdump/blob/master/doc/README.rst

Just download a pre-built binary tarball for your OS and
Architecture. ARM64 (aarch64) builds are available for both Linux and
MacOS. After extracting the executable, ~cp~ or ~ln -s~ to a suitable
directory in your ~PATH~ like ~/usr/local/bin~.

The first time you run ~slackdump~, you will have to authenticate with
Slack to get a session key so that ~slackdump~ will be able to work with
the Slack API.

On 64-bit systems, you can use automatic login which should /just work/
and will extract Slack cookies and auth token from an existing Slack
browser session.

When you run ~slackdump~, you will prompted for the slack workspace name in
the form ~https://<workspace>slack.com/~. It uses ~playwright~ browser
automation to authenticate to Slack through a Chrome instance. This even
works with SSO (Okta, etc). I had no problems using this on MacOS, but on
Linux with Wayland Desktop Environment, ~chrome~ runs in XWayland mode
by default and kept crashing.

I wonder if automatic auth would work if I specify the option ~-b firefox~?

I had to manually extract my Slack session token and cookie following the
instructions from

https://github.com/rusq/slackdump/blob/master/doc/login-manual.rst

using the Firefox browser. You then have to create an ~.env~ file in the
same location as the ~slackdump~ binary. The file should contain the
following two env vars:

- ~SLACK_TOKEN=~
- ~COOKIE=~

Note that the values do not need to be quoted.

** Running slackdump

You can run ~slackdump~ without any arguments and you can select from
menu items using the arrow keys. If you would like to use ~slackdump~
in scripts or automation pipelines, however, you can invoke it in
non-interactive mode using the appropriate option flags. See the examples
in the sections below.

*** List all users in workspace

If you don't specify the ~-o <filename>~ option, the list of users will be
printed to ~stdout~. The ~-r~ (format) option allows you to specify output
in ~json~ or ~text~.

#+begin_src sh
slackdump -u -o slack_users.txt -r text
#+end_src

Option flag ~-list-users~ does the same as ~-u~

~slackdump~ will cache the results of ~slackdump -u~ so that when you dump
conversation threads, usernames will be mapped to Slack user ID's.

*** List all channels in workspace

~slackdump~ docs on [[https://github.com/rusq/slackdump/blob/master/doc/usage-list.rst#viewing-conversations][getting a channel list]]

#+begin_src sh
slackdump --list-channels
#+end_src

You can also do ~slackdump -c~.

*** Dumping a conversation thread

#+begin_src sh
slackdump -r text -o my_thread.txt https://myworkspace.slack.com/archives/<UUID>
#+end_src

Note that the URL must be the final argument; all option flags must come
before this argument. If you don't specify an output format with ~-r~, the
Slack convo thread will be dumped to ~json~ by default with a filename
corresponding to the convo link URL UUID. Even if you specify the format
as ~text~, however, both ~.txt~ /AND/ ~.json~ will be generated.

*Note*: on Linux, ~-o filename~ does not work for me; ~slackdump~
automatically creates two files, one named ~*.json~, the other ~*.txt~
where ~*~ is replaced by the thread UUID.

*** slackdump text format conventions


**** types of slackID's

Speakers will have their SlackID as well as Name listed, but references
to other users starting with ~@~ other channels starting with ~#~ will
only show SlackID. SlackID for users starts with ~U~, SlackID for
subteams starts with ~S~, and SlackID for channels starts with ~C~.

- slackID prefixes
  - ChannelID ~#C~
  - DM ChannelID ~#D~
  - SubteamID ~^S~
  - UserID ~@U~


**** speaker declarations

The speaker declaration has the following format:

#+begin_src text
> Hong Gildong [U123ABCRQ99P] @ DD/MM/YYYY HH:MM:SS Z:
#+end_src

It starts with a ~>~, followed by the user's first and last name,
and then within ~[]~ square brackets, UserID, followed by ~@~ and
then a UTC timestamp.

**** user reference

#+begin_src text
<@U...>
#+end_src

~<~ UserID ~>~

**** subteam reference

#+begin_src text
<!subteam^S...>
#+end_src

~<~ !SubteamID ~>~

**** channel reference

#+begin_src text
<#C...|>
#+end_src

~<~ ChannelID ~>~

