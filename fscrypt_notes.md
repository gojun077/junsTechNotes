fscrypt notes
==================

# Summary

- Created on: Aug 1 2022
- Created by: gojun077@gmail.com
- Last Updated: Jan 18 2024

From https://github.com/google/fscrypt :

> `fscrypt` is a high-level tool for the management of [Linux native
> filesystem
> encryption](https://www.kernel.org/doc/html/latest/filesystems/fscrypt.html). `fscrypt`
> manages metadata, key generation, key wrapping, PAM integration, and
> provides a uniform interface for creating and modifying encrypted
> directories. For a small low-level tool that directly sets policies, see
> [`fscryptctl`](https://github.com/google/fscryptctl).

> To use `fscrypt`, you must have a filesystem that supports the Linux
> native filesystem encryption API (which is also sometimes called
> "fscrypt"; this documentation calls it "Linux native filesystem
> encryption" to avoid confusion). Only certain filesystems, such as
> [ext4](https://en.wikipedia.org/wiki/Ext4) and
> [f2fs](https://en.wikipedia.org/wiki/F2FS), support this API. For a full
> list of supported filesystems and how to enable encryption support on
> each one, see [Runtime
> dependencies](https://github.com/google/fscrypt#runtime-dependencies).

When Ted T'so first added `ext4` filesystem encryption in the 4.1 Linux
kernel circa 2015, the only way to access ext4 encryption was by using the
tool `e4crypt`. The tool was rudimentary and supported directory encryption
and decryption on `ext4`, but once a directory was decrypted, it was not
easy to re-encrypt it during the same user session.

`fscrypt` is written in Go and was open-sourced by Google. It is much more
user-friendly than `e4crypt` and can support more filesystems that use
Linux native fs encryption offered by the kernel. This guide includes
common commands for using `fscrypt`.

**Note**: according to
https://www.kernel.org/doc/html/v4.18/filesystems/fscrypt.html `ext4`,
`f2fs`, and `ubifs` support filesystem-level encryption.


# Topics

## Install fscrypt

### Fedora

**Note**: As of Fedora 39, `fscrypt` is available from the default
repos!

You will want to install the following:

- `fscrypt`
- `pam_fscrypt`

`pam_fscrypt` package installs a config file `/usr/share/pam.d/fscrypt`

The 2nd package will unlock your fscrypt directories upon user login with
PAM.

#### Building from source

First clone `git@github.com:google/fscrypt.git` and run `make` in the repo
directory. Note that although `fscrypt` has support for `libpam`, this
still isn't working yet on Fedora (as of 38). To build `fscrypt`, however,
you must have headers for `libpam` installed on your system.

```sh
cd fscrypt
make
sudo make install  # installs 'fscrypt' to /usr/local/bin
```

#### Edit files in /etc/pam.d/

First edit the file `/etc/pam.d/login` and append the following line
to the *auth* section:

```
auth       optional     pam_fscrypt.so
```

In the *session* section, insert the following 2 lines before the
line `session include system-auth`:

```
session    [success=1 default=ignore] pam_succeed_if.so service = systemd-user quiet
session    optional     pam_fscrypt.so
```

Finally, append the following line to `/etc/pam.d/passwd`:

```
password   optional     pam_fscrypt.so
```

I'm not sure if this is necessary, but I also symlinked
`/usr/share/pam.d/fscrypt` to `/etc/pam.d`

**References**:

https://wiki.archlinux.org/title/Fscrypt

Note: some of the file names in the above link will be different, as
the guide is for Archlinux.

#### Edit /etc/ssh/sshd_config

`fscrypt` auto-unlock of encrypted directories when logging in via `ssh`
does not work with `ChallengeResponseAuthentication` or
`KbdInteractiveAuthentication`. You must use `PasswordAuthentication yes`
and `usePAM yes`for `fscrypt` PAM support.

Make sure you have set either `ChallengeResponseAuthentication no` or
`KbdInteractiveAuthentication no` or commented out.

### Debian-based distros

As of Jul 2023, `fscrypt` is available from the default Debian 12 Bookworm
repositories with the following packages:

- `fscrypt`
- `libpam-fscrypt`

It is also available from the default repos for Ubuntu 22.04 and above.

## Configuring fscrypt

`fscrypt` always looks for config file `/etc/fscrypt.conf` and the
directory `/.fscrypt` which contains the sub-dirs `policies` and
`protectors`. If these don't yet exist, run:

```sh
sudo fscrypt setup  # must always be run as root
```

## Create an fscrypt-encrypted directory

Note that you can only encrypt an empty directory; once the directory has
been encrypted, you can `unlock` it and start placing files inside the dir.

```sh
$ fscrypt encrypt /path/to/my/secureDir
```

You will be prompted to set a passphrase for the directory that can be
used to `fscrypt lock` and `fscrypt unlock` the dir.

## Get detailed info on filesystems

This is helpful for finding if Linux native filesystem encryption is
available for your FS and whether or not it has been enabled.

```
$ fscrypt status
filesystems supporting encryption: 1
filesystems with fscrypt metadata: 2

MOUNTPOINT  DEVICE          FILESYSTEM  ENCRYPTION     FSCRYPT
/           /dev/dm-0       xfs         not supported  Yes
/boot       /dev/nvme0n1p2  xfs         not supported  No
/boot/efi   /dev/nvme0n1p1  vfat        not supported  No
/tier2      /dev/nvme1n1p1  f2fs        supported      Yes
/tier3      /dev/sda1       f2fs        not enabled    No
```

## Enable encryption on compatible filesystem

If you have already formatted your filesystem using `mkfs`, you will have
to unmount the fs and run one of the following commands:

```sh
# if your FS is ext4:
sudo tune2fs -O encrypt /dev/deviceName

# if your FS is f2fs:
sudo fsck.f2fs -O encrypt /dev/deviceName

# remount your partition
sudo mount /dev/deviceName /mountpoint
```

However, if you only have a single storage device and it is too much of a
hassle to power off your machine, reboot into rescue mode or a live usb
environment just to run `tune2fs` or `fsck.f2fs` on the root partition,
another option is to create an empty file of a certain size using either
`fallocate` (recommended) or `dd`, formatting the file with a
`fscrypt`-supported file system (`ext4`, `f2fs`, or `ubifs`), enabling the
file system encryption option for the fs, and finally mounting the file on
a mount point.

```sh
fallocate -l 1G crypt-mnt
mkfs.ext4 crypt-mnt
```


## Unlock an fscrypt-ed directory

```sh
sudo fscrypt unlock /path/to/crypt/dir
```

## Lock an fscrypt-ed directory

```sh
sudo fscrypt lock /path/to/crypt/dir
```



## References

- https://wiki.archlinux.org/title/Fscrypt
- https://github.com/google/fscrypt/issues/295
  + `fscrypt` not working with PAM on Fedora
