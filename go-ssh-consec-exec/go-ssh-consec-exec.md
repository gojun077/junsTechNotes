Notes for go-ssh-consec-exec.go
==================================

> Example command invocation:

```sh
$ ./go-ssh-consec-exec -f=hostlist -u=foo -k=/Users/foo/.ssh/foo-dev \
  -p=mypw.json
My file name is: hostlist
myuser is: foo
keyfile is: /Users/foo/.ssh/foo-dev
key pw file is: mypw.json
Connecting to 10.89.0.2:22...
root part size: 37659287552, root part avail: 4707250176, used ratio: 0.8750
Connecting to 10.89.0.3:22...
root part size: 37659287552, root part avail: 47398912, used ratio: 0.9987
...
```
