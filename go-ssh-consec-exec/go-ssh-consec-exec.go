// go-ssh-consec-exec.go
// Created on: 2020.09.13
// Created by: gojun077@gmail.com
// Last Updated: 2020.09.15
// Contributors: gojun077@gmail.com

/*
This program reads a file containing a list of hosts and will loop
through the list of hosts attempting to connect via ssh to each host
and execute a remote command.

This program takes the following arguments:
- name of file containing a newline-delimited list of IP's/hosts
- name of login user
- path to user's ssh private keyfile
- path to json file containing ssh privkey PW
*/

package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"golang.org/x/crypto/ssh"
	"io/ioutil"
	"os"
	"time"
)

// json file must have key-keyval of form "password": "s3kr3t"
type Config struct {
	PWkey string `json:"password"`
}

func getKeySigner(privateKeyFile string, pw string) ssh.Signer {
	privateKeyData, err := ioutil.ReadFile(privateKeyFile)
	if err != nil {
		fmt.Printf("Error loading private key file! %v\n", err)
	}

	privateKey, err := ssh.ParsePrivateKeyWithPassphrase(
		privateKeyData, []byte(pw))
	if err != nil {
		fmt.Printf("Error parsing private key! %v\n", err)
	}
	return privateKey
}

func main() {
	start := time.Now()
	// argument flags BEGIN
	myfile := flag.String("f", "",
		"name of file containing hostnames/ip's, one per line")
	myuser := flag.String("u", "", "ssh login user name")
	mykey := flag.String("k", "", "path to ssh private key file")
	mypass := flag.String("p", "",
		"path to json file containing ssh privkey PW")
	myport := flag.String("P", "22", "TCP port used by sshd")
	flag.Parse()
	// argument flags END

	if len(os.Args) < 5 {
		flag.Usage()
		os.Exit(1)
	}

	myfileval := *myfile
	fmt.Printf("My file name is: %s\n", myfileval)
	myuserval := *myuser
	fmt.Printf("myuser is: %s\n", myuserval)
	mykeyval := *mykey
	fmt.Printf("keyfile is: %s\n", mykeyval)
	mypassval := *mypass
	fmt.Printf("key pw file is: %s\n", mypassval)
	myportval := *myport

	mycmd := `python -c 'import os
statvfs = os.statvfs("/")
root_partsize=statvfs.f_frsize*statvfs.f_blocks
root_partfree=statvfs.f_frsize*statvfs.f_bavail
root_partpctused=(root_partsize-root_partfree)/float(root_partsize)
print("root part size: %d, root part avail: %d, used ratio: %0.4f"
     %(root_partsize, root_partfree, root_partpctused))'`

	f, err := os.Open(*myfile)
	if err == nil {
		defer f.Close()
	}

	jsonFile, err := os.Open(mypassval)
	if err != nil {
		fmt.Printf("Error opening json file! %v\n", mypassval)
	}
	defer jsonFile.Close()

	var pwcfg Config
	jdecode := json.NewDecoder(jsonFile)
	if err != nil {
		fmt.Printf("Error decoding json! %v\n", err)
	}
	jdecode.Decode(&pwcfg)
	pw := pwcfg.PWkey

	privateKey := getKeySigner(mykeyval, pw)

	config := &ssh.ClientConfig{
		User:            myuserval,
		Auth:            []ssh.AuthMethod{ssh.PublicKeys(privateKey)},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	fscanner := bufio.NewScanner(f)
	for fscanner.Scan() {
		host := fscanner.Text()
		fmt.Printf("Connecting to %s...\n", host)
		client, err := ssh.Dial("tcp", host+":"+myportval, config)
		if err != nil {
			fmt.Printf("Error dialing server! %v", err)
		}
		// Multiple sessions per client are allowed
		session, err := client.NewSession()
		if err != nil {
			fmt.Printf("Failed to create session! %v", err)
		}
		defer session.Close()
		// print session output to local stdout
		session.Stdout = os.Stdout
		session.Run(mycmd)
		if err != nil {
			fmt.Printf("Error executing cmd! %v", err)
		}
	}

	elapsed := time.Since(start)
	fmt.Printf("Total execution time: %v\n", elapsed)
}
