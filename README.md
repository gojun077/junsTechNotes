Jun's Tech Notes
================

# Summary
- Last Updated: 2018.06.07
- personal notes, scripts, code, etc.
- content not shared in my public repos
    + https://github.com/gojun077/jun-dotfiles
    + https://bitbucket.org/gojun077/bin-scripts

