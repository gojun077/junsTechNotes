Installing Fedora 37 on RPI 4B
===================================

# Summary

- Created on: Jan 11 2023
- Created by: gojun077@gmail.com
- Last Updated: Jan 11 2023

Starting with Fedora 37, the Raspberry Pi Single Board Computer (SBC) is
a supported platform.

# Topics

## Get Fedora Installation Files

Unlike regular Linux distros that can be installed from `.iso` files,
installing Linux on ARM devices requires raw image files that can be
written directly to boot media like micro sdcard or external USB ssd
drives.

As of Jan 2023, you can download `aarch64` raw images from
https://getfedora.org/en/server/download/


## Write raw image to microSD card or external USB storage

I have used `rpi-imager` to install aarch64 raw images for RPI4B from RPi
OS, Archlinux, and Ubuntu, but Fedora has its own imaging tool
`arm-image-installer`. In
[this](https://ask.fedoraproject.org/t/fedora-arm-64-does-not-work-on-raspberry-pi-4/29111/12)
thread a Fedora community QA person also successfully used `rpi-imager` to
write the Fedora 37 raw image to a microSD card, although on the first boot
it hangs at the step *Automatic Boot Loader Update*; after a reboot,
however, the RPi4
