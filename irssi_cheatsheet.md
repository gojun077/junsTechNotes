IRSSI cheatsheet
===================

# Summary
- Last Updated: 2018.06.07

# Most-used Commands

## Authentication

### Freenode
- Identify yourself to use a pre-registered Nick
    + `/msg nickserv identify <nick> <PW>`
    + or `/msg nickserv <PW>`
> When you send /msg to nickserv, a new IRSSI tab should open

- Release a nick

> You need to do this sometimes when you are disconnected
> but Freenode still thinks you are connected

    ```
    /msg nickserv release <nick>
    ```

- Register SSL key fingerprint for your nick
    + First get your openssl key fingerprint

        ```
        openssl x509 -sha1 -fingerprint -noout -in irssi.pem \
            | sed -e 's/^.*=//;s/://g;y/ABCDEF/abcdef/'
        ```
    + In irssi connect to Freenode and register the fingerprint

```
        /connect Freenode
        /msg NickServ identify YOUR_PASSWORD
        /msg NickServ cert add YOUR_FINGERPRINT
        ```


## Connect to IRC Network and Join Channels
- connect to chat network
    + `/connect <netname>` (i.e. freenode, dalnet, etc.)
    + most IRC nets are defined in `irssi.conf`

- join channel
    + `/join #channelname`

- leave channel
    + `/part` (from channel window)

## Window navigation

- Jump to irssi windows 1-10
    + `Meta-1`, `Meta2`, `Meta-0`
- Jump to previous / next window
    + `C-shift-p`
    + `C-shift-n`
- Go to specified window
    + `/window <number>`
- Close window
    + `/window close`

# References
- https://irssi.org/documentation/startup/
