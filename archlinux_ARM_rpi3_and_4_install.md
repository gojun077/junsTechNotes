Archlinux ARM Installation Guide for RPi3B & RPi4B
=====================================================

# Summary

- Created: Jun 2019
- Last Updated: Sep 18 2021
- Created by: gojun077@gmail.com

As of June 2019, enabling serial console without also enabling some RPi
`dtoverlay` options will cause a Kernel panic on boot.  Starting with the
RPi3, the serial console UART is now used by the bluetooth subsystem, while
serial console uses a mini UART. In Archlinux ARM images for both ARMv7 and
ARMv8 this issue is present.

# Topics

## Prepare SD card for writing Archlinux ARM image

https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3

Insert your sdcard into an sdcard reader on a linux machine. When you
run `lsblk -f` you should see a new drive, perhaps `sdb`, `sdc`,
perhaps even `mmcblk0`

First run `fdisk /dev/sd<?>` on the new "drive" which is your SDcard. Note
that you must label your partitions with *both* UUID and PARTUUID; if your
`/boot` partition is missing a PARTUUID, uboot will not be able to find
your `/` partition! You can check if your device has PARTUUID with
`blkid /dev/<name>`.

```sh
# blkid /dev/sdb
/dev/sdb: PTUUID="b3a5f026" PTTYPE="dos"
```

You should see an 8-char value for `PTUUID`.


After deleting any existing partitions, you should create a W95 FAT32(LBA)
primary partition (type `0c`), but make it *1GB* in size, instead of the
*200M* recommended in the partitioning guide. The reason is because if you
later need to do a large update after several months of not updating your
Archlinux install, you will probably run out of space in `/boot` because
you will need to create all new `initrd` images and other `uboot`
bootloader files.

Next create a second primary partition taking up all the rest of the
space left on the SDcard. You can leave this partition as the default
type 83 Linux partition.

Before writing your changes to the partition table and exiting, make sure
that Partition UUID's have been set; during one installation the 8-char hex
was set to 0x00000000 which caused uboot to fail. You can check with fdisk
`x` then `i`, which will print the current PTUUID, if any. You can set a
new one by entering a string of the form `0x0123beef` where `0x` denotes
start of hex and the 8 chars must be between 0 and f (16 values).

Note that setting the disk label is non-destructive and is stored before
the partition table when using MBR.

## Create file systems on the new SDcard partitions

```sh
sudo mkfs.vfat /dev/<NameOfBootPart>
sudo mkfs.ext4 /dev/<NameOfRootPart>
```

Here's what the partitions and file systems should look like when
you're done:

```sh
$ lsblk -f
...
mmcblk0
├─mmcblk0p1        vfat        FAT32          BB88-DE8C
└─mmcblk0p2        ext4        1.0            5ae85e4a-f441-46c3-bfa5-34d8ec7ff4cd
```

## Mount SDcard partitions

Create a temp dir with mountpoints for the ARM image we will download
in the next step. Then mount the vfat partition on `boot` and the ext4
partition on `root`


```sh
mkdir -p /mnt/{boot,root}
sudo mount /dev/mmcblk0p1 /mnt/boot
sudo mount /dev/mmcblk0p2 /mnt/root
```


Now the mounted partitions should show up as follows:

```sh
$ lsblk
...
mmcblk0            179:0    0  14.8G  0 disk
├─mmcblk0p1        179:1    0     1G  0 part  /mnt/boot
└─mmcblk0p2        179:2    0  13.8G  0 part  /mnt/root
```


## Download tarball for Archlinux ARMv8 (Aarch64), write to SDCard

Change to root user and execute the following:

```sh
cd /mnt
wget http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-aarch64-latest.tar.gz
bsdtar -xpf ArchLinuxARM-rpi-aarch64-latest.tar.gz -C root
sync  # immediately write all cached data to disk
```

Once the tarball has been decompressed into `/mnt/root`, we now need to
move the bootloader files in `/mnt/root/boot` into our mountpoint
for the boot partition, `/mnt/boot`:

```sh
cd /mnt  # just to make sure we're in the right dir
mv root/boot/* boot
```

## Make changes to boot files to enable Serial Console on RPi3

### For ARMv7 image (32-bit)

To enable the serial console, you need to edit two files in
the `/boot` partition; `/boot/cmdline.txt` and `/boot/config.txt`

...

### For ARMv8 image (Aarch 64-bit)

To enable the serial console on RPi3, you only need to edit a single file,
`/boot/config.txt`. By default, `/boot/config.txt` contains the line
`enable_uart=1` but you need to add one line above it as follows:


```sh
dtoverlay=pi3-disable-bt
enable_uart=1
```

For RPi4, you don't need the line `dtoverlay=pi3-disable-bt`; as long
as the uart is enabled, serial console connection will just work.

The `dtoverlay=pi3-disable-bt` setting disables Bluetooth so you can use
the UART for serial console on RPi3 only. Without this setting, you will
encounter a kernel panic on boot.

Note that in the ARMv8 image, there is no `/boot/cmdline.txt` like there is
on the 32-bit image.

64-bit Archlinux ARM uses `uboot`, so if you make any changes to reflected
in `/boot/boot.txt`, you must run `./mkscr` from a Archlinux machine that
has the pkg `uboot-tools` installed to update the uboot images with changes
from `boot.txt`


If you look at `root/etc/fstab` you will notice that the only entry
listed is for the boot device which will be mounted on `/boot`; but
where is the entry for `/`?

```
# Static information about the filesystems.
# See fstab(5) for details.

# <file system> <dir> <type> <options> <dump> <pass>
/dev/mmcblk0p1  /boot   vfat    defaults        0       0
```

You don't need to specify the `/` partition in `fstab`, as uboot is able to
find it from `boot.txt` above. Optionally you can also set the `pass`
setting for `fsck` to `2`(check after partitions marked as `1`) from `0`
(disable).

**Note about `fstab` for RPi4B < rev 1.4**: for some reason the SDCard
device on older RPi4 hardware uses `mmcblk1` instead of `mmcblk0` the
online guide has a `sed` snippet for replacing *0* with *1* in the sdcard
block device name; this is NOT needed for the RPi3 and also not needed for
RPi4 Model B Rev 1.4. You can check your hardware revision by printing the
value of `/sys/firmware/devicetree/base/model`

- `Raspberry Pi 4 Model B` (older revision requiring `mmcblkN` edit)
- `Raspberry Pi 4 Model B Rev 1.4`

**Note about RPi4 Model B Rev 1.4 eMMC error**: As recently as Sep 18 2021
the latest Archlinux ARMv8 image fails to boot, and emits the following
errors during uBoot booting process:

```
mmc1: error -22 whilst initialising SD card
mmc1: invalid bus width
```

Refer to the following forum thread:

https://archlinuxarm.org/forum/viewtopic.php?f=67&t=15422&sid=99a82d45380b9be9026bd2939593256c

The official Raspberry Pi OS (Debian 32-bit) image and Ubuntu 21.04 ARM64-bit
Server image boot without any problems, however, so this is definitely an
ArchlinuxARM issue.


## Unmount SDcard partitions, insert card in RPi3/4 and boot into Arch

```sh
sudo umount boot root
```

Insert the SDCard into your RPi and boot into Archlinux ARM.


### Connecting to your RPi 3B via Serial Console using GNU Screen

Connect your USB serial console dongle to your machine and 3 wires
from the dongle should be connected to its GND Ground, TXD Transmit,
and RXD Receive jumpers, while the other side should be connected
to the GPIO pins corresponding to GND, RXD, and TXD (note that the
wire connected to TXD on the dongle must connect to RXD on the RPi

`dmesg -wH`

```
[ 8월28 22:58] usb 3-1: new full-speed USB device number 2 using xhci_hcd
[  +0.179481] usb 3-1: New USB device found, idVendor=10c4, idProduct=ea60, bcdDevice= 1.00
[  +0.000004] usb 3-1: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[  +0.000003] usb 3-1: Product: CP2102 USB to UART Bridge Controller
[  +0.000002] usb 3-1: Manufacturer: Silicon Labs
[  +0.000002] usb 3-1: SerialNumber: 0001
[  +0.053714] usbcore: registered new interface driver usbserial_generic
[  +0.000025] usbserial: USB Serial support registered for generic
[  +0.004808] usbcore: registered new interface driver cp210x
[  +0.000609] usbserial: USB Serial support registered for cp210x
[  +0.000773] cp210x 3-1:1.0: cp210x converter detected
[  +0.002408] usb 3-1: cp210x converter now attached to ttyUSB0
```

Now launch GNU Screen:

```sh
sudo screen /dev/ttyUSB0 115200
```

To use serial console without invoking root, you must add your user to
group `dialout` on Ubuntu.


Login with `alarm:alarm` for user/pass and then `su -` using the password
`root`. Note that `root` ssh is blocked, so you will have to ssh in as
`alarm` initially, change to root with `su -` and default password
`root`.

When you initially connect via serial console, you might not see the
`Login:` prompt, so just type `alarm` if this happens. Then you should see
the `Password:` prompt.

After setting up the network and getting Internet, the first
thing you must do is generate gpg keys for pacman:

```sh
pacman-key --init
pacman-key --populate archlinuxarm
```

Once that's done, you should update your system with `pacman -Syyu`


## Add ip to wired iface eth0 (temporary)

Communication via serial console can be flaky, so it's best to just
manually add an IP for right now and then connect via `ssh`. If there
is a gateway available on the subnet for the ip address which you
manually added, you can add the GW as the default route.

```sh
ip a add 192.168.0.17/24 dev eth0
ip r add default via 192.168.0.1
```


## Change hostname

```sh
$ hostnamectl set-hostname myhost123
$ hostnamectl status
 Static hostname: myhost123
       Icon name: computer
      Machine ID: 5328653ee27d4ceca7dec620328b61c7
         Boot ID: aa82bef4be9046af9c808777d092a455
Operating System: Arch Linux ARM
          Kernel: Linux 5.11.4-1-ARCH
    Architecture: arm64
```

Add matching entries to `/etc/hosts`

```
127.0.0.1     localhost
::1            localhost
127.0.1.1      myhost123
```


## Update system clock

`timedatectl set-ntp true`


## Timezone and Localization Settings

```sh
ln -sf /usr/share/zoneinfo/Asia/Seoul /etc/localtime
#hwclock --systohc  # this doesn't work for RPi3/4
```


## Install packages

```sh
pacman-key --init  # if you didn't do earlier
pacman-key --populate archlinuxarm  # if you didn't do earlier
pacman -Syyu raspberrypi-firmware iwd sudo ethtool dhclient git \
  vim screen dmidecode which bind ipython lm_sensors i2c-tools \
  rrdtool archey3 wget
```

And for RPi4b only, also install package `rpi-eeprom` for upgrading
the firmware and bootloader.


## Check RPi4B firmware and bootloader update status

First we need to tell `rpi-eeprom-update` to use the *stable* instead of
*critical* channel:

```sh
vim /etc/default/rpi-eeprom-update
# make sure the next line uses 'stable' instead of 'critical'
FIRMWARE_RELEASE_STATUS="stable"
```

Now run `rpi-eeprom-update` without any arguments to see the current
state of the bootloader and firmware:

```sh
# rpi-eeprom-update
*** UPDATE AVAILABLE ***
BOOTLOADER: update available
   CURRENT: Thu Sep  3 12:11:43 UTC 2020 (1599135103)
    LATEST: Tue Jul  6 10:44:53 UTC 2021 (1625568293)
   RELEASE: stable (/lib/firmware/raspberrypi/bootloader/stable)

  VL805_FW: Using bootloader EEPROM
     VL805: up to date
   CURRENT: 000138a1
    LATEST: 000138a1
```

To automatically apply bootloader/FW update at the next boot, add the
`-a` flag to the command:

```sh
# rpi-eeprom-update -a
*** INSTALLING EEPROM UPDATES ***

BOOTLOADER: update available
   CURRENT: Thu Sep  3 12:11:43 UTC 2020 (1599135103)
    LATEST: Tue Jul  6 10:44:53 UTC 2021 (1625568293)
   RELEASE: stable (/lib/firmware/raspberrypi/bootloader/stable)

  VL805_FW: Using bootloader EEPROM
     VL805: up to date
   CURRENT: 000138a1
    LATEST: 000138a1
   CURRENT: Thu Sep  3 12:11:43 UTC 2020 (1599135103)
    UPDATE: Tue Jul  6 10:44:53 UTC 2021 (1625568293)
    BOOTFS: /boot

EEPROM updates pending. Please reboot to apply the update.
To cancel a pending update run "sudo rpi-eeprom-update -r".
```

Bootloader and firmware updates add features like booting from USB3
ports, PXE network boot, lowered thermals, etc.

## Change passwords for `alarm` and `root`, create non-root user

```sh
passwd
passwd alarm
useradd -m archjun
passwd archjun
usermod -a -G wheel archjun
```

Run `visudo` and uncomment the line containing `wheel` in `/etc/sudoers`;


## Permanent Network Setup

In `/etc/systemd/network` create the following files for wired and
wireless interfaces and delete the rest.

- `eth.network`
- `20-wlan0.network`

Sample `eth.network` config

```
[Network]
Description=Settings for RPi3B ethernet iface
Address=192.168.0.18/24
Gateway=192.168.0.78

[Match]
Name=eth*
```

Sample `20-wlan0.network` config

```
[Network]
Description=Settings for RPi3B wireless iface
DHCP=ipv4
ActivationPolicy=always-down

[Match]
Name=wlan0

[DHCPv4]
RouteMetric=20
```

In the example above, the wlan iface will not be set to *up* on boot


## Setup temperature sensors

Install the following:

```sh
pacman -S lm_sensors i2c-tools rrdtool
```

and then as root execute `sensors-detect` and answer `yes` to the
prompts of whether to scan various interfaces.

After running this tool, `lm_sensors.service` will be enabled and
started.

To enable logging of sensor data (temperature, mostly) to `journalctl`,
you need to enable `sensord.service` which is included as part of the
`lm_sensors` package, although it is not enabled by default:

```sh
sudo systemctl enable sensord
sudo systemctl start sensord
```

Note that the `rrdtool` package must be installed (Round-Robin
Database) in order for `sensord` to start.

Now you can see temperature, fan speed and other sensor data with
`journalctl -usensord`


## Enable TRIM for Solid-State Disks (SSD), eMMC, or SDcard

https://wiki.archlinux.org/title/Solid_state_drive

Check if your hardware supports TRIM / wear-leveling

`lsblk --discard` and check that *non-zero* values exist for `DISC-GRAN`
or `DISC-MAX`.

The `util-linux` package contains `fstrim.timer` which runs
`fstrim.service` every week.

`ExecStart` in `fstrim.service` runs the following command:

```sh
/usr/bin/fstrim --listed-in /etc/fstab:/proc/self/mountinfo \
  --verbose --quiet-unsupported
```

Note that ArchlinuxARM uses `uboot` bootloader, so `/etc/fstab` only
contains an entry for `/boot`, while `/` does not appear at all in `fstab`
on machines using `uboot`; `uboot` is able to tell the system where the `/`
partition resides without relying on `fstab`.

Amazingly enough, SDcards, tiny as they are, have on-board controllers
and use wear-leveling:

- https://www.bunniestudios.com/blog/?p=898
  + tiny 32-bit ARM controller embedded in SDcard
- https://electronics.stackexchange.com/questions/27619/is-it-true-that-a-sd-mmc-card-does-wear-levelling-with-its-own-controller

So it should be ok to enable `fstrim` on SDcards from more reputable
vendors (avoid cheap Chinese knockoff cards).

To enable and run `fstrim.timer`,

```sh
systemctl enable fstrim.timer
systemctl start fstrim.timer
```

It will then discard unused blocks every week.


## Enable temperature monitoring with sensord

Earlier we installed the packages `lm_sensors`, `rrdtool`, and `i2c-tools`
which are needed for enabling temperature monitoring.


```sh
sudo systemctl enable sensord
sudo systemctl start sensord
```

## Enable Hardware Random Number Generator

```sh
pacman -S rng-tools
```

Edit `/etc/conf.d/rngd` and add the following:
`RNGD_OPTS="-o /dev/random -r /dev/hwrng"`


```sh
systemctl stop haveged
systemctl disable haveged
systemctl enable rngd
systemctl start rngd
```

## How to Install pkgs from Arch User Repository (AUR)

https://wiki.archlinux.org/title/Arch_User_Repository

```sh
pacman -S --needed base-devel
```

Then from `aur.archlinux.org/packages` search for a package by name. and
click on the link. From the package page, at the far-right under *Package
Actions* copy the link for `Download snapshot` and use `wget` or curl to
download `<pkgName>.tar.gz` to your machine. `tar xvf` the tarball, cd
into the newly-created directory and you should see `PKGBUILD` and
`.SRCINFO` files. Open up `PKGBUILD` to make sure there are no malicious
commands inside. If it looks fine, run the command

```sh
# do not run as root
makepkg -is  # -i install, -s syncdeps
```

If you are using a machine with multiple cores, edit `/etc/makepkg.conf`
and uncomment the line `#MAKEFLAGS="-j2"`. This setting specifies the
number of processes to use when running `make`. For a modern multicore
machine, `-j4` should speed up your build by 2x or 3x. Adjust this setting
as appropriate.

This will build a binary package from the recipe in the `PKGBUILD` that can
be installed by `pacman`. The binary package will be in `tar.zst` format,
compressed with Facebook's Zstandard compression algo. It features near-realtime
extraction or archives and provides good compression similar to `.7z`

Now install the binary package with `pacman -U <mybinary>.tar.zst`


## References

- https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3
- https://archlinuxarm.org/wiki/Raspberry_Pi
