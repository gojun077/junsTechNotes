Shared Scratchpad
======================

> Scratchpad that can be used across different machines. Contains
> url's, code snippets, notes and other info but no passwords.

# Scratches

## Dec 2019

https://docs.microsoft.com/en-us/windows/wsl/wsl2-install

> WSL2 installation requires Win10 build `18917`, but the version
> installed on the company's win10 workstations is `18362.576`

-----------------------------------------------------------------------

> UTF8 cjk characters not properly rendering in WSL (Windows Terminal)
> is a font issue; in the past Consolas font did not support UTF8
> chars, but as of 2019.12.23 if you set WSL default font to Consolas,
> cjk renders properly! Monofur is my favorite terminal font, but on
> windows when I installed powerline for monofur it cannot render
> cjk...

-----------------------------------------------------------------------

## Jan 2020

> shortcut command for VcXsvr on Win10

```
"C:\ProgramFiles\VcXsrv\vcxsrv.exe" :0 -nodecoration -screen 0 @1 -clipboard -wgl
```

-----------------------------------------------------------------------

https://jblevins.org/projects/markdown-mode/ Markdown Mode for Emacs

-----------------------------------------------------------------------

books about Graphite and Grafana

- The Art of Monitoring
- Monitoring with Graphite
- Practical Monitoring (tool agnostic)
- Effective Monitoring & Alerting (tool agnostic)

-----------------------------------------------------------------------

https://blog.avinetworks.com/python-best-practices

> Avi Networks demo's `class PyExecCmd` which is a wrapper around
> the `subprocess` module to reduce the risk of shell exploits.

-----------------------------------------------------------------------

https://noahgift.com/publication/datacamp_automation/

> Noah Gift's course on DataCamp - Command Line Automation in Python


https://s3.amazonaws.com/assets.datacamp.com/production/course_16871/slides/chapter2.pdf

> Datacamp lecture by Noah Gift illustrating shell command execution
> in Python with `subprocess`. Also shows how module `shlex` can
> be used to sanitize shell input to avoid malicious shell commands.

-----------------------------------------------------------------------

https://www.amazon.com/Python-DevOps-Ruthlessly-Effective-Automation/dp/149205769X

> *Python for DevOps* book by Noah Gift.

"python:ref: date arithmetic with datetime" in my Gmail (reference)

-----------------------------------------------------------------------

https://wiki.bitlbee.org/HowtoTwitter/StreamDeprecation

> Note about how Twitter no longer supports streams (bitlbee)

-----------------------------------------------------------------------

https://www.learningfromincidents.io/blog/characteristics-of-next-level-incident-reports-in-software

> Recommended by John Allspaw

-----------------------------------------------------------------------

https://stackoverflow.com/questions/51221730/markdown-link-to-header

> How to link to headers in your document with Markdown. Note that
> when headers are converted into link references, all spaces are
> converted to hyphens; for example the header `# my header` must be
> referred to in a link as `[some text](#my-header)`

-----------------------------------------------------------------------

https://www.itg.ias.edu/content/keyboard-shortcuts-capture-screen-shot-mac-os-x

> Keyboard shortcuts for taking screenshots in MacOS X

- `Cmd-Shift-3` full screen screenshot
- `Cmd-Shift-4` select area screenshot
- `Cmd-Shift-4-<SPC>` select active window screenshot
- `Cmd-Shift-5` open screenshot app

-----------------------------------------------------------------------

Emacs TRAMP mode

https://www.gnu.org/software/emacs/manual/html_node/emacs/Remote-Files.html

> `C-x C-f` -> `/ssh:macbook:.emacs` Tramp mode is smart enough to
> read your config in `~/.ssh` and use ssh host aliases found there

-----------------------------------------------------------------------

> note that `PST` (starting in early Nov) is `UTC - 8`, `PDT`
> (starting in mid-March) is `UTC - 7`

-----------------------------------------------------------------------

http://m.health.chosun.com/svc/news_view.html?contid=2020011303356

> 무청 & 깻잎 are high in calcium

-----------------------------------------------------------------------

https://www.reddit.com/dev/api/ Reddit API docs

-----------------------------------------------------------------------

Tailscale is a mesh network that uses Wireguard on the backend

https://tailscale.com/

-----------------------------------------------------------------------

https://nullprogram.com/blog/2020/01/21/

> *Go's Tooling is an Undervalued Technology* blog post from
> 2020.01.21

RSS feed: https://nullprogram.com/feed/

-----------------------------------------------------------------------

> Currently I've set `compile-command` in my `.emacs` file to

```
env GOOS=linux GOARCH=amd64 go build -v && go test -v && go vet
```

> On WSL I successfully built a binary `exe` that runs on Windows with
> the following emacs compile options:

```
env GOOS=windows GOARCH=amd64 go build -v && go test -v && go vet
```

-----------------------------------------------------------------------

https://github.com/PacktWorkshops/The-Go-Workshop

-----------------------------------------------------------------------

## Feb 2020

> important info about laser printer toner cartridges: cartridges
> don't die just b/c they run out of toner; when the waste toner
> compartment in a cartridge fills up, it cannot be used anymore, no
> matter how much usable toner it still contains!

https://www.lighting-gallery.net/index.php?topic=5556.0

Laser printers without smart chips in the toner cartridges?

> My experience with BW laser printers: - You may do about ~1..2
> refills for the same (non-reffillable) toner cartridge. After that
> the cartridge stops working properly (the quality drops, it may
> indicate "empty toner" or so)

> The problem is not any chip or so (I haven't seen any at all so far
> in laser printers), but the problem is, the toner cartridge contains
> a kind of pocket for "waste toner" (it collects the residuals of the
> toner that does not jump from the selenium drum to the paper, filth
> from the paper,...). And this pocket becomes full and starts to
> spill over. This is then detected by the circuit (it causes leakages
> on the HV drum excitation and that is sensed by the circuitry) and
> often reported as "Empty toner" warning. And of course, this
> situation degrades the print quality really considerably.

-----------------------------------------------------------------------

https://www.percona.com/blog/2017/10/12/open_files_limit-mystery/

Percona Database Performance BlogPercona Database Performance Blog

> A Mystery with MySQL open_files_limit - Percona Database Performance
> In this blog, we'll look at a mystery around setting the MySQL
> `open_files_limit` variable in MySQL and Percona Server for MySQL.

-----------------------------------------------------------------------

https://www.theverge.com/2020/2/3/21120248/microsoft-teams-down-outage-certificate-issue-status

Feb 4th

> Microsoft Teams goes down after Microsoft forgot to renew a
> certificate An embarrassing mistake for Microsoft’s Slack competitor

-----------------------------------------------------------------------

https://www.vice.com/en_us/article/3a8av8/researchers-found-a-list-of-electronic-arts-ea-slack-channels

> The exposed channel names and other information could have related
> to secretive company projects.

-----------------------------------------------------------------------

http://shopping.interpark.com/product/productInfo.do?prdNo=6382519584&gclid=EAIaIQobChMI6Y60-4m85wIVyWkqCh1YfwNEEAQYASABEgIQiPD_BwE

> Boyue Likebook Muses e-reader with stylus 243,660원 with 7% discount
> coupon as of 2/6

-----------------------------------------------------------------------

https://access.redhat.com/solutions/4136481

> GNU Screen deprecated and no longer available from RHEL 8 default
> repos but will still be offered from EPEL

-----------------------------------------------------------------------

https://gpgtools.tenderapp.com/discussions/problems/101909-gpg-keychain-password-prompt

Prevent your gpg password from being saved on the MacOS keychain

-----------------------------------------------------------------------

https://www.dansilvestre.com/chrome-keyboard-shortcuts/

- `Cmd-t` new tab
- `Cmd-w` close tab
- `Cmd-n` new browser window
- `Cmd-shift-n` new incognito browser window
- `Cmd-shift-w` close browser
- `Cmd-option-leftArrow` previous tab
- `Cmd-option-rightArrow` next tab
- `Cmd-9` jump to last tab
- `Cmd-1..8` jump to tab number (1-8 only)

-----------------------------------------------------------------------

https://superuser.com/questions/360418/pgup-and-pgdn-on-a-macbook

- `Fn-upArrow` PgUp
- `Fn-downArrow` PgDn
- `Cmd-upArrow` / `Fn-leftArrow` Home
- `Cmd-downArrow` /`Fn-rightArrow` End

> Note that in emacs on MacOS `Meta-shift-<` `Meta-shift->` do not
> work for jumping to the beginning or end of a file; instead you must
> use the MacOS keys for Home and End which are `Fn-leftArrow` and
> `Fn-rightArrow`, respectively

-----------------------------------------------------------------------

http://osxdaily.com/2011/09/06/switch-between-desktops-spaces-faster-in-os-x-with-control-keys/

- `C-leftArrow` Change to previous workspace
- `C-rightArrow` Change to next workspace
- `C-<num>` Change to workspace num
  + must enable in Keyboard -> shortcuts -> Mission Control menu
- `C-upArrow` Workspace menu (mission control)

-----------------------------------------------------------------------

http://pragmaticemacs.com/emacs/sorting-an-org-mode-table/

Sorting an org-mode table in emacs

> `C-c ^ {a,n}` where `a` is for alphabetical, `n` for numerical sort

-----------------------------------------------------------------------

https://www.youtube.com/watch?v=8Tj6Yx4oEXc

This is the sound of underground

-----------------------------------------------------------------------

https://susam.in/blog/from-tower-of-hanoi-to-counting-bits/

-----------------------------------------------------------------------

## March 2020

https://qntm.org/trick Programming Trick Questions

-----------------------------------------------------------------------

https://www.newstatesman.com/culture/books/2020/02/william-gibson-apocalypse-it-s-been-happening-least-100-years

Interview with William Gibson

-----------------------------------------------------------------------

https://stackoverflow.com/questions/35742775/is-it-possible-to-post-files-to-slack-using-the-incoming-webhook

Posting files to incoming Slack webhooks is impossible!

-----------------------------------------------------------------------

https://www.gamesradar.com/fifa-21-guide/

> FIFA21 for PS5 and XBox Series X (Gen5 consoles) will probably be
> released around 2020.09.25, according to the article

-----------------------------------------------------------------------

`https://en.wikipedia.org/wiki/Organ_Sonatas_(Bach)`

> The organ sonatas, BWV 525–530 by Johann Sebastian Bach are a
> collection of six sonatas in trio sonata form. Each of the sonatas
> has three movements, with three independent parts in the two manuals
> and obbligato pedal.

> I enjoyed BWV 529 in particular, Sonata No. 5 in C major (3 mvmts
> ..., largo, ...)

-----------------------------------------------------------------------

https://hackaday.com/2020/03/02/project-rubicon-the-nsa-secretly-sold-flawed-encryption-for-decades/

> NSA co-opted the producer of commercial encryption equipment and
> sold flawed hardware to lots of governments for decades!

-----------------------------------------------------------------------

https://hackaday.com/2020/03/02/multi-band-receiver-on-a-chip-controlled-by-arduino/

> Shortwave, AM, FM receiver on a chip controlled by Arduino

-----------------------------------------------------------------------

https://www.troyhunt.com/everything-you-wanted-to-know-about-sql/

> Tips on avoiding SQLi attacks from security consultant Troy Hunt

-----------------------------------------------------------------------

https://securityintelligence.com/an-example-of-common-string-and-payload-obfuscation-techniques-in-malware/

> Examples of text obfuscation techniques in malware, esp. in
> cases where encryption keys must be stored in the code but not
> in plaintext...

-----------------------------------------------------------------------

Solaris troubleshooting links

http://unixadminschool.com/blog/2011/03/deal-with-memory-errors-correctable-and-uncorrectable/

https://support.oracle.com/knowledge/Sun%20Microsystems/1006513_1.html

https://www.linuxquestions.org/questions/aix-43/sendmail-permission-issues-getting-dbm-map-alias0-unsafe-map-file-etc-mail-alias-4175513612/

-----------------------------------------------------------------------

https://www.theguardian.com/books/2018/feb/25/skin-in-the-game-by-nassim-nicholas-taleb-digested-read

> Book review of Nassim Taleb's *Skin in the game*

-----------------------------------------------------------------------

http://ka.lpe.sh/2012/08/03/mysql-find-column-name-in-any-tables-having-it-in-whole-database/

> Find column name in any table(s) having it in whole database

```sql
SELECT table_name, column_name from information_schema.columns
  WHERE column_name LIKE '%column_name_to_search%';
```

-----------------------------------------------------------------------

https://kb.iu.edu/d/aaxw Defining and using macros in Emacs

-----------------------------------------------------------------------

https://jupyter.org/install

> New version of `jupyter notebook` is `jupyter lab`

> after installing *lab*, navigate to the directory containing your
> ipython notebooks and run `jupyter lab`

-----------------------------------------------------------------------

https://www.freecodecamp.org/news/running-commands-linux-hosts-using-pssh/

-----------------------------------------------------------------------

https://stackoverflow.com/questions/107701/how-can-i-remove-ds-store-files-from-a-git-repository

-----------------------------------------------------------------------

https://git-scm.com/book/en/v2/Git-Basics-Tagging

> Creating an annotated tag in Git is simple. The easiest way is to
> specify -a when you run the tag command:

`git tag -a v1.4 -m "my version 1.4"`

> This will apply the tag to the latest commit. You can see the tag
> with `git show ...` or `git log`

-----------------------------------------------------------------------

https://giphy.com/

> Slack at work supports giphy with `/giphy <searchTerm>`

https://giphy.com/gifs/snl-thumbs-up-awesome-d2Z9QYzA2aidiWn6

-----------------------------------------------------------------------

https://medium.com/devnotes/manage-windows-on-macos-using-amethyst-3817b2804f29

> Keyboard shortcuts for Amethyst tiling window manager on OSX

- move window focus clockwise `opt+shift+J`
- move window focus counterclockwise `opt+shift+K`

-----------------------------------------------------------------------

https://www.youtube.com/watch?v=hinZO--TEk4 1h 16min

> Training Sentiment Model Using BERT and Serving it with Flask API
> by Abishek Thakur (4-time Kaggle GM and CTO at boost.ai)

https://github.com/abhishekkrthakur/bert-sentiment/

-----------------------------------------------------------------------

https://rachelbythebay.com/w/2018/03/15/core/

> *Bus errors, core dumps, and binaries on NFS*

> Interesting article about things that can happen when you run
> binary executables from an NFS share (tl;dr -- don't do this).

-----------------------------------------------------------------------

https://github.com/DormyMo/SpiderKeeper Scalable admin UI for web spiders

> Manage your spiders from a dashboard. Schedule them to run automatically

- With a single click deploy the scrapy project
- Show spider running stats
- Provide api

-----------------------------------------------------------------------

pretty-print `curl` json output with `jq`:

`curl https://jsonplaceholder.typicode.com/todos/1 | jq .`

-----------------------------------------------------------------------

https://en.wikipedia.org/wiki/The_Lego_Batman_Movie

Ranked higher than the Lego Movie 2 on Rotten Tomatoes...

-----------------------------------------------------------------------

https://thediplomat.com/2019/03/the-truth-about-radiation-in-fukushima/

-----------------------------------------------------------------------

https://techcrunch.com/2020/03/11/aws-launches-bottlerocket-a-linux-based-os-for-container-hosting/

-----------------------------------------------------------------------

https://www.linkedin.com/content-guest/article/notes-from-ucsf-expert-panel-march-10-dr-jordan-shlain-m-d-/

> UCSF expert panel on nCov-19

-----------------------------------------------------------------------

https://github.com/ZuzooVn/machine-learning-for-software-engineers

> List of resources for Machine Learning self-study

-----------------------------------------------------------------------

https://github.com/jwasham/coding-interview-university

Coding interview resources

-----------------------------------------------------------------------

https://www.tensorflow.org/install/pip

> tensorflow installation guide

> As of 2020.03.15, `tf` only support up to python 3.7 and MacOS
> versions do not have any GPU support. Only nVidia GPU's with
> CUDA drivers are supported.

> Nightly builds currently support python 3.8 on Linux. When `tf` v2.2
> comes out, python 3.8 will be supported.

https://github.com/tensorflow/tensorflow/issues/33374

-----------------------------------------------------------------------

https://anil.io/blog/python/pyenv/using-pyenv-to-install-multiple-python-versions-tox/

```sh
$ cd ~/Downloads/wayne_chen
# Create a local pyenv
$ pyenv local 3.7.6
# make sure pyenv is loaded in this session, then check python ver
$ python --version
Python 3.7.6
$ pip3 --version
pip 19.2.3 from /Users/pekoh/.pyenv/versions/3.7.6/lib/python3.7/site-packages/pip (python 3.7)
$ pip3 install tensorflow
...
```

-----------------------------------------------------------------------

https://phmkorea.com/6207 Article about container housing in Korea

-----------------------------------------------------------------------

https://github.com/ggreer/the_silver_searcher/issues/551

> `ag` silver searcher multiple search terms using the `|` operator

-----------------------------------------------------------------------

https://en.wikipedia.org/wiki/List_of_shortwave_radio_broadcasters

https://en.wikipedia.org/wiki/List_of_radio_stations_in_North_Korea

-----------------------------------------------------------------------

https://docs.mathjax.org/en/latest/upgrading/whats-new-3.0.html

> MathJax 3.0 was recently released

-----------------------------------------------------------------------

https://en.wikipedia.org/wiki/Sangean

> Shortwave Radio manufacturer from Taiwan; products are highly
> regarded

https://www.amazon.com/Sangean-ANT-60-Short-Wave-Antenna/dp/B000023VW2

> Sangean 23-ft Short Wave antenna $13.64

-----------------------------------------------------------------------

https://github.com/ianyh/Amethyst Amethyst Tiling WM kb shortcuts

> Amethyst uses two modifier combinations.
- `mod1`: `option + shift`
- `mod2`: `ctrl + option + shift`

- `option + shift + space` cycle layout forward
- `ctrl + option + shift + space` cycle layout back
- `ctrl + option + shift + K` swap focused window clockwise
- `ctrl + option + shift + J` swap focused window counter clockwise

-----------------------------------------------------------------------

https://community.grafana.com/t/getting-a-rendered-panel-via-api/1559/12

> Did you know it is possible to download a rendered image file
> of a Grafana dashboard via the Grafana API? The endpoint is of
> the form `/render/dashboard/<dashName>`

-----------------------------------------------------------------------

https://t.co/5cYcOpEtf9

> "Wuhan Virus", "Chinese Virus", COVID-19. Doesn't matter the name -
> my hometown will forever be known for that and only that. I drew
> this comic to shine light on what people don't know: the beautiful
> culture, rich history, and strong people of Wuhan. 加油!

-----------------------------------------------------------------------

> Top Six Open Source Tools for Monitoring #Kubernetes and #Docker

https://t.co/rJaTSekx0K

-----------------------------------------------------------------------

https://commons.wikimedia.org/wiki/File:Aircraft_Rescue_Firefighting_training.jpg

-----------------------------------------------------------------------

https://www.adaptivecapacitylabs.com/HowComplexSystemsFail.pdf

> by Dr. Richard Cook (recommended by John Allspaw)

https://www.researchgate.net/publication/220579448_Collaborative_Cross-Checking_to_Enhance_Resilience

https://www.researchgate.net/publication/224753269_The_Messy_Details_Insights_From_the_Study_of_Technical_Work_in_Healthcare

https://www.researchgate.net/publication/334548988_Being_Bumpable_Consequences_of_resource_saturation_and_near-saturation_for_cognitive_demands_on_ICU_practitioners

-----------------------------------------------------------------------

https://en.wikipedia.org/wiki/El_Chavo_del_Ocho

> El Chavo (originally known as El Chavo del Ocho) is a Mexican
> television sitcom that gained enormous popularity in Hispanic
> America, Brazil, Spain and other countries.

> The show is centered around the adventures and tribulations of the
> title character—a poor orphan nicknamed "El Chavo" (which means "The
> Kid"), played by the show's creator, Roberto Gómez Bolaños
> "Chespirito" —and other inhabitants of a fictional low-income
> housing complex, or, as called in Mexico, vecindad.

-----------------------------------------------------------------------

https://www.quora.com/How-does-it-feel-to-work-at-EA-India-Hyderabad

> Apparently Electronic Arts also uses contractors from TCS, Wipro,
> and Infosys. In LatAm, EA uses SoftTek...

-----------------------------------------------------------------------

https://www.foxnews.com/travel/marriott-furlough-tens-thousands-workers-coronavirus

-----------------------------------------------------------------------

https://twitter.com/omespino/status/1241544334329208838

> need to execute some command but "space" is blocked by WAF or regex?
> try

```sh
IFS=,;`cat<<<cat,/etc/passwd`
cat$IFS/etc/passwd
cat${IFS}/etc/passwd
cat</etc/passwd
{cat,/etc/passwd}
X=$'cat\x20/etc/passwd'&&$X
```

-----------------------------------------------------------------------

Thierry Lang - Love of my life (piano)

-----------------------------------------------------------------------

https://hackaday.com/2020/03/25/table-held-up-by-strings-teaches-physics/

-----------------------------------------------------------------------

https://unix.stackexchange.com/questions/61655/multiple-similar-entries-in-ssh-config

-----------------------------------------------------------------------

From Let's Encrypt Twitter:

> What do @Heroku, @Squarespace, @ServerPilot, and @Shopify all have
> in common? Not only do they use Let's Encrypt but they give back to
> support our work!

-----------------------------------------------------------------------

https://github.com/ParallelSSH/parallel-sshii

> Asynchronous parallel SSH client library.

> Run SSH commands over many - hundreds/hundreds of thousands - number
> of servers asynchronously and with minimal system load on the client
> host.

> Native code based client with extremely high performance - based on
> libssh2 C library.

Seems to be better than `paramiko` and wrapping ssh in `subprocess`

https://github.com/ParallelSSH/parallel-ssh/wiki/Recipes

https://parallel-ssh.readthedocs.io/en/latest/#in-a-nutshell

> *readthedocs* for pssh is the most comprehensive

`pip install parallel-ssh`

-----------------------------------------------------------------------

https://www.amazon.com/Seeking-SRE-Conversations-Running-Production/dp/1491978864

> Seeking SRE

-----------------------------------------------------------------------

https://www.usenix.org/conference/lisa12/training/day-over-edge-system-administration

> USENIX12 prez by David Blank-Edelman

-----------------------------------------------------------------------

https://www.slideshare.net/chrismceniry/on-the-edge-systems-administration-with-golang

> USENIX17 prez by Chris McEniry

-----------------------------------------------------------------------

https://medium.com/hepsiburadatech/control-your-ssh-servers-by-using-a-go-api-779a68557826

> SSH in Golang

-----------------------------------------------------------------------

**Security with Go**

> by John Daniel Leon, Packt Publishing, Jan 2018

> The book has an entire chapter on SSH, called *Using the Go SSH
> Client*

https://github.com/PacktPublishing/Security-with-Go

https://godoc.org/golang.org/x/crypto/ssh

-----------------------------------------------------------------------

https://godoc.org/golang.org/x/crypto/ssh#ParsePrivateKeyWithPassphrase

> Note that there is a built-in function known as
> `ParsePrivateKeyWithPassphrase` that can handle privkeys with
> passwords.

-----------------------------------------------------------------------

https://www.fastcompany.com/90479846/the-untold-origin-story-of-the-n95-mask

-----------------------------------------------------------------------

https://testinfra.readthedocs.io/en/latest/

> With Testinfra you can write unit tests in Python to test actual
> state of your servers configured by management tools like Salt,
> Ansible, Puppet, Chef and so on.

> Testinfra aims to be a Serverspec equivalent in python and is
> written as a plugin to the powerful Pytest test engine

-----------------------------------------------------------------------

https://github.com/rhasspy/rapidfuzz

> Rapid fuzzy string matching in Python and C++ using the Levenshtein
> Distance

-----------------------------------------------------------------------

https://www.youtube.com/watch?v=ms59YS77ffo

> Fallout Tactics redux LP - location of Advanced Power Armor in
> a fridge (which is only accessible by a hidden ladder.


## April 2020

-----------------------------------------------------------------------

https://skarlso.github.io/2019/02/17/go-ssh-with-host-key-verification/

-----------------------------------------------------------------------

https://www.cs.au.dk/~chili/PBI/emacs.html

**Emacs tip**

> To count the number of occurrences of some string (from where the
> cursor is), type M-x count-matches RET, then type the string pattern
> you want to count. Note that you can use regular expressions
> (tadaa!)

-----------------------------------------------------------------------

https://stackoverflow.com/questions/35906991/go-x-crypto-ssh-how-to-establish-ssh-connection-to-private-instance-over-a-ba

> establish ssh connection to private instance over a bastion node

-----------------------------------------------------------------------

https://stackoverflow.com/questions/112396/how-do-i-remove-the-passphrase-for-the-ssh-key-without-having-to-create-a-new-ke

`ssh-keygen -p -P <old_PW> -N <new_PW> -f /path/to/myencSSHkey`

> If you set the new password to `''` empty string, you effectively
> remove the passphrase from the key. This is useful for getting a
> clean SSH private key for cases where scripts will choke when
> asked for a SSH key password.

-----------------------------------------------------------------------

https://stackoverflow.com/questions/42105432/how-to-use-an-encrypted-private-key-with-golang-ssh

> use ssh key auth with encrypted private key requiring passphrase

-----------------------------------------------------------------------

https://tutorialedge.net/golang/parsing-json-with-golang/

-----------------------------------------------------------------------

> From Go: Design Patterns for Real-World Projects

`golang.fyi/ch10/json1.go`

```go
package main

import (
	"encoding/json"
	"fmt"


func main() {
	file, err := os.Open("book.dat")
		if err != nil {
			fmt.Println(err)
			return
		}

	var books []Book
	dec := json.NewDecoder(file)
	if err := dec.Decode(&books); err != nil {
		fmt.Println(err)
		return
	}
}
```

> And one gotcha I ran into; `JSON` syntax is quite strict when it
> comes to double-quotes versus single quotes; In JSON, you **must**
> use double-quotes for strings.

https://stackoverflow.com/questions/4162642/single-vs-double-quotes-in-json/34855065

> One problem I was having was that a `test.json` file I was using had
> single quotes surrounding strings, which generated lots of JSON parsing
> errors:

```
Error decoding json. invalid character '\'' looking for beginning of object key stg
```

-----------------------------------------------------------------------

https://stackoverflow.com/questions/16465705/how-to-handle-configuration-in-go

-----------------------------------------------------------------------

https://t.co/BJI2EdA3rR

> General launch of TailScale, based on Wireguard VPN

-----------------------------------------------------------------------

https://stackoverflow.com/questions/24182950/how-to-get-hostname-from-ip-linux

`nslookup <ipaddr>`

-----------------------------------------------------------------------

https://www.enigma.com/blog/post/the-secret-world-of-newline-characters

> Post about handling newlines in Python

-----------------------------------------------------------------------

https://nbviewer.jupyter.org/github/pambot/enigma-pandas/blob/master/enigma-pandas.ipynb

> optimizing pandas; also includes info about using numba.jit for
> speeding up vanilla python code

-----------------------------------------------------------------------

https://www.gnu.org/software/emacs/manual/html_node/emacs/Regexp-Search.html

> Emacs regexp search `C-M-s`

https://www.emacswiki.org/emacs/FindingNonAsciiCharacters

> `C-M-s [[:nonascii:]]`

-----------------------------------------------------------------------

https://stackoverflow.com/questions/23356779/how-can-i-store-the-find-command-results-as-an-array-in-bash

> Awesome...

```sh
mapfile -d $'\0' myarray < <(find . \( -name "*.txt" -o -name "*.conf" -o -name "*.properties" \) -print0)
```


-----------------------------------------------------------------------

https://numba.pydata.org/numba-doc/dev/user/parallel.html

https://numba.pydata.org/numba-doc/latest/user/parallel.html#numba-parallel

https://numba.pydata.org/numba-doc/latest/user/5minguide.html

-----------------------------------------------------------------------

https://scottiestech.info/2018/08/07/run-cron-jobs-in-windows-subsystem-for-linux/

`sudo service cron start` (WSL 1)

> Also added my user to the `crontab` group, just in case, as in:

https://stackoverflow.com/questions/41281112/crontab-not-working-with-bash-on-ubuntu-on-windows

> Not sure that this is strictly necessary, tho

-----------------------------------------------------------------------

https://stackoverflow.com/questions/3074288/get-final-url-after-curl-is-redirected

> I was trying to find the final url after redirection for the purposes
> of discovering an IP:port for Internet radio; the `curl` cmd above
> didn't work for me, but the following `wget` snippet did:

`curl -Ls -o /dev/null -w %{url_effective} http://url:<port>`

-----------------------------------------------------------------------

https://twitter.com/TatianaTMac/status/1247605419230388224

> Learn more about coding by searching for code snippets in Github

-----------------------------------------------------------------------

https://www.vladionescu.me/posts/scaling-containers-in-aws.html

-----------------------------------------------------------------------

https://developers.google.com/edu/python/regular-expressions

> Good guide to Python Regex

-----------------------------------------------------------------------

https://gist.github.com/asabaylus/3071099

> Internal Anchors in Markdown

-----------------------------------------------------------------------

https://physics-astronomyblog.blogspot.com/2019/02/nasas-1-billion-spacecraft-has-taken.html

> New photos of Jupiter

-----------------------------------------------------------------------

**Emacs show num lines in file**

`C-x l`

**Emacs show number of lines in selected**

`M-=`

-----------------------------------------------------------------------

https://twitter.com/eevee/status/1249799073357377536

> LoL requires its own kernel driver on Win10; "anti-cheat"

-----------------------------------------------------------------------

https://twitter.com/lukeweston/status/1249901037160284161

> Some people are using FPGA's to cheat at games and evade anti-cheat
> kernel modules

https://github.com/EngineOwningSoftware/pcileech-webradar/blob/master/readme.md

-----------------------------------------------------------------------

> Accessing Mac desktop remotely from Windows, Linux

https://www.howtogeek.com/214220/how-to-access-your-macs-screen-from-windows-and-vice-versa/

https://apple.stackexchange.com/questions/84221/can-i-remotely-access-my-mac-from-linux-using-screen-sharing

-----------------------------------------------------------------------

https://medium.com/@NetflixTechBlog/netflix-images-enhanced-with-aws-lambda-9eda989249bf

> Netflix rewrote its dynamic image processing service in Golang (single static
> binary) b/c the JVM startup time of 500~800ms on Lambda was prohibitive

-----------------------------------------------------------------------

Helpful libraries when creating cli apps in Python

- https://github.com/erikrose/blessings

  > Python wrapper for terminal capabilities

- https://github.com/urwid/urwid

  > console user interface library for Python

- https://www.youtube.com/watch?v=WAitSilLDUA

  > Pycon 2015 Thomas Ballinger - *Terminal Whispering*

-----------------------------------------------------------------------

Color in Python stdout text

- https://stackoverflow.com/questions/287871/how-to-print-colored-text-in-terminal-in-python

- https://pypi.python.org/pypi/colorama

  > Cross-platform colored CLI text (POSIX and Windows)

- https://www.devdungeon.com/content/colorize-terminal-output-python

-----------------------------------------------------------------------

https://stackoverflow.com/questions/639531/search-in-all-fields-from-every-table-of-a-mysql-database

> There is no simple command to search for a snippet of text across all
> tables; the recommended method if your DB is not too large is to do
> a `mysqldump` of your DB to a file and to `grep` the resulting
> text file.

-----------------------------------------------------------------------

https://www.redhat.com/en/command-line-heroes/season-4/consoles

> Sega Dreamcast, released in 1999, still has homebrew fans today who
> have reverse-engineered modem play and Dreamcast online servers!
> Podcast transcript available

-----------------------------------------------------------------------

https://learn-gevent-socketio.readthedocs.io/en/latest/gevent.html

> Python gevent async io and lightweight multi-threading

https://sdiehl.github.io/gevent-tutorial/

> gevent For the Working Python Developer

> The goal is to give you the tools you need to get going with gevent, help you
> tame your existing concurrency problems and start writing asynchronous
> applications today.

-----------------------------------------------------------------------

https://devops.college/monitoring-in-a-post-nagios-world-2f6bb738ee96

> Monitoring in a post-nagios world

-----------------------------------------------------------------------

https://reproducible-science-curriculum.github.io/publication-RR-Jupyter/02-exporting_the_notebook/index.html

> Exporting Jupyter notebooks to pdf (first convert to html, then from
> html to pdf)

-----------------------------------------------------------------------

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/windows_integration_guide/summary-direct

> Integrating Linux boxes into Windows Active Directory (use `sssd`)

-----------------------------------------------------------------------

Info about JMX (Java Management Extensions)

https://www.sderosiaux.com/articles/2017/02/14/all-the-things-we-can-do-with-jmx/

> There are pre-existing platform MXBeans: the ones already packaged with the
> JRE that expose the JVM internals (memory, cpu, threads, system,
> classes). Here is the platform MXBean `java.lang:type=Memory` attributes and
> values...

> Java exposes a client API in javax.management[.remote] to connect to any JMX
> agent through RMI and retrieve a MBeanServerConnection to request the MBeans
> attributes, their values, etc.

> The connection scheme is quite ugly:
> `service:jmx:rmi:///jndi/rmi://localhost:9010/jmxrmi` but trust me, it works!

-----------------------------------------------------------------------

https://stackoverflow.com/questions/1571600/is-there-any-jmx-rest-bridge-available

> Note that JMX is *not* REST-compatible OOTB, but there are various tools
> that can serve as a JMX to REST bridge, such as Jolokia.

-----------------------------------------------------------------------

https://db-engines.com/en/system/Graphite%3BInfluxDB%3BOpenTSDB

> Comparison of Graphite, InfluxDB and OpenTSDB

> InfluxDB is much more popular as of 2020... did you know Graphite was
> written in Python and doesn't support sharding? I know that EA uses
> Graphite in a cluster, but maybe it's time to move to something more
> modern...

-----------------------------------------------------------------------

https://hackernoon.com/which-is-the-fastest-version-of-python-2ae7c61a6b2b

> Python 3.7 vs 2.7 speed averaged across multiple kinds of tests is
> about 1.2 times faster but of course has many more features.


-----------------------------------------------------------------------

https://www.sec.gov/cgi-bin/browse-edgar?CIK=EA&action=getcompany

> EDGAR page for Electronic Arts

-----------------------------------------------------------------------

https://appliedgo.net/diydashboard/

> Tutorial sending custom data to Grafana using the `simplejson`
> plugin. In practice, you need to have a storage backend aggregate
> your metrics and then use Grafana's integration with the storage
> backend to get the data you need.

-----------------------------------------------------------------------

https://grafana.com/docs/grafana/latest/features/datasources/

The following data sources are officially supported:

- AWS CloudWatch
- Azure Monitor
- Elasticsearch
- Google Stackdriver
- Graphite
- InfluxDB
- Loki
- Microsoft SQL Server (MSSQL)
- Mixed
- MySQL
- OpenTSDB
- PostgreSQL
- Prometheus
- Testdata

-----------------------------------------------------------------------

https://stackoverflow.com/questions/6600878/find-all-packages-installed-with-easy-install-pip

> `pip freeze` to see all the python packages you have installed with `pip`

-----------------------------------------------------------------------

https://github.com/kaogilvie/GCal-python-trigger/blob/master/trig_init.py

> Cool example of OAuth2 auth flow using just python `requests` and no
> oauth2 libraries! Since this is from 2014, don't know if it still works
> in 2020...

-----------------------------------------------------------------------

https://docs.python.org/3/library/sqlite3.html

> Python3 `sqlite3` module docs; part of Python `stdlib`!

-----------------------------------------------------------------------

Fous De La Mer - Waiting For The Sun

-----------------------------------------------------------------------

https://www.sqlite.org/lang_datefunc.html

> Info on storing dates in `sqlite3`; there is no separate `DATE` datatype in
> `sqlite3`, but you can store them as ISO8601 compliant `TEXT` datatype. A time
> string can be in any of the following formats:


```
YYYY-MM-DD
YYYY-MM-DD HH:MM
YYYY-MM-DD HH:MM:SS
YYYY-MM-DD HH:MM:SS.SSS
YYYY-MM-DDTHH:MM
YYYY-MM-DDTHH:MM:SS
YYYY-MM-DDTHH:MM:SS.SSS
HH:MM
HH:MM:SS
HH:MM:SS.SSS
now
DDDDDDDDDD
```

-----------------------------------------------------------------------

https://scottberkun.com/2020/its-time-to-learn/

> A response to Marc Andreesen's essay *Time to Build*

-----------------------------------------------------------------------

https://lwn.net/Articles/818000/

> Python 2.7.18, the end of an era

> I like the mailing list announcement by Benjamin Peterson"
> `<benjamin-AT-python.org>`:

> I'm eudaemonic to announce the immediate availability of Python 2.7.18.

> Python 2.7.18 is a special release. I refer, of course, to the fact that
> "2.7.18" is the closest any Python version number will ever approximate `e`,
> Euler's number. Simply exquisite!

> A less transcendent property of Python 2.7.18 is that it is the last Python
> 2.7 release and therefore the last Python 2 release. It's time for the CPython
> community to say a fond but firm farewell to Python 2. Users still on Python 2
> can use `e` to compute the instantaneously compounding interest on their
> technical debt.

-----------------------------------------------------------------------

https://www.gwern.net/GPT-2-music#

> Automatic MIDI generation using `GPT-2` DL model trained on public
> domain MIDI files automatically transcribed to text notation.

-----------------------------------------------------------------------

https://launchbylunch.com/posts/2014/Feb/16/sql-naming-conventions/

-----------------------------------------------------------------------

https://www.sqlite.org/rowidtable.html

> If the rowid is not aliased by `INTEGER PRIMARY KEY` then it is not persistent
> and might change. In particular the VACUUM command will change rowids for
> tables that do not declare an INTEGER PRIMARY KEY. Therefore, applications
> should not normally access the rowid directly, but instead use an INTEGER
> PRIMARY KEY.


https://www.sqlitetutorial.net/sqlite-check-constraint/

> CHECK constraints in sqlite3; must be added when TABLE is created

-----------------------------------------------------------------------

https://docs.python.org/3/library/sqlite3.html#using-shortcut-methods

> Using the nonstandard execute(), executemany() and executescript() methods of
> the Connection object, your code can be written more concisely because you
> don’t have to create the (often superfluous) Cursor objects explicitly


-----------------------------------------------------------------------

https://dba.stackexchange.com/questions/144/when-should-i-use-a-unique-constraint-instead-of-a-unique-index

> Unique Index seems to be preferred; UNIQUE CONSTRAINT creates a
> unique index behind the scenes; if you set a unique constraint on
> a column and then also set a unique index on that column, you will
> end up with *TWO* indexes! Avoid doing this...

-----------------------------------------------------------------------

> Tong District Kyrgyzstan bordering Issyk Kul lake is quite
> scenic and picturesque

https://en.wikipedia.org/wiki/Tong_District

https://www.tripadvisor.com/Hotel_Review-g8012725-d8333240-Reviews-Natalia_Hotel-Kaji_Say_Issyk_Kul_Province.html


-----------------------------------------------------------------------

## May 2020

https://twitter.com/wtm_offensi/status/1256890015323430913

> This is what it looks like when you get caught by Google SRE :) @epereiralopez
> and me finally dropped a shell in a, less significant than hoped,
> system. Suddenly a greetings.txt appeared :) Kudo's to the kind people of SRE!

-----------------------------------------------------------------------

working with mysql in ansible

- https://groups.google.com/forum/#!msg/ansible-project/XrQ08TdtcRk/I4s7rF7GkyMJ
- https://stackoverflow.com/questions/28188508/insert-data-into-mysql-tables-using-ansible
- https://docs.ansible.com/ansible/latest/modules/mysql_db_module.html
- https://docs.ansible.com/ansible/latest/modules/list_of_database_modules.html

> There is a native Ansible module for working with `mysql` called
> `mysql_db`. However, it requires that the remote host have `mysql`,
> `mysqldump`, and `pymysql` installed.

-----------------------------------------------------------------------

https://github.com/emacs-mirror/emacs/blob/master/etc/NEWS.27

> Emacs 27 has lots of changes to basic behavior surrounding startup,
> init, new integrations with `systemd`, XDG, gtk2/3, etc.

-----------------------------------------------------------------------

https://www.philosophicalhacker.com/post/data-point-for-job-seeking-devs/

> Interesting post by a graduate from Recurse Center (aka Hacker School)
> on his job search and interview experiences

-----------------------------------------------------------------------

https://www.usenix.org/system/files/nsdi20spring_hauer_prepub.pdf

> USENIX paper 'Meaningful Availability' by Google featuring windowed
> user uptime as a way to avoid biases in time-based availability and
> success ratio metrics.

-----------------------------------------------------------------------

https://queue.acm.org/detail.cfm?id=3096459

> ACM Queue 'The Calculus of Service Availability' and the *rule of
> the extra 9* for critical service dependencies.

-----------------------------------------------------------------------

https://stackoverflow.com/questions/7152762/how-to-redirect-print-output-to-a-file-using-python

> It is possible to write to a file without using `fileObj.write()`;
> `print("foo", file=fileObj)` works too! And the nice thing is that
> unlike `write()`, with `print()` you don't have to insert an extra
> `\n` char.

-----------------------------------------------------------------------

https://matthew-brett.github.io/teaching/string_formatting.html

> nice primer on `str.format()`, f-strings `f"My name is {name}"`
> and old-school `%` string param's

-----------------------------------------------------------------------

https://stackoverflow.com/questions/36816640/how-to-handled-sqlite-errors-such-as-has-no-column-named-that-are-not-raised-a

> For error-handling in DB apps written in Python, the SO post above
> recommends not using `OR IGNORE` sql statements for `INSERT` or
> `UPDATE` operations on tables with CHECK constraints. Instead, the
> post recommends wrapping queries inside of a `try...except` block
> and logging exceptions.

-----------------------------------------------------------------------

https://bpdnews.com/news/2020/5/11/department-issued-phone-comes-in-handy-as-officers-use-mating-call-to-capture-escaped-peacock-from-franklin-park-zoo

> Awesome

-----------------------------------------------------------------------

> From `ID_AA_Carmack` John Carmack's twitter:

> My stock suggestions for aspiring game programmers is to try to both clone
> classic games to learn from the ground up, and to work on mods to large scale
> games to learn from a high level. For the ground-up work, this site is an
> amazing resource: https://t.co/d8sPTz9EtN <spriters-resource.com>

-----------------------------------------------------------------------

https://stackoverflow.com/questions/4940032/how-to-search-for-a-string-in-text-files

> When searching for a string inside a text file, it's sometimes better to
> convert the entire file into a string blob with `f.read()` and search the
> entire blob rather than iterating through the file line-by-line with
> `for line in f:`. Note however, that you cannot mix and match
> `for line in f` and `f.read()` within the same `with(open, 'r') as f`
> block!!! Once you use  `f.read()`, everything from the fileObj is
> read into the read buffer, so `for line` will not loop over anything!

-----------------------------------------------------------------------

http://116.202.241.212:7006/

>New streaming server for jazzheart

-----------------------------------------------------------------------

https://stackoverflow.com/questions/180986/what-is-the-difference-between-re-search-and-re-match

> TL;DR they are quite similar, but in general you should just
> go with `re.search()`

-----------------------------------------------------------------------

https://www.infoq.com/presentations/facebook-google-bpf-linux-kernel

> InfoQ prez about how extended Berkeley Packet Filters (eBPF) make
> the Linux kernel more extensible without having to write kernel
> modules. The analogy used in the prez is that JS is to the Web
> as eBPF is to the Linux kernel.

-----------------------------------------------------------------------

https://www.infoq.com/presentations/history-comments-git/

> prez about using git history as a diary

-----------------------------------------------------------------------

https://www.infoq.com/articles/polypane-developer-browser-interview

> Interview with the creator of Polypane, a powerful browser for
> developers.

-----------------------------------------------------------------------

https://www.digitalocean.com/blog/just-how-managed-are-digitaloceans-managed-databases

-----------------------------------------------------------------------

https://stackoverflow.com/questions/45965007/multiline-f-string-in-python

-----------------------------------------------------------------------

https://www.quora.com/What-is-BLOB-in-SQLite-How-do-you-use-it

> BLOB is a binary large object. It can be used to store any kind of data -
> image, music file, .doc, .pdf and etc.

-----------------------------------------------------------------------

https://ctftime.org/event/list/upcoming

https://ctftime.org/event/1041 Google CTF 2020 8/22 00:00 UTC

-----------------------------------------------------------------------

- https://oooverflow.io/dc-ctf-2020-finals/ DEFCON CTF 2020

> Winning team `A*O*E` https://twitter.com/teama0e a Chinese CTF team

- https://github.com/A-0-E/writeups CTF writeups from team A0E

-----------------------------------------------------------------------

https://github.com/pwning/public-writeup PPP CTF team writeups

> PPP (Plaid Parliament of Pwning) is CMU's CTF team

-----------------------------------------------------------------------

https://staffeng.com/stories/katie-sylor-miller

> Dev with Front-end background became a Staff Engineer at Etsy
> responsible for Front-End Architecture

-----------------------------------------------------------------------

https://www.self.com/gallery/stretches-that-will-help-you-touch-your-toes

-----------------------------------------------------------------------

https://jackwarren.info/posts/guides/racket/racket-command-line/

> Writing CLI apps with Racket

-----------------------------------------------------------------------

https://twitter.com/copyconstruct/status/1052072688372477952

> Traffic engineering at FB

-----------------------------------------------------------------------


## August 2020


> Writeups for Google CTF 2020 *Reversing - Beginner* challenge

https://github.com/Dvd848/CTFs/blob/master/2020_GoogleCTF/Beginner.md

> This writeup was rated 5.0 on CTFtime https://ctftime.org/writeup/23042 It
> uses a tool called `angr` (python) which uses various constraint solvers on
> the backend (SAT solver, z3, etc) to brute-force the `SUCCESS` branch in the C
> binary.

-----------------------------------------------------------------------

https://ctftime.org/writeup/23052

> In this writeup, the analyst uses the `z3` python module for the z3 constraint
> solver to model the instructions in the binary.

-----------------------------------------------------------------------

https://docs.angr.io/

> angr is a multi-architecture binary analysis toolkit, with the capability to
> perform dynamic symbolic execution (like Mayhem, KLEE, etc.) and various
> static analyses on binaries.

-----------------------------------------------------------------------

https://github.com/Ragnar-Security/ctf-writeups/tree/master/google-ctf/Reverse_Engineering/Beginner

> In this writeup the analyst says their go-to tools for binary analysis
> are `ghidra` for disassembly, then `gdb` if necessary, and then
> `angr`.

-----------------------------------------------------------------------

https://github.com/aelsabbahy/goss

> Infrastructure unit testing in Golang; inspired by Ruby's serverspec

https://www.reddit.com/r/devops/comments/5xtr8h/goss_v030_yaml_serverspec_alternative_now/

> Goss has to run on the system locally

-----------------------------------------------------------------------

## Sept 2020

https://stackoverflow.com/questions/40247726/go-execute-a-bash-command-n-times-using-goroutines-and-store-print-its-resul

https://stackoverflow.com/questions/18405023/how-would-you-define-a-pool-of-goroutines-to-be-executed-at-once

> run shell commands using goroutines/concurrency in Golang with os/exec

-----------------------------------------------------------------------

https://github.com/go-cmd/cmd

> This package is a small but very useful wrapper around os/exec.Cmd that makes
> it safe and simple to run external commands in highly concurrent,
> asynchronous, real-time applications. It works on Linux, macOS, and Windows.

> only three methods: Start, Stop, and Status. When possible, it's better to use
> `go-cmd/Cmd` than `os/exec.Cmd` because `go-cmd/Cmd` provides:

- Channel-based fire and forget
- Real-time stdout and stderr
- Real-time status
- Complete and consolidated return
- Proper process termination
- 100% test coverage, no race conditions

-----------------------------------------------------------------------

https://blog.kowalczyk.info/article/wOYk/advanced-command-execution-in-go-with-osexec.html

-----------------------------------------------------------------------

https://kukuruku.co/post/ssh-commands-execution-on-hundreds-of-servers-via-go/

-----------------------------------------------------------------------

https://github.com/danderson/py-remoteexec

> Remote Exec lets you ship python code to a remote machine and run it there,
> all without installing anything other than the standard Python interpreter on
> the server.

> The original idea came from Avery Pennarun's awesome sshuttle
> (http://github.com/apenwarr/sshuttle), which uses this great hack to function
> with any server that has Python installed (i.e. all of them).

-----------------------------------------------------------------------

https://github.com/apenwarr/port

> minicom is too complicated. This command talks to a serial port

-----------------------------------------------------------------------

https://ispycode.com/GO/Files-And-Directories/Change-File-Permissions

> Instead of using Golang's `os.Exec()` to run shell commands, you
> can also use native Golang `os` methods to do common sysadmin tasks
> affecting the filesystem.

https://ispycode.com/GO/Files-And-Directories

-----------------------------------------------------------------------

https://stackoverflow.com/questions/41415337/running-external-python-in-golang-catching-continuous-exec-command-stdout

> Use `cmd.Start()` and `cmd.Wait()` instead of `cmd.Run()`

-----------------------------------------------------------------------

https://stackoverflow.com/questions/7933460/how-do-you-write-multiline-strings-in-go

> *Q*: Does Go have anything similar to Python's multiline strings:

```python
"""
line 1
line 2
line 3
"""
```

> *A*: you can use a raw string literal, where the string is delimited by
> backticks instead of double quotes:

```golang
`line 1
line 2
line 3`
```

https://www.digitalocean.com/community/tutorials/how-to-format-strings-in-go

> With multiple lines, strings can be grouped into clean and orderly text,
> formatted as a letter, or used to maintain the linebreaks of a poem or song
> lyrics.

> To create strings that span multiple lines, back quotes are used to enclose
> the string. Keep in mind that while this will preserve the line returns, it is
> also creating a raw string literal.

-----------------------------------------------------------------------

Antonin Dvorak - Slavonic Dance Op. 72 no. 2 (London Philharmonic)

https://www.youtube.com/watch?v=wX52fGwrG20

-----------------------------------------------------------------------

https://en.wikipedia.org/wiki/The_50_Greatest_Pieces_of_Classical_Music

-----------------------------------------------------------------------

> Copy regex match to clipboard in Emacs

`C-u M-s o <regex pattern>`

https://stackoverflow.com/questions/2289883/emacs-copy-matching-lines

-----------------------------------------------------------------------

http://go-colly.org/

> Web scraping framework for Golang

> Note that this only handles server-side rendered content, and won't
> work for content dynamically rendered on the client by Javascript

-----------------------------------------------------------------------

https://gregtczap.com/blog/golang-scraping-strategies-frameworks/

> discusses web-scraping libraries for Golang

-----------------------------------------------------------------------

https://github.com/chromedp/chromedp

> Package chromedp is a faster, simpler way to drive browsers supporting
> the Chrome DevTools Protocol in Go without external dependencies.

-----------------------------------------------------------------------

https://github.com/tebeka/selenium

> Golang client for selenium web driver; works with chromedriver. Note that
> this client is not officially supported by the Selenium project; this
> project is 3rd-party. The officially supported plugin languages for
> Selenium are Java, Python, C#, Ruby, JS, and Kotlin. See
> https://www.selenium.dev/documentation/en/

-----------------------------------------------------------------------

https://github.com/headzoo/surf

https://surf.readthedocs.io/overview/

> native golang headless browser; alternative to Selenium The nice thing
> about Surf is that it doesn't have any dependencies on Selenium, which is
> a Java app that also requires a browser for headless mode execution (like
> Chrome + chromedriver).

> If you're running web scraping from your local dev machine, installing
> dependencies is not so problematic, but if you're running a scraper that
> requires client-side Java Script web page rendering, you might not be
> able to download Selenium and all its deps.

-----------------------------------------------------------------------

F5 BIGIP icontrolREST client libraries

- https://clouddocs.f5.com/api/icontrol-rest/
- https://github.com/scottdware/go-bigip
- https://devcentral.f5.com/s/articles/getting-started-with-icontrol-interface-taxonomy-language-libraries-20464?tab=series&page=1
- https://github.com/F5Networks/bigsuds

-----------------------------------------------------------------------

https://www.sohamkamani.com/blog/2017/10/18/parsing-json-in-golang/

> Good tut on using structs and maps to unmarshal/marshal JSON in
> Golang

-----------------------------------------------------------------------

https://mholt.github.io/json-to-go/

> Convert raw json to a Go type definition for struct for marshaling
> and unmarshaling JSON in Golang. Saves a lot of time!

-----------------------------------------------------------------------

https://mholt.github.io/curl-to-go/

> Convert curl calls to Go code

-----------------------------------------------------------------------

> Remote file transfer over SSH in Golang

- https://dev.to/koddr/how-to-create-a-native-macos-app-on-go-golang-and-react-js-with-full-code-protection-part-1-463k

- https://dev.to/koddr/the-easiest-way-to-embed-static-files-into-a-binary-file-in-your-golang-app-no-external-dependencies-43pc

- https://stackoverflow.com/questions/38242598/golang-scp-file-using-crypto-ssh

- https://gist.github.com/jedy/3357393

- https://stackoverflow.com/questions/53256373/sending-file-over-ssh-in-go

- https://github.com/tmc/scp/blob/master/scp.go

> Doesn't look like it's worth it to import `tmc/scp` when all it's
> doing is running `scp` from the cli after reading in a local or
> remote file into an io buffer...

- https://github.com/bramvdbogaerde/go-scp

> This module looks pretty good but the downside is that it only supports
> sending files to remote hosts, not copying files from remote hosts to
> local machine...

- https://github.com/pkg/sftp

> This package has the most stars (910) among the modules I looked at so far

- https://www.tecmint.com/sftp-command-examples/

> Refresher: `pwd`, `lpwd`, `ls`, `lls`, `put`, `get` ...
> where prefix `l` means `local`

-----------------------------------------------------------------------

GUI's in Golang

- https://golangr.com/gui/
- https://about.sourcegraph.com/go/gophercon-2019-simple-portable-efficient-graphical-interfaces-in-go/
  + Intro to Gio
- https://fyne.io/blog/2019/03/19/building-cross-platform-gui.html
- https://github.com/therecipe/qt Golang QT bindings

-----------------------------------------------------------------------

Github Actions (Like Gitlab runners)

- https://medium.com/swlh/setting-up-github-actions-for-go-project-ea84f4ed3a40
- https://docs.github.com/en/free-pro-team@latest/actions

> For public repos Github gives you 500 MB and 2000 minutes of VM run
> time per month, assuming a Linux build box; Windows build boxes consume
> hours at 2x the rate; MacOS consumes at 10x(!) the rate of Linux

-----------------------------------------------------------------------

https://stackoverflow.com/questions/2137357/getpasswd-functionality-in-go

> Get password via CLI without echo

-----------------------------------------------------------------------

- https://gist.github.com/dzlab/939e4d1ef143f5683a2a62a203362ad8
- https://ofstack.com/Golang/23392/remote-file-transfer-using-go-language.html

> Examples of using pkg/sftp Golang module

-----------------------------------------------------------------------

https://blog.logrocket.com/making-http-requests-in-go/


-----------------------------------------------------------------------

> When running a Golang prog on MacOS, I ran into the problem of
> fork/exec running out of file handles... I discovered that `ulimit -n`
> is set really low (256!) by default and had to edit this manually.

- https://docs.riak.com/riak/kv/2.1.4/using/performance/open-files-limit/#mac-os-x
- https://wilsonmar.github.io/maximum-limits/

-----------------------------------------------------------------------

https://docs.python.org/2/library/threading.html

> CPython implementation detail: In CPython, due to the Global Interpreter Lock,
> only one thread can execute Python code at once (even though certain
> performance-oriented libraries might overcome this limitation). If you want
> your application to make better use of the computational resources of
> multi-core machines, you are advised to use multiprocessing. However,
> threading is still an appropriate model if you want to run multiple I/O-bound
> tasks simultaneously.

https://stackoverflow.com/questions/54550888/python-execute-for-loop-in-multithreading

```python
import threading
import time

def mt(i):
    print (i)
    time.sleep(1)

def main():
    for i in range(5):
        threadProcess = threading.Thread(name='simplethread', target=mt, args=[i])
        threadProcess.daemon = True
        threadProcess.start()
main()
```

-----------------------------------------------------------------------

https://keyboardinterrupt.org/multithreading-in-python-2-7/

> uses `multiprocessing` module

-----------------------------------------------------------------------

- https://medium.com/@cep21/gos-append-is-not-always-thread-safe-a3034db7975
- https://stackoverflow.com/questions/22517614/golang-concurrent-array-access
-----------------------------------------------------------------------

O'Reilly Book - Concurrency in Go

-----------------------------------------------------------------------

https://www.ardanlabs.com/blog/2014/01/concurrency-goroutines-and-gomaxprocs.html

> Any function or method in Go can be created as a goroutine. We can consider
> that the main function is executing as a goroutine, however the Go runtime
> does not start that goroutine. Goroutines are considered to be lightweight
> because they use little memory and resources plus their initial stack size is
> small. Prior to version 1.2 the stack size started at 4K and now as of version
> 1.4 it starts at 8K. The stack has the ability to grow as needed.

> The operating system schedules threads to run against available processors and
> the Go runtime schedules goroutines to run within a logical processor that is
> bound to a single operating system thread. By default, the Go runtime
> allocates a single logical processor to execute all the goroutines that are
> created for our program. Even with this single logical processor and operating
> system thread, hundreds of thousands of goroutines can be scheduled to run
> concurrently with amazing efficiency and performance. It is not recommended to
> add more that one logical processor, but if you want to run goroutines in
> parallel, Go provides the ability to add more via the GOMAXPROCS environment
> variable or runtime function.

-----------------------------------------------------------------------

https://github.com/markbates/pkger

> github.com/markbates/pkger is a tool for embedding static files into Go
> binaries. It will, hopefully, be a replacement for
> github.com/gobuffalo/packr/v2.

-----------------------------------------------------------------------

- https://bjj-world.com/ultimate-bjj-core-workout-program/
- https://bjj-world.com/bjj-rib-injury-recovery-tips/
- https://bjj-world.com/bjj-for-small-guys-how-to-beat-bigger-grapplers/

-----------------------------------------------------------------------

From *Introducting Python, 2ed (O'Reilly 2019)* by Bill Lubanovic:

> In Python, threads do not speed up CPU-bound tasks because of an
> implementation detail in the standard Python system called the Global
> Interpreter Lock (GIL).This exists to avoid threading problems in the Python
> interpreter, and can actually make a multithreaded program slower than its
> single-threaded counterpart,or even a multi-process version.

-----------------------------------------------------------------------

https://requests.readthedocs.io/en/v0.8.3/user/advanced/

> Python `requests` concurrency support via `gevent` for overcoming
> network I/O bottlenecks. This is mostly for historical purposes;
> `requests` is now at `v2.22.0` circa Oct 2020, and no longer includes
> `async`.

-----------------------------------------------------------------------

## Nov 2020

- https://www.rath.org/on-the-beauty-of-pythons-exitstack.html
  + Golang's `defer` in Python

- https://aws.amazon.com/blogs/containers/advice-for-customers-dealing-with-docker-hub-rate-limits-and-a-coming-soon-announcement/
  + AWS planning a public Docker image repository

- https://thenewstack.io/6-scary-outage-stories-from-ctos/

- https://sobolevn.me/2020/10/higher-kinded-types-in-python

-----------------------------------------------------------------------

BJJ stretching exercises for the Hips

- https://www.youtube.com/watch?v=OVqqt_4U_9s
- https://www.youtube.com/watch?v=I_Pejs8By3Y
- https://www.youtube.com/watch?v=kxNsGHxExFY
- https://www.youtube.com/watch?v=r-jcLT00Tyc
- https://www.youtube.com/watch?v=yEVXJNp-EWs
- https://www.youtube.com/watch?v=6heuJUtuL0Y

-----------------------------------------------------------------------

https://changelog.com/gotime

> The Go Time Golang podcast

https://changelog.com/gotime/148

> Go Time podcast with Golang stdlib contributor Brad Fitzpatrick

https://changelog.com/gotime/148

> Go Time podcast about context.Context with Francesc Campoy

https://changelog.com/gotime/142

> Go Time podcast about Infrastructure


-----------------------------------------------------------------------

https://junglebrothers.com/3-essential-stretches-for-tight-jiu-jitsu-players/

> These 3 stretches seem pretty doable and should be done before and
> after every class.

-----------------------------------------------------------------------

- https://keys.openpgp.org/atom.xml RSS feed for opengpgp.org

- https://keys.openpgp.org/

> Keyserver that uses Sequoia-PGP, an openpgp rewrite in Rustlang.

-----------------------------------------------------------------------

- https://www.mmaleech.com/tight-hips-from-jiu-jitsu/
- http://bjjstrengthtraining.com/movement-drills-to-improve-your-closed-guard/
- https://bjjfanatics.com/blogs/news/how-to-develop-a-strong-closed-guard

-----------------------------------------------------------------------

- https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work
- https://emacs.stackexchange.com/questions/18514/how-to-automatically-sign-commits-with-magit

-----------------------------------------------------------------------

- https://cilium.io/blog/2020/11/10/ebpf-future-of-networking/
- https://grafana.com/blog/2020/11/09/trace-discovery-in-grafana-tempo-using-prometheus-exemplars-loki-2.0-queries-and-more/

-----------------------------------------------------------------------

https://www.nature.com/articles/s41392-020-00352-y

> A systematic review of SARS-CoV-2 vaccine candidates

-----------------------------------------------------------------------

https://archive.ph/Mll5H Parler hacked; DB creds hardcoded in php

-----------------------------------------------------------------------

- https://nobl9.com/resources/going-to-kubecon-in-search-of-reliability-talks-heres-the-ultimate-guide/

- https://training.linuxfoundation.org/training/kubernetes-fundamentals/

- https://github.com/cncf/curriculum

- https://github.com/kelseyhightower/kubernetes-the-hard-way

- https://medium.com/platformer-blog/how-i-passed-the-cka-certified-kubernetes-administrator-exam-8943aa24d71d#:~:text=CKA%20is%20a%20tough%20exam,the%20time%20of%20the%20exam

- https://hackernoon.com/how-to-pass-the-certified-kubernetes-administrator-exam-in-7-days-saw3uhu

-----------------------------------------------------------------------

- https://www.amazon.com/Zone-Win-Organizing-Compete-Disruption/dp/1682301710

> Zone to Win by Geoffrey Moore

- https://www.amazon.com/gp/product/0062292986

> Crossing the Chasm by Geoffrey Moore

- https://www.infoq.com/articles/book-review-think-for-yourself

- https://www.infoq.com/articles/unicorn-project

-----------------------------------------------------------------------

https://stackoverflow.com/questions/9952061/git-ignore-compiled-google-go

> If you are using the go tool to build your code you can use the `-o`
> flag to specify the output file name, so you can for example use
> `go build -o bin/elf` and then add `bin/*` to your `.gitignore`

I like my solution better:

`env GOOS=darwin GOARCH=amd64 go build -v -o bin/$(basename $(pwd)) && go test -v && go vet`

> The above snippet for `-o` will always write the binary to the the
> `bin/` subfolder in the project folder hierarchy using the basename
> of the project folder itself. No need to hard-code all Golang binaries
> to use the same name as in the example above with `bin/elf`.

-----------------------------------------------------------------------

https://twitter.com/MIT_CSAIL/status/1331256258989461505

-----------------------------------------------------------------------

## Dec 2020

https://expel.io/blog/evilginx-into-cloud-detected-red-team-attack-in-aws/

-----------------------------------------------------------------------

https://www.pybootcamp.com/blog/how-to-write-dockerfile-python-apps/

> How to write a great Dockerfile for Python apps

-----------------------------------------------------------------------

> BWV 721 "Erbarm dich mein, o Herre Gott" (have mercy on me, O Lord God)
> on Pipe Organ

- https://www.youtube.com/watch?v=RFrjYrjr3qQ Gert van Hoef
- https://www.youtube.com/watch?v=oQXgCA0eIVk Feike Asma
- https://www.youtube.com/watch?v=xsVG10aYbZY Yves Castagnet
- https://www.youtube.com/watch?v=loCDPdB4qPA Robert Hovencamp

> interpretation on Piano:

- https://www.youtube.com/watch?v=CXe-PPqAkmw Naoumoff

-----------------------------------------------------------------------

https://prog21.dadgum.com/29.html

> A Spellchecker Used to Be a Major Feat of Software Engineering

https://prog21.dadgum.com/23.html

> Purely Functional Retrogames, Part 1

> In a purely functional language, none of this works. If Pac-Man eats a dot,
> the maze can't be directly updated. If Pac-Man hits a blue ghost, there's no
> way to directly change the state of the ghost. How could this possibly work?

https://twitter.com/dadgumjames

-----------------------------------------------------------------------

- https://jwilm.io/blog/announcing-alacritty/
- https://github.com/alacritty/alacritty

> Super-fast terminal emulator written in Rustlang. Binaries available
> for Mac OS, Linux, Windows. I'm using it as an `iterm2` replacement
> on Mac OS, and am quite happy with it.

-----------------------------------------------------------------------

Trello API docs

- https://developer.atlassian.com/cloud/trello/rest/api-group-cards/#api-cards-post
- https://developer.atlassian.com/cloud/trello/
- https://developer.atlassian.com/cloud/trello/guides/rest-api/api-introduction/
- https://developer.atlassian.com/cloud/trello/guides/rest-api/webhooks/
-----------------------------------------------------------------------

Connection Re-use in Python `requests`

- https://requests.readthedocs.io/en/master/user/advanced/
- https://stackoverflow.com/questions/24873927/python-requests-module-and-connection-reuse

-----------------------------------------------------------------------

https://aws.amazon.com/blogs/aws/new-vpc-insights-analyzes-reachability-and-visibility-in-vpcs/

> Today, we are happy to announce VPC Reachability Analyzer, a network
> diagnostics tool that troubleshoots reachability between two endpoints in a
> VPC, or within multiple VPCs.

> VPC Reachability analyzer looks at the configuration of all the resources in
> your VPCs and uses automated reasoning to determine what network flows are
> feasible. It analyzes all possible paths through your network without having
> to send any traffic on the wire.

-----------------------------------------------------------------------

https://keremerkan.net/posts/wireguard-mtu-fixes/

> Adjusting the MTU setting for Wireguard tunnels to work with PPPoE

> *NOTE*: In my recollection, you have to enable ping so that clients
> can on the fly determine the appropriate MTU to use.

https://en.wikipedia.org/wiki/Path_MTU_Discovery

https://www.reddit.com/r/WireGuard/comments/g8tay9/wireguard_mtu_effects_ssl_handshake/

-----------------------------------------------------------------------

https://status.cloud.google.com/incident/zall/20013

Preliminary incident statement on 12/14 global Google outage

https://research.google/pubs/pub48190/

> Paper on Google's global auth service *Zanzibar* which was involved
> in yesterday's global outage

-----------------------------------------------------------------------

Non-blocking Python requests for Python3

- https://github.com/ross/requests-futures

-----------------------------------------------------------------------

- https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uswgi-and-nginx-on-ubuntu-18-04
- https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uwsgi-and-nginx-on-ubuntu-20-04

> Using `nginx` as a reverse proxy for Flask and uwsgi

https://en.wikipedia.org/wiki/Reverse_proxy


-----------------------------------------------------------------------

https://web.eecs.utk.edu/~azh/blog/morechallengingprojects.html

> List of challenging programming projects that all programmers
> should try

-----------------------------------------------------------------------

https://realpython.com/python-concurrency/#threading-version

> concurrency with built in concurrent.futures in Python 3.6+

https://stackoverflow.com/questions/6785226/pass-multiple-parameters-to-concurrent-futures-executor-map

> unpacking iterable with multiple values inside `executor.map()` from
> `concurrent.futures`

-----------------------------------------------------------------------

http://cs428.cs.byu.edu/

> Course page for *Real-world Software Engineering* from BYU. The readings
> and lecture slides are great and the books covered in this course include
> *The Mythical Man Month* (Fred Brooks), *Peopleware*, and *Facts and
> Fallacies of Software Engineering* (Robert Glass).

- http://brucefwebster.com/2008/04/11/the-wetware-crisis-the-dead-sea-effect/

- http://brucefwebster.com/2008/01/10/the-wetware-crisis-tepes/

> TEPES stands for Talent, Experience, Professionalism, Education, and
> Skill. These are the five aspects that separate an outstanding software
> engineer from a merely good one, much less a mediocre or wretched one.

- http://brucefwebster.com/2008/06/16/anatomy-of-a-runaway-it-project/

- http://brucefwebster.com/2013/09/13/the-real-software-crisis-byte-magazine-january-1996/

> The bottleneck for growth in software orgs is sourcing developer
> talent

- http://brucefwebster.com/2008/04/15/the-wetware-crisis-the-themocline-of-truth/

> In many large or even medium-sized IT projects, there exists a *thermocline of
> truth*, a line drawn across the organizational chart that represents a barrier
> to accurate information regarding the project’s progress. Those below this
> level tend to know how well the project is actually going; those above it tend
> to have a more optimistic (if unrealistic) view.

-----------------------------------------------------------------------

## Jan 2021

- https://dzone.com/articles/flask-101-adding-editing-and-displaying-data
- https://www.codementor.io/@alirezamika/autoscraper-and-flask-create-an-api-from-any-website-in-less-than-5-minutes-and-with-fewer-than-20-lines-of-python-1chs1do4f5
- https://kanchanardj.medium.com/how-to-display-database-content-in-your-flask-website-8a62492ba892
- https://stackoverflow.com/questions/31859903/get-the-value-of-a-checkbox-in-flask
- https://flask-httpauth.readthedocs.io/en/latest/ HTTPBasicAuth for Flask
- https://stackoverflow.com/questions/22919182/flask-http-basicauth-how-does-it-work
- https://www.digitalocean.com/community/tutorials/how-to-add-authentication-to-your-app-with-flask-login
- https://techmonger.github.io/10/flask-simple-authentication/
- https://www.makeschool.com/academy/track/standalone/advanced-ios-development/flask-auth

> The last link shows how to hash password's with a random salt using
> `bcrypt`

- https://programmer.group/use-flask-pagination-and-laypage-plug-in-respectively.html

> Paginating results from mysql DB in Flask

-----------------------------------------------------------------------

https://media.ccc.de/v/rc3-257534-all_programming_language_suck_just_build_your_own_language_oriented_programming_with_racket#t=369

> Language Oriented Programming w/ Racket

https://www.compose.com/articles/what-postgresql-has-over-other-open-source-sql-databases/

> What makes Postgresql special

-----------------------------------------------------------------------

https://kubernetes.io/blog/2019/07/23/get-started-with-kubernetes-using-python/

> Simple flask app running in Kubernetes

-----------------------------------------------------------------------

https://kubernetes.io/docs/setup/best-practices/multiple-zones/

-----------------------------------------------------------------------

https://www.cockroachlabs.com/docs/cockroachcloud/deploy-a-python-to-do-app-with-flask-kubernetes-and-cockroachcloud.html

> deploy dockerized flask app to kubernetes

-----------------------------------------------------------------------

https://www.digitalocean.com/community/tutorials/how-to-build-and-deploy-a-flask-application-using-docker-on-ubuntu-18-04

> docker image containing flask, nginx, and uwsgi; deploy dockerized
> Flask app

-----------------------------------------------------------------------

https://cloud.google.com/load-balancing/docs/https

-----------------------------------------------------------------------

https://testdriven.io/blog/running-flask-on-kubernetes/

-----------------------------------------------------------------------

- https://aws.amazon.com/rds/aurora/global-database/
- https://cloud.google.com/spanner
  + https://cloud.google.com/spanner/docs/instances

> Globally distributed DB's (multi-region)

-----------------------------------------------------------------------

https://www.redhat.com/sysadmin/building-buildah

> Build container images with Dockerfiles and `buildah`

-----------------------------------------------------------------------

https://learning.oreilly.com/library/view/python-standard-library/0596000960/ch02s21.html

> *The Python Standard Library*, O'Reilly section on `crypt` module

https://docs.python.org/3/library/crypt.html

-----------------------------------------------------------------------

https://bitworks.software/en/2019-03-12-tornado-persistent-mysql-connection-strategy.html

> handling db connections in Python

-----------------------------------------------------------------------

https://github.com/nahuakang/cs50w-project1/blob/master/application.py

> Nice example of using the Flask "View Decorators" pattern to decorate
> routes that require login.

https://stackoverflow.com/questions/7478366/create-dynamic-urls-in-flask-with-url-for

> explanation of flask `url_for()` function

-----------------------------------------------------------------------

https://pythonbasics.org/flask-sessions/

Using Flask session to keep track of users' login status

-----------------------------------------------------------------------

https://www.youtube.com/watch?v=mt9QtHHx-9M

> Donald Trump - America's African President | The Daily Show

-----------------------------------------------------------------------

https://cs.wellesley.edu/~cs304/lectures/flask/activities-3.html

>Flask's `flash()` to give feedback to user

-----------------------------------------------------------------------

https://firstround.com/review/how-to-build-a-culture-of-ownership-and-other-engineering-leadership-tips-from-plaid-and-dropbox/

> How to Build a Culture of Ownership, and Other Engineering Leadership Tips
> from Plaid & Dropbox

-----------------------------------------------------------------------

https://technology.riotgames.com/news/leveraging-golang-game-development-and-operations

> How RiotGames uses Golang

-----------------------------------------------------------------------

https://www.technologyreview.com/2021/01/30/1017086/cdc-44-million-vaccine-data-vams-problems/

> Multiple issues plague Deloitte's VAMS system for the CDC

-----------------------------------------------------------------------

https://t.co/pGrj2lWQr6

> 90's TV

-----------------------------------------------------------------------

https://hackaday.com/2021/01/30/a-heat-reclaimer-for-your-woodstove-the-one-thing-its-not-is-cool/

> DIY Heat reclaimer for wood stove; the comments section is really
> enlightening

-----------------------------------------------------------------------

https://realpython.com/products/cpython-internals-book/

-----------------------------------------------------------------------

https://frostming.com/2021/01-22/introducing-pdm/

> PEP 582 was originated in 2018 and is still a draft proposal till the time I
> wrote this article, but I found out it is exactly what node_modules are in
> Python.

-----------------------------------------------------------------------

https://twitter.com/vanyaef/status/1355166372112822274

> "Napoleon Absent, Coalition Ascendant" new translation of von Clauswitz

-----------------------------------------------------------------------

http://academic.brooklyn.cuny.edu/education/progler/writings/published/batesonreview.html

> Book: Steps to an Ecology of Mind

-----------------------------------------------------------------------

- https://www.alexedwards.net/blog/organising-database-access
- https://www.alexedwards.net/blog/configuring-sqldb
- https://www.reddit.com/r/golang/comments/3s1cok/best_way_of_sharing_the_sqldbhandler/

> Concurrent DB access in Golang

-----------------------------------------------------------------------

https://lets-go.alexedwards.net/

> Ebook about building web apps in Golang

-----------------------------------------------------------------------

https://blog.tecladocode.com/flashing-messages-with-flask/

-----------------------------------------------------------------------

https://www.w3schools.com/html/html_form_input_types.asp

-----------------------------------------------------------------------

https://qoppac.blogspot.com/2021/02/so-you-want-to-be-quantsystematic-trader.html

-----------------------------------------------------------------------

## Feb 2021

https://www.arp242.net/go-easy.html Go is not an easy language

-----------------------------------------------------------------------

https://duretti.dev/writing/unintended-predatory-delay-is-still-delay

> Post about the promotion process at software companies

-----------------------------------------------------------------------

https://lostnintendohistory.github.io/DS-TV-OUT

> Nintendo DS Lite TV Out Restoration Project

-----------------------------------------------------------------------

## March 2021

https://www.beatport.com/genre/breaks-breakbeat-uk-bass/9/top-100

- Ondamike - Banging the Beats
- DJ30A & Huda Hudia - Fine Day (remix of Orbital orig)
- Ondamike - Up Up Up

-----------------------------------------------------------------------

- Mariya Takeuchi - Plastic Love (1985) Japanese "City Pop" genre
- dosii - 샴푸의 요정 (remake of 빛과 소금's original; city pop/chill style)
- Lay.bn - 도시
- 이루리 - dive
- 수잔 - 소년소녀 (city pop style)
- Bronze - Birds Eye View
- 이유림 - 슬픔이 없는 그 곳에 기다릴게
- 유키카 - 그늘
- dosii - 반투명
- BehindTheMoon - 도시의밤
- Wonder Girls - Rewind

-----------------------------------------------------------------------
Korean R&B

- Colde - 와르르 (WA-R-R)

-----------------------------------------------------------------------

## April 2021

https://www3.cs.stonybrook.edu/~skiena/373/videos/

> Professor Skiena's lectures to accompany his book, *The Algo Design Manual*


-----------------------------------------------------------------------

> detach from a container without stopping it

`C-p` + `C-q`

-----------------------------------------------------------------------

Coleman Hawkins - Smoke Gets in Your Eyes (Sax)

-----------------------------------------------------------------------

https://github.com/cmus/cmus/blob/master/Doc/cmus-tutorial.txt

-----------------------------------------------------------------------

사이먼도미닉 - 씻겨줘 (from 2018 앨범 다크룸)
사이먼도미닉 - 귀가본능
사이먼도미닉 - 얼라

https://www.youtube.com/watch?v=RIq5hQE_cgM

-----------------------------------------------------------------------

https://medium.com/krakensystems-blog/transforming-and-sending-nginx-log-data-to-elasticsearch-using-filebeat-and-logstash-part-1-61e4e19f5e54

-----------------------------------------------------------------------

## May 2021

https://www.reddit.com/r/rust/comments/56x4r3/are_golang_binaries_more_portable_than_binaries/

> Rust only links against `glibc`, whereas Golang static binaries
> will run on any supported platform and doesn't depend on `glibc`

-----------------------------------------------------------------------

https://blog.kchung.co/faster-python-with-go-shared-objects

> Google's Go programming language has the ability to generate shared
> libraries/objects which can be loaded by other applications. Those
> applications can then access the compiled Go code without having to have
> a Go compiler or know anything about Go.

> And if you didn't already know, Python has the ability to import code
> from shared objects!

-----------------------------------------------------------------------

https://stepik.org/lesson/9268/step/1

> Stepik Rust Lang Basics tutorial

https://docs.rust-embedded.org/discovery/index.html

> Embedded dev with Rust using the STM32F3Discovery $15 dev board

https://www.digikey.com/en/products/detail/stmicroelectronics/STM32F3DISCOVERY/3522185

https://github.com/rust-embedded/discovery/issues/274

> Note that new versions of the Discovery board (> 2020) use different
> components so ch 14-16 might not work as-is

> -----------------------------------------------------------------------

https://github.com/slhck/ffmpeg-normalize

> The normalization will be performed with the loudnorm filter from FFmpeg,
> which was originally written by Kyle Swanson. It will bring the audio to
> a specified target level. This ensures that multiple files normalized
> with this filter will have the same perceived loudness.

-----------------------------------------------------------------------

https://github.com/dhamaniasad/HeadlessBrowsers

> List of Headless Browsers

https://github.com/geziyor/geziyor

> Geziyor is a blazing fast web crawling and web scraping framework. It can
> be used to crawl websites and extract structured data from them. Geziyor
> is useful for a wide range of purposes such as data mining, monitoring
> and automated testing. Able to do *JS Rendering*


-----------------------------------------------------------------------

https://www.oreilly.com/library/view/jenkins-2-up/9781491979587/

*Jenkins 2: Up and Running* by Brent Laster May 2018

-----------------------------------------------------------------------

https://www.infoq.com/presentations/distributed-computing-medieval/

> Andy Walker discusses the principles of distributed computing used in
> medieval times, and the need to understand high latency, low reliability
> systems, bad actors, data migration, and abstraction.

-----------------------------------------------------------------------

https://www.accenture.com/us-en/blogs/cyber-defense/red-teaming-jenkins-attack-framework

JAF - Jenkins Attack Framework

-----------------------------------------------------------------------

https://developers.soundcloud.com/blog/how-to-successfully-hand-over-systems

-----------------------------------------------------------------------

https://stackoverflow.com/questions/8801729/is-it-possible-to-have-different-git-configuration-for-different-projects

`git config --add --local ..`

-----------------------------------------------------------------------

## June 2021

https://www.splicetoday.com/sports/kipping-as-ethos

> Half-assing it in workouts and in life

-----------------------------------------------------------------------

https://mbuffett.com/posts/kubernetes-setup/

Unironically Using K8s for my Personal Blog

-----------------------------------------------------------------------

https://www.entrepreneur.com/article/317602

The 5-hour rule used by Bill Gates, Jack Ma, and Elon Musk

> the importance of reading 1 hour per day, 5 days a week

-----------------------------------------------------------------------

https://www.brighttalk.com/webcast/6793/456663/homelab-cloud-native-ad-blocking-on-raspberry-pi

uses microk8s from Canonical

-----------------------------------------------------------------------

https://ubuntu.com/blog/microk8s-memory-optimisation

microk8s now works on systems with less than 1GB RAM!

-----------------------------------------------------------------------

https://www.youtube.com/watch?v=epvHqXxq4M0

Penta Tonic - Zeitgeist

-----------------------------------------------------------------------

Snippet - Get Public IP

`dig @ns1.google.com o-o.myaddr.l.google.com TXT +short | tr -d \"`

-----------------------------------------------------------------------

Another site to get your external IP

`http://checkip.amazonaws.com`

-----------------------------------------------------------------------

https://aws.amazon.com/premiumsupport/knowledge-center/route-53-find-ecs-support-dns-resolver/

-----------------------------------------------------------------------

http://lists.openstack.org/pipermail/openstack-discuss/2021-May/022718.html

Openstack moving its IRC channels from Freenode to OFTC

-----------------------------------------------------------------------

https://help.trello.com/article/821-using-markdown-in-trello

formatting text in Trello

-----------------------------------------------------------------------

http://cidrdb.org/cidr2020/papers/p16-haas-cidr20.pdf

SSD characteristics, Linux, and DB's


-----------------------------------------------------------------------



-----------------------------------------------------------------------



-----------------------------------------------------------------------



-----------------------------------------------------------------------



-----------------------------------------------------------------------



-----------------------------------------------------------------------



-----------------------------------------------------------------------



-----------------------------------------------------------------------



-----------------------------------------------------------------------



-----------------------------------------------------------------------



-----------------------------------------------------------------------



-----------------------------------------------------------------------



-----------------------------------------------------------------------



-----------------------------------------------------------------------



-----------------------------------------------------------------------



-----------------------------------------------------------------------

https://www.youtube.com/watch?v=aWWtj6cVwBg&list=PLEcQkygM8pMRAsNq4ieXWWuZV_A28P0Kk&index=3

> 아월 - 멍


https://www.digitalocean.com/community/tutorials/ufw-essentials-common-firewall-rules-and-commands
https://insanity.industries/post/simple-wifi/
https://wiki.archlinux.org/title/Iwd
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-working_with_zones
https://wiki.archlinux.org/title/installation_guide
