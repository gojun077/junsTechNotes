ImageMagick notes
====================

# Summary

- Created on: May 12 2023
- Created by: gopeterjun@naver.com
- Last Updated: May 12 2023

https://imagemagick.org/index.php

ImageMagick is an open source tool for editing images and can be used from
the command line to automate the editing and conversion of thousands of
images at a time.

# Topics

## generate icon from png file

Most websites have a small image, `favicon.ico`, that appears in the
browser tab. Icon image files must be of type `.png` or `.ico`.
Imagemagick can automatically convert an image file to .ico format with
the following:

```sh
$ convert <imgFileName> \
  -define icon:auto-resize=128,64,48,32,16 favicon.ico
```

