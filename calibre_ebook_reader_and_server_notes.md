Calibre Notes
================

# Summary

- Created on: May 9 2023
- Created by: gopeterjun@naver.com
- Last Updated: Jan 12 2023

Calibre is an open-source ebook library, reader, and server program written
in Python. This guide contains my notes on setting up Calibre in server
mode so you can read books remotely over http/https with a web browser.

# Topics

## Setup calibre server

create sqlite DB to store users and create new users

```sh
$ calibre-server --userdb /path/to/users.sqlite --manage-users

1) Add a new user
2) Edit an existing user
3) Remove a user
4) Cancel

What do you want to do? [1-4]: 1
Enter the username: admin
Enter the new password for admin:
Re-enter the new password for admin, to verify:
User admin added successfully!

$ calibre-server --userdb /path/to/users.sqlite --manage-users

1) Add a new user
2) Edit an existing user
3) Remove a user
4) Cancel


What do you want to do? [1-4]: 1
Enter the username: reader
Enter the new password for reader:
Re-enter the new password for reader, to verify:
User reader added successfully!
```

Note that when a new user is created, they have read/write permissions
on the Calibre Library. If you want to remove write permissions, you
must edit the user

```sh
$ calibre-server --userdb /path/to/users.sqlite --manage-users

1) Add a new user
2) Edit an existing user
3) Remove a user
4) Cancel

What do you want to do? [1-4]: 2
Existing user names:
admin, reader
Enter the username: reader

reader has read-write access
1) Show password for reader
2) Change password for reader
3) Change read/write permission for reader
4) Change the libraries reader is allowed to access
5) Cancel

What do you want to do? [1-5]: 3

Prevent reader from making changes (i.e. remove write access)? [y/n]: y

reader has readonly access
1) Show password for reader
2) Change password for reader
3) Change read/write permission for reader
4) Change the libraries reader is allowed to access
5) Cancel

What do you want to do? [1-5]: 5


1) Add a new user
2) Edit an existing user
3) Remove a user
4) Cancel

What do you want to do? [1-4]: 4
```

Launch calibre-server in daemon mode with auth enabled

```sh
calibre-server --access-log /tier2/calibre/access.log \
--log /tier2/calibre/server.log --max-log-size 50 --daemonize \
--port 8080 --userdb /tier2/calibre/users.sqlite --enable-auth \
/tier2/Calibre_Library
```

If you try opening `users.sqlite` with `sqlite3` you will see there is a
single table `users` and that **all the information is saved in
cleartext**!  Keep in mind that Calibre web server is not intended to be a
robust multi-user web app for serving books over the Internet; it should
only really be used on an internal network with trusted users.

Note that `calibre-server` does not support SSL/TLS, so if you want TLS
support you will have to use a reverse proxy like `nginx` in front of
`calibre-server` to terminate TLS and forward HTTP traffic on the backend
to `8080/tcp` or whatever port that `calibre-server` is listening on.

**Reference**:
https://manual.calibre-ebook.com/generated/en/calibre-server.html


## Use reverse proxy for HTTPS termination

The official calibre docs recommend using `nginx` or `apache` in front of
`calibre` for handling HTTPS. Calibre theoretically supports TLS certs with
the option flags `--ssl-certfile` and `--ssl-keyfile`, but when I tried
specifying the .crt and .key files to use for TLS, I got the following
errors in the calibre logs:

```
Unhandled exception in state: State: do_ssl_handshake Client: 100.66.200.49:58916 Request: WebSocketConnection
Traceback (most recent call last):
  File "/usr/lib64/calibre/calibre/srv/loop.py", line 625, in tick
    conn.handle_event(event)
  File "/usr/lib64/calibre/calibre/srv/loop.py", line 213, in do_ssl_handshake
    self.socket._sslobj.do_handshake()
ssl.SSLError: [SSL: HTTP_REQUEST] http request (_ssl.c:992)
```

This isn't a big problem because we can use `nginx` as a reverse proxy in
front of Calibre to handle TLS traffic and forward plain HTTP to
`localhost:8080` or whatever non-privileged port (`> 1024`) you wish to
use.

## Sample systemd .service file for calibre

Create a systemd service file `/etc/systemd/system/calibre-server.service`
containing the following:

```
[Unit]
Description=calibre ebook library headless server
After=network-online.target time-sync.target
Wants=network-online.target

[Service]
Type=simple
User=jundora
TimeoutSec=5min
KillSignal=SIGTERM
RemainAfterExit=yes
ExecStart=/home/jundora/bin/start_calibre-server.sh

[Install]
WantedBy=multi-user.target
```

Now enable and start:

```sh
sudo systemctl enable calibre-server
sudo systemctl start calibre-server
```

**References**:

- https://www.digitalocean.com/community/tutorials/how-to-create-a-calibre-ebook-server-on-ubuntu-20-04
- https://github.com/fkrueger/calibre-server_selinux
  + `readme.md` has an example `calibre-server.service`

**Note**: do not use the option `--enable-local-write` that is used
in the Digital Ocean guide; this allows unauthenticated local connections
to make changes, but we use authentication. Only the *admin* user has rights
to CRUD ebooks in the library.


## nginx reverse-proxy to handle HTTPS/TLS traffic

https://ssl-config.mozilla.org/

Start with the following `nginx.conf` containing Mozilla's pre-configured
SSL settings for nginx:

```nginx
# generated 2023-05-09, Mozilla Guideline v5.6, nginx 1.17.7, OpenSSL 1.1.1k, intermediate configuration
# https://ssl-config.mozilla.org/#server=nginx&version=1.17.7&config=intermediate&openssl=1.1.1k&guideline=5.6
server {
    listen 80 default_server;
    listen [::]:80 default_server;

    location / {
        return 301 https://$host$request_uri;
    }
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    ssl_certificate /path/to/signed_cert_plus_intermediates;
    ssl_certificate_key /path/to/private_key;
    ssl_session_timeout 1d;
    ssl_session_cache shared:MozSSL:10m;  # about 40000 sessions
    ssl_session_tickets off;

    # curl https://ssl-config.mozilla.org/ffdhe2048.txt > /path/to/dhparam
    ssl_dhparam /path/to/dhparam;

    # intermediate configuration
    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
    ssl_prefer_server_ciphers off;

    # HSTS (ngx_http_headers_module is required) (63072000 seconds)
    add_header Strict-Transport-Security "max-age=63072000" always;

    # OCSP stapling
    ssl_stapling on;
    ssl_stapling_verify on;

    # verify chain of trust of OCSP response using Root CA and Intermediate certs
    ssl_trusted_certificate /path/to/root_CA_cert_plus_intermediates;

    # replace with the IP address of your resolver
    resolver 127.0.0.1;
}
```

Within the `server {}` block for `ssl https`, add the following proxy
directive:

```nginx
...
    location / {
        proxy_pass http://127.0.0.1:<backendPortNum>;
    }
...
}
```

Note that in Fedora, there are no `/etc/nginx/sites-available/` or
`/etc/nginx/sites-enabled/` folders. On distros like Ubuntu, for example,
first you create a virtual site config file in the `sites-available` folder
and then create a symlink from the config file to the `sites-enabled`
folder.

Instead, you will create virtual host config files in `/etc/nginx/conf.d/`
and after restarting `nginx.service` the new virtual host will be active.

Here is my sample `/etc/nginx/conf.d/calibre.conf`

```nginx
# http server block for old clients like iPad1 that cannot use
# TLS 1.2+
server {
    listen 8081 default_server;
    listen [::]:8081;

    client_max_body_size 64M; default is 1M, but ebooks are big!

    location / {
        proxy_pass http://127.0.0.1:9080;
        # proxy buffer settings to speed up ebook downloads
        proxy_buffers 16 16k;
        proxy_buffer_size 16k;
        proxy_temp_file_write_size 16k;
        proxy_max_temp_file_size 64M;  # default setting is 1024m
    }
}

# https server block using Let's Encrypt certs via Tailscale
server {
    listen 8080 ssl http2;
    listen [::]:8080 ssl http2;
    server_name argonaut.finch-blues.ts.net;

    client_max_body_size 64M;

    location / {
        proxy_pass http://127.0.0.1:9080;
        # proxy buffer settings to speed up ebook downloads
        proxy_buffers 16 16k;
        proxy_buffer_size 16k;
        proxy_temp_file_write_size 16k;
        proxy_max_temp_file_size 64M;  # default setting us 1024m
    }

    ssl_certificate /var/lib/tailscale/certs/argonaut.finch-blues.ts.net.crt
    ssl_certificate_key /var/lib/tailscale/certs/argonaut.finch-blues.ts.net.key
    ssl_session_timeout 1d;
    ssl_session_cache shared:MozSSL:10m;  # about 40000 sessions
    ssl_session_tickets off;

    # curl https://ssl-config.mozilla.org/ffdhe2048.txt > /path/to/dhparam
    ssl_dhparam /etc/nginx/ssl-dhparams.txt;

    # intermediate configuration
    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
    ssl_prefer_server_ciphers off;

    # HSTS (ngx_http_headers_module is required) (63072000 seconds)
    add_header Strict-Transport-Security "max-age=63072000" always;

    # OCSP stapling
    ssl_stapling on;
    ssl_stapling_verify on;

    # verify chain of trust of OCSP response using Root CA and Intermediate certs
    ssl_trusted_certificate /var/lib/tailscale/certs/lets-encrypt-x3-cross-signed.pem;

    # replace with the IP address of your resolver
    resolver 192.168.21.1;
}
```

The virtual host defined by `calibre.conf` accepts HTTPS traffic on port
8080 and forwards plain HTTP to `localhost:9080`. Since `calibre` is only
listening on `localhost`, there is no need to create a firewall rule.

I also added a server block for http on `tcp 8081` to accomodate super old
browsers like Safari 5 that don't work with TLS 1.2+. Since calibre-server is
using authentication, (basic auth), old clients connecting to tcp 8081 will
still have to provide credentials to browse the Calibre library.

Note that for `ssl_trusted_certificate` I am using the Let's Encrypt
intermediate CA cert downloaded from
https://letsencrypt.org/certs/lets-encrypt-x3-cross-signed.pem

Normally if you are using `certbot` to manage your Let's Encrypt TLS certs,
`/etc/letsencrypt/...` will contain `fullchain.pem` which includes your
FQDN's TLS cert as well as the full chain of CA attestations.

Because I am using Let's Encrypt certs issued via Tailscale, however, in
`/var/lib/tailscale/certs/` only a private `.key` and certificate `.crt`
file exist; there is no `fullchain.pem`. This is no problem; you can `wget`
the intermediate CA chain pem directly from Let's Encrypt at the link above.

**References**:

- https://manual.calibre-ebook.com/server.html


## selinux and web server port bind restrictions

By default, selinux uses the context label `http_port_t` on `nginx`
network connections. This context label is only allowed to bind to the following
port OOTB:

```sh
$ sudo semanage port -l | grep http_port_t
http_port_t                    tcp      80, 81, 443, 488, 8008, 8009, 8443, 9000
```

I wanted nginx to bind to port `tcp 8081`, which is not included in the
selinux label above. It is possible to *add* ports to a label if they
aren't already defined in another label with

```sh
sudo semanage -a -t http_port_t -p tcp <portNum>
```

- `-a` / `--add`
- `-t` label type
- `-p` protocol

In the case of `tcp 8081`, however it already belongs to the context label
`transproxy_port_t`:

```sh
root@argonaut: # semanage port -l | grep 8081
transproxy_port_t              tcp      8081
```

However, since `tcp 8081` is already part of the label `transproxy_port_t`,
we cannot add tcp 8081 to label `http_port_t`. Instead, we have to modify
label `http_port_t` to include `tcp 8081` so the port can be included in
more than one SElinux context label:

```sh
$ sudo semanage port -m -t http_port_t -p 8081
$ sudo semanage port -l | grep 8081
http_port_t                    tcp      8081, 7908, 7907, 8086, 80, 81, 443, 488, 8008, 8009, 8443, 9000
transproxy_port_t              tcp      8081
```

As you can see above, `tcp 8081` is now included in *two* labels.

**Sources**:
- https://www.howtoforge.com/how-to-install-a-calibre-ebook-server-on-ubuntu-20-04/
- https://fedoraproject.org/wiki/Nginx
- https://bugzilla.redhat.com/show_bug.cgi?id=1048623
  + note about using `semanage port --modify` flag


## Firewall rules

Assuming that we are using nginx as a reverse proxy in front of calibre-server,
we need to add firewall rules permitting TCP 8080 and 8081.

### firewalld

```sh
sudo firewall-cmd --add-port 8080/tcp --zone home
sudo firewall-cmd --add-port 8081/tcp --zone home
sudo firewall-cmd --runtime-to-permanent
```

### ufw


## calibre clients

Calibre has a native desktop client that works quite well and has built-in
support for a variety of ebook formats such as `.epub`, `.azw3`, `.mobi`,
`.djvu`, `.cbr`, etc.

If `calibre-server` is running, you don't even need the desktop client;
you can connect to calibre with a web browser and enjoy a near-native experience
for `.epub` files (which are just compressed HTML) that render well in the
browser. `.pdf` support in the browser is not as good, as it tries to extract
raw text from the PDF and display that, but you can still download the native
PDF files and read those in the browser.

The biggest headache is for outdated browsers that use unsupported versions of
Javascript. For example, old Apple iPads no longer receive updates and are
forever stuck on ancient versions of Webkit engine in Safari, you will have
to access Calibre via a non-Javascript version of the mobile site. Luckily
Calibre is smart enough to detect these old browsers and give you the option
to connect to a non-JS page. The downside is that you will not be able to
read books within the browser, but must instead download the entire ebook and
open the file with `iBooks`, which supports `.epub` and `.pdf` OOTB.

If you are using a web browser in front of `calibre-server` as a reverse
proxy for HTTPS termination, note that ancient browsers like Safari < v6
cannot use TLS 1.2 or greater, so for all practical purposes old browsers
will not be able to open HTTPS sites. As a workaround for this, in the
nginx virtual host file for calibre, I listen on two ports, HTTP 8081 and
HTTPS 8080, and use 8081 for connecting to Calibre on my iPad1. This is
non-ideal and something I wouldn't do if I was exposing Calibre to the
Internet.

### Calibre OPDS endpoint

OPDS stands for Open Publication Distribution System and is an application
of the Atom Syndication Format that allows ereader clients to import books
from a book catalog using OPDS format. Calibre also natively supports OPDS
via the `/opds` endpoint. Simply point your ebook client application to
the Calibre server address followed by `/opds` and after authenticating
the client will be able to browse and download books.


## References

- https://manual.calibre-ebook.com/generated/en/calibre-server.html
  + list of cli options for calibre server
