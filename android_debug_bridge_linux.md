Setting up Android Debug Bridge on Linux
=============================================

# Summary

- Created: 23 Feb 2020
- Created by: gojun077@gmail.com
- Last Updated: 05 Aug 2024


Android Debug Bridge (henceforce `adb`) enables you to communicate with a
device running Android (usually a smartphone) from a system running Linux
(usually a dev notebook or PC). This guide will walk you through the steps
to get `adb` to work.

If you are lucky, you won't need any additional setup at all.  But if your
device is really new or rare, you will have to write your own `udev` rule
so that you will have permission to access the device on Linux.

# Walkthrough

## 1. Check that your device is being detected by your PC

Insert the USB charging cable to your smartphone/Android device and connect
the other end to your computer. Run `lsusb` and make sure that your device
is being detected. Here are some examples for different devices:

```sh
[archjun@latitude630 ~]$ lsusb  # HTC Sensation from 2012
Bus 002 Device 006: ID 0bb4:0f87 HTC (High Tech Computer Corp.)
...
[archjun@latitude630 bin]$ lsusb
Bus 002 Device 008: ID 1004:633e LG Electronics, Inc. G2 Android Phone [MTP mode]
...
junbuntu@pjU36JC:~$ lsusb
Bus 002 Device 013: ID 2717:ff08 Xiaomi Inc. Redmi Note 3 (ADB Interface)
...
```

An interesting note about the LG G3 (incorrectly listed as G2) above, is
that if it is in *MTP mode*, `adb devices` will not be able to detect
anything. From the G3, you must manually set the USB connection type from
`MDP` (external HD) to `PTP` (Photo Transfer mode) for it to be detected by
`adb`.

If your device doesn't show up in `lsusb` you may need to enable *Developer
Mode* from your smartphone, which will allow debugging over USB. After
Android software updates on my Android One Xiaomi A2, I have to go into
*System*, *Developer Options* and re-enable *USB Debugging*.

The last device listed above looks like a Xiaomi Redmi Note 3, but it is
actually from a Xiaomi A2 Android One phone.

## Start Android Debug Bridge daemon and check devices

For many Linux distro's the package name should simply be `adb`.
For Debian-based systems you can install with `sudo apt install
adb`. If the USB ID for your device is registered in a special
udev file for Android devices, you will be able to access it
through `adb` without using root privileges. If your device's USB ID
is not listed in in the `udev` file for Android devices, you
can either launch `adb` as root or you can create a custom `udev`
entry for your device.

Here is a opensource community-maintained udev list of USB ID's for
hundreds of Android devices (mostly smartphones):

https://github.com/M0Rf30/android-udev-rules


```sh
junbuntu@pjU36JC:~/Documents/android-udev-rules$ sudo adb start-server
* daemon not running; starting now at tcp:5037
* daemon started successfully
junbuntu@pjU36JC:~/Documents/android-udev-rules$ adb devices
List of devices attached
40b153e	device
```

If your device shows up as `unauthorized`, you will have to click *OK* on
a connection authorization dialog on your smartphone. To see this dialog, you
will probably need developer mode to be enabled on your Android phone.

`adb` correctly detects the Xiaomi A2 connected via USB. When you're done,
stop the daemon with `(sudo) adb kill-server`.  Note that even if you
launched the daemon with `sudo`, subsequent commands can be run as the
regular user.

## Get a shell on your Android device

Get a shell on your usb-connected Android device with
`adb shell`.

If your device has not been rooted, you will not have access to any of the
system partitions, but you can still `adb {push,pull}` to user storage
paths like `/sdcard/Movies`, etc. The paths that you can write to without
root will differ by device. On the Xiaomi A2, the user-writeable paths are
under `/sdcard/` and include:

```sh
jasmine_sprout:/sdcard $ ls -al
total 180
drwxrwx--x 22 root sdcard_rw 4096 2020-02-23 22:44 .
drwx--x--x  4 root sdcard_rw 4096 2009-01-01 01:00 ..
-rw-rw----  1 root sdcard_rw    0 2018-11-21 00:54 .fe_tmp
drwxrwx--x  2 root sdcard_rw 4096 2009-01-01 01:00 Alarms
drwxrwx--x  4 root sdcard_rw 4096 2018-11-20 22:05 Android
drwxrwx--x 32 root sdcard_rw 4096 2020-02-16 16:31 Books
drwxrwx--x  4 root sdcard_rw 4096 2018-11-30 11:20 DCIM
drwxrwx--x  2 root sdcard_rw 8192 2020-02-20 08:18 Download
drwxrwx--x  3 root sdcard_rw 4096 2019-04-06 08:20 KakaoTNavi
drwxrwx--x  4 root sdcard_rw 4096 2018-11-28 11:20 KakaoTalk
drwxrwx--x  3 root sdcard_rw 4096 2018-11-21 00:44 MIUI
drwxrwx--x  3 root sdcard_rw 4096 2020-02-23 22:44 Movies
drwxrwx--x  2 root sdcard_rw 4096 2018-11-21 00:54 Music
drwxrwx--x  2 root sdcard_rw 4096 2009-01-01 01:00 Notifications
drwxrwx--x  3 root sdcard_rw 4096 2019-12-24 16:33 Pictures
drwxrwx--x  2 root sdcard_rw 4096 2009-01-01 01:00 Podcasts
drwxrwx--x  2 root sdcard_rw 4096 2009-01-01 01:00 Ringtones
drwxrwx--x  2 root sdcard_rw 4096 2020-02-20 11:07 Slack
drwxrwx--x  6 root sdcard_rw 4096 2019-07-22 11:19 Telegram
drwxrwx--x  2 root sdcard_rw 4096 2019-04-05 18:36 bcs
drwxrwx--x  2 root sdcard_rw 4096 2018-12-18 08:52 bluetooth
drwxrwx--x  2 root sdcard_rw 4096 2019-12-24 16:33 mfax_claim
drwxrwx--x  2 root sdcard_rw 4096 2018-11-20 22:48 rdtmp
```

The device hostname is `jasmine_sprout` for the Xiaomi A2.


## Install downloaded .apk packages via adb

> When I first got my Onyx Boox Note Air Android 10 eink tablet in
> January 2021, the *Google Play* store was not installed due to
> recent troubles between China and the US (Huawei phones being banned
> from Android, export restrictions on US semiconductors to China,
> etc).

> I therefore downloaded the F-droid app store `.apk` to my computer
> and started adb server with `adb start-server`. Supposedly once the
> server is running you should be able to install from the terminal with

```sh
adb install /path/to/myfile.apk
```

but I wasn't able to get this working on my Onyx Boox Note Air. Instead
I had to manually click on the `apk` file through the Android UI once
I had used `adb push` to upload the file to my device.

### Weird behavior on Onyx Boox Note Air (Android 10)

> `adb install` fails despite my allowing *unknown packages* in
> *Settings -> Applications* on the Boox Note Air. Even
> `adb shell` fails to give me an interactive session on the
> device itself; I can only run one-off commands via `adb shell 'cmd'`


### Push files from adb server to device or pull from device to host

You can download and upload files to your Android device using:

```sh
adb push /path/to/file/on/adbserver/myfile /writable/path/on/device
adb pull /user/readable/path/myfile /local/path/
```

Note that when using `adb push/pull`, wildcards *do not work*.
You must specify the full filename to push or pull to or from
your device.

Writable paths on my Xiaomi A2 are mostly under `/sdcard/`. You can use
`adb push` to upload `.apk` packages to your phone's `Download` folder
and then use an Android File Manager app to select the `.apk` and install
it if `adb install` or `adb sideload` aren't working for you.


## Wireless Debugging

In the sections above, you have already enabled `USB Debugging` in the
*Developer Options* menu in Android. But did you know you can debug
Android devices over WiFi as well?

From the *Developer Options* menu, select *USB Debugging Security* and also
*Wireless Debugging*. Then tap on the item `Pair device with pairing code`.
You will be given a 6-digit code which you must then enter into `adb`.

```sh
$ adb pair 100.66.200.49:34599
Enter pairing code: 195157
Successfully paired to 100.66.200.49:34599 [guid=adb-izcmqwhipvin8hbm-2mONcw]
```

Make note of the IP and port you see listed in the field *IP address &
Port*. You will see that the port listed in this field is different from
the one above that you used to pair your device to the Android phone
wirelessly.

You can use a tool like `scrcpy` to connect to the wireless device using
the IP and port listed.

```sh
scrcpy --tcpip=100.67.253.26:40087
scrcpy 2.6.1 <https://github.com/Genymobile/scrcpy>
INFO: Connecting to 100.67.253.26:40087...
INFO: Connected to 100.67.253.26:40087
/usr/share/scrcpy/scrcpy-server: 1 file pushed, 0 skipped. 472.2 MB/s (71112 bytes in 0.000s)
[server] INFO: Device: [Xiaomi] Xiaomi Mi A2 (Android 13)
[server] ERROR: Could not create default audio encoder for opus
List of audio encoders:
    --audio-codec=aac --audio-encoder='OMX.google.aac.encoder'
    --audio-codec=flac --audio-encoder='OMX.google.flac.encoder'
INFO: Renderer: opengl
INFO: OpenGL version: 4.6 (Compatibility Profile) Mesa 24.2.0-asahi20240727
INFO: Trilinear filtering enabled
WARN: Demuxer 'audio': stream explicitly disabled by the device
INFO: Texture: 1080x2160
```

You can specify which audio codec to use with `--audio-codec=aac`, for
example:

```sh
scrcpy --audio-codec=aac --tcpip=192.168.21.223:40087
scrcpy 2.6.1 <https://github.com/Genymobile/scrcpy>
INFO: Connecting to 192.168.21.223:40087...
INFO: Connected to 192.168.21.223:40087
/usr/share/scrcpy/scrcpy-server: 1 file pushed, 0 skipped. 226.5 MB/s (71112 bytes in 0.000s)
[server] INFO: Device: [Xiaomi] Xiaomi Mi A2 (Android 13)
[server] WARN: Could not get initial audio timestamp
INFO: Renderer: opengl
INFO: OpenGL version: 4.6 (Compatibility Profile) Mesa 24.2.0-asahi20240727
INFO: Trilinear filtering enabled
INFO: Texture: 1080x2160
```

When the phone is connected via `scrcpy`, you can also see the device via
`adb devices -l`:

```sh
$ adb devices -l
List of devices attached
100.66.200.49:45335    offline product:chopin_global model:21061110AG device:chopin transport_id:1
```

Note that the POCO X3 GT is known as code name `choping`, while the
Redmi Note 10 Pro 5G is `chopin`.
