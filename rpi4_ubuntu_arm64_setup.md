RPi4 Ubuntu ARM64 Setup Guide
=================================

# Summary

- Created on: Sep 17 2021
- Created by: gojun077@gmail.com
- Last Updated: Apr 20 2023

Although the RPi3/4 official Raspberry Pi OS is based on Debian, other
Linux distros also work on RPi hardware. Archlinux ARM in particular is
finicky and requires lots of manual settings to prepare an SDCard to boot
an *Alarm* (Arch Linux on ARM) environment, but in contrast, **Ubuntu just
works OOTB**. Since I run my RPi's headless, this guide will only cover the
Ubuntu Server edition. It is also recommended to use the `rpi-imager`
utility which can download OS images to be written onto microSD cards and
can even write raw `.img` files to USB SSD drives so you can skip microSD
entirely and just boot from SSD.


# Topics

## Download Ubuntu ARM64 raw image

https://ubuntu.com/download/raspberry-pi

Make sure you download a raw image (Ubuntu uses `.img` format), not an
installation `.iso` for live USB install.

- `.img` O
- `.img.xz` O
  + first extract with `unxz`
- `.iso` X
  + ISO files are used to create installation USB sticks


## Serial Console setup

Unlike for ArchlinuxARM or RPi OS (Debian), *there is no need to edit*
`/boot/config.txt` and set `enable_uart=1`; Ubuntu ARM64 Server edition
images work with serial console OOTB.


## Initial Login

Use the temporary user:pass `ubuntu`:`ubuntu`; you will be prompted
to change the `ubuntu` user's password upon your first login.


## Set temporary IP on wired iface

```sh
# run as root
ip a add 192.168.0.11/24 dev eth0
ip r add default via 192.168.0.88
```

## Setup wifi

Refer to [this](./wpa_supplicant_dhcpcd_and_systemd_networkd_setup.md)
guide.


## Set hostname and time

```sh
hostnamectl status
hostnamectl set-hostname <myHost>
vim /etc/hosts
# make sure 127.0.1.1 is set to <myHost>
timedatectl status
timedatectl set-timezone Asia/Seoul
```

## Create new user

```sh
useradd -m -s /bin/bash junbuntu
passwd junbuntu
usermod -a -G sudo junbuntu
```

## Update System, Install Packages

```sh
# run as root
apt update
apt dist-upgrade
apt install lm-sensors ipython3 ncdu raspi-config
```


## Check RPi4B firmware and bootloader update status

First we need to tell `rpi-eeprom-update` to use the *stable* instead of
*critical* channel:

```sh
vim /etc/default/rpi-eeprom-update
# make sure the next line uses 'stable' instead of 'critical'
FIRMWARE_RELEASE_STATUS="stable"
```

Now run `rpi-eeprom-update` without any arguments to see the current
state of the bootloader and firmware:

```sh
# rpi-eeprom-update
*** UPDATE AVAILABLE ***
BOOTLOADER: update available
   CURRENT: Thu Sep  3 12:11:43 UTC 2020 (1599135103)
    LATEST: Tue Jul  6 10:44:53 UTC 2021 (1625568293)
   RELEASE: stable (/lib/firmware/raspberrypi/bootloader/stable)

  VL805_FW: Using bootloader EEPROM
     VL805: up to date
   CURRENT: 000138a1
    LATEST: 000138a1
```

To automatically apply bootloader/FW update at the next boot, add the
`-a` flag to the command:

```sh
# rpi-eeprom-update -a
*** INSTALLING EEPROM UPDATES ***

BOOTLOADER: update available
   CURRENT: Thu Sep  3 12:11:43 UTC 2020 (1599135103)
    LATEST: Tue Jul  6 10:44:53 UTC 2021 (1625568293)
   RELEASE: stable (/lib/firmware/raspberrypi/bootloader/stable)

  VL805_FW: Using bootloader EEPROM
     VL805: up to date
   CURRENT: 000138a1
    LATEST: 000138a1
   CURRENT: Thu Sep  3 12:11:43 UTC 2020 (1599135103)
    UPDATE: Tue Jul  6 10:44:53 UTC 2021 (1625568293)
    BOOTFS: /boot

EEPROM updates pending. Please reboot to apply the update.
To cancel a pending update run "sudo rpi-eeprom-update -r".
```

Bootloader and firmware updates add features like booting from USB3
ports, PXE network boot, lowered thermals, etc.


## Make permanent network settings

We will only use `systemd-networkd` for network device mgmt,
not `netplan.io` or `NetworkManager`. Create the files `eth.network`
and `wlan.network` in `/etc/systemd/network`

### `eth.network`

```
[Network]
Description=Settings for RPi4B ethernet iface
Address=192.168.0.10/24
DHCP=no
DNSSEC=no

[Match]
Name=eth*
```

Also note that Ubuntu by default uses `netplan`, which uses YAML configs
to define network settings. In `/etc/netplan` there are some files which
define some settings for `eth0`; you can delete these as we will not be
using `netplan`

```sh
rm /etc/netplan/10-rpi-ethernet-eth0.yaml
rm /etc/netplan/50-cloud-init.yaml
```

I was only able to get the wireless settings working after a system reboot;
On your first boot if you do a firmware upgrade on your RPi4, you might
have to go through *two* reboots before `eth0` comes up properly with the
settings in `/etc/systemd/network/eth.network`. If for some reason wifi
with wpa_supplicant comes up fine but only eth0 is not working, try to
manually set the static IP you want and then reboot one more
time. `systemctl restart systemd-networkd` was not sufficient for me to get
the wired iface setup with the desired IP.


### `wlan.network`

```
[Network]
Description=Settings for RPi4B rev 1.4 wireless iface
DHCP=ipv4
#ActivationPolicy=always-down

[Match]
Name=wlan*

[DHCPv4]
RouteMetric=20
```


## Enable temperature monitoring


```sh
systemctl enable lm-sensors
systemctl start lm-sensors
```


## Boot from SSD connected to USB 3.0 port on RPI4

Unlike the RPI3, the RPI4 has two USB 3.0 ports that offer I/O speeds
superior to microSD. If you connect a SATA SSD to one of the RPI4's USB
3.0 ports via a SATA-to-USB adapter, you no longer have to worry about the
limited lifespan of microSD cards which don't have wear-leveling and only
last for about 1 year under heavy write loads. The fastest microSD cards
have R/W of about 35MB/s, but through the USB3 ports on the RPI4, you can
get transfer speeds of over 100MB/s.

Before Ubuntu 21.10 for ARM64, booting an Ubuntu kernel from an SSD
required you to manually decompress the kernel `vmlinuz` to `vmlinux`
because the bootloader cannot handle compressed kernels. You also had to
edit `/boot/config.txt` to specify that the uncompressed kernel be used at
boot. Unfortunately, every time you did a kernel update you had to
decompress `vmlinuz` again for the new kernel version.

For versions 21.10 Impish Indra and above, however, you don't have to
do this anymore.

First launch `rpi-imager` as root and click the *CHOOSE OS* button, select
*Custom* and specify the Ubuntu 21.10 `.img` file for ARM64 which you
already downloaded. Connect your SSD drive to your Linux workstation via
SATA-to-USB3 cable and then click the button *CHOOSE STORAGE*, making sure
to select your SSD connected over USB. Finally click on the *WRITE* button.
You have to run `rpi-imager` as root. If you are on Wayland, GUI apps are
not supposed to be able to run as root, so you will have to execute `xhost
si:localuser:root` before trying to run `sudo rpi-imager`.

On your RPi4, run `rpi-eeprom-update` and verify that your firmware
version is at least `000138a1` (from Tue Jul 6 UTC 2021) or above. If it is
an older version, upgrade your firmware.

Power off your RPi4, remove any inserted microSD cards, and connect your
SSD to one of the blue-colored USB 3.0 ports. Connect your USB-to-serial
console cable to the correct GPIO pins and make sure that the TX and RX
pins are reversed from the serial cable pins to the GPIO pins,
i.e. assuming that `0` denotes your USB-to-serial adapter cable and `1` is
the RPI GPIO side, `TX_0 -> RX_1`, `RX_0 -> TX_1`.

One nice thing about booting from SSD over USB is that rebooting works now.
When a microSD card is inserted, `systemctl reboot` simply halts the system
and then you have to disconnect/reconnect power manually; but booting over
USB doesn't have this limitation; reboot just works!

## Note about SATA-to-USB3.0 adapters

Just any old SATA-to-USB3.0 adapter won't work for booting your SSD over
the RPI4's USB 3.0 ports. The adapter must support USB Attached SCSI
Protocol (UASP) and only a small number of adapters have firmware that
properly implements it. I have had good results with the `ASMedia`
`ASM1153E` chipset firmware on Linux. This is sold in Korea as the brand
*UGreen*. Here is a list of SATA-to-USB cables known to work:

https://jamesachambers.com/new-raspberry-pi-4-bootloader-usb-network-boot-guide/

Cheaper adapters which use JMicron chipsets/firmware have not worked, in my
experience (with a 2-bay hard disk dock and a SATA-to-USB3.0 cable with 12V DC
power jack).

In addition to UGreen, Startech also comes highly
recommended. Unfortunately, Startech isn't available in Korea and their
products are also really expensive.  I bought my UGreen SATA-to-USB3 cables
with UASP support for just 12,500 KRW each.


## Thoughts on various Linux Distros for Raspberry Pi4 / ARM SBC's

I have only run Linux in headless mode on all my RPi SBC's, both 3B and 4B
models, so I always choose to install the minimal images for each Linux
distro that come without any Desktop Environment or X11/Wayland. If you
install OS images (not `.iso` live environment images) onto microSD media,
99% of your installation attempts will succeed. Since I install into
headless only envo's, some distro's require editing `/boot/config.txt` or
other settings to enable the *serial console* (mandatory for me as I don't
connect a monitor to my RPi boards during installation). In the case of
Ubuntu, however, serial console support just works OOTB without the need
to edit any of the config files in `/boot`! +1 Ubuntu

The Raspberry Pi Foundation's Raspbian/RPiOS is a solid choice and
generally *just works*, but the biggest drawback of RPi OS is that as of
Jan 2022 the default OS image is still 32-bit only because the firmware
blobs for the BCM GPU are 32-bit only. There is an experimental 64-bit
version of RPi OS, but you won't be able to use GPU acceleration if you go
that route.

With microSD cards, I have used ArchlinuxARM for ARMv8 (64-bit) and Ubuntu
ARM64. I am a big fan of Archlinux and used to use it as my daily driver on
my x86-64 machines, but it is a big hassle to setup for ARM. The biggest
selling point of Archlinux and related distros is access to bleeding edge
packages that closely track upstream and the most recent Linux kernels.
Unfortunately, Archlinux ARM lags behind the main Archlinux project and I
experienced lots of bugs with the RPi4B v1.4 hardware revision not being
able to detect the eMMC microSD slot at all! The v1.2 hardware revision
didn't have this issue, however. Ubuntu never had issues like this,
probably because Canonical treats ARM64 as a first-class platform and
invests engineering resources into making sure that Ubuntu runs well on it.

As of Apr 2023, I have successfully boot from USB using Ubuntu ARM64,
Fedora 37 aarch64, and RPi OS 64-bit Minimal (Debian Bullseye).


### Deprecated notes on RPi4B boot from USB (2022)

I have only had success with RPiOS 32-bit (didn't get to try the 64-bit
beta for RPiOS) and Ubuntu 21.10+, although I attempted to boot from
SSD-over-USB with ArchlinuxARM and Manjaro Linux multiple times. Before
Ubuntu 21.10, booting the RPi4 from USB required a manual workaround of
decompressing `vmlinuz` to `vmlinux` because `uboot` doesn't support
compressed kernel images. Also every time you updated your kernel you had
to make sure to decompress the new kernel boot image so you could boot from
SSD over USB. Thankfully starting from Ubuntu 21.10 the boot kernel is no
longer compressed so you don't need to do anything to successfully boot
from SSD over USB!

So my advice as of Jan 2022 is to stick with Ubuntu 21.10 or above if
you want to boot from USB SSD with minimum hassle.
