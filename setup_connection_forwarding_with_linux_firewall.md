Connection Forwarding with Linux Firewall HOWTO
==================================================

# Summary

- Created on: Sep 20 2021
- Created by: gojun077@gmail.com
- Last Updated: Apr 12 2023

Connection forwarding can be achieved by enabling port forwarding and
creating `iptables` rules directly, but it is easier to use `firewalld` or
`ufw` (Uncomplicated Firewall) to enable connection forwarding instead of
working with the raw iptables rule chains directly. `ufw` is the simpler of
the two and is used most commonly on Debian/Ubuntu distros, while
`firewalld` is used more often on RHEL/Fedora distros.


# Topics

## Enable ufw or firewalld

**ufw**

Before enabling ufw, first run the following command to tell ufw
to allow ssh from any source address: `sudo ufw allow ssh`. Also
you want to allow multicast packets from your home wifi router.
Assuming your router has an address of `192.168.1.1`, add the
following rule: `sudo ufw allow in from 192.168.1.1`


```sh
sudo ufw enable
```

**firewalld**

```sh
sudo systemctl start firewalld
```


## Enable port forwarding

### sysctl settings (Common on all distros)

First check if port forwarding is enabled on your system:

```sh
$ sudo sysctl net.ipv4.ip_forward
net.ipv4.ip_forward = 0
```

A value other than `1` means it isn't enabled. To enable port forwarding in
the current session, you can use the `sysctl` command directly, but to make
the setting permanent, you should create a new file in `/usr/lib/sysctl.d/`
containing the setting `net.ipv4.ip_forward = 1`

```sh
$ sudo su -
# sysctl net.ipv4.ip_forward=1
net.ipv4.ip_forward = 1
# printf "%s\n" "net.ipv4.ip_forward=1" \
  > /usr/lib/sysctl.d/98-ipv4-forward.conf
```

### UFW-specific settings

You will need to edit 3 different files.

- add NAT rules to the file `/etc/ufw/before.rules`
- enable port forwarding in `/etc/ufw/sysctl.conf`
- allow forwarded packets in `/etc/default/ufw`

In `/etc/ufw/before.rules` add the NAT rules at the top of the file before
other rules are processed:

```
# NAT table rules
*nat
:POSTROUTING ACCEPT [0:0]
# Forward internal traffic out through wlan0 (for internet access)

-A POSTROUTING -s 192.168.0.0/24 -o wlan0 -j MASQUERADE
# enable 'COMMIT' below to process these nat table rules
COMMIT
```

In `/etc/ufw/sysctl.conf`, make sure `net/ipv4/ip_forward=1` is
uncommented. Note that the syntax differs from that of files in
`/usr/lib/sysctl.d/`; `ufw/sysctl.conf` uses `/` instead of `.` as
delimiters.

Finally, in `/etc/default/ufw`, make sure that
`DEFAULT_FORWARD_POLICY` is set to *ACCEPT* instead of *DROP*.

```
DEFAULT_FORWARD_POLICY="ACCEPT"
```

### firewalld-specific settings

Unlike `ufw`, the settings you need to make will be made through
`firewall-cmd`, which is the CLI tool used to control `firewalld`.
First you need to find the zone of the network interface which has
external network (Internet) access:

```sh
$ sudo firewall-cmd --get-zone-of-interface=enp5s0
public
```

If, for some reason, your network iface is not yet part of a *zone*,
you can add it with the following:

```sh
sudo firewall-cmd --zone=public --add-interface=enp5s0 --permanent
sudo firewall-cmd --reload
```

To apply the change to the current session, you must reload `firewall-cmd`.

Now that the external network iface has been added to a zone, you need to
allow IP masquerading in the zone with `--enable-masquerade`:

```
sudo firewall-cmd --zone-public --enable-masquerade --permanent
sudo firewall-cmd --reload
```

Note that you can disable IP masquerading with `--remove-masquerade`.

## Allow DNS traffic from the Internal Network

Internal network hosts that don't have Internet access on their own will
need query a nameserver, whether it be the gateway on the internal network
or a totally external one like `1.1.1.1` or `8.8.8.8`. Assume that the
internal network uses the subnet `192.168.0.0/24`.

### ufw

```sh
sudo ufw allow from 192.168.0.0/24 to any port 53
```

You can then view your rules with `ufw status numbered` and delete rules
with `ufw delete <number>`.


### firewalld

```sh
sudo firewall-cmd --zone public \
  --add-rich-rule='rule family="ipv4" source address="192.168.0.0/24" port protocol="tcp" port="53" accept' \
  --permanent
sudo firewall-cmd --reload
```

## Allow multicast traffic from your home wifi router

Multicast traffic goes to `224.0.0.0/24` and can come from your wifi router
itself or any clients connected to your wifi router. We want UFW to allow this
traffic. Let's assume your router is at `192.168.21.1` and that wireless
clients have addresses in the range `192.168.21.0/24`.

### ufw

```sh
sudo ufw allow from 192.168.21.0/24 to 224.0.0.0/24
sudo ufw allow in from 192.168.21.1
```


## apt-cacher-ng

This is a tool for Debian-based distros that allows you to cache packages
locally that can be used by other Debian machines in your local network.
The other machines must be able to connect to `TCP 3142`.

Here is an example rule for `ufw` that allows traffic to TCP 3142 from the
internal network:

```sh
sudo ufw allow from 192.168.21.0/24 proto tcp to any port 3142
```

If you are using firewalld, instead of creating a rich rule it is better to
add this rule to a zone you are using just for your internal network.


## Sane ssh rules

### UFW

Only allow ssh from internal IP range:

```sh
sudo ufw allow from 192.168.21.0/24 proto tcp to any port 22
```

Allow ssh from tailscale tunnel interface

```sh
sudo ufw allow in on tailscale0
```

### Firewalld

```sh
sudo firewall-cmd --add-interface=tailscale0 --zone=trusted \
  --permanent
sudo firewall-cmd --reload
```

## Misc

**Note**: the following is deprecated an doesn't reflect my current
setup

> I previously didn't have any G/W setup on `192.168.0.0/24` which is my
> internal 1 Gbit LAN for my RPi cluster. It uses a TP SG-108 TPLink 8-port
> 1-Gbit dumb switch (doesn't support VLAN tagging, but does support
> QoS). I did, however, enable packet forwarding and IP masquerading on
> `pj1` so that an old ASUS U36JC notebook connected to the LAN can get
> internet access through `pj1`.

> From `pj0` I did the following to specify that the G/W for the internal
> LAN should be `192.168.0.11` (`pj1`).

```sh
sudo ip r del 192.168.0.0/24
sudo ip r add 192.168.0.11 dev eth0
sudo ip r add 192.168.0.0/24 via 192.168.0.11
```

https://serverfault.com/questions/953198/why-nexthop-has-invalid-gateway-when-it-seems-to-be-defined


## References

- https://eatpeppershothot.blogspot.com/2016/02/internet-connection-sharing-through.html
- https://github.com/gojun077/jun-dotfiles/tree/main/ufw
- https://www.digitalocean.com/community/tutorials/ufw-essentials-common-firewall-rules-and-commands
