UFW notes
================

# Summary


Tips and tricks


# Topics


## Allow traffic from IPv4 Range to specific port

Allow TCP 10250 from the internal network to enable k3s metrics server.


```sh
sudo ufw allow from 192.168.21.0/24 proto tcp to any port 10250
```

## Allow all trafic to all ports on specific interface

```sh
sudo ufw allow in on tailscale0 to any
```

In the example above, all traffic coming from the `tailscale0` iface
will be allowed.
