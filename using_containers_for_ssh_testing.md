Using Containers on Linux for SSH Testing
=============================================

# Summary
- Created on: Sep 11 2020
- Created by: gojun077@gmail.com
- Last Updated: Apr 11 2022

Podman is compatible with `docker` and can run docker containers without
the docker daemon. Podman is also used by Kubernetes but can also be used
stand-alone.

Although you can run docker commands as a regular (non-root) user, the
docker daemon that carries out those requests runs as root. So,
effectively, regular users can make requests through their containers that
harm the system, without there being clarity about who made those requests.

Podman, by contrast, is not a SPOF daemon and allows the execution of
containers as non-root user.

In this guide, I plan to install `sshd` inside container environments for
the mass-testing of ssh command automation against dozens of containers.  I
don't have enough RAM to concurrently run dozens of Linux VM's, but with my
old core i5 Gen1 from 2009 with 8GB of DDR2 RAM can handle dozens of
containers without difficulty.

In general, you don't need `sshd` running inside your containers as all
container runtimes offer native commands for getting a shell in the
container without connecting via `ssh`:

- `lxc-attach -n <containerName>`
- `docker attach <containerName>`
- `podman attach <containerName>`

Although I know this, I specifically want to test behavior of the `sshd`
daemon, so I have explicitly installed `sshd` in various containers.


# Topics

## Setting up podman on Ubuntu 20.04

https://podman.io/getting-started/installation.html

```sh
echo "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_20.04/ /" | sudo tee /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
curl -L https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_20.04/Release.key | sudo apt-key add -
sudo apt update
sudo apt dist-upgrade
sudo apt install podman buildah fuse-overlayfs
```

### `podman pull` image from Docker Hub

```sh
junbuntu@pjU36JC:~$ podman pull alpine:latest
Trying to pull docker.io/library/alpine:latest...
Getting image source signatures
Copying blob df20fa9351a1 done
Copying config a24bb40132 done
Writing manifest to image destination
Storing signatures
a24bb4013296f61e89ba57005a7b3e52274d8edd3ae2077d04395f806b63d83e
junbuntu@pjU36JC:~$ podman images
REPOSITORY                TAG     IMAGE ID      CREATED       SIZE
docker.io/library/alpine  latest  a24bb4013296  3 months ago  5.85 MB
```

When trying to pull an image as a non-root user, you may get the following
error:

```
ERRO[0000] User-selected graph driver "overlay" overwritten by graph driver "vfs" from database - delete libpod local files to resolve 
Error: vfs driver does not support overlay.mount_program options
```

To deal with this issue, navigate to
`~/.local/share/containers/storage/libpod` and remove the file
`bolt_state.db`


### Run the downloaded container image

```sh
junbuntu@pjU36JC:~$ podman run -it alpine ash
/ # ls
bin    etc    lib    mnt    proc   run    srv    tmp    var
dev    home   media  opt    root   sbin   sys    usr
/ # ps
PID   USER     TIME  COMMAND
    1 root      0:00 ash
    8 root      0:00 ps
/ # exit
```

Once you exit from the session, the container will also terminate because
the running process is `ash`. In most cases you will specify `-d` to run
your container in detached mode.

If you want to run your container in the background, execute with `podman
run -dt --rm alpine <executable>`

Then to get a shell in the container running in the background:

```sh
$ podman ps
CONTAINER ID  IMAGE                            COMMAND  CREATED        STATUS            PORTS   NAMES
12f7c2c7e9ee  docker.io/library/alpine:latest  /bin/sh  2 minutes ago  Up 2 minutes ago          pedantic_golick
junbuntu@pjU36JC:~$ podman attach pedantic_golick
ERRO[0000] normal attach
/ # exit
junbuntu@pjU36JC:~$ podman ps
CONTAINER ID  IMAGE   COMMAND  CREATED  STATUS  PORTS   NAMES
```

### Set up networking

Rootless containers cannot be assigned IP addresses, but port forwarding to
localhost is OK for rootless containers.

```sh
junbuntu@pjU36JC:~$ podman run -dt --rm --publish-all -p 8080:8080/tcp alpine
2f7def772e0369c21b3874f9251b4325b9fb50ed06b8c4a8387b171a11b2d109
junbuntu@pjU36JC:~$ podman port -l
8080/tcp -> 0.0.0.0:8080
```

If you want to have bridged networking and ip addresses assigned to your
containers with `podman`, you will have to invoke it with `sudo`:

```sh
junbuntu@pjU36JC:~$ sudo podman run -dit --name centos1 centos:7
[sudo] password for junbuntu:
Trying to pull docker.io/library/centos:7...
Getting image source signatures
Copying blob 75f829a71a1c done
Copying config 7e6257c9f8 done
Writing manifest to image destination
Storing signatures
acd82a2a6c2721a3cd15638feefa5bff388f8daa0151907ee7f5aff32dffdce0
junbuntu@pjU36JC:~$ sudo podman ps
CONTAINER ID  IMAGE                       COMMAND    CREATED        STATUS            PORTS   NAMES
acd82a2a6c27  docker.io/library/centos:7  /bin/bash  9 seconds ago  Up 8 seconds ago          centos1
junbuntu@pjU36JC:~$ podman ps
CONTAINER ID  IMAGE   COMMAND  CREATED  STATUS  PORTS   NAMES
```

Note that running `podman ps` as regular user doesn't show any running
containers, but that running it with `sudo` does show the container
launched as root.

To find the IP address of the container you just launched:

```sh
$ podman inspect centos1 | grep -i ipaddress
[sudo] password for junbuntu:
            "IPAddress": "10.88.0.2",
```

You will also notice that a new bridge interface has been created to
communicate with your rootfull containers:

```sh
$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
...
6: cni-podman0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 72:42:0c:7b:7c:74 brd ff:ff:ff:ff:ff:ff
    inet 10.88.0.1/16 brd 10.88.255.255 scope global cni-podman0
       valid_lft forever preferred_lft forever
    inet6 fe80::7042:cff:fe7b:7c74/64 scope link
       valid_lft forever preferred_lft forever
7: veth34900f75@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master cni-podman0 state UP group default
    link/ether aa:93:43:82:1d:0e brd ff:ff:ff:ff:ff:ff link-netns cni-c7ce0955-153d-fff7-c485-e6e268469c3f
    inet6 fe80::a893:43ff:fe82:1d0e/64 scope link
       valid_lft forever preferred_lft forever
...
```

The bridge created by podman is named `cni-podman0` and it created a
virtual ethernet iface named `veth34900f75@if3` for the container you just
launched.

### Setup SSH Daemon and misc packages

The ip for container `centos1` is `10.88.0.2` and it responds to `ping`,
but no other services are running on the container and neither `upstart`
nor `systemd` are available within. If you want to test `ssh` against your
centos7 container, you will need to install the `openssh-server` package,
generate `host_rsa` and `host_dsa` keys, edit `/etc/ssh/sshd_config` to
specify using the `host_dsa` key instead of the elliptic curve keys.

By default most containers don't have `sudo` installed; The centos7 we are
using is no exception, and only the root user exists. We will have to
install the `sudo` package, which will create `/etc/sudoers` and add the
`visudo` and `sudo` commands.


```sh
[root@acd82a2a6c27 /]# yum install openssh-server sudo
...
[root@acd82a2a6c27 /]# ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key -N ''
Generating public/private rsa key pair.
Your identification has been saved in /etc/ssh/ssh_host_rsa_key.
Your public key has been saved in /etc/ssh/ssh_host_rsa_key.pub.
...
[root@acd82a2a6c27 /]# ssh-keygen -t ecdsa -f /etc/ssh/ssh_host_ecdsa_key -N ''
Generating public/private dsa key pair.
Your identification has been saved in /etc/ssh/ssh_host_ecdsa_key.
Your public key has been saved in /etc/ssh/ssh_host_ecdsa_key.pub.

[root@acd82a2a6c27 /]# ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -N ''
Generating public/private dsa key pair.
Your identification has been saved in /etc/ssh/ssh_host_ecdsa_key.
Your public key has been saved in /etc/ssh/ssh_host_ecdsa_key.pub.

[root@acd82a2a6c27 /]# vi /etc/ssh/sshd_config
```

Inside `/etc/ssh/sshd_config` make sure that the following looks like:

```sh
HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_dsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key
...
PubkeyAuthentication yes
```

Note that `host_dsa` keys are now deprecated as of OpenSSH v7 as the dsa
algorithm is no longer secure.

We don't want to ssh into the containers as `root`, so we will create a new
user `foo`, create the `/home/foo/.ssh` directory, and the file
`/home/foo/.ssh/authorized_keys`

```sh
[root@3edc8304f18d /]# useradd foo
[root@3edc8304f18d /]# passwd foo
Changing password for user foo.
New password:
BAD PASSWORD: The password fails the dictionary check - it is based on a dictionary word
Retype new password:
passwd: all authentication tokens updated successfully.
[root@3edc8304f18d /]# su - foo
[foo@3edc8304f18d ~]$ mkdir .ssh
[foo@3edc8304f18d ~]$ touch authorized_keys
[foo@3edc8304f18d ~]$ vi authorized_keys
# add ssh pubkey to authorized_keys
[foo@3edc8304f18d ~]$ chmod 400 authorized_keys
```

Finally, you need to start the sshd daemon directly as containers do not
contain `systemd` or other service managers.

```sh
/usr/sbin/sshd -D &
```

Now to allow user `foo` to use sudo without a password:

```sh
echo "foo ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
```

Once you exit from your `podman attach <containerName` session, however,
your centos container will stop running and you will have to do all the
manual steps above all over again on a new container.

To save your work, commit changes in the current container before
exiting from the terminal session.

```sh
junbuntu@pjU36JC:~$ sudo podman ps
[sudo] password for junbuntu:
CONTAINER ID  IMAGE                       COMMAND    CREATED      STATUS          PORTS   NAMES
3d209d3e5487  docker.io/library/centos:7  /bin/bash  2 hours ago  Up 2 hours ago          centos5
3edc8304f18d  docker.io/library/centos:7  /bin/bash  2 hours ago  Up 2 hours ago          centos2
578de2cb2070  docker.io/library/centos:7  /bin/bash  2 hours ago  Up 2 hours ago          centos3
fd8ebfdf16f9  docker.io/library/centos:7  /bin/bash  2 hours ago  Up 2 hours ago          centos4
junbuntu@pjU36JC:~$ sudo podman commit centos2 -a gojun077
Getting image source signatures
Copying blob 613be09ab3c0 skipped: already exists
Copying blob 679d5d6dec29 done
Copying config 1117ac9d09 done
Writing manifest to image destination
Storing signatures
1117ac9d09d4963f8855365a032fe6a913a16ae8f7028a5fd6199de275705967
junbuntu@pjU36JC:~$ sudo podman images
REPOSITORY                TAG     IMAGE ID      CREATED         SIZE
<none>                    <none>  1117ac9d09d4  34 seconds ago  306 MB
docker.io/library/centos  7       7e6257c9f8d8  4 weeks ago     211 MB
junbuntu@pjU36JC:~$ sudo podman image tag 1117ac9d09d4 localhost:centos7_sshd
junbuntu@pjU36JC:~$ sudo podman images
REPOSITORY                TAG           IMAGE ID      CREATED        SIZE
localhost/localhost       centos7_sshd  1117ac9d09d4  5 minutes ago  306 MB
docker.io/library/centos  7             7e6257c9f8d8  4 weeks ago    211 MB
```

By default, images that are created locally do not have any *repository* or
*tag*. You have to add them with `podman image tag`:

```sh
$ podman image tag <imageID> repo/name1/name2:version
```

### Remove unneeded containers

Note that `podman ps` only shows running containers. The containers that
you created earlier but are not currently running are still on your system
in `/var/lib/containers` for rootful containers, and in
`~/.local/share/containers` for rootless containers.

You can view all containers, running or not with `podman ps --all` and you
can remove no longer needed containers with `podman rm`

```sh
junbuntu@pjU36JC:~$ sudo podman ps --all
CONTAINER ID  IMAGE                       COMMAND    CREATED      STATUS                        PORTS   NAMES
3d209d3e5487  docker.io/library/centos:7  /bin/bash  2 hours ago  Exited (137) 2 minutes ago            centos5
3edc8304f18d  docker.io/library/centos:7  /bin/bash  2 hours ago  Exited (137) 2 minutes ago            centos2
578de2cb2070  docker.io/library/centos:7  /bin/bash  2 hours ago  Exited (137) 2 minutes ago            centos3
acd82a2a6c27  docker.io/library/centos:7  /bin/bash  2 hours ago  Exited (0) About an hour ago          centos1
fd8ebfdf16f9  docker.io/library/centos:7  /bin/bash  2 hours ago  Exited (137) 2 minutes ago            centos4
junbuntu@pjU36JC:~$ sudo podman rm -a
```

### Launch sshd-enabled image

When you launch a container, you can specify a command for it to
execute. The command should be the final argument:

```sh
junbuntu@pjU36JC:~$ sudo podman run -dit --name test2 localhost:centos7_sshdC /usr/sbin/sshd -D
a06ddd1dcc2108687bc406edcf3c311740171c36774ae9170a33bd096d4e4e4f
junbuntu@pjU36JC:~$ sudo podman inspect test2 | grep -i ipaddress
            "IPAddress": "10.88.0.8",
junbuntu@pjU36JC:~/.local/share/containers$ ssh -i ~/.ssh/archjun_rsa foo@10.88.0.8
Warning: Permanently added '10.88.0.8' (ECDSA) to the list of known hosts.
Last login: Sat Sep 12 14:00:35 2020 from gateway
[foo@a06ddd1dcc21 ~]$
[foo@a06ddd1dcc21 ~]$ exit
logout
Connection to 10.88.0.8 closed.
```

As you can see, the sshd daemon started successfully and we were able to
ssh into the container created from the custom image we created in the
previous section.

You can launch multiple containers from the same image with a bit of bash:

```sh
for i in {1..20}; do
  sudo podman run -dt --name test${i} localhost:centos7_sshdC \
  /usr/sbin/sshd -D
done
```

And to get all the ip addresses assigned to the containers:

```sh
junbuntu@pjU36JC:~$ for i in {1..20}; do
  sudo podman inspect test${i} | grep -i ipaddress \
  | awk -F ": " '{print $2}' | tr -d '"' | tr -d ','
done
10.88.0.51
10.88.0.52
10.88.0.53
10.88.0.54
10.88.0.55
10.88.0.56
10.88.0.57
10.88.0.58
10.88.0.59
10.88.0.60
10.88.0.61
10.88.0.62
10.88.0.63
10.88.0.64
10.88.0.65
10.88.0.66
10.88.0.67
10.88.0.68
10.88.0.69
10.88.0.70
```

Finally, stopping all containers can be done much more simply:

```sh
sudo podman stop -a
```

You can see all your stopped containers with `sudo podman ps -a`:

```sh
junbuntu@pjU36JC:~/bin$ sudo podman ps -a
[sudo] password for junbuntu:
CONTAINER ID  IMAGE                             COMMAND            CREATED       STATUS                   PORTS   NAMES
031560b7b9dc  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  24 hours ago  Exited (0) 23 hours ago          test11
03297079abab  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  24 hours ago  Exited (0) 23 hours ago          test9
0d898c252d3a  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  24 hours ago  Exited (0) 23 hours ago          test7
14f127bbf01a  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  24 hours ago  Exited (0) 23 hours ago          test19
2691a3bb6a74  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  24 hours ago  Exited (0) 23 hours ago          test15
29b04dc93533  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  24 hours ago  Exited (0) 23 hours ago          test20
37a4594b21c6  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  24 hours ago  Exited (0) 23 hours ago          test3
607b898c9470  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  24 hours ago  Exited (0) 23 hours ago          test6
7b1e16e598e8  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  24 hours ago  Exited (0) 23 hours ago          test16
7fc9cfe5c68d  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  24 hours ago  Exited (0) 23 hours ago          test4
8b83e81150bc  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  24 hours ago  Exited (0) 23 hours ago          test13
96e52dcf20b6  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  24 hours ago  Exited (0) 23 hours ago          test17
9922739c5a9f  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  24 hours ago  Exited (0) 23 hours ago          test18
a06ddd1dcc21  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  25 hours ago  Exited (0) 23 hours ago          test2
aa303cf9c072  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  24 hours ago  Exited (0) 23 hours ago          test10
bbd6bf80c849  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  24 hours ago  Exited (0) 23 hours ago          test8
c4feeebc96f6  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  24 hours ago  Exited (0) 23 hours ago          test1
d043cf2bae2f  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  24 hours ago  Exited (0) 23 hours ago          test5
e8382662f657  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  24 hours ago  Exited (0) 23 hours ago          test14
fe9c7bb2ea6c  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  24 hours ago  Exited (0) 23 hours ago          test12
```

Once you're ready to start your stopped containers again, run `sudo podman
start <containerName>`

```sh
for i in {1..20}; do
  sudo podman start test${i}
done
```


### Create a pod containing multiple containers

pods are an interesting concept in which multiple containers grouped within
a pod all share the same network namespace and IP address of the pod;
individual containers can be accessed through port forwarding mapping the
pod's external port to the container's internal port. For my goal of
testing `ssh` against dozens of "hosts" (containers), this is not so
helpful, however.


```sh
junbuntu@pjU36JC:~$ sudo podman pod create
a1c5ab70771bef2d8af8163e722e6968bbd9842b9cefe8377dc5eb0fcc7940cb
junbuntu@pjU36JC:~$ sudo podman pod ps
POD ID        NAME             STATUS   CREATED        # OF CONTAINERS  INFRA ID
a1c5ab70771b  tender_poincare  Created  9 seconds ago  1                e02a51e40e57
junbuntu@pjU36JC:~$ sudo podman ps -a --pod
CONTAINER ID  IMAGE                             COMMAND            CREATED         STATUS            PORTS   NAMES               POD ID        PODNAME
a06ddd1dcc21  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  9 minutes ago   Up 9 minutes ago          test2
e02a51e40e57  k8s.gcr.io/pause:3.2                                 49 seconds ago  Created                   a1c5ab70771b-infra  a1c5ab70771b  tender_poincare
junbuntu@pjU36JC:~$ sudo podman run -dt --pod tender_poincare localhost:centos7_sshd /usr/sbin/sshd -D
19990dd7c1ed560df1487dda28b8a163294d20d627239b8285af8974ce76c5b9
junbuntu@pjU36JC:~$ sudo podman ps -a --pod
CONTAINER ID  IMAGE                             COMMAND            CREATED         STATUS             PORTS   NAMES               POD ID        PODNAME
19990dd7c1ed  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  5 seconds ago   Up 4 seconds ago           friendly_williams   a1c5ab70771b  tender_poincare
a06ddd1dcc21  localhost/localhost:centos7_sshd  /usr/sbin/sshd -D  11 minutes ago  Up 11 minutes ago          test2
e02a51e40e57  k8s.gcr.io/pause:3.2                                 2 minutes ago   Up 5 seconds ago           a1c5ab70771b-infra  a1c5ab70771b  tender_poincare
junbuntu@pjU36JC:~$ sudo podman inspect friendly_williams | grep -i ipaddress
            "IPAddress": "",
```

As you can see, the container `friendly_williams` launched within pod
`tender_poincare` does not have an ip address.

https://developers.redhat.com/blog/2019/01/15/podman-managing-containers-pods


### Monitor the status of all your containers

There is a command `podman stats` which must be executed as root that
displays the CPU, Memory, Network, and Disk usage of each running
container.

```
ID            NAME     CPU %   MEM USAGE / LIMIT  MEM %   NET IO             BLOCK IO      PIDS
3d209d3e5487  centos5  --      2.265MB / 8.136GB  0.03%   1.258kB / 61.32kB  -- / --       1
3edc8304f18d  centos2  --      106.3MB / 8.136GB  1.31%   399.6kB / 13.3MB   0B / 61.15MB  2
578de2cb2070  centos3  --      2.22MB / 8.136GB   0.03%   1.258kB / 62.58kB  -- / --       1
fd8ebfdf16f9  centos4  --      2.073MB / 8.136GB  0.03%   1.258kB / 61.95kB  -- / --       1
```

Note that `podman stats` will exit as soon as any running container stops.


### References

- https://developers.redhat.com/blog/2019/02/21/podman-and-buildah-for-docker-users/
- https://github.com/containers/podman
- https://github.com/containers/podman/blob/master/docs/tutorials/rootless_tutorial.md
- https://til.cybertec-postgresql.com/post/2019-09-21-Using-Podman-in-bridged-network-mode/
- https://github.com/containers/buildah/tree/master/demos
- https://podman.io/getting-started/network.html
