macOS setup guide
=====================

# Summary

- Created on: 12 Apr 2023
- Created by: gojun077@gmail.com
- Last Updated: 03 Aug 2024

This contains my notes of setting up macOS. Most of my work laptops for the
past 8 years have been Macbooks. I have removed the company-specific bits
from my notes and gathered general tips and config snippets in this file.

# Topics

## Enable mouse right-click

Enter *System Settings* -> *Mouse* menu and enable *Secondary click*
`Click Right Side`.

By default, right-click when using the trackpad, is set to *Click with Two
Fingers*, and can be changed from *System Settings* -> *Trackpad*.

## Setup git

Before MacOS Catalina, you could install `git` directly from `brew`, but
for recent versions of MacOS, you need to reinstall *XCode Command Line
Tools*

https://stackoverflow.com/questions/58280652/git-doesnt-work-on-macos-catalina-xcrun-error-invalid-active-developer-path

```sh
xcode-select --install
```

For more information about setting up `git` as well as ssh key auth with
Github, Gitlab, Bitbucket, etc, refer to [git_tips.md](git_tips.md)

## Install keybase for macos

https://keybase.io/download

Download the `.dmg` for MacOS (versions available for both Intel amd64 and
Apple Silicon arm64 architectures)

To use Keybase File System, you need to enable a closed-source kernel
extension *FUSE for macOS*. Go into Keybase Settings -> Files and click the
checkbox for *FUSE for macOS* and click the green *Yes, enable* button.

You will then be taken to the Privacy & Security menu where you must
press the button *Enable System Extensions*. You will then be prompted
to reboot into Recovery mode:

> To enable system extensions, you need to modify your security settings in
> the Recovery environment. To do this, shut down your system. Then press
> and hold the Touch ID or power button to launch Startup Security Utility
> and enable kernel extensions from the Security Policy button.

You should see loading startup options and then an *Options* gear icon to
the right and a hard disk icon *Macintosh HD* to the left.

You will then be prompted to enter your user password. You will then see a
menu with 4 choices, *Restore from Time Machine* ... to *Disk Utility*.
You don't need to select any of these; from the top left menu next to the
apple icon, click on the *Utilities* menu and select *Startup Security
Utility*. You will then have to enter your user password to unlock the
FileVault encrypted hard disk. Then in the *Security Policy* for "Macintosh
HD", select *Reduced Security* and check the box *Allow user management of
kernel extensions from identified developers*. You will then be prompted
once more to enter your password.

While you are in recovery mode, now is a good time to also Disable SIP
(System Integrity Protection) so that the Yabai WM can control the MacOS
window server. Refer to the guide [yabai_WM_macos.md](./yabai_WM_macos.md)
for more details.

Reboot and you will be prompted once more to allow loading the kernel
extension for Keybase (FUSE for MacOS). After approving, you will have to
reboot once more for Keybase File System (KBFS) to be activated.

## Setup ~/.ssh directory

First `mkdir ~/.ssh`. We will now create symlinks from the dotfiles
repo and from Keybase FS.

I store my ssh private-public keypairs in Keybase FS private folder
subdir `../ssh/`.

```sh
ln -s /path/to/keybase/fs/ssh/mykey ~/.ssh
```

Also create symlinks for your ssh client configs...


## Setup GPG

> On MacOS, the best gpg keyring manager as of 2021.03 is MacGPG2
> (GnuPG 2 for macos)

https://github.com/GPGTools/MacGPG2

> In the `.emacs` file for MacOS from your `dotfiles` repo, the path
> to gpg is set as `/usr/local/MacGPG2/bin/gpg2`


## Setup gpg keys

I also store my gpg keys in KBFS. Once you have MacGPG2 setup, you can
import your ASCII armored `.asc` private key into GPG2. Once you have
imported your private-public keypair, set the owner trust to
`ULTIMATE`. You need to have your gpg keys imported into the MacOS keyring
to decrypt your `.gpg` encrypted files stored on KBFS.

## Clone dotfiles repo

```sh
git clone git@github.com:gojun077/jun-dotfiles.git ~/dotfiles
```

## Create symlinks for dotfiles

Create symlinks from `~/dotfiles/` for the following

- `~/.config/alacritty/alacritty.toml`
- `~/.bash_aliases`
- `~/.bash_profile`
- `~/.bashrc`
- `~/.emacs`
- `~/.screenrc`
- `~/.skhdrc`
- `~/.vimrc`
- `~/.yabairc`

All the dotfiles for macOS are in `~/dotfiles/macOS/`

```sh
# if ~/.config/alacritty doesn't exist, 'mkdir -p' it
ln -s ~/dotfiles/macOS/alacritty.toml ~/.config/alacritty
ln -s ~/dotfiles/macOS/bash_aliases_mac ~/.bash_aliases
ln -s ~/dotfiles/macOS/bash_profile_mac ~/.bash_profile
ln -s ~/dotfiles/macOS/bashrc_mac ~/.bashrc
ln -s ~/dotfiles/macOS/emacs_mac ~/.emacs
ln -s ~/dotfiles/macOS/screenrc_mac ~/.screenrc
ln -s ~/dotfiles/macOS/skhdrc ~/.skhdrc
ln -s ~/dotfiles/macOS/vimrc_mac ~/.vimrc
ln -s ~/dotfiles/macOS/yabairc ~/.yabairc
```

Open a new terminal window and source the following (only needs
to be done once per new terminal session):

```sh
cd ~/dotfiles/macOS
source login_settings_source_me.sh
```

This will properly set up the `PATH` and other settings that should only
be set once like in traditional `.bash_profile` on Linux.

## Install Homebrew

Homebrew is an open source package manager for Mac OS. You will need to
install `brew` in order to install many CLI packages you will need in your
day-to-day work.

https://brew.sh/

```sh
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

This will also add `/opt/homebrew/bin` to your `PATH`. If you later
change your default shell, you will have to manually specify brew to your
PATH variable. Make sure that that `/opt/homebrew/bin` always comes
before `/usr/bin`!

Also run `brew doctor` to make sure your homebrew settings are
correct.

### Setup github-credential-manager for brew (optional)

- https://github.com/git-ecosystem/git-credential-manager/blob/release/docs/README.md
- https://github.com/git-ecosystem/git-credential-manager/blob/release/docs/install.md

The latest version of GCM as of 2023 is written in dotnet and supports
Linux, MacOS and Windows. I wasn't able to install GCM via `brew`, but if
you go to the releases page in the above repo, you can download pre-built
`.pkg` binaries for MacOS, for both x64 and ARM64 architectures.

After installing the `.pkg` file your current user will automatically
be setup to authenticate with Github.

```sh
$ brew install --cask git-credential-manager
==> Downloading https://formulae.brew.sh/api/cask.jws.json

==> Downloading https://github.com/git-ecosystem/git-credential-manager/releases/download/v2.4.1/gcm-osx-arm64-2.4.1.pkg
==> Downloading from https://objects.githubusercontent.com/github-production-release-asset-2e65be/158405551/0ee5c97d-902a-49f8-8de5
############################################################################################################################ 100.0%
==> Installing Cask git-credential-manager
==> Running installer for git-credential-manager with sudo; the password may be necessary.
Password:
installer: Package name is Git Credential Manager
installer: Installing at base path /
installer: The install was successful.
🍺  git-credential-manager was successfully installed!
```

On MacOS the default GCM credential store is the macOS keychain. You can
also explicitly specify that GCM use the macOS keychain as follows:

```sh
export GCM_CREDENTIAL_STORE=keychain
# or
git config --global credential.credentialStore keychain
```


## More fonts for OSX

MacOS OOTB contains only a limited number of fonts and most of them aren't
terminal or IDE-friendly. You can get thousands more fonts from the nerd
fonts project on Github.

https://github.com/ryanoasis/nerd-fonts

> For Mac OS, use homebrew to install nerd fonts

```sh
brew tap homebrew/cask-fonts  # initial install
brew install --cask <nameOfFontPkg>
```

In my case, I like to use `monofur` in Emacs, so I would run

```sh
$ brew install --cask homebrew/cask-fonts/font-monofur-nerd-font
==> Downloading https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/Monofur.zip
==> Downloading from https://objects.githubusercontent.com/github-production-release-asset-2e65be/27574418/9824db14-e485-4c4f-8b18-b0
############################################################################################################################## 100.0%
==> Installing Cask font-monofur-nerd-font
==> Moving Font 'MonofurNerdFontPropo-Regular.ttf' to '/Users/macjun/Library/Fonts/MonofurNerdFontPropo-Regular.ttf'
==> Moving Font 'MonofurNerdFont-Italic.ttf' to '/Users/macjun/Library/Fonts/MonofurNerdFont-Italic.ttf'
==> Moving Font 'MonofurNerdFont-Regular.ttf' to '/Users/macjun/Library/Fonts/MonofurNerdFont-Regular.ttf'
==> Moving Font 'MonofurNerdFontMono-Bold.ttf' to '/Users/macjun/Library/Fonts/MonofurNerdFontMono-Bold.ttf'
==> Moving Font 'MonofurNerdFontMono-Italic.ttf' to '/Users/macjun/Library/Fonts/MonofurNerdFontMono-Italic.ttf'
==> Moving Font 'MonofurNerdFontMono-Regular.ttf' to '/Users/macjun/Library/Fonts/MonofurNerdFontMono-Regular.ttf'
==> Moving Font 'MonofurNerdFontPropo-Bold.ttf' to '/Users/macjun/Library/Fonts/MonofurNerdFontPropo-Bold.ttf'
==> Moving Font 'MonofurNerdFontPropo-Italic.ttf' to '/Users/macjun/Library/Fonts/MonofurNerdFontPropo-Italic.ttf'
==> Moving Font 'MonofurNerdFont-Bold.ttf' to '/Users/macjun/Library/Fonts/MonofurNerdFont-Bold.ttf'
🍺  font-monofur-nerd-font was successfully installed!
```

and for Hangul text:

```sh
$ brew install --cask homebrew/cask-fonts/font-nanum-gothic-coding
==> Cloning https://github.com/google/fonts.git
Cloning into '/Users/macjun/Library/Caches/Homebrew/Cask/font-nanum-gothic-coding--git-sparse'...
==> Checking out branch main
Already on 'main'
Your branch is up to date with 'origin/main'.
Warning: No checksum defined for cask 'font-nanum-gothic-coding', skipping verification.
==> Installing Cask font-nanum-gothic-coding
==> Moving Font 'NanumGothicCoding-Bold.ttf' to '/Users/macjun/Library/Fonts/NanumGothicCoding-Bold.ttf'
==> Moving Font 'NanumGothicCoding-Regular.ttf' to '/Users/macjun/Library/Fonts/NanumGothicCoding-Regular.ttf'
🍺  font-nanum-gothic-coding was successfully installed!
```


To list all True Type Fonts (`.ttf`) on MacOS:

```sh
system_profiler SPFontsDataType
```

This is helpful when you need to know the exact name of the font so
you can specify it in a LaTeX preamble, for example.


**References**

- https://stackoverflow.com/questions/1113040/list-of-installed-fonts-os-x-c
- https://apple.stackexchange.com/questions/35852/list-of-activated-fonts-with-shell-command-in-os-x



## Install nixOS package manager

https://nixos.org/download

```sh
sh <(curl -L https://nixos.org/nix/install)
```

The script will prompt you to enter the `sudo` password multiple times and
will create *32* build users for nixos. This will require you to enter your
sudo password `32 * 7 = 224` times! Luckily, you can copy-paste your
password.

We need to install nixos package manager because we will use it to install
a version of emacs with native elisp compilation enabled.


## Setup Emacs editor (GUI)

Refer to my notes on Emacs setup [emacs_on_macos.md](emacs_on_macos.md)


### Enable file read/write access for `Emacs.app`

> Open *General Settings* -> *Security & Privacy* -> *Privacy*, select
> *Full Disk Access* in the left pane, then click `+` and add `Emacs.app`


### Allow Emacs to display files opened from 3rd-party apps

> If `emacs` is already running, you won't be able to open a file
> with using your favorite editor from another application. For
> example, in P4V if you select a file from a depot and choose
> *Open With* and select `Emacs.app`, nothing will happen.

> Instead what you must do is ensure that `(start-server)` is in
> you emacs init file or `.emacs` and then ensure that 3rd party
> apps invoke `/Applications/Emacs.app/Contents/MacOS/bin/emacsclient`
> instead of `emacs` when opening a file to be displayed in an
> editor.

### Change default MacOS keyboard shortcuts

- Disable Input method toggle `C-SPC`, `C-M-SPC`
    + need `C-SPC` for Emacs `set mark`
- `Shift-Option`(ALT)-lt(gt)`

## Edit MacOS Ventura+ keyboard settings

There are some annoying options that are automatically enabled in
the *System Settings* -> *Keyboard* menu on MacOS Ventura and later
versions. Then go to *All Input Sources* menu:

- `Correct spelling automatically`
- `Capitalize words automatically`
- `Add period with double-space`

The last one in particular is super-annoying when I am coding or trying to
write text in a browser. Adding a period after a double space is detected
is buggy for me and often inserts a period when I don't want one.

## Setup Flycut Clipboard (deprecated - use maccy instead)

https://github.com/TermiT/Flycut/releases

Also set Flycut Preferences to start on boot; you might have to use the
System Preferences menu to set this up, as the native Flycut 2 options
didn't work for me on MacOS Big Sur 11.2.2

## Maccy clipboard app

https://github.com/p0deje/Maccy

The default hotkey to open Maccy clipboard is `Cmd-Shift-C`, but this
conflicts with the Arc browser URL copy keyboard shortcut; I therefore
changed it to `Cmd-shift-M`. If you install from the Apple App Store,
you have to pay to install, but if you install via `brew`, it is free.
Maccy is open source.

In the settings, select *Start on Login* and *Don't steal app focus*. The
second setting is useful when using Maccy with a tiling WM like `yabai`. I
have yabai setup so that focus follows mouse.

Select the options *Paste automatically* and *Paste without formatting*.

To enable auto-paste, you will have to manually give `maccy`
*Accessibility* rights.


## Python on MacOS

MacOS 14.x ships with Python 3.x as the default Python. However, the
default Python update cycle from Apple is much slower than the release
cadence of the PSF. Although you can use homebrew to install multiple
versions of Python, it is much better to use a Python version manager
like `pyenv`.

## Multiple Python version management with pyenv

https://anil.io/blog/python/pyenv/using-pyenv-to-install-multiple-python-versions-tox/

`pyenv` is a tool to isolate different python versions from one another.
I've found it to be very helpful on MacOS which comes with an ancient
version of Python used for the system; installing different Python versions
through homebrew is also problematic; by using `pyenv` I'm able to keep
track of different Python versions and their corresponding `pip` clients as
well as the associated dependencies.


First install pyenv from homebrew

```sh
brew update
brew install pyenv
```

In my `login_settings_run_once.sh` script in my dotfiles repo, I execute
`eval "$(pyenv init --path)"` to load `pyenv` as a daemon.  In the child
shell sessions which inherit the environment declarations from the
*run_once* script, I can interact with `pyenv`. For more info on listing
available versions of python, seeing the currently active python, and more,
refer to [pyenv_guide.md](pyenv_guide.md)


## Setup Alacritty terminal

MacOS comes with `Terminal.app` by default, and many people also use the
open source `iTerm2`. I now use `alacritty` which is written in Rust.

https://github.com/alacritty/alacritty

You can also install via `brew install --cask alacritty`

**Note**: when installing from `brew`, my MBP M1 16" from work refuses
to open `Alacritty.app` because it hasn't been codesigned by Apple. I
do not encounter this issue when installing `alacritty` from nixos package
manager, however:

```sh
nix-env --install --attr nixpkgs.alacritty
ln -s ~/.nix-profile/Applications/Alacritty.app /Applications
```

I like that it uses a single ~~alacritty.yaml~~ `alacritty.toml` as its
config. It works well with session multiplexers like GNU Screen and
emulates `xterm` even better than iterm2. In fact with the latter, I had
weird bugs where ssh sessions would not show colored text.

Alacritty doesn't have these compatibility issues and it is blazingly fast!

Historical note:

> One weird issue I'm getting with MacOS Big Sur 11.2.2 is that when I
> first open a GNU Screen session in Alacritty I can't copy anything from
> the terminal; this is resolved by detaching and re-attaching to the GNU
> Screen session.

Note that on MacOS, the `option` key does not properly work as `Meta` in
Alacritty. To work around this, you must explicitly map each key combo that
uses `Meta` in your ~~alacritty.yml~~ `alacritty.toml` as in
https://github.com/alacritty/alacritty/issues/93

This is still an issue as of Mar 2023. Alacritty does not have this issue
on Linux.

Another workaround is to add the following to `alacrity.toml`:

```toml
[window]

# Option as Alt (macOS only)
option_as_alt = "OnlyLeft"
```

Alacritty looks for config files in the following paths:

```
$XDG_CONFIG_HOME/alacritty/alacritty.toml
$XDG_CONFIG_HOME/alacritty.toml
$HOME/.config/alacritty/alacritty.toml
$HOME/.alacritty.toml
```

**References**

https://wiki.archlinux.org/title/Alacritty

## Replace default shell zsh with modern bash

Install a modern `bash` from `brew` or nixos pkg manager `nix-env`.  If you
installed from nixos, create a symlink from `~/.nix-profile/bin/bash` to
`/usr/local/bin`.

If you install `bash` from brew, it will become available in
`/opt/homebrew/bin/bash`.

When you edit `/etc/shells` make sure it includes the path to your
downloaded bash binary, whether it is in the homebrew directory or
`/usr/local/bin/bash`

**Note**: I do not recommend installing bash via `nix-env`; just install
from Homebrew instead. After installing `bash` from the nixos package
manager and setting the default shell to the symlink for nixos-installed
`bash`, I still could not use up arrow history completion or TAB
completion, although the BSD-provided `/bin/bash` worked without problems.


Homebrew installs `bash` to `/opt/homebrew/bin/bash`, but MacOS doesn't
recognize this as a valid system path; create a symlink as follows:

```sh
sudo ln -s /opt/homebrew/bin/bash /usr/local/bin
```

And then run change shell as root:

```sh
sudo chsh -s /usr/local/bin/bash "$USER"
```

You can also change the login shell in System Settings -> Users & Groups ->
and right-clicking on your user, selecting *Advanced* and selecting your
login shell. Note that the default shell in MacOS is `/bin/zsh`


Exit all your terminal sessions and start new ones. `echo $SHELL` to make
sure `/usr/local/bin/bash` is the shell.

Also verify that `/usr/bin/env bash --version` is returning a modern
bash version.

**Note**: If you make a mistake with `chsh` Change shell command (for
example you later delete the binary specified in `chsh`), whenever you try
to launch a terminal app, it will fail with error 127 (shell not found). In
this case, click on the Apple menu and select *System Settings* -> *Users &
Groups* and right-click on your user, then select *Advanced Options*. You
will be prompted for your password and then you will be presented with a
menu that enables you to select your Login shell, Home directory, etc.

Also when troubleshooting `$SHELL` it is helpful to temporarily change
into another shell like `/bin/csh` as a sanity check for shell history,
TAB completion, and PS1 prompt, and other shell features.

## AutoRaise (DEPRECATED - use Rectangle or Yabai instead)

On Windows and Linux, when you move your mouse over a window, that window
automically grabs focus. This is not the case on MacOS, however. For a
window to grab focus, you must first click somewhere within the window to
activate it.

This feature is especially important in Tiling Window Managers like `i3`,
`awesome` and `sway`. Users coming to MacOS from such Linux Desktop
Environments are often thrown for a loop when they realize that simply
hovering over a window with your mouse doesn't set the focus to that
window. Many times I will start typing and notice that text is showing up
in another window which still has focus although my mouse is hovering
elsewhere.

https://github.com/sbmpost/AutoRaise

You can download a `.dmg` disk image file for MacOS. Open the file and
copy `Autoraise.app` to the `/Applications/` folder. When you first
try to run the app you might have to manually allow it in the *Privacy &
Security* -> *Accessibilty* settings menu.

**Note**: if you use the
[yabai](https://github.com/koekeishiya/yabai/tree/master) tiling WM for
MacOS, you can set it up to do *focus follows mouse*, in which case you
don't need to use Autoraise.

You can also manually move focus between windows in a space by using the
native MacOS keyboard shortcut `Ctrl-F4`


**References**

- https://github.com/alacritty/alacritty/wiki/Keyboard-mappings
- https://github.com/alacritty/alacritty/issues/6720
  + starting in release `v0.13` the key binding handling will be changed

## Rectangle (and Rectangle Pro)

Rectangle is a tiling window manager for MacOS with a core open-source
version and a paid _Pro_ version. It works well and doesn't require
elevated privileges like `yabai`, which requires you to partially disable
System Integrity Protection (SIP) which is no-go for many corporate Macs
which have MDM enabled (like Kandji and Crowdstrike Falcon).

The open-source version of
[Rectangle](https://github.com/rxhanson/Rectangle) is perfectly usable and
allows you to customize the shortcuts for sending active windows into
various tiling arrangements. The developer of Rectangle has stated in
[this](https://github.com/rxhanson/Rectangle/issues/91) Github issue that
he will not add *focus follows mouse* or similar functionality to Rectangle.

A simple workaround is to use the native MacOS keyboard shortcut `Ctrl-F4`
to cycle between windows in a workspace. When using *Rectangle* you will
generally only have two to four windows per screen, so cycling active
windows between this number of windows is doable.

## ssh-agent for MacOS

> To check which SSH keys are loaded in `ssh-agent`, run the following

`ssh-add -l`

> On a new system and after certain updates (`brew upgrade` updating
> `openssh` and related pkg's for example), no ssh keys will be loaded
> in the ssh-agent.

> You can permanently add keys to your ssh-agent on older MacOS releases
> (below Sierra) by simply running

```sh
ssh-add -K /path/to/privkey
```

> However, MacOS versions above Sierra also require the following settings
> in your ssh config file `~/.ssh/config` under *global settings*.

```
UseKeychain yes
AddKeysToAgent yes
```

https://apple.stackexchange.com/questions/48502/how-can-i-permanently-add-my-ssh-private-key-to-keychain-so-it-is-automatically

## Enable ssh server

Open *System Settings* -> *General* -> *Sharing* and enable *Remote Login*.
If you press the `i` icon, you can also set which users can connect via ssh
and whether they have full disk access or not.

**Note**: the instructions above are for MacOS Venture 13 and above; the
steps and menu items are slightly different for older MacOS versions


## skhd hotkey daemon for MacOS

`skhd` is made by the developer of `yabai`. The 2 go well together.

```sh
brew install koekeishiya/formulae/skhd
skhd --start-service
```

Also make sure the default MacOS keyboard shortcuts have been disabled.

## Window Managers for MacOS

### ~~Amethyst Tiling Window Manager~~ DEPRECATED

**Note**: as of 2023, I prefer `yabai` Tiling WM for MacOS instead
of Amethyst. See my yabai HOWTO for details.

https://github.com/ianyh/Amethyst

> Not as good as `i3`, but it's on par with Win10's *FancyZones*

I have encounterd lots of flakiness in `amethyst` since MacOS 12, so
now I just use it on startup to organize windows and then I kill it off.
Amethyst causes some problems with how the OS grabs focus for status
notifications...

### Yabai Tiling WM for MacOS

See [yabai_WM_macos.md](yabai_WM_macos.md)

Yabai is a Binary Space Partitioning (BSP) Window Manager reminiscent of
the famous Linux WM's `i3` for X11 and `sway` for Wayland. Installation
requires partial disablement of System Integrity Protection and some manual
steps, but the upstream project on Github is updated frequently as of 2024.

### Rectangle Window Manager

https://github.com/rxhanson/Rectangle

Rectangle is not a full-blown BSP Window Manager, but you can achieve many
of the same things as a BSP WM using Rectangle.

## Microsoft RDP Client

> You can install the official MS RDP Client from the App Store Windows
> Some VM's require the key combination `Ctrl` + `Alt` + `End` to reset
> your password.  However, some touchbar Macbooks don't have an `End`
> key. In these cases use the following key combo instead:

`Fn + Ctrl + Option + Delete(Bkspc)`


## Screenshot hotkeys on MacOS

https://support.apple.com/en-us/HT201361https://support.apple.com/en-us/HT201361

- `Shift-Cmd-3` full screen screenshot
- `Shift-Cmd-4` select area of screen for screenshot
- `Shift-Cmd-4-SPC` select window for screenshot
- `Shift-Cmd-5` Screenshot app shortcut
    + set delay
    + set path to save screenshots
    + etc


## Install TeX (LaTeX) distro for MacOS

The official TeX distro for MacOS is MacTeX, which supports both Intel and
Apple Silicon

https://www.tug.org/mactex/

You can install via `brew` or download a `.dmg` for manual installation.
I will not be using the GUI LaTeX editor included in mactex, so I
installed the `mactex-no-gui` homebrew cask instead:

```sh
brew install --cask mactex-no-gui
```

## Clone bin-scripts to ~/bin

```sh
git clone git@bitbucket.org:gojun077/bin-scripts.git
```

## Setup crontab on MacOS

> Setting up crontab/cron on MacOS is a bit involved due to the new
> security settings in versions Sierra and above.

Settings -> Privacy & Security -> Full Disk Access

> and then add `/sbin/cron` and `/usr/bin/crontab` to the list
> of programs with FDA.


## Edit ulimit nopen (max open files) (DEPRECATED)

> In versions of MacOS Sierra and above, the regular user is given a
> a soft limit of 256 open files at a time. This is really inconvenient
> and will result in the *too many open files* error when you are trying
> to use concurrency in Python or Golang. Here is a link that discusses
> how to work around this issue:

https://docs.riak.com/riak/kv/2.1.4/using/performance/open-files-limit/#mac-os-x

> The method described in the link was only hit-and-miss for me, however.
> Here's what worked for me:

```sh
$ ulimit -n
256
$ sudo launchctl limit maxfiles 65536 200000
$ launchctl limit
	cpu         unlimited      unlimited
	filesize    unlimited      unlimited
	data        unlimited      unlimited
	stack       8388608        67104768
	core        0              unlimited
	rss         unlimited      unlimited
	memlock     unlimited      unlimited
	maxproc     1392           2088
	maxfiles    65536          200000
$ launchctl limit maxfiles 65536 200000
Could not set resource limits: 1: Operation not permitted
[pekoh@APSEONOTE392 ~]$ ulimit -n 65536 200000
[pekoh@APSEONOTE392 ~]$ ulimit -n
65536
```

> Note that you must first run the MacOS-only `launchctl` cmd as
> `sudo` to change the per-session maxfiles setting. Then you can
> run `ulimit -n` as regular user to the setting you made with
> `launchctl` will be applied to your current session.


## Extended file attributes on MacOS

> If you see a `@` at the end of the file permission octals, it means that
> an extended attribute is set on the file and you must use `ls -@l` to
> view the extended attribs.

> If you see a `+` at the end of the file permission octals, it means that
> you have an ACL on the file. You can view ACL's with `ls -le`

> The most common case of extended attributes being added to a file is if
> you download a file from the Internet. MacOS will automatically quarantine
> such files, preventing you from executing them. You can remove `xattr`
> with the following command:

```sh
sudo xattr -d com.apple.quarantine my_file
```


## Note about Apple ID

If you buy a used MacBook or you have to return a MacBook you used at work,
make sure the old owner of the Macbook logs out of their Apple ID before
purchasing it! If the original owner does not log out of their Apple ID
and you lose contact with the owner, there is no way to remove that Apple
ID from the machine and it effectively becomes e-waste.


## 3rd party package list


### brew

```
bash
blender  # cask
cmus
colima  # containers on linux on macos (dockerd)
diceware
docker  # docker client
firefox  # cask
font-monofur-nerd-font  # cask
font-nanum-gothic-coding  # cask
git-credential-manager  # cask
inkscape  # cask
maccy  # cask
mactex-no-gui  # cask
midnight-commander  # tui file manager
pyenv
skhd  # koekeishiya/formulae/skhd
telnet
tiger-vnc
tty-clock
visual-studio-code  # cask
yabai  # koekeishiya/formulae/yabai
zotero  # cask; zotero 7 supports Apple Silicon
```

Install with `brew install pkgName` for regular formulae and with
`brew install --cask caskName` for casks

brew cheatsheet https://devhints.io/homebrew

- `brew search pkgName`
- `brew info pkgName`
- `brew list`
- `brew list --cask`
- `brew uninstall pkgName`
- `brew update`
- `brew upgrade`
- `brew outdated`
- `brew doctor`

### nix

```
coreutils
emacs29-macport
imagemagick
k9s
musikube
ncdu
screen  # GNU Screen
silver-searcher  # provides 'ag'
tmux
wget
```

Install with `nix-env --install --attr nixpkgs.foo`
Search for packages using `nix repl`

Note that after installing via nix-os package manager, you must create
symlinks for each of the binaries from `~/.nix-profile/bin` to
`/usr/local/bin`

### Tailscale (Wireguard-based mesh VPN)

Tailscale as of 2024 is available through the Apple App Store, which
means the app is code signed by Apple. Install via the App Store!

### Colima

https://github.com/abiosoft/colima/tree/main

Containers on Linux on MacOS

Provides an open-source docker daemon that can work with a docker client
on MacOS. Docker Desktop went closed source with a commercial license in
late 2022, so if you want to use `docker` without license limitations
on MacOS, you will have to use something like Redhat's `podman`, Rancher
Labs `rancher`or `colima`.


```sh
brew install colima
```

Note that colima only provides a docker daemon; you will still need a
`docker` client. Install the client with `brew install docker`.

To run colima as a service, `brew services start colima`

Or to run `colima` daemon ad-hoc: `colima start -f`




### List of packages for manual install (.dmg or .pkg)

```sh
KakaoTalk
keybase
MacGPG2
```
