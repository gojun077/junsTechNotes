#!/usr/bin/python3
# add_date_field_trace_pt.py
#
# Last Updated: 2017-04-17
# Jun Go gojun077@gmail.com
#
# trace_pt files only contain HH:MM.SS timestamps without Year,
# Month or Day. The trace_pt filename, however, contains MMDD
# (but no year). This script will add an extra field to the
# beginning of every line starting from the 2nd line to the
# 3rd to last line


import argparse
import os

PATH = '/home/archjun/Downloads/parsed/'  # CHANGE ME!
YEAR = input('Enter Year in YYYY: ')
MMDD = input('Enter Month and Day in MMDD: ')


def trace_pt_date(L, ofname):
    """
    list, str -> file

    Given a list containing all lines of a parsed trace_pt file,
    insert a new field YYYYMMDD at the beginning of every line
    (except for the first line and the last 2 lines)
    """
    for i in range(1:len(L) -2 ):
        L[i] = YEAR + MMDD + ' ' + L[i]

    with open(os.path.join(PATH, ofname), 'w') as f:
        for line in L:
            f.write(line)


class ftypeException(Exception):
    """
    Exception raised when incorrect file type is used
    """


parser = argparse.ArgumentParser(
    description="Opens a parsed trace_pt file and adds a date field.")
parser.add_argument('filename', type=str, help="Path to file to open")
args = parser.parse_args()
if 'trace_pt' not in args.filename:
    raise ftypeException("Please pass a parsed trace_pt file to this program.")

with open(args.filename, 'r') as f:
    traceFileL = []
    # append all lines in 'f' to list
    for line in f:
        traceFileL.append(line)


trace_pt_date(traceFileL, args.filename + '_dateField')
