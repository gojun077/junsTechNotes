:PROPERTIES:
:ID:       8d619147-6f18-4237-867b-0af5fa46f715
:END:
#+TITLE: jira-cli notes
#+SUBTITLE: JIRA from the command line
#+AUTHOR: Peter Jun Koh
#+EMAIL: gopeterjun@naver.com
#+DESCRIPTION: a JIRA util that allows you to create tickets via CLI
#+KEYWORDS: JIRA, atlassian, productivity, management
#+LANGUAGE:  en

* Summary

- Created on: [2024-06-17 Mon]
- Last Updated: [2024-10-16 Wed 14:12]

https://github.com/ankitpokhrel/jira-cli

A command-line utility written in Go that enables CRUD operations in
JIRA. It does not completely replace the web GUI, but you can do many
JIRA operations from ~jira-cli~.


* Topics

** Setup

From the [[https://github.com/ankitpokhrel/jira-cli/releases][releases page]], download the standalone binary tarball for your
OS and architecture. arm64 binaries are available for both MacOS and
Linux.

First you need a JIRA API token. Next set the environment variable

- ~JIRA_API_TOKEN~

However, for JIRA Cloud, you do not need to set the env var
~JIRA_AUTH_TYPE~ although the official docs do not state this explicitly.

An alternative to setting this env var is to create a ~~/.netrc~ file
containing the following:

#+begin_src ini
machine <yourCompany>.atlassian.net
login foo.bar@yourco.biz
password PassWord-or-API-token
#+end_src

Note that the ~machine~ field should only contain the FQDN, without any
protocol prefix like ~https://~.

Now type ~jira init~ and follow the on-screen prompts asking you if you
are using JIRA Cloud or self-hosted JIRA, asking for your Jira server
URL, login email, default project, and default board.

If the information provided is all correct, a ~jira-cli~ config file will
be generated in ~~/.config/.jira/.config.yml~ on Linux.

** Hotkeys

~jira-cli~ provides a TUI. You can view shortcuts by pressing ~?~

** JIRA Operations

*** List active JIRA user

#+begin_src sh
$ jira me
hong.gildong@mycorp.biz
#+end_src

*** List all JIRA issues assigned to you

#+begin_src sh
jira issue list -afoo.bar@yourco.biz
#+end_src

Your assigned tasks will then be listed in an ~ncurses~ interface which you
can navigate using both ~vim~ style navigation or the arrow keys. If you
press ~<ENTER>~ after selecting a ticket, it will be opened in your default
browser.

If you wish to view the issue within your terminal, highlight the issue in
the list and press ~v~ to /view/ it.

*** Create issue

~jira-cli~ supports Markdown and JIRA flavored Markdown when you create
tickets from /templates/ named with the ~.tmpl~ file extension.

#+begin_src sh
# create JIRA issue description from template file
jira issue create --template /path/to/mytemplate.tmpl
#+end_src

To fill in fields such as issue ~type~, ~priority~, ~epic~, etc, you can
use option flags with the ~jira issue create~ command.

#+begin_src sh
$ jira issue create --help
Create an issue in a given project with minimal information.

USAGE
  jira issue create [flags]

FLAGS
  -t, --type string                   Issue type
  -P, --parent string                 Parent issue key can be used to attach epic to an issue.
                                      And, this field is mandatory when creating a sub-task.
  -s, --summary string                Issue summary or title
  -b, --body string                   Issue description
  -y, --priority string               Issue priority
  -r, --reporter string               Issue reporter (username, email or display name)
  -a, --assignee string               Issue assignee (username, email or display name)
  -l, --label stringArray             Issue labels
  -C, --component stringArray         Issue components
      --fix-version stringArray       Release info (fixVersions)
      --affects-version stringArray   Release info (affectsVersions)
      --custom stringToString         Set custom fields (default [])
  -T, --template string               Path to a file to read body/description from
      --web                           Open in web browser after successful creation
      --no-input                      Disable prompt for non-required fields
  -h, --help                          help for create
#+end_src

*** Edit issue

~jira issue edit~

You will then be presented with an interactive menu to select values
for fields.

If you specify the /issue ID/ with arguments and option flags, you can run
the command in non-interactive mode

*** Change issue status

~jira issue move~

#+begin_src sh
jira issue move JIRA-123 "ticketStatus" \
    --comment "foo" \
    --web
#+end_src

*** View issues

~jira issue view ISSUEKEY~

This command roughly converts the Atlassian document to Markdown and
displays it through the pager ~less~.
