K8s on RPi: microk8s and k3s
====================================

# Summary

- Created on: Sep 18 2021
- Created by: gojun077@gmail.com
- Last Updated: Oct 3 2021

Canonical's Micro Kubernetes distribution (*microk8s*) is suitable for
running on resource-constrained ARM SBC's, for edge computing apps and
other use cases where you don't have lots of memory or compute. `microk8s`
will run on environments with just 1GB RAM and runs fine on RPi3B boards or
better.

`microk8s` runs all k8s components within containers which run on `snapd`.

# Topics

## Install 



## Open ports in firewall for k8s master node

https://microk8s.io/docs/ports

- 16443 API server 	SSL encrypted. Clients need to present a valid password from a Static Password File.
- 10250 kubelet 	Anonymous authentication is disabled. X509 client certificate is required.
- 10255 kubelet 	Read only port for the Kubelet.
- 25000 cluster-agent 	Proper token required to authorise actions.
- 12379 etcd 	SSL encrypted. Client certificates required to connect.
- 10257 kube-controller 	Serve HTTPS with authentication and authorization.
- 10259 kube-scheduler 	Serve HTTPS with authentication and authorization.
- 19001 dqlite 	SSL encrypted. Client certificates required to connect.
