// golang-sftp-example/main.go
// Created on: 13 Dec 2020
// Created by: gojun077@gmail.com
// Last Updated: Sun 22 Dec 2024
//
// This example program takes five arguments:
// - hostname
// - username
// - path to ssh key
// - mode ('get' or 'put')
// - file
//   + list of files with absolute paths to send or receive
//
// If the shh privkey password is not already cached in 'ssh-agent', the
// user will be prompted to enter their ssh key PW which will not be echoed
// to stdout thanks to the 'terminal' method from module 'crypto/ssh'

package main

import (
	"bufio"
	"flag"
	"fmt"
	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
	"golang.org/x/crypto/ssh/terminal"
	"io/ioutil"
	"net"
	"os"
	"path"
	"time"
)

func getKeySigner(privateKeyFile string, pw string) ssh.Signer {
	privateKeyData, err := ioutil.ReadFile(privateKeyFile)
	if err != nil {
		fmt.Printf("Error loading private key file! %v\n", err)
	}
	privateKey, err := ssh.ParsePrivateKeyWithPassphrase(
		privateKeyData, []byte(pw))
	if err != nil {
		fmt.Printf("Error parsing private key! %v\n", err)
	}
	return privateKey
}

func trySSHAgent() (ssh.AuthMethod, error) {
	sshAgent, err := net.Dial("unix", os.Getenv("SSH_AUTH_SOCK"))
	if err != nil {
		return nil, err
	}

	client := agent.NewClient(sshAgent)
	return ssh.PublicKeysCallback(client.Signers), nil
}

func main() {
	start := time.Now()
	// argument declaration BEGIN
	hostname := flag.String("h", "", "hostname to make sftp connection to")
	user := flag.String("u", "", "username to login with on remote host")
	mykey := flag.String("k", "", "path to ssh private key file")
	port := flag.String("P", "22", "TCP port used by remote host's SSHD")
	myfile := flag.String("f", "",
		"path to file with list of filenames with abs path, one per line")
	mode := flag.String("mode", "", "'put' for send, 'get' for recv")
	flag.Parse()
	// argument declaration END

	myhost := *hostname
	myuser := *user
	mykeyval := *mykey
	myportval := *port
	mymode := *mode
	myfileval := *myfile

	if len(os.Args) < 6 {
		flag.Usage()
		os.Exit(1)
	}

	if mymode != "put" && mymode != "get" {
		fmt.Println("'mode' must be one of 'put' or 'get'!")
		os.Exit(1)
	}

	f, err := os.Open(myfileval)
	if err != nil {
		fmt.Printf("Error opening file %v!\n", myfileval)
	}
	defer f.Close()

	// check for privkey password in ssh-agent first
	var authMethod ssh.AuthMethod
	agentAuth, err := trySSHAgent()
	if err == nil {
		authMethod = agentAuth
	} else { // fall back to password prompt if ssh-agent fails
		// ask user to input SSH privkey PW (not echoed to terminal)
		fmt.Print("Enter privkey password: ")
		// ReadPassword(fd int) takes a single arg, file descriptor;
		// recall that fd 0 is 'stdin'
		bytePassword, err := terminal.ReadPassword(0)
		fmt.Println("")
		if err != nil {
			fmt.Printf("Error reading password from stdin! %v!\n", err)
		}
		pw := string(bytePassword)
		authMethod = ssh.PublicKeys(getKeySigner(mykeyval, pw))
	}

	config := &ssh.ClientConfig{
		User:            myuser,
		Auth:            []ssh.AuthMethod{authMethod},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}
	// read in list of filenames from 'myfile'
	fscanner := bufio.NewScanner(f)
	fileL := []string{}
	for fscanner.Scan() {
		fufu := fscanner.Text()
		fileL = append(fileL, fufu)
	}

	// connect to remote host
	fmt.Printf("Connecting to %s...\n", myhost)
	conn, err := ssh.Dial("tcp", myhost+":"+myportval, config)
	if err != nil {
		fmt.Printf("Error dialing server! %v\n", err)
	}
	// create sftp client session on top of ssh connection
	sftpClient, err := sftp.NewClient(conn)
	if err != nil {
		fmt.Printf("Error creating sftp client! %v\n", err)
	}
	defer sftpClient.Close()
	cwd, err := sftpClient.Getwd()
	if err != nil {
		fmt.Printf("Error getting 'cwd'! %v\n", err)
	}
	fmt.Printf("remote cwd is: %s\n", cwd)
	// 'put' files on or 'get' files from remote host
	for _, file := range fileL {
		//fmt.Printf("Copying file %s to remote host...\n", file)
		// if mode is "put"
		if *mode == "put" {
			srcFile, err := os.Open(file)
			if err != nil {
				fmt.Printf("Error opening %s! %v\n", file, err)
			}
			defer srcFile.Close()

			var remoteFileName = path.Base(file)
			dstFile, err := sftpClient.Create(remoteFileName)
			if err != nil {
				fmt.Printf("Error creating remote file %v! %v\n",
					remoteFileName, err)
			}
			defer dstFile.Close()

			buf := make([]byte, 1024)
			for {
				n, _ := srcFile.Read(buf)
				if n == 0 {
					break
				}
				dstFile.Write(buf)
			}
			fmt.Printf("SUCCESS! Copied local file '%s' to remote file '%v'\n",
				file, remoteFileName)
		} else if *mode == "get" { // if mode is "get"
			srcFile, err := sftpClient.Open(file)
			if err != nil {
				fmt.Printf("Error opening remote file %v! %v\n", srcFile, err)
			}
			defer srcFile.Close()
			var localFileName = path.Base(file)
			dstFile, err := os.Create(localFileName)
			if err != nil {
				fmt.Printf("Error creating local file %v! %v\n",
					localFileName, err)
			}
			defer dstFile.Close()
			_, err = srcFile.WriteTo(dstFile)
			if err != nil {
				fmt.Printf("Error writing %s to %v! %v\n", file, dstFile,
					err)
			}
			fmt.Printf("SUCCESS! Copied remote file %s to local file %v\n",
				file, localFileName)
		}
	}
	t_elapsed := time.Since(start)
	fmt.Printf("Total execution time: %v\n", t_elapsed)
}
