Setting up Cygwin on Win10
============================

# Summary
- Last Updated: 2018.06.12
- How to install cygwin on Win10 and enable the following:
    + `apt-cyg`
    + `sshd`

# Cygwin Installation
- Download `.msi` from Cygwin site
- Run GUI installer
    + select pkg mirror closest to you
    + packages to select
        + `vim`
        + `openssh`
        + `ncdu`
        + `curl`
        + `wget`
        + `lynx`
        + `ipython2`
        + `ipython3`
        + `screen`
        + `make`

# Post-install settings

## Enable sshd
- Navigate to `C:\cygwin`
- Edit `Cygwin.bat` by adding `set CYGWIN=binmode ntsec`

    ```
    @echo off

    C:
    chdir C:\cygwin\bin
    set CYGWIN=binmode ntsec
    bash --login -i
    ```
- Check that cygwin is properly installed
    + launch `C:\cygwin\Cygwin.bat`
    + in the spawned console, enter `cygrunsrv -h`
        + you should see Cygwin help options displayed on screen
        + if you get an error you must re-install Cygwin

- run `Cygwin.bat` as Administrator
    + in the spawned console enter `ssh-host-config`

- Answer the prompts appearing in the console:

```
*** Query: Should privilege separation be used? <yes/no>: yes
*** Query: New local account 'sshd'? <yes/no>: yes
*** Query: Do you want to install sshd as a service?
*** Query: <Say "no" if it is already installed as a service> <yes/no>: yes
*** Query: Enter the value of CYGWIN for the deamon: [] binmode ntsec
*** Query: Do you want to use a different name? (yes/no) no
*** Query: Create new privileged user account 'cyg_server'? (yes/no) yes
*** Query: Please enter the password:
*** Query: Renter:
```

- Finally, start the sshd daemon in the same privileged console
    + `cygrunsrv -S sshd`
    + OR `/usr/sbin/sshd`

- Test ssh connection to localhost
    + `ssh <username>@localhost`

- https://docs.oracle.com/cd/E24628_01/install.121/e22624/preinstall_req_cygwin_ssh.htm

## Install apt-cyg package manager

```
wget https://raw.githubusercontent.com/transcode-open/apt-cyg/master/apt-cyg
rm apt-cyg; mv apt-cyg.1 apt-cyg
install apt-cyg /bin
apt-cyg update
```

- Search for a package (make sure you run *update* first)
    + `apt-cyg searchall emacs`

- https://github.com/transcode-open/apt-cyg

# Enable ICMP for IPv4 in Win10 Firewall
- Search for `방화벽` in the system search bar
- In the *Windows Defender 방화벽* menu, select *고급 설정*
- Click *Inbound rules*
- find *File and Printer Sharing (Echo Request - ICMPv4-In)*
- right click the rule and select *Enable rule*
- https://kb.iu.edu/d/aopy
