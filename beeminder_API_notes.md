Beeminder API Notes
======================

# Summary

- Created on: Nov 2 2019
- Created by: gojun077@gmail.com
- Last Updated: Apr 11 2021

> My personal notes showing Python requests examples of interacting
> with the Beeminder API


# Topics

## User endpoint

https://www.beeminder.com/api/v1/users/gojun077/

- The username is included in the endpoint
  + auth therefore only requires `auth_token` (i.e. client secret)
- Get a personal auth token
    + First log into Beeminder
    + https://www.beeminder.com/api/v1/auth_token.json to get `auth_token`
    + Append it to API requests as an add'l GET or POST parameter

## List all goals for user 'gojun077'


### Python Requests

Note that the `auth_token` has been redacted to 'foo'

```
import requests
url = "https://www.beeminder.com/api/v1/users/"
myuser = "gojun077"
goals = "goals"
myparams = {"auth_token" : "foo"}
resp = requests.get(f"{url}/{myuser}/goals", params=myparams)
resp.status_code
>>> 200

print(resp.url)
>>> https://www.beeminder.com/api/v1/users/gojun077/goals?auth_token=foo

resp_data = resp.json()
len(resp_data)
>>> 11  # 11 active beeminder goals

resp_data[0].keys()
>>> dict_keys(['slug', 'title', 'description', 'goalval', 'rate', 'goaldate', 'graph_url', 'thumb_url', 'goal_type', 'autodata', 'healthkitmetric', 'losedate', 'deadline', 'leadtime', 'alertstart', 'use_defaults', 'id', 'ephem', 'queued', 'panic', 'updated_at', 'burner', 'yaw', 'lane', 'lanewidth', 'delta', 'runits', 'limsum', 'frozen', 'lost', 'won', 'contract', 'delta_text', 'safebump', 'limsumdate', 'limsumdays', 'baremin', 'baremintotal', 'roadstatuscolor', 'lasttouch', 'safebuf', 'sadbrink', 'coasting', 'fineprint', 'yaxis', 'nomercy', 'initday', 'initval', 'curday', 'curval', 'lastday', 'dir', 'exprd', 'kyoom', 'odom', 'edgy', 'noisy', 'aggday', 'plotall', 'steppy', 'rosy', 'movingav', 'aura', 'numpts', 'road', 'roadall', 'fullroad', 'secret', 'pledge', 'mathishard', 'headsum', 'datapublic', 'graphsum', 'rah', 'last_datapoint', 'callback_url'])

resp_data[0]['title']
>>> 'pomodoro'  # This goal is named 'pomodoro'

resp_data[0]['lastday']
>>> 1496332800  # UNIX epoch time stamp of last data point

from datetime import datetime
datetime.utcfromtimestamp(resp_data[0]['lastday'])
>>> datetime.datetime(2017, 6, 1, 16, 0)  # epoch time in UTC+0

datetime.fromtimestamp(resp_data[0]['lastday'])
>>> datetime.datetime(2017, 6, 2, 1, 0)  # epoch time in KST (UTC+9)
```


## Show data for a specific goal

> Let's say I want to see data about my goal *personalblog*. I can make
> the following GET requests:


### Python requests

```python
import requests

url = "https://www.beeminder.com/api/v1/users/"
myuser = "gojun077"
goal = "personalblog"
myparams = {"auth_token" : "foo"}
resp = requests.get(f"{url}/{myuser}/goals/{goal}", params=myparams)
resp.status_code
>>> 200

resp_data = resp.json()
print(resp.url)
>>> https://www.beeminder.com/api/v1/users/gojun077/goals/personalblog?auth_token=foo

resp_data.keys()
>>> dict_keys(['slug', 'title', 'description', 'goalval', 'rate', 'goaldate', 'graph_url', 'thumb_url', 'goal_type', 'autodata', 'healthkitmetric', 'losedate', 'deadline', 'leadtime', 'alertstart', 'use_defaults', 'id', 'ephem', 'queued', 'panic', 'updated_at', 'burner', 'yaw', 'lane', 'lanewidth', 'delta', 'runits', 'limsum', 'frozen', 'lost', 'won', 'contract', 'delta_text', 'safebump', 'limsumdate', 'limsumdays', 'baremin', 'baremintotal', 'roadstatuscolor', 'lasttouch', 'safebuf', 'sadbrink', 'coasting', 'fineprint', 'yaxis', 'nomercy', 'initday', 'initval', 'curday', 'curval', 'lastday', 'dir', 'exprd', 'kyoom', 'odom', 'edgy', 'noisy', 'aggday', 'plotall', 'steppy', 'rosy', 'movingav', 'aura', 'numpts', 'road', 'roadall', 'fullroad', 'secret', 'pledge', 'mathishard', 'headsum', 'datapublic', 'graphsum', 'rah', 'last_datapoint', 'callback_url'])

resp_data['slug']
>>> 'personalblog'

resp_data['title']
>>> 'PersonalBlog'

resp_data['lastday']
>>> 1501344000  # last data point timestamp in UNIX epoch time

datetime.utcfromtimestamp(resp_data['lastday'])
>>> datetime.datetime(2017, 7, 29, 16, 0)
...
```

> As you can see from `resp.url` above, the values in the dict `myparams`
> (API auth_token) are parsed into an HTML query by the `requests` library.


## Submit new datapoint to goal

> This is described in https://api.beeminder.com/#postdata of the
> Beeminder API docs.


### cURL

```sh
$ curl -X POST https://www.beeminder.com/api/v1/users/alice/goals/gym/datapoints.json \
 -d auth_token=abc123 \
 -d timestamp=1325523600 \
 -d value=130.1 \
 -d comment=sweat+a+lot+today

{ "timestamp": 1325523600,
    "daystamp": "20120102",
    "value": 130.1,
    "comment": "sweat a lot today",
    "id": "4f9dd9fd86f22478d3000008",
    "requestid":"abcd182475925" }
```

### Python requests

> Unlike the example of making an HTTP GET request, when you make an
> HTTP POST request, you must submit a payload to the HTTP endpoint.

```python
import json
import requests
import datetime

url = "https://www.beeminder.com/api/v1/users"
myuser = "gojun077"
mygoal = "dualnback2021-q2"
epoch_utc = int(datetime.datetime.utcnow().timestamp())
endpt = f"{url}/{myuser}/goals/{mygoal}/datapoints.json"
payload = {"auth_token": "<redacted>",
            "timestamp": epoch_utc,
            "value": 1.0,
            "comment": "submitted via Beeminder API"}
header = {"Content-Type": "application/json"}
resp_post = requests.post(endpt, headers=header, data=json.dumps(payload))

>>> resp_post
<Response [200]>

>>> data = resp_post.json()
>>> data

{'timestamp': 1618114045,
 'value': 1.0,
 'comment': 'submitted via Beeminder API',
 'id': '6072f4e855c13327a200118e',
 'updated_at': 1618146536,
 'requestid': None,
 'canonical': '11 1 "submitted via Beeminder API"',
 'fulltext': '2021-Apr-11 entered at 22:08 via api',
 'origin': 'api',
 'daystamp': '20210411',
 'status': 'created'}
```


# References
http://api.beeminder.com
