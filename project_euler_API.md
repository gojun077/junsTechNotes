Project Euler API Notes
===========================

# Summary

- Created on: May 21 2021
- Created by: gojun077@gmail.com
- Last Updated: May 23 2021

> Project Euler is a free site with 757 (as of May 21 2021) math and
> computation-focused programming problems. New problems are released about
> every 15 days. According to the site's *About* page, there are more than
> 1 million registered users.


- https://projecteuler.chat/viewtopic.php?t=3148
  + Project Euler API
- https://projecteuler.chat/viewtopic.php?t=7080
  + Crawling Project Euler website
- https://projecteuler.chat/viewtopic.php?f=12&t=3003
  + Project Euler http endpoints, public & private


# Topics

## Get user stats

> User stats are public if you have enabled this option in your
> account settings. The endpoint is of the form

- `https://projecteuler.net/profile/myusername.txt`
- `https://projecteuler.net/profile/myusername.xml`

> In the case of https://projecteuler.net/profile/gopeterjun.txt I
> get the following comma-delimited string:

```
gopeterjun,South_Korea,Python,,0
```

> The xml endpoint https://projecteuler.net/profile/gopeterjun.xml gives
> me the following xml output:


```xml
<?xml version="1.0"?>
<profile>
   <username>gopeterjun</username>
   <country>South_Korea</country>
   <language>Python</language>
   <solved></solved>
   <level>0</level>
</profile>
```

As you can see above, the fields are

- username
- country
- language
- solved
  + number of questions solved
- level
  + starts from 0, +1 for every 25 questions solved

> You can parse XML responses with the python `lxml` library.


```python
import requests
from lxml import etree

euler_url = "https://projecteuler.net/profile/gopeterjun.xml"
sess_euler = requests.Session()
resp_euler = sess_euler.get(euler_url)
root = etree.XML(resp_euler.text)
```

> Now let's see what XML tags exist in the XML tree:


```python
for child in root:
    print(child.tag)
```

> This prints the tags in the order they appear: 

```
username
country
language
solved
level
```

> Since we know the order of the tags, we can retrieve the value in each
> tag by using indexes.

```python
username = root[0].text
country = root[1].text
language = root[2].text
solved = root[3].text
level = root[4].text
```

If a user hasn't solved any problems, there won't be any value inside the
XML tags `<solved></solved>` and when it is parsed by `lxml`, the variable
`solved` above will be a `NoneType` with value `None`. Don't forget to
handle this in your code.


## Get data for all problems

> If you navigate to the following endpoint

- https://projecteuler.net/minimal=problems

> you can see a list of all the released problems. It has the
> following format:


```
ID##Description##Published##Updated##Solved By##Solve Status
1##Multiples of 3 and 5##1002301200##1509908991##966452##0
...
757##Stealthy Numbers##1621094400##1620936675##274##0
```

> The fields are

- `ID` (problem number, starting from *1*)
- `Description`
- `Published` (date in UNIX epoch time)
- `Updated` (date in UNIX epoch time)
- `Solved By` (number of users who have solved the problem)
- `Solve Status` (*0* unsolved, *1* solved)
  + this field is only shown if you are logged in (session cookie)

> Note that the last field, `Solve Status` is only shown if you make a
> request to the `/minimal=problems` endpoint while logged in and use a
> valid session cookie in your request. It is not possible to log in
> programmatically to Project Euler via HTTP Basic or Digest Auth.


### Get data including *Solve Status* via session cookie

> You can also get this data programmatically via Python, although you will
> first have to use a browser extension like `Export cookie JSON file for
> Puppeteer` for Chrome which allows you to export cookies to a JSON file.

> After logging into Project Euler, click the icon for the *Export cookie*
> browser extension, and your cookie will be exported to JSON. Open the
> file and you will see a JSON list containing two items. Within each item
> are a variety of key-keyval pairs, but you only need to look at the first
> item in the JSON list and note the first two key-keyval pairs:

- `"name": "PHPSESSID"`
- `"value": "26-char alphanum"`

> *Note*: the exported cookie JSON file contains two items in a JSON
> list. The first item contains `"name": "PHPSESSID"`, while the second item
> contains `"name": "keep_alive"`. In the 2nd item, the `expires` field
> shows the cookie expiring 30 days from now. In the 1st item, however,
> the `expires` field has a value of `-1`; does this mean that the cookie
> never expires?

> Now create a new Python dict like `myC = {"PHPSESSID": "123abc..."}`
> and specify this dict as the cookie in your GET request:

```python
import requests

url_euler = "https://projecteuler.net/minimal=problems"
euler_sess = requests.Session()
euler_resp = euler_sess.get(url_euler, cookies=myC)
```

> You should verify that `euler_resp.status_code` is 200 and the string
> from `euler_resp.text` should contain
> `ID##Description##Published##Updated##Solved By##Solve Status\r\n` in the
> first 62 chars. Even if you are not properly logged in, `status_code`
> will still be `200`, but the field `##Solve Status` will be missing
> from the response text. The text looks something like the following:

```
'ID##Description##Published##Updated##Solved By##Solve Status\r\n1##Multiples of 3 and 5##1002301200##1509908991##966452##0\r\n2##Even Fibonacci numbers##1003510800##1355693477##770136##0\r\n3##Largest p
rime factor##1004724000##1355693477##553060##0\r\n4##Largest palindrome product##1005933600##1355693477##489015##0\r\n5##Smallest multiple##1007143200##1355693477##492457##0\r\n6##Sum square difference##100835280
0##1578606785##495510##0\r\n7##10001st prime##1009562400##1355693477##423800##0\r\n8##Largest product in a series##1010772000##1596131631##354638##0\r\n9##Special Pythagorean triplet##1011981600##1355693477##3598
60##0\r\n10##Summation of primes##1013191200##1355693477##329516##0\r\n...
```

> As you can see, the field separator is `##` and the line separator is
> `\r\n`. You can split the super-long string with `.split("\r\n")` and
> then further split each line with `.split("##")`.
