Web Server HOWTO
===================

# Summary

- Created on: May 26 2023
- Created by: gopeterjun@naver.com
- Last Updated: May 26 2023

This guide contains tips and tricks for setting up various webservers.

# Topics

## nginx

`nginx` is a high-performance web server written in C. It is used on
bare-metal as well as in containers and can be used as a web server,
reverse proxy, and k8s ingress.

`nginx` has a dizzying array of options and performance tuning it can be an
arcane art. `nginx` configuration syntax is relatively complicated, but
there are config file generators (Mozilla's HTTPS config generator, for
example) for `nginx` that generate reasonable defaults.


### Changing web server root path

#### RHEL-based distros

The default root path for `nginx` on RHEL-based Linux distros is
`/usr/share/nginx/html/`. If you wish to change this directory
to a custom path, i.e. `/home/myuser/html/`, you need to add an
SELinux context for the new path as well as make sure that e`x`ecute
permissions are enabled for `other` for every parent directory of
the file(s) you wish to serve. You can check the permissions on the
parent directories of your target dir with the `namei` command:

```sh
$ namei -om ~/argonaut_web
f: /home/jundora/argonaut_web
 dr-xr-xr-x root    root    /
 drwxr-xr-x root    root    home
 drwx--x--x jundora jundora jundora
 drwxr-xr-x jundora jundora argonaut_web
```

Originally my `argonaut_web` directory was not readable or executable by
`other`. As a result, I kept getting 403 permission denied errors in
`nginx`. I fixed this with `chmod -R o+x argonaut_web` after which the html
content and subfolders in `argonaut_web` became readable by `nginx`.

**References**

- https://www.nginx.com/blog/using-nginx-plus-with-selinux/
- https://stackoverflow.com/questions/6795350/nginx-403-forbidden-for-all-files

## caddy

Caddy is a webserver written entirely in Go that supports HTTPS out of the
box and can automatically setup HTTPS certs from Let's Encrypt as well as
TLS certs for local-only testing.

The `Caddyfile` syntax is a real breath of fresh air and can be written
in `toml`, `json`, or `yaml`.

Changing the webserver root doesn't require any `chmod` file permission
changes. It just works! I haven't tried `caddy` on a RHEL-based distro
using SELinux, but you will probably have to edit the file context for
the new webserver root to be able to use it without 403 errors...
