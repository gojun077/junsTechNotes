vim config guide
==================

# Summary

- Created on: Feb 2 2024
- Created by: gopeterjun@naver.com
- Last Updated: Feb 2 2024

# Topics

## Default paths

```
# color themes shipped with vim
/usr/share/vim/vim90/colors/
# vim user config path
~/.vim/
# user-supplied color themes
~/.vim/colors/
```

`VIMRUNTIME` is the vim runtime path, which you can define in your
`.vimrc`
