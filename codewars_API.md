Codewars API Notes
=========================

# Summary

- Created on: May 21 2021
- Created by: gojun077@gmail.com
- Last Updated: May 21 2021

> Codewars is a crowd-sourced online code challenge site that supports a
> wide variety of languages for each of its coding problems.  Difficulty is
> measured in *kyu*, with 1 kyu being the hardest difficulty and 8 kyu
> being the easiest.

> Codewars offers a simple public API that as of May 2021 does *not*
> require an API key to access. You can get information on what problems a
> user has solved, etc.


# Topics

## Get list of solved problems for some user

https://dev.codewars.com/#list-completed-challenges

> Lists challenges completed by a user, 200 items per page. Use page
> parameter (zero based) to paginate.


```sh
$ curl https://www.codewars.com/api/v1/users/gojun077/code-challenges/completed?page=0 | jq
{
  "totalPages": 1,
  "totalItems": 16,
  "data": [
    {
      "id": "586d6cefbcc21eed7a001155",
      "name": "Character with longest consecutive repetition",
      "slug": "character-with-longest-consecutive-repetition",
      "completedLanguages": [
        "python",
        "go"
      ],
      "completedAt": "2020-09-18T17:17:22.503Z"
    },
...
{
      "id": "54da5a58ea159efa38000836",
      "name": "Find the odd int",
      "slug": "find-the-odd-int",
      "completedLanguages": [
        "python",
        "racket",
        "go"
      ],
      "completedAt": "2020-08-02T11:51:31.222Z"
    }
  ]
}
```


> We can get the same info via python requests:

```python
import requests

url = "https://www.codewars.com/api/v1/users/gojun077"
ep = "/code-challenges/completed?page=0"
resp = requests.get(url+ep)
data = resp.json()

In [22]: data.keys()
Out[22]: dict_keys(['totalPages', 'totalItems', 'data'])

In [23]: data['totalItems']
Out[23]: 16

In [24]: data['data'][0]
Out[24]:
{'id': '586d6cefbcc21eed7a001155',
 'name': 'Character with longest consecutive repetition',
 'slug': 'character-with-longest-consecutive-repetition',
 'completedLanguages': ['python', 'go'],
 'completedAt': '2020-09-18T17:17:22.503Z'}
```

> Note that the problem difficulty is not listed here. You need to use the
> https://www.codewars.com/api/v1/code-challenges/{challenge} endpoint to
> get problem difficulty data (*kyu*).


## Get problem difficulty

https://dev.codewars.com/#code-challenges-api

```python
url = "https://www.codewars.com/api/v1/code-challenges"
query = "/character-with-longest-consecutive-repetition"

resp = requests.get(url+query)
data = resp.json()
>>> data

{'id': '586d6cefbcc21eed7a001155',
 'name': 'Character with longest consecutive repetition',
 'slug': 'character-with-longest-consecutive-repetition',
 'category': 'algorithms',
 'publishedAt': '2017-01-04T21:45:20.002Z',
 'approvedAt': '2017-01-07T03:29:44.213Z',
 'languages': ['python',
  'haskell',
  'csharp',
  'shell',
  'javascript',
  'ruby',
  'groovy',
  'go',
  'coffeescript',
  'c',
  'java',
  'typescript',
  'scala',
  'nim',
  'kotlin',
  'vb',
  'elixir',
  'rust'],
 'url': 'https://www.codewars.com/kata/586d6cefbcc21eed7a001155',
 'rank': {'id': -6, 'name': '6 kyu', 'color': 'yellow'},
 'createdAt': '2017-01-04T21:45:25.184Z',
 'createdBy': {'username': 'suic',
  'url': 'https://www.codewars.com/users/suic'},
 'approvedBy': {'username': 'tachyonlabs',
  'url': 'https://www.codewars.com/users/tachyonlabs'},
 'description': '...',
 'totalAttempts': 20534,
 'totalCompleted': 4407,
 'totalStars': 157,
 'voteScore': 631,
 'tags': ['Algorithms', 'Strings', 'Data Types', 'Fundamentals', 'Logic'],
 'contributorsWanted': True,
 'unresolved': {'issues': 0, 'suggestions': 0}}
```

> In the `query` value, you can use either the problem `id` or
> human-readable `slug`. In the example above, I used the slug value.
> Note that the problem difficulty is stored in the key `'rank'`
> whose values are in turn a dict. 6 kyu is represented as `-6`. Easy
> questions are those with kyu 8 to 6; medium have kyu 5 to 4, and hard
> have kyu 3 to 1.
