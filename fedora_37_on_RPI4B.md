Installing Fedora 37 ARM64 raw image on RPi 4B
=======================================================

# Summary

- Created on: 7 Apr 2023
- Created by: gojun077@gmail.com
- Last Updated: 21 Aug 2024


# Topics

## Get ARM64 raw image for Fedora Server

https://getfedora.org/en/server/download/

Choose the `Raw` image, not ISO or QEMU images.


```sh
wget https://download.fedoraproject.org/pub/fedora/linux/releases/37/Server/aarch64/images/Fedora-Server-37-1.7.aarch64.raw.xz
# NOTE: you don't need to `unxz` the compressed raw image
# the Fedora `arm-image-installer` can handle .xz files
```

## Use Fedora `arm-image-installer` to write raw image to disk

**Note**: Fedora `aarch64` images cannot be written to disk (ssd or
microSD) via the `rpi-imager` tool; Fedora has its own tool
`arm-image-installer` that works directly with `.raw.xz` disk images (no
need to `unxz` the compressed `.raw` files like you have to do with
`rpi-imager`).

https://fedoraproject.org/wiki/Architectures/ARM/Raspberry_Pi

I am writing the `.raw.xz` disk image directly to a SATA SSD connected to
a USB3 port. Note that I also use the option `--addconsole` to enable
serial console connections to the RPI4B:


```sh
$ sudo arm-image-installer --image=Fedora-Server-37-1.7.aarch64.raw.xz \
--target=rpi4 --media=/dev/sdc --resizefs \
--addkey=/home/jundora/.ssh/archjun_rsa.pub --addconsole

=====================================================
= Selected Image:
= Fedora-Server-37-1.7.aarch64.raw.xz
= Selected Media : /dev/sdc
= U-Boot Target : rpi4
= Root partition will be resized
= Console for rpi4 will be added.
= SSH Public Key ~/.ssh/archjun_rsa.pub will be added.
=====================================================

*****************************************************
*****************************************************
******** WARNING! ALL DATA WILL BE DESTROYED ********
*****************************************************
*****************************************************

 Type 'YES' to proceed, anything else to exit now

= Proceed? YES
= Writing:
= Fedora-Server-37-1.7.aarch64.raw.xz
= To: /dev/sdc ....
7507017728 bytes (7.5 GB, 7.0 GiB) copied, 84 s, 89.4 MB/s
0+805490 records in
0+805490 records out
7516192768 bytes (7.5 GB, 7.0 GiB) copied, 214.119 s, 35.1 MB/s
= Writing image complete!
= Resizing /dev/sdc ....
Checking that no-one is using this disk right now ... OK

Disk /dev/sdc: 119.24 GiB, 128035676160 bytes, 250069680 sectors
Disk model: SSD 830 Series
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 33553920 bytes
Disklabel type: dos
Disk identifier: 0x5c5e303a

Old situation:

Device     Boot   Start      End  Sectors  Size Id Type
/dev/sdc1  *       2048  1230847  1228800  600M  6 FAT16
/dev/sdc2       1230848  3327999  2097152    1G 83 Linux
/dev/sdc3       3328000 14680063 11352064  5.4G 8e Linux LVM

/dev/sdc3:
New situation:
Disklabel type: dos
Disk identifier: 0x5c5e303a

Device     Boot   Start       End   Sectors   Size Id Type
/dev/sdc1  *       2048   1230847   1228800   600M  6 FAT16
/dev/sdc2       1230848   3327999   2097152     1G 83 Linux
/dev/sdc3       3328000 250069679 246741680 117.7G 8e Linux LVM

The partition table has been altered.
Calling ioctl() to re-read partition table.
Syncing disks.
= Raspberry Pi 4 Uboot is already in place, no changes needed.
= SSH key ~/.ssh/archjun_rsa.pub : Not Found!
= WARNING: No SSH Key Added.
= Adding console ttyS0,115200 to kernel parameters ...
sed: can't read /tmp/root/etc/default/grub: No such file or directory

= Installation Complete! Insert into the rpi4 and boot.
```

**Note**: `arm-image-installer` supports other SBC's in addition to
RPI4; see https://github.com/im-0/arm-image-installer/blob/master/SUPPORTED-BOARDS


## Connect SATA SSD to USB3 port on RPI4B, boot from SSD

Note that booting from USB attached SSD (SATA or m2.NVMe) requires a
bootloader/firmware from Sept 2020 or later `9-03-2020`. The tools for
updating the bootloader and firmware of RPi4B can only be found on RPiOS
(Debian). The tools are `vcgencmd` and `rpi-eeprom-update`. The
`rpi-imager` tool, used for formatting microSD and other media (even block
devices), *can* be found from the package managers of other Linux distros
like Ubuntu, Fedora, and NixOS, however.

Since this is my initial boot for Fedora 37 Server on the RPI4B, I will
connect via serial console to make the initial network and user settings.

```sh
# using a USB-to-RPI-serial dongle
$ sudo screen /dev/ttyUSB0 115200
...
[  OK  ] Started plymouth-start.ser…e - Show Plymouth Boot Screen.
[  OK  ] Started systemd-ask-passwo…uests to Plymouth Directory Watch.
[  OK  ] Reached target cryptsetup.…get - Local Encrypted Volumes.
[  OK  ] Reached target paths.target - Path Units.
[  OK  ] Finished systemd-udev-sett…To Complete Device Initialization.
         Starting multipathd.servic…per Multipath Device Controller...
[  OK  ] Started multipathd.service…apper Multipath Device Controller.
[  OK  ] Reached target local-fs-pr…reparation for Local File Systems.
[  OK  ] Reached target local-fs.target - Local File Systems.
[  OK  ] Reached target sysinit.target - System Initialization.
[  OK  ] Reached target basic.target - Basic System.
[   13.247188] dracut-initqueue[623]:   WARNING: File locking is disabled.
         Starting dbus-broker.servi… - D-Bus System Message Bus...
[  OK  ] Found device dev-mapper-fe…ice - /dev/mapper/fedora-root.
[  OK  ] Reached target initrd-root…e.target - Initrd Root Device.
[  OK  ] Started dbus-broker.service - D-Bus System Message Bus.
[  OK  ] Finished dracut-initqueue.…rvice - dracut initqueue hook.
[  OK  ] Reached target remote-fs-p…eparation for Remote File Systems.
[  OK  ] Reached target remote-cryp…et - Remote Encrypted Volumes.
[  OK  ] Reached target remote-fs.target - Remote File Systems.
         Starting dracut-pre-mount.…ice - dracut pre-mount hook...
[  OK  ] Finished dracut-pre-mount.…rvice - dracut pre-mount hook.
         Starting systemd-fsck-root…heck on /dev/mapper/fedora-root...
[  OK  ] Finished systemd-fsck-root… Check on /dev/mapper/fedora-root.
         Mounting sysroot.mount - /sysroot..
...
[  OK  ] Stopped target network.target - Network.
[  OK  ] Stopped target remote-cryp…et - Remote Encrypted Volumes.
[  OK  ] Stopped target timers.target - Timer Units.
[  OK  ] Stopped dracut-pre-pivot.s…dracut pre-pivot and cleanup hook.
[  OK  ] Stopped target initrd.target - Initrd Default Target.
[  OK  ] Stopped target basic.target - Basic System.
[  OK  ] Stopped target initrd-root…e.target - Initrd Root Device.
[  OK  ] Stopped target initrd-usr-…get - Initrd /usr File System.
[  OK  ] Stopped target paths.target - Path Units.
[  OK  ] Stopped target remote-fs.target - Remote File Systems.
[  OK  ] Stopped target remote-fs-p…eparation for Remote File Systems.
[  OK  ] Stopped target slices.target - Slice Units.
[  OK  ] Stopped target sockets.target - Socket Units.
[  OK  ] Stopped target sysinit.target - System Initialization.
[  OK  ] Stopped target local-fs.target - Local File Systems.
[  OK  ] Stopped target local-fs-pr…reparation for Local File Systems.
[  OK  ] Stopped target swap.target - Swaps.
...
        Starting dbus-broker.servi… - D-Bus System Message Bus...
[  OK  ] Listening on cockpit.socket - Cockpit Web Service Socket.
[  OK  ] Reached target sockets.target - Socket Units.
[  OK  ] Started dbus-broker.service - D-Bus System Message Bus.
[  OK  ] Reached target basic.target - Basic System.
         Starting abrtd.service…RT Automated Bug Reporting Tool...
         Starting chronyd.service - NTP client/server...
         Starting dracut-shutdown.s…tore /run/initramfs on shutdown...
         Starting initial-setup.ser…ial Setup configuration program...
[  OK  ] Started irqbalance.service - irqbalance daemon.
         Starting lm_sensors.servic…m - Hardware Monitoring Sensors...
         Starting polkit.service - Authorization Manager...
         Starting rsyslog.service - System Logging Service...
         Starting smartd.service…rting Technology (SMART) Daemon...
         Starting sshd-keygen@ecdsa…SSH ecdsa Server Key Generation...
         Starting sshd-keygen@ed255…H ed25519 Server Key Generation...
         Starting sshd-keygen@rsa.s…enSSH rsa Server Key Generation...
[  OK  ] Reached target nss-user-lo…[0m - User and Group Name Lookups.
         Starting systemd-logind.se…ice - User Login Management...
[  OK  ] Finished dracut-shutdown.s…estore /run/initramfs on shutdown.
================================================================================
================================================================================

1) [x] Language settings                 2) [x] Time settings
       (English (United States))                (US/Eastern timezone)
3) [x] Network configuration             4) [x] Root password
       (Wired (eth0) connected)                 (Root account is disabled)
5) [ ] User creation
       (No user will be created)


Please make a selection from the above ['c' to continue, 'q' to quit, 'r' to
refresh]:
```

On the first boot, Fedora 37 Server ARM64 will walk you through basic setup
via text prompts. The basics are to change your Time Zone, set your
hostname via the Network configuration menu, and create a user/pass with
sudo access.

Once you are done login with your new user and password. Since my RPI4B is
already connected to ethernet and my router runs a DHCP server, I didn't
have to manually setup `eth0` as NetworkManager automatically obtained a
DHCP lease and ipv4 address. But I had to manually setup `wlan0` with
`nmcli`.


## Manually extend LVM Logical Volume and xfs file system

For some reason, `arm-image-installer` did not expand the LVM logical
volume to use up all available space on my 128GB SATA SSD. As you can see
below, the LVM physical volume was allocated 117GB of space, but the LVM
logical volume is only 5.41g in size:

```sh
pidora@pimax ~]$ lsblk -a
NAME            MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
sda               8:0    0 119.2G  0 disk
├─sda1            8:1    0   600M  0 part /boot/efi
├─sda2            8:2    0     1G  0 part /boot
└─sda3            8:3    0 117.7G  0 part
  └─fedora-root 253:0    0   5.4G  0 lvm  /
zram0           252:0    0   7.6G  0 disk [SWAP]
[pidora@pimax ~]$ sudo lvs
  LV   VG     Attr       LSize Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  root fedora -wi-ao---- 5.41g
[pidora@pimax ~]$ sudo pvs
[sudo] password for pidora:
  PV         VG     Fmt  Attr PSize   PFree
  /dev/sda3  fedora lvm2 a--  117.65g 112.24g
```

We can fix this with `lvextend` and finally by extending the xfs file
system the logical volume `root` with `xfs_growfs /`:

```sh
[pidora@pimax ~]$ sudo lvextend -l +100%FREE /dev/fedora/root
  Size of logical volume fedora/root changed from 5.41 GiB (1385 extents) to 117.65 GiB (30119 extents).
  Logical volume fedora/root successfully resized.
[pidora@pimax ~]$ sudo xfs_growfs /
meta-data=/dev/mapper/fedora-root isize=512    agcount=4, agsize=354560 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=1, sparse=1, rmapbt=0
         =                       reflink=1    bigtime=1 inobtcount=1 nrext64=0
data     =                       bsize=4096   blocks=1418240, imaxpct=25
         =                       sunit=0      swidth=0 blks
naming   =version 2              bsize=4096   ascii-ci=0, ftype=1
log      =internal log           bsize=4096   blocks=16384, version=2
         =                       sectsz=512   sunit=0 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
data blocks changed from 1418240 to 30841856
[pidora@pimax ~]$ lsblk -a
NAME            MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
sda               8:0    0 119.2G  0 disk
├─sda1            8:1    0   600M  0 part /boot/efi
├─sda2            8:2    0     1G  0 part /boot
└─sda3            8:3    0 117.7G  0 part
  └─fedora-root 253:0    0 117.7G  0 lvm  /
zram0           252:0    0   7.6G  0 disk [SWAP]
```

Now the mountpoint `/` for the root partition is using all available space,
117.7G.
