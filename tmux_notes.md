Tmux Notes
===============

# Summary

- Created on: 12 Jul 2022
- Created by: gojun077@gmail.com
- Last Updated: 08 Oct 2024

Tmux is a terminal multiplexer inspired by GNU Screen that uses keychords
similar to those of `screen`, but uses the prefix `C-b` instead of
`C-a`. `tmux` has a much smaller codebase and doesn't offer the *kitchen
sink* capabilities of `screen` such as zmodem transfers, serial console
functionality, etc. I use both `screen` and `tmux` at the same time in the
following manner:

I have one or two `screen` instances running on my local machine at all
times and `tmux` instances running on my commonly-accessed remote machines.
I used to use `screen` on both my local and remote machines, but then I ran
into the complexities of sending commands to GNU Screen within `screen` (an
*Inception* or Russian *Matryoshka* doll situation) which requires the
prefix `C-a` to be entered *twice* to send a command to the remote `screen`
instance.

If I access a remote `tmux` session through a locally running `screen`
session, however, I can avoid entering double `C-a` prefixes. If I want to
send a control command to my local `screen` instance, I use `C-a`, but if I
want to send a control command to my remote `tmux` instance, I use `C-b`.

`ncurses` TUI's work under both GNU `screen` and `tmux`, but I have found
the latter to have fewer incompatibilities with particularly 'blingy' TUI's
like `k9s`, especially when you are using custom k9s skins.

This file contains notes, recommendations, and snippets for working with
`tmux`.

# Topics

## Dealing with the TERM shell env variable

If you don't use a terminal multiplexer like GNU `screen` or `tmux`, it is
customary to `export TERM='...'` in your `.bashrc`. This is an
anti-pattern, however, for terminal multiplexers because they set `TERM`
env var on the fly. Terminals running within either multiplexer are
assigned `TERM=screen-256color` most commonly.

## Customizing tmux settings

While GNU screen has `~/.screenrc`, tmux has `~/.tmux.conf`


### tmux plugin manager (tpm)

https://github.com/tmux-plugins/tpm

`tmux` has a plugin manager that can install a variety of official plugins
from Github. First create the following directory and clone the tpm repo
from Github:

```sh
mkdir -p ~/.tmux/plugins
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
```

Add the following line to the bottom of your `~/.tmux.conf`:

```
# Initialize TMUX plugin manager
run '~/.tmux/plugins/tpm/tpm'
```

Apply changes from `~/.tmux.conf` to your running tmux session with
`tmux source-file ~/.tmux.conf`

Now within your tmux session, `C-b I` and all the plugins listed in
your `.tmux.conf` will be automatically installed by `tpm`.

- `C-b I`
  + install plugins from Github or any git repo
  + refreshes `tmux` environment
- `C-b U`
  + update plugin(s)
- `C-b M-u`
  + remove/uninstall plugins not on the plugin list

#### tmux plugin tmux-yank

https://github.com/tmux-plugins/tmux-yank

`tmux-yank` plugin allows you to copy text from `tmux` to the system
clipboard and vice-versa. After installation, enter copy-paste mode with
`C-b [` and select text as normal, but instead of `C-w` to copy your
selection to the `tmux` copy buffer, press `y`(like vim copy) to copy text
to the system clipboard.

To paste text from the system clipboard into `tmux`, simply `C-S-v`.

**Note**: On MacOS, access to the system keyboard may require a wrapper
script `reattach-to-user-namespace` (see
https://github.com/ChrisJohnsen/tmux-MacOSX-pasteboard)


#### tmux plugin tmux-logging (Note: I don't use this anymore)

https://github.com/tmux-plugins/tmux-logging

3rd party plugin that enables logging on-demand.

- `C-b P` start logging in current pane
- `C-b M-p` save all visible text in the current pane
- `C-b M-P` save all pane history to file
  + depends on `history-limit`
- `C-b M-c` clear pane history

This plugin is optional - `tmux` contains the built-in command `pipe-pane`
since 1.0 that allows you to pipe all `tmux` output to a file:

```sh
set-option -g default-command 'tmux pipe-pane -o "cat >>~/tmux_logs/tmux-#{session_name}-#{window_index}-#{pane_index}-`date +%Y%m%dT%H%M%S`.log" ; /bin/bash -l'
```

I prefer this to the behavior of the `tmux-logging` plugin because
`pipe-pane` is *set-and-forget*, whereas with the plugin you have to
remember to turn on logging per-pane.

#### tmux-themepack

This plugin is packaged in the default repos for Linux distros like
Debian under the name `tmux-themepack-jimeh`. You can also clone it
from Github:

```sh
git clone https://github.com/jimeh/tmux-themepack.git ~/.tmux-themepack
```

Or in my case, I would clone to `~/.tmux/plugins/tmux-themepack` because
that is where I store all my plugins for `tmux-plugin-manager`.

Within `~/.tmux.conf` you must then source your desired theme:

```conf
source-file "${HOME}/.tmux/plugins/tmux-themepack/powerline/default/green.tmuxtheme
```

You can also install via `tpm` by adding the following in `~/.tmux.conf`:

```conf
set -g @plugin 'jimeh/tmux-themepack
set -g @themepack 'powerline/block/blue'
```

If you install via `tpm` via `C-b I` within `tmux`, it will by default
install the plugin files under `~/.tmux/plugins/`

Note that to use `powerline` themes you must have the `powerline` status utility
and powerline fonts installed.

If you are a fan of `powerline` bling, I also recommend installing
`powerline-go` which is available from the default Fedora repos and
provides a powerline `PS1$` terminal prompt.


## tmux copy-paste mode + tmux-yank plugin

The key chord to *enter* copy-paste mode is similar to that of GNU Screen's
`C-a [` - `tmux` uses `C-b [`. Start marking text with `C-SPC` similar to
how you would set mark in Emacs (however in GNU `screen`you can start
marking text with just `SPC`), and `C-w` to copy the selection into the
`tmux` copy buffer.

To paste copied text into another `tmux` pane, navigate to the other pane
`C-b <paneNo>` and paste text with `C-b ]` (similar to GNU Screen's `C-a
]`. You can exit copy-paste mode in the original pane by pressing `q`.

If you have the `tmux-yank` plugin installed, you can copy text from
within `tmux` to the system clipboard. Selection is the same, `C-SPC`
and yank with `y` or native `C-w` will copy text into both the `tmux`
copy buffer and the system clipboard.


## Create named sessions with Tmux

```sh
$ tmux new -s <session_name>
```

`new` is an alias for `new-session`. The new session will attach to
the current terminal unless `-d` (detach) option is specified.

**Note**: `-s` and `-S` are totally different options; the first will create a
session with *name*, but the option with upper-case `S` does the following:

> Specify a full alternative path to the server socket.  If `-S` is
> specified, the default socket directory is not used and any `-L` flag is
> ignored.

And the `-L` option is used to specify the socket name of the `tmux`
session:

> tmux stores the server socket in a directory under TMUX_TMPDIR or /tmp if
> it is unset. The default socket is named *default*. This option allows a
> different socket name to be specified, allowing several independent tmux
> servers to be run. Unlike `-S` a full path is not necessary; the sockets
> are all created in a directory *tmux-UID* under the directory given by
> `$TMUX_TMPDIR` or in `/tmp`

Note that the default socket is named `default`.

## List existing tmux sessions

```sh
$ tmux list-sessions
$ tmux ls
```

## Kill tmux session

```sh
tmux kill-session -t <sessionName>
```

If you are within a `tmux` session you don't need to type `tmux`; just
`C-b : kill-session -t mysession`

## Kill all tmux sessions

```sh
tmux kill-server
```

https://askubuntu.com/questions/868186/how-to-kill-all-tmux-sessions-or-at-least-multiple-sessions-from-the-cli

## Attach to tmux session

```sh
$ tmux attach -t <session_name>
$ tmux a -t <session_name>
$ tmux a  # attach to last session
```

## Common control commands

- `C-b c`: new window/terminal
- `C-b [`: enter history buffer
  - `M-v`: Meta-v, move one screen up/back (Emacs keybindings)
  - `C-v`: move one screen down/forward (Emacs keybindings)
  - `C-SPC`: set mark to select text to copy (Emacs keybindings)
  - `C-w`: copy text to copy buffer (Emacs keybindings)
  - `C-s`: search for string, `n` for next match (Emacs keybindings)
  - `C-r`: reverse search for string (Emacs keybindings)
- `C-b %`: windows vertical split (like GNU Screen `C-a |`)
- `C-b "`: window horizontal split (like GNU Screen `C-a S`)
- `C-b x`: kill pane (like GNU Screen `C-a Q`)
- `C-b f`: find-window - search for string in window
- `C-b q`: `display-panes` briefly displays number of all panes on screen
- `C-b w`: view panel/split layout for current tab/window
  + also shows info on other windows and sessions
- `C-b` + `arrow keys`: up, down, left, right among panes
- `C-b x`: remove active split pane (Screen `C-a Q`)
- `C-b :new -s <session_name>`: create a new named session
- `C-b d`: detach from session
- `C-b ?`: list shortcuts
- `C-b ,`: rename window (tab)
- `C-b` + `[`: enter copy mode; `q` to exit copy mode
- `C-q`: unfreeze pane
- `C-s`: freeze pane
- `C-b: kill-window -t <num>`: kill tab `<num>`


## Splits

Unlike in GNU Screen, splits in `tmux` are unique to each tab/window. So
if I create a horizontal split with `C-b` + `"` in tab `0:bash`, if I
move to tab `1:bash` with `C-b` + `n`, the horizontal split is no longer
visible.

Also within a single `tmux` tab/window with multiple *panes* (splits), each
pane can run an independent process. i.e., in tab `0:bash`, I can have
panes `0` and `1`.


## Send commands to specific pane(s) or all panes

https://www.fosslinux.com/105802/effortlessly-sending-commands-to-multiple-tmux-panes.htm

**Send command to specific pane**

```sh
tmux send-keys -t [pane-id] "cmd" C-m
```

To list all panes in the current `tmux` session:

```sh
tmux list-panes
```

**Send command to all panes**


```sh
for _pane in $(tmux list-panes -a -F "#{pane_id}"); do
  tmux send-keys -t ${_pane} "my-cmd" C-m;
done
```

**Send command to specific pane in window**

```sh
tmux send-keys -t [window-id]:[pane-id] <cmd> <ENTER>
```

To list all windows in your `tmux` session:

```sh
tmux list-windows
```

**Synchronize input across all panes**

```sh
tmux set-window-option synchronize-panes on(off)
```

This will allow you to simultaneously type the same exact text into all
panes.

## Enable 256 colors in tmux

In your `~/.tmux.conf` add the following line:

```sh
set -g default-terminal "tmux-256color"
```

You can also use `screen-256color` above and it will still work.

## References

- https://tmuxcheatsheet.com/
- https://github.com/tmux/tmux/wiki/FAQ#how-do-i-use-a-256-colour-terminal
