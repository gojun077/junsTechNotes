Airbyte HOWTO
====================

# Summary

- Created on: Nov 13 2023
- Created by: gopeterjun@naver.com
- Last Updated: Nov 13 2023

[Airbyte](https://github.com/airbytehq/airbyte) is a popular open source
tool for extracting data from hundreds of proprietary services like JIRA,
Trello, Zendesk, etc and loading this data into NoSQL DB's, SQL DB's,
Google Big Table, Snowflake Data Lake, MinIO and other storage. It also
integrates well with AI tools like OpenAI.

The reason I am using Airbyte, however, is to get a backup of all my data
in Trello. Trello stores data for cards and boards in MongoDB, but there is
no way via Trello itself to export all your cards and boards although
Trello does allow Premium subscribers to
[export](https://support.atlassian.com/trello/docs/exporting-data-from-trello/)
some board info in `.csv` format.

You can also use the Trello API to download cards in `json` format, but
this is a piecemeal process, and getting all of your data out of Trello via
the API and then creating a backend DB + schema to store this data is a
headache.

Fortunately, if you use Airbyte, there is a built-in integration for Trello
that allows you to backup *ALL* data stored in Trello under a given user
account.

Airbyte also supports loading this data into various backends OOTB. Now
I don't have to worry about manually extracting data via the Trello API
and can focus on writing SQL queries to answer questions about my
data.

# Topics

## Install Airbyte self-hosted for `docker compose`




## Install Airbyte self-hosted for Kubernetes


