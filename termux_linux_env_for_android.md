Termux Linux terminal emulator for Android
==============================================

# Summary

- Created on: Apr 7 2023
- Created by: gojun077@gmail.com
- Last Updated: Apr 16 2023

Although Android runs on a heavily-customized Linux kernel, OOTB it does not
give users any access to the underlying Linux OS (i.e. shell, coreutils, etc).
There is a popular terminal emulator, `termux`, that can be installed
from the open source app store *F-droid*.

- https://f-droid.org/en/packages/com.termux/
- https://wiki.termux.com/wiki/Main_Page


# Topics

## Package installation and upgrading

Under the hood, `termux` uses the `apt` package manager like Ubuntu, but
it's best to stick with the termux package manager `pkg`:

```sh
# update repos
pkg update
# upgrade all packages
pkg upgrade
# install package
pkg install <pkgName>
```

You can also specify a single package repository mirror to use that is
closer to your geographical area for faster downloads or select a group of
package repo mirrors in a geography:

```sh
termux-change-repo
```

You will then be presented with an ncurses text UI where you can navigate with
the termux arrow keys and toggle options with `<SPACE>`.

## HOME, default user, and termux housekeeping

By default, `HOME` in termux is `/data/data/com.termux/files/home`.
Due to the special nature of permissions in Android OS (a fork of Linux
mainline) most apps run in a sandbox and there is no `sudo` nor is write
access allowed to system partitions like `/`, `/bin`, `/var`, etc.

Your user will be randomly generated and you can find your username with
`whoami`. It will be of the form `u0_aXXX`. You can use this username to
connect remotely to termux over `ssh` via password auth. First be sure to
set your user password with `passwd` before trying to ssh from a remote
host.


## Connect to Android phone over ssh through termux sshd

https://wiki.termux.com/wiki/Remote_Access

```sh
pkg update
pkg install openssh
# now execute 'sshd', which will run in the background
sshd
```

Note that termux sshd listens on port `8022` by default. You can enable
passwordless ssh login to termux by copying your ssh key to termux:

```sh
ssh-copy-id -i /path/to/mykey.pub user@hostname
```

If you already have `IdentityFile` defined for a remote host in your local
ssh client config file(s) you will have to run the above command with the
`-f` (`--force`) option to copy the add'l key to the remote host.

If this fails you can always manually add your pubkey on a newline in
`~/.ssh/authorized_keys` via a physical termux session on your Android
device.

Note that there is some weirdness with `ssh-agent` on termux; therefore
the tmux devs have created a special wrapper script `ssha`:

> If you wish to use an SSH agent to avoid entering passwords, the Termux
> openssh package provides a wrapper script named `ssha` (note the `a` at
> the end) for ssh, which:

>- Starts the ssh agent if necessary (or connect to it if already running).
>- Runs the `ssh-add` if necessary.
>- Runs the `ssh` with the provided arguments.

> This means that the agent will prompt for a key password at first run,
> but remember the authorization for subsequent runs.

Whether you launch `ssh-agent` manually or via `.bash_profile`/`.bashrc` in
termux, when you run `ps auxwww` you will not be able to find any process
for `ssh-agent` although you will be able to find the sockets under
`${PREFIX}/tmp/`. Also if you run a terminal multiplexer like `screen`
or `tmux`, manually adding a key to the agent with `ssh-add` in one tab
does not carry over to another tab! The only way to get this working is to
first use the termux `ssha` wrapper to ssh into a remote host and manually
enter the ssh key password; then subsequent `ssha` attempts will no longer
prompt you for the password.


## tmux terminal multiplexer

For the memory constrained environment on Android, you should keep the
window history size small.
