Creating an Openstack Pike Training VM
========================================

# CentOS 7 Image from Vagrant Cloud
- https://app.vagrantup.com/centos/boxes/7
- the resulting `vmdk` is less than 3 GB
## Edit Vagrantfile
Note that first network iface is NAT
- enable host-only networking (2nd iface)
    - I used `192.168.50.4` accessible through vboxnet0

# Packstack
## Setup Packages
- `sudo yum install -y centos-release-openstack-pike`
- `sudo yum update -y`
- `sudo yum install -y openstack-packstack`
## Hostname settings
- create hostname
    - `hostnamectl set-hostname rdo-pike`
- edit `/etc/hosts`
```
127.0.0.1   localhost rdo-pike localhost.localdomain
::1         localhost rdo-pike localhost.localdomain
192.168.50.4   rdo-pike
```
## Admin settings
- Create root password
    - `sudo passwd root`
- Stop and disable firewalld
- Stop and disable NetworkManager
- edit `/etc/ssh/sshd_config` to allow root and password logins
    - this is for students who are not familiar with `vagrant ssh`
    - `PermitRootLogin yes`
    - `PasswordAuthentication yes`
    - restart sshd
        - `systemctl restart sshd`

## Openstack settings
- change to root user
- `packstack --gen-answer-file aio_atto.ans`

## Run packstack
- `packstack --answer-file aio_atto.ans`

## Post-Install
- `source /root/keystonerc_admin`

# References
- https://www.rdoproject.org/install/packstack/
- https://linuxacademy.com/howtoguides/posts/show/topic/12453-deploying-openstack-rdo-allinone-vm-for-multidomain-support
    - note that some of the info is outdated
