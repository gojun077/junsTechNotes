NixOS package manager HOWTO
==============================

# Summary

- Created by: gopeterjun@naver.com
- Created on: 28 Jan 2024
- Last Updated: 06 Dec 2024

# Topics

## Installation

https://nixos.org/download/

### MacOS

multi-user install works great on MacOS

### Fedora

multi-user install does not work with SELinux-enabled distros, but
you can install `nix` via the single-user script.

## search for packages

### nix repl

```sh
$ nix repl
Welcome to Nix 2.19.3. Type :? for help.

nix-repl> :l <nixpkgs>
Added 20319 variables.

nix-repl>
```

At the `nix-repl>` prompt, type the name of the software package you're
looking for followed by the `<TAB>` key for autocompletion.

`C-d` to exit the `nix repl`.


## install packages (imperative)

In this example, we will install an up-to-date version of `bash`, as the
default version shipped with MacOS is quite outdated, as MacOS uses `zsh`
as the default shell.

```sh
$ nix-env --install --attr nixpkgs.bash
```

## uninstall packages (imperative)

```sh
nix-env --uninstall alacritty
```

Note that the above command doesn't use `--attr` nor does it use the
prefix `nixpkgs.`

## view currently installed package list (channel)

```sh
$ sudo nix-channel --list
Password:
warning: $HOME ('/Users/peter') is not owned by you, falling back to the one defined in the 'passwd' file ('/var/root')
nixpkgs https://nixos.org/channels/nixpkgs-unstable
```

## update package list (channel)

```sh
$ sudo nix-channel --update
Password:
warning: $HOME ('/Users/peter') is not owned by you, falling back to the one defined in the 'passwd' file ('/var/root')
unpacking channels...
warning: $HOME ('/Users/peter') is not owned by you, falling back to the one defined in the 'passwd' file ('/var/root')
```

https://discourse.nixos.org/t/how-to-upgrade-packages/6151


## upgrade packages

**Note**: the cmd below could have unintuitive results! IT SHOULD BE
AVOIDED. Instead, you should try to re-install updated packages or use
package lists stored in `.nix` files.

```sh
nix-env -u
```

Instead, just re-install existing packages with

```sh
$ nix-env --install --attr nixpkgs.foo
```

Where `foo` is the package name

Here is an example of installing a newer version of the `tree`
package after updating the `nix-channel`:

```sh
$ nix-env --install --attr nixpkgs.tree
replacing old 'tree-2.1.1'
installing 'tree-2.1.3'
this path will be fetched (0.03 MiB download, 0.11 MiB unpacked):
  /nix/store/c2rx6mvad345nyj5yyxcbhkyqyda65ih-tree-2.1.3
copying path '/nix/store/c2rx6mvad345nyj5yyxcbhkyqyda65ih-tree-2.1.3' from 'https://cache.nixos.org'...
building '/nix/store/mbhxgx60fg2960cir5n6q7y2h8lc4nvk-user-environment.drv'...
```

## List installed packages

```sh
nix-env -qa --installed "*"

# or the following
nix-env -q
```

## Installing google-cloud-sdk in nix

https://nixos.wiki/wiki/Google_Cloud_SDK

If you install `google-cloud-sdk` from `nix`, you will not be able to run
`gcloud components install` for adding additional components like
`gke-gcloud-auth-plugin`, for instance.

The workaround listed in the link above is to create a pseudo-package in
your nix config as follows:

```nix
let
  gdk = pkgs.google-cloud-sdk.withExtraComponents( with pkgs.google-cloud-sdk.components; [
    gke-gcloud-auth-plugin
  ]);
in
{
  packages = [
    gdk
  ];
}
```

You could also add the above to a `shell.nix` nix shell file so that your
nix shell environment will install `gcloud` CLI with component support
enabled.


## Note about nix-env

`nix-env` is provided as a convenience to users to enable imperative
package management, but the whole point of nixos is to enable *declarative*
package management. There are several different ways to define packages
declaratively and to install/update the entire list as IaC.


## Declarative Package Management using .nix files

This approach uses the `.nix` file format.

## Troubleshooting

### MacOS 15 Sequoia Beta - 'the user _nixbld1 does not exist'

```sh
$ sudo nix-channel --update
warning: $HOME ('/Users/macjun') is not owned by you, falling back to the one defined in the 'passwd' file ('/var/root')
unpacking channels...
warning: $HOME ('/Users/macjun') is not owned by you, falling back to the one defined in the 'passwd' file ('/var/root')
error: the user '_nixbld1' in the group 'nixbld' does not exist
error: program '/nix/store/ylwh78ikfn6w4vz1hw1qj0bdj414swqx-nix-2.19.3/bin/nix-env' failed with exit code 1
```

https://github.com/NixOS/nix/issues/10892

This issue is caused by MacOS 15 services taking up the UID's `301`, `302`,
`303`, `304`:

```sh
# list non-nix UID's
dscl . list /Users UniqueID | grep -v _nixbld | sort -n -k2
nobody                   -2
root                     0
daemon                   1
_uucp                    4
_taskgated               13
...
_aonsensed               300
_modelmanagerd           301
_reportsystemmemory      302
_swtransparencyd         303
_naturallanguaged        304
_oahd                    441
macjun                   501
```

Previously `_nixbld1`, `_nixbld2`, `_nixbld3`, `_nixbld4` occupied UID's
301 to 304, but now these UID's are used by `_modelmanagerd`,
`_reportsystemmemory`, `_swtransparencyd`, and `_naturallanguaged`,
respectively.

The workaround is to create new UID's for `_nixbld{1..4}` which have become
orphaned. It seems that UID 400-440 is free, so I used a shell snippet
shared by a user in the issue thread from Github:

```sh
for i in {1..4}; do
  sudo dscl . -create "/Users/_nixbld${i}" UniqueID $(( 400 + ${i} ))
  sudo dscl . -create "/Users/_nixbld${i}" PrimaryGroupID 30000
  sudo dscl . -create "/Users/_nixbld${i}" IsHidden 1
  sudo dscl . -create "/Users/_nixbld${i}" RealName "_nixbld${i}"
  sudo dscl . -create "/Users/_nixbld${i}" NFSHomeDirectory '/var/empty'
  sudo createhomedir -cu "_nixbld${i}"
  sudo dscl . -create "/Users/_nixbld${i}" UserShell /sbin/nologin
done
```

Finally, let's check the UID's of `_nixbld*` users

```sh
# check _nixbld* UID's
dscl . list /Users UniqueID | grep _nixbld | sort -n -k2
_nixbld5                 305
_nixbld6                 306
_nixbld7                 307
...
_nixbld30                330
_nixbld31                331
_nixbld32                332
_nixbld1                 401
_nixbld2                 402
_nixbld3                 403
_nixbld4                 404
```

Now that `_nixbld{1..4}` users now have UID 401 to 404, `nix` commands
should work again.


## References

- https://checkoway.net/musings/nix/
  - declarative package management
- https://github.com/NixOS/nix/issues/2374
  - nix multi-user install doesn't work with SELinux
