Common Linux Utils HOWTO
============================

# Summary

- Created on: Jan 18 2024
- Created by: gopeterjun@naver.com
- Last Updated: Jan 18 2024

This guide covers the packages `util-linux` (previously `util-linux-ng`)
and `coreutils` packages containing commonly-used Linux utilties for system
administration.

# Topics

## list of binaries in `util-linux`

```
/bin/dmesg
/bin/findmnt
/bin/lsblk
/bin/more
/bin/mountpoint
/bin/su
/bin/wdctl
/sbin/agetty
/sbin/blkdiscard
/sbin/blkid
/sbin/blkzone
/sbin/blockdev
/sbin/chcpu
/sbin/ctrlaltdel
/sbin/findfs
/sbin/fsck
/sbin/fsck.cramfs
/sbin/fsck.minix
/sbin/fsfreeze
/sbin/fstrim
/sbin/hwclock
/sbin/isosize
/sbin/mkfs
/sbin/mkfs.bfs
/sbin/mkfs.cramfs
/sbin/mkfs.minix
/sbin/mkswap
/sbin/pivot_root
/sbin/runuser
/sbin/sulogin
/sbin/swaplabel
/sbin/switch_root
/sbin/wipefs
/sbin/zramctl
/usr/bin/addpart
/usr/bin/choom
/usr/bin/chrt
/usr/bin/delpart
/usr/bin/fallocate
/usr/bin/fincore
/usr/bin/flock
/usr/bin/getopt
/usr/bin/hardlink
/usr/bin/ionice
/usr/bin/ipcmk
/usr/bin/ipcrm
/usr/bin/ipcs
/usr/bin/last
/usr/bin/lscpu
/usr/bin/lsipc
/usr/bin/lslocks
/usr/bin/lslogins
/usr/bin/lsmem
/usr/bin/lsns
/usr/bin/mcookie
/usr/bin/mesg
/usr/bin/namei
/usr/bin/nsenter
/usr/bin/partx
/usr/bin/prlimit
/usr/bin/resizepart
/usr/bin/rev
/usr/bin/setarch
/usr/bin/setpriv
/usr/bin/setsid
/usr/bin/setterm
/usr/bin/taskset
/usr/bin/uclampset
/usr/bin/unshare
/usr/bin/utmpdump
/usr/bin/whereis
/usr/sbin/chmem
/usr/sbin/ldattach
/usr/sbin/readprofile
/usr/sbin/rtcwake
/sbin/getty
/usr/bin/lastb
/usr/bin/linux32
/usr/bin/linux64
```

https://en.wikipedia.org/wiki/Util-linux

## list of binaries in `coreutils`

```
/usr/bin/[
/usr/bin/arch
/usr/bin/b2sum
/usr/bin/base32
/usr/bin/base64
/usr/bin/basename
/usr/bin/basenc
/usr/bin/cat
/usr/bin/chcon
/usr/bin/chgrp
/usr/bin/chmod
/usr/bin/chown
/usr/bin/cksum
/usr/bin/comm
/usr/bin/cp
/usr/bin/csplit
/usr/bin/cut
/usr/bin/date
/usr/bin/dd
/usr/bin/df
/usr/bin/dir
/usr/bin/dircolors
/usr/bin/dirname
/usr/bin/du
/usr/bin/echo
/usr/bin/env
/usr/bin/expand
/usr/bin/expr
/usr/bin/factor
/usr/bin/false
/usr/bin/fmt
/usr/bin/fold
/usr/bin/groups
/usr/bin/head
/usr/bin/hostid
/usr/bin/id
/usr/bin/install
/usr/bin/join
/usr/bin/link
/usr/bin/ln
/usr/bin/logname
/usr/bin/ls
/usr/bin/md5sum
/usr/bin/mkdir
/usr/bin/mkfifo
/usr/bin/mknod
/usr/bin/mktemp
/usr/bin/mv
/usr/bin/nice
/usr/bin/nl
/usr/bin/nohup
/usr/bin/nproc
/usr/bin/numfmt
/usr/bin/od
/usr/bin/paste
/usr/bin/pathchk
/usr/bin/pinky
/usr/bin/pr
/usr/bin/printenv
/usr/bin/printf
/usr/bin/ptx
/usr/bin/pwd
/usr/bin/readlink
/usr/bin/realpath
/usr/bin/rm
/usr/bin/rmdir
/usr/bin/runcon
/usr/bin/seq
/usr/bin/sha1sum
/usr/bin/sha224sum
/usr/bin/sha256sum
/usr/bin/sha384sum
/usr/bin/sha512sum
/usr/bin/shred
/usr/bin/shuf
/usr/bin/sleep
/usr/bin/sort
/usr/bin/split
/usr/bin/stat
/usr/bin/stdbuf
/usr/bin/stty
/usr/bin/sum
/usr/bin/sync
/usr/bin/tac
/usr/bin/tail
/usr/bin/tee
/usr/bin/test
/usr/bin/timeout
/usr/bin/touch
/usr/bin/tr
/usr/bin/true
/usr/bin/truncate
/usr/bin/tsort
/usr/bin/tty
/usr/bin/uname
/usr/bin/unexpand
/usr/bin/uniq
/usr/bin/unlink
/usr/bin/users
/usr/bin/vdir
/usr/bin/wc
/usr/bin/who
/usr/bin/whoami
/usr/bin/yes
/usr/sbin/chroot
```

https://www.gnu.org/software/coreutils/


## Commands and examples

### fallocate

From the manpage:

> `fallocate` is used to manipulate the allocated disk space for a file,
> either to deallocate or preallocate it. For filesystems which support the
> fallocate(2) system call, preallocation is done quickly by allocating
> blocks and marking them as uninitialized, requiring no IO to the data
> blocks. This is much faster than creating a file by filling it with
> zeroes.

Although you can use `dd` to write zeroes to a file, `fallocate` is much
faster.

Examples:

```sh
# fallocate -l <file size: M, G> /path/to/file
fallocate -l 1G test.img

# create swap file

fallocate -l 1G myswap.swap
sudo mkswap myswap.swap
sudo swapon myswap.swap
sudo swapon -s # list swap space
sudo chmod 600 myswap.swap

# create file to mount as file system
fallocate -l 1G crypt-mnt
mkfs.ext4 crypt-mnt
tune2fs -O encrypt /dev/deviceName
sudo mount -t ext4 -o owner crypt-mnt /my/mount/point
```

The `-o owner` option allows an ordinary user to mount the fs as long
as they are the owner of the device. Note that if you mount a file as
a `loop` device, you cannot write to that mount point!
