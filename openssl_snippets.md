openssl CLI snippets
=========================

# Summary

- Created on: Mar 28 2022
- Created by: gojun077@gmail.com
- Last Updated: Apr 18 2024

Useful `openssl` snippets for verifying local and remote SSL certificates.


# Topics

## Get SSL cert full info from local pem file


```sh
openssl x509 -text -noout -in myfile.pem
```

## Get SSL cert expiration date from local pem file

```sh
openssl x509 -enddate -noout -in myfile.pem
```

## Get SSL cert expiration date from remote server (HTTPS)

```sh
echo | \
  openssl s_client -showcerts -connect eatpeppershothot.cyou:443 | \
  openssl x509 -enddate -noout
```

This should give you something like:

```sh
depth=2 C = US, O = Internet Security Research Group, CN = ISRG Root X1
verify return:1
depth=1 C = US, O = Let's Encrypt, CN = R3
verify return:1
depth=0 CN = eatpeppershothot.cyou
verify return:1
DONE
notAfter=Jun  7 06:55:09 2022 GMT
```

## Get SSL cert full info from remote server (HTTPS)

```sh
echo | \
  openssl s_client -showcerts -connect eatpeppershothot.cyou:443 | \
  openssl x509 -text
```

## Get TLS cert full info from remote non-HTTPS service

SSL certs can also be used in non-HTTPS contexts, for example in redis on
TCP 639, for ftp+tls (ftps) on TCP 21, etc. The `openssl` options change
when checking non-HTTPS endpoints. See below:

```sh
echo | \
  openssl s_client -showcerts -starttls ftp -connect \
  newftp.unity3d.com:21 | openssl x509 -text
```

This returns the following:

```
depth=2 C = US, O = DigiCert Inc, OU = www.digicert.com, CN = DigiCert Global Root CA
verify return:1
depth=1 C = US, O = DigiCert Inc, CN = DigiCert SHA2 Secure Server CA
verify return:1
depth=0 C = DK, L = Copenhagen, O = Unity Technologies ApS, CN = *.unity3d.com
verify return:1
220 FTP Server ready.
DONE
...
```

## Get TLS cert expiration from FTP(S) service

```sh
echo | \
  openssl s_client -showcerts -starttls ftp -connect \
  newftp.test.it.unity3d.com:21 | openssl x509 -enddate -noout
```

This will return much shorter output showing just the cert expiration

```
depth=2 C = US, O = Internet Security Research Group, CN = ISRG Root X1
verify return:1
depth=1 C = US, O = Let's Encrypt, CN = R3
verify return:1
depth=0 CN = newftp.test.it.unity3d.com
verify return:1
220 FTP Server ready.
DONE
notAfter=Oct  4 15:16:45 2022 GMT
```


Note that the `-starttls` option for `openssl s_client` can take the
following values:

- smtp
- pop3
- imap
- ftp
- xmpp
- xmpp-server
- telnet
- irc
- mysql
- postgres
- lmtp
- nntp
- sieve
- ldap

## Note about openssl on MacOS

The default `openssl` provided by MacOS is LibreSSL, which is a non-GNU
fork of the original openssl project around the time of the *Heartbleed*
vulnerability. In 2022, however, the original project is more active than
LibreSSL and all the commands in this guide depend on the **GNU version**
of `openssl`.

To install the GNU openssl, use homebrew. You will also have to create
an alias to openssl, I used `opensslgnu` and created a symlink to
`/usr/local/bin/`.


## Concatenate multiple `.crt` files into a bundled `.crt`

Let's Encrypt issues `.crt` files that contain the cert for the domain, the
intermediate cert, and the root CA cert. I believe Let's Encrypt names this
file `chain.pem`. But if you get a `.zip` or other archive from a SSL/TLS
cert issuer like Comodo or Digicert, you will see something like the
following after extracting files from the archive:

- `server.key`
  + private key for `server.crt`
- `server.csr`
  + cert signing request
- `server.crt`
  + domain cert
- `intermediate.crt`
- `rootCA.crt`

To combine the 3 certs into a single cert like Let's Encrypt, you will need
to concatenate the files into a single file in the following order:

```sh
cat server.crt intermediate.crt rootCA.crt > tls.crt
# verify that the cert is valid
openssl verify -verbose -purpose sslserver -CAfile tls.crt server.crt
```

You should get the output `server.crt: OK`

## Generate sha256 hash of a string

```sh
$ echo -n "foobar" | openssl dgst -sha256
SHA2-256(stdin)= c3ab8ff13720e8ad9047dd39466b3c8974e592c2fa383d4a3960714caef0c4f2
```
