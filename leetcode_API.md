Leetcode API Notes
=======================

# Summary

- Created on: Apr 14 2021
- Created by: gojun077@gmail.com
- Last Updated: Sep 26 2021


# Topics

## Leetcode API

> Leetcode has two API endpoints which list problems and whether they
> have been solved by the currently logged-in user or not.


- https://leetcode.com/api/problems/algorithms
- https://leetcode.com/api/problems/all

> It returns the keys `user_name`, `num_solved`, `num_total`, `ac_easy`,
> `ac_medium`, `ac_hard`, `frequency_high`, `frequency_mid` all of which
> contain a single value of type `int`, except for `user_name` and
> `category_slug` which contains a single value of type `str`. The key
> we're interested in is `stat_status_pairs` which returns a json array
> where each record contains data about individual problems.

> The difference between the two endpoints above is that the `..algorithms`
> endpoint has `num_total` 1655 questions, while the `..all` endpoint has
> `num_total` 1825.

> `ac_{easy,medium,hard}` refers to the number of accomplished/solved
> problems for the currently logged-in user.


## Logging in

> Even if you don't log in, you can still get info from the `..algorithms`
> and `all` endpoints, but `user_name`, `num_solved`, etc. will all be set
> to Null (NoneType).

> To see which problems your username has solved, however, you must first
> login. You can login programmatically via session cookie (although this
> requires that you first manually login via a web browser). It is also
> possible to see information on how many problems a user has solved by
> scraping their public profile page.

### Login via cookie from browser session using Python requests

> If you have already logged in via the web interface, you can export your
> leetcode session cookie with a web extension like *Export cookie JSON*.
> There are various keys in the json cookie file, but the only ones you
> need to worry about are the key-keyval pairs

- `"name": "LEETCODE_SESSION"`
- `"value": "<691-char alphanum str>"`

> In Python, create a new dict to store your leetcode session cookie.
> It should contain a single key-keyval pair:

```python
lc_cookie = {"LEETCODE_SESSION": "691 char string"}
```

> Now you can pass this cookie into your GET request as follows:

```python
import requests

lc_url = "https://leetcode.com/api/problems/all"
lc_sess = requests.Session()
lc_resp = lc_sess.get(lc_url, cookies=lc_cookie)
```

> *Note*: with every request in the session, your cookie will be
> updated, which you can verify with `lc_sess.cookies.get_dict()`.
> You can also see cookie expiration with `lc_resp.cookies` which
> will show you a CSRF token and its expiration date (which is usually
> 1 year for the CSRF token only).

> If everything worked correctly you should get `.status_code` of `200` and
> when you parse the response object to JSON you should see your username
> for the key `"user_name"`:

```
In [71]: lc_resp.status_code
Out[71]: 200

In [72]: lc_data = lc_resp.json()

In [73]: type(lc_data)
Out[73]: dict

In [74]: lc_data.keys()
Out[74]: dict_keys(['user_name', 'num_solved', 'num_total', 'ac_easy', 'ac_medium', 'ac_hard', 'stat_status_pairs', 'frequency_high', 'frequency_mid', 'category_slug'])

In [75]: lc_data['user_name']
Out[75]: 'gojun077'
```

> *NOTE*: in the exported JSON cookie file, there is a a key named
> `"expires"` which tells you the date when your Leetcode session cookie
> will expire. The value is in UNIX epoch time and the expiration is 14
> days from the present.


### Get detailed data on each problem (logged-in user)

> Assuming that you have successfully logged in via session cookie, you can
> now see detailed data on all the LC problems including `status` of the
> problem.

> The data for all the problems on Leetcode can be found in the list of
> dicts returned by key `'stat_status_pairs'`.

```python
In [102]: lc_data['stat_status_pairs'][5]
Out[102]:
{'stat': {'question_id': 2002,
  'question__article__live': None,
  'question__article__slug': None,
  'question__article__has_video_solution': None,
  'question__title': 'Stone Game VIII',
  'question__title_slug': 'stone-game-viii',
  'question__hide': False,
  'total_acs': 934,
  'total_submitted': 2387,
  'frontend_question_id': 1872,
  'is_new_question': False},
 'status': None,
 'difficulty': {'level': 3},
 'paid_only': False,
 'is_favor': False,
 'frequency': 0,
 'progress': 0}
```

> If you have solved a problem, the key value for key `"status"` will
> be `"ac"` (accomplished).

```python
lc_prob_L = lc_data_b['stat_status_pairs']

for i,p in enumerate(lc_prob_L):
    if p["status"] == "ac":
       print(f"Solved problem index is {i}")
...
```

> The above snippet prints the indexes of the 31 LC problems I've solved as
> of May 23 2021.  Let's look at one of them:

```python
In [105]: lc_data_b['stat_status_pairs'][39]
Out[105]:
{'stat': {'question_id': 1961,
  'question__article__live': None,
  'question__article__slug': None,
  'question__article__has_video_solution': None,
  'question__title': 'Maximum Ice Cream Bars',
  'question__title_slug': 'maximum-ice-cream-bars',
  'question__hide': False,
  'total_acs': 24776,
  'total_submitted': 31087,
  'frontend_question_id': 1833,
  'is_new_question': False},
 'status': 'ac',
 'difficulty': {'level': 2},
 'paid_only': False,
 'is_favor': False,
 'frequency': 0,
 'progress': 0}
```

> We can see that 31,087 users attempted this problem and that 24,776
> successfully solved it. It is classified as a *medium* difficulty
> problem.


## Find number of solved questions for some user via web scraping

> Note that in addition to using the API, you can also scrape the public
> page `https://leetcode.com/<username>` for the number of problems solved
> by any user as long as you know their username. The advantage of this
> method is that it does not require login.

> The examples here use Python SDK for Selenium for web scraping and
> headless Chrome.

> Using *Element Inspection* from the Chrome Developer Tools, I identified
> a tag and value I want to scrape:

```html
<div class="total-solved-count__2El1 css-57pydk">29</div>
```

> In 2021, naive screen-scraping of native HTML from webpages will often
> not work, as most webpages now load lots of Javascript that must be
> rendered client-side in order to display content on the webpage. I also
> tried naively running Beautiful Soup on the text results of
> `requests.get(myurl)`, but without client-side rendering of JS I couldn't
> find the tag above.

> The solution is to use something like Selenium with a headless browser to
> render the webpage and execute client-side Javascript. Nowadays (2021) it
> is easy to run browsers like FF and Chrome in headless mode, controlled
> by Selenium and other web testing frameworks.

### Using Selenium with CSS Selectors

> I want to make a partial match on the CSS selector `total-solved-count`
> without the trailing UID.

> Useful links:
- https://selenium-python.readthedocs.io/locating-elements.html#locating-elements-by-css-selectors
- https://saucelabs.com/resources/articles/selenium-tips-css-selectors

From the Saucelabs link above:

> CSS in Selenium has an interesting feature of allowing partial string
> matches using `^=`, `$=`, or `*=`.

> Note that on Ubuntu 20.04 LTS you need to have the packages
> `python3-selenium` and `chromium-chromedriver` installed for
> the following to work.


```python
from selenium import webdriver

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('window-size=1920,1080')
browser = webdriver.Chrome(options=options)
browser.get("https://leetcode.com/gojun077")

>>> browser.find_element_by_css_selector("div[class*='total-solved-count']").text
'31'
```

> `*=` Matches a substring, i.e. in CSS: `a[id*='id_pattern']`
> Searching for substring `total-solved-count` in `<div class>`
> tags yields the text string `'31'`.

> You can also use CSS selectors to match nested CSS tags using
> *CSS combinators*.

https://www.w3schools.com/css/css_combinators.asp

- `~` general sibling selector
- `+` adjacent sibling selector
- `>` child selector
- ` ` (SPACE) descendant selector

> The Leetcode user profile page also shows the number of questions solved
> by difficulty, *Easy*, *Medium*, and *Hard*. This information could be
> used to weight medium and hard problems with a higher value than that of
> easy ones in a custom grading scheme. For example, easy problems could be
> given a multiplier of 1, medium problems 2, and hard problems a
> multiplier of 3.

> As of Apr 19 2021 I have solved a total of 31 Leetcode problems, 24 easy,
> 7 medium, and 0 hard. I could therefore assign this a score of

`24*1 + 7*2 = 38`

> So how can I scrape `24`, the count of Easy problems I've solved
> so far? It resides in the following nested CSS block:

```html
...
<div class="css-57z4bo-StatisticWrapper" data-difficulty="Easy">
  <div class="css-1e6c3bq-DifficultyLabel e5i1odf7">Easy</div>
  <div class="css-x9b7oa">
    <span class="difficulty-ac-count__jhZm">24</span>
    <span class="difficulty-total-count__y_em">480</span>
  </div>
</div>
```

> If you select on the outermost CSS tag, you will get 3 strings as
> follows:

```python
...
browser.find_element_by_css_selector(
    "div[class*='css-57z4bo-StatisticWrapper'][
        data-difficulty='Easy']").text

'Easy\n24480'
```

> Here is an example of selecting the children elements from the nested CSS
> block above to get the number of easy problems solved by user `gojun077`:

```python
...
easy_p0 = "div[class*='css-57z4bo-StatisticWrapper'][data-difficulty='Easy']"
easy_c0 = f"{easy_p0} > div[class*='css-x9b7']"
easy_solved = f"{easy_c0} > span[class*='difficulty-ac-count']"
browser.find_element_by_css_selector(easy_solved).text

'24'
```

> You can also get the total number of easy questions, the number of
> medium questions solved, etc.

```python
...
easy_p0 = "div[class*='css-57z4bo-StatisticWrapper'][data-difficulty='Easy']"
easy_c0 = f"{easy_p0} > div[class*='css-x9b7']"
easy_total = f"{easy_c0} > span[class*='difficulty-total-count']"
browser.find_element_by_css_selector(easy_total).text

'480'

med_p0 = "div[class*='css-57z4bo-StatisticWrapper'][data-difficulty='Medium']"
med_c0 = f"{med_p0} > div[class*='css-x9b7']"
med_solved = f"{med_c0} > span[class*='difficulty-ac-count']"
browser.find_element_by_css_selector(med_solved).text

'7'

med_p0 = "div[class*='css-57z4bo-StatisticWrapper'][data-difficulty='Medium']"
med_c0 = f"{easy_po} > div[class*='css-x9b7']"
med_total = f"{easy_c0} > span[class*='difficulty-total-count']"
browser.find_element_by_css_selector(med_total).text

'968'

hard_p0 = "div[class*='css-57z4bo-StatisticWrapper'][data-difficulty='Hard']"
hard_c0 = f"{hard_p0} > div[class*='css-x9b7']"
hard_solved = f"{hard_c0} > span[class*='difficulty-ac-count']"
browser.find_element_by_css_selector(hard_solved).text

'0'

hard_p0 = "div[class*='css-57z4bo-StatisticWrapper'][data-difficulty='Hard']"
hard_c0 = f"{hard_p0} > div[class*='css-x9b7']"
hard_total = f"{hard_c0} > span[class*='difficulty-total-count']"
browser.find_element_by_css_selector(hard_total).text

'387'
```


### Using Selenium with XPath Selectors

> Although using CSS selectors is preferred because of a more readable
> syntax, XPath (using XML syntax) is able to select elements that cannot
> be accessed with CSS selectors.

> You can use the Chrome Developer Tools to copy an element's XPath. For
> example, to get the value `24`, the number of easy questions user
> `gojun077` has solved, you would use the following XPath:

```python
...
myX = "//*[@id='profile-root']/div[2]/div/div[2]/div[1]/div[2]/div/div[2]/div/div[1]/div[2]/span[1]"
browser.find_element_by_xpath(myX).text

'24'
```

XPath for the total number of easy questions:

- `//*[@id='profile-root']/div[2]/div/div[2]/div[1]/div[2]/div/div[2]/div/div[1]/div[2]/span[2]`

XPath for the number of medium questions solved:

- `//*[@id='profile-root']/div[2]/div/div[2]/div[1]/div[2]/div/div[2]/div/div[2]/div[2]/span[1]`

XPath for the total number of medium questions:

- `//*[@id='profile-root']/div[2]/div/div[2]/div[1]/div[2]/div/div[2]/div/div[2]/div[2]/span[2]`

XPath for the number of hard questions solved:

- `//*[@id='profile-root']/div[2]/div/div[2]/div[1]/div[2]/div/div[2]/div/div[3]/div[2]/span[1]`

...

## Login to Leetcode using Selenium, save session cookie for reuse



# References

- https://medium.com/@mattmkim/codetime-a-chrome-extension-that-rewards-me-for-solving-leetcode-problems-cc2831c52724
- https://www.foxerlee.top/2020/02/05/Leetcode-crawler/
