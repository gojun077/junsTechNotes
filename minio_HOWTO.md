minio S3-compatible object storage HOWTO
============================================================

# Summary

- Created on: Apr 19 2023
- Created by: gopeterjun@naver.com
- Last Updated: Jul 17 2023


# Topics

## Installation on ARM64 machine or VM

https://min.io/docs/minio/linux/operations/install-deploy-manage/deploy-minio-single-node-single-drive.html#minio-snsd

https://dl.min.io/server/minio/release/linux-arm64/

There are `.rpm` and `.deb` packages available. I used the `.deb` for
my RPI3B running RPi OS 64-bit Debian Bullseye.

It installs the following:

```sh
$ sudo dpkg -i minio_20230504214430.0.0_arm64.deb
$ dpkg -L minio
/usr
/usr/local
/usr/local/bin
/usr/local/bin/minio
/etc
/etc/systemd
/etc/systemd/system
/etc/systemd/system/minio.service
```

https://blog.min.io/configuring-minio-with-systemd/

### Create minio-user

Although the `.deb` package installs the binary and a systemd service
file, it doesn't setup the user `minio-user` or permissions for the
directory or mount point that will be used for object storage. We have
to do this manually. Run the following as `root`:

```sh
groupadd -r minio-user
useradd -M -r -g minio-user minio-user
mkdir -p /var/lib/minio
chown minio-user:minio-user /var/lib/minio
```

I will use `/var/lib/minio` as the object storage area for my setup
on RPI3B; it is ideal to use a separate storage device, but this is for
PoC and testing, so it should be ok.

### Create Environment file

```sh
touch /etc/default/minio
```

Populate the environment file with the following:

- `MINIO_ROOT_USER=`
- `MINIO_ROOT_PASSWORD=`
- `MINIO_VOLUMES="/path/to/storage"`

The following link shows various CLI options and environment variables for
minio:

https://min.io/docs/minio/linux/reference/minio-server/minio-server.html#minio-console

Note that minio command line options can be passed within
`/etc/default/minio` via the variable `MINIO_OPTS=""`

In my case, I populated this env var with the following:

```
MINIO_OPTS="--address :9000 --console-address :9001"
```

If you do not specify a port for `console-address`, minio will listen
on a random high port for the web console (which you can connect to with
a web browser).

### Minio web console

https://min.io/docs/minio/linux/administration/minio-console.html

### TLS Cert setup

https://min.io/docs/minio/linux/operations/network-encryption.html

https://github.com/minio/minio/blob/master/docs/tls/README.md

If you are using certs from Let's Encrypt, the certs will be named
`<FQDN>.crt`, `FQDN.key`, but Minio requires that the TLS certs
be named:

- `public.crt`
- `private.key`

The default path to TLS certs is `~/.minio/certs` in the `$HOME`
directory of the user running `minio`.

You can specify a custom TLS cert path with the command line option
`--certs-dir` or `-S`

You will also have to add some additional environment vars to your
`/etc/default/minio` file to look like:

```
MINIO_ROOT_USER=yourAdminUserHere
MINIO_ROOT_PASSWORD=<REDACTED>
MINIO_VOLUMES=/var/lib/minio  # use whatever mountpoint you wish
#MINIO_PROMETHEUS_URL=
MINIO_SERVER_URL=https://your.domain.net
MINIO_CONSOLE_ADDRESS=https://your.domain.net
MINIO_OPTS="--address :9000 --console-address :9001 -S /etc/minio/certs/"
```

**Note**: If you don't specify a minio root user and password, the default
values will be used, `minioadmin:minioadmin`, so specify something to
avoid this behavior.

Now after restarting `minio.service` TLS should be working when you point
your web browser to https:// endpoint at port `9000` (which will automatically
forward you to `9001`)


### caddy webserver as reverse proxy in front of minio

Caddy will listen for TLS/HTTPS on `tcp 9000, 9001` and pass HTTP traffic
to Minio listening on `tcp 9002, 9003` on the backend. First we must make
some changes to `/etc/default/minio`:

```
MINIO_ROOT_USER=myadmin
MINIO_ROOT_PASSWORD=REDACTED
MINIO_VOLUMES="/var/lib/minio"
MINIO_OPTS="--address 127.0.0.1:9002 --console-address 127.0.0.1:9003"
```

Next we need to edit `/etc/caddy/Caddyfile`:

```
# tcp 80 or 443
rpi3b.finch-blues.ts.net {
        # Set this path to your site's directory.
        root * /usr/share/caddy

        # Enable the static file server.
        file_server

        # Another common task is to set up a reverse proxy:
        # reverse_proxy localhost:8080

        # Or serve a PHP site through php-fpm:
        # php_fastcgi localhost:9000
}

rpi3b.finch-blues.ts.net:9000 {
        reverse_proxy localhost:9002
}

rpi3b.finch-blues.ts.net:9001 {
        reverse_proxy localhost:9003
}
```

Note that I am using Caddy with Tailscale TLS certs from Let's
Encrypt. Caddy v2.5+ supports `tailscale cert` OOTB, but some add'l
tailscale settings might need to be edited depending on your Linux distro,
i.e. on Debian where `caddy` runs under a non-root user named `caddy`,
you have to edit `/etc/default/tailscaled` so that this non-root user
has access to `tailscaled` to request a TLS cert:

```
TS_PERMIT_CERT_UID=caddy
```

The advantage of using Caddy as a reverse proxy in front of Minio is that
it terminates HTTPS/TLS and also supports as well as certs with `certbot` or
`tailscale` in this case.

Later we may need to add some settings to forward proxy headers for the
Minio API, but for now the web console is showing up without any problems:

![minio_web](images/minio_web_console_behind_caddy_rev_proxy.png)

**References**:

- https://caddyserver.com/docs/quick-starts/reverse-proxy
- https://tailscale.com/kb/1190/caddy-certificates/


## Installing minio on k8s

https://min.io/docs/minio/kubernetes/upstream/operations/installation.html

There is a k8s operator for `minio` which installs a CRD. The installation
guide uses the `kubectl minio` plugin to install `minio`.

https://min.io/docs/minio/kubernetes/upstream/reference/kubectl-minio-plugin.html#command-kubectl.minio

Download the appropriate `kubectl-minio` operator for your arch (`amd64`,
`arm64`, etc) at https://github.com/minio/operator/releases

I used the arm64 binary because I plan to run `kubectl-minio` on my
RPI4B:

```sh
$ wget https://github.com/minio/operator/releases/download/v5.0.6/kubectl-minio_5.0.6_linux
_arm64 -O kubectl-minio
--2023-07-17 13:19:56--  https://github.com/minio/operator/releases/download/v5.0.6/kubectl-minio_5.0.6_linux_arm64
Resolving github.com (github.com)... 20.200.245.247
Connecting to github.com (github.com)|20.200.245.247|:443... connected.
HTTP request sent, awaiting response... 302 Found
Location: https://objects.githubusercontent.com/github-production-release-asset-2e65be/154578184/48133692-05fe-4895-96b
5-6afe0c69a12c?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20230717%2Fus-east-1%2Fs3%2Faws
4_request&X-Amz-Date=20230717T041957Z&X-Amz-Expires=300&X-Amz-Signature=f980e0c99d523ea17a732ff513eb1d271ea1f9981f2aa1d
178da07b4e88ba651&X-Amz-SignedHeaders=host&actor_id=0&key_id=0&repo_id=154578184&response-content-disposition=attachmen
t%3B%20filename%3Dkubectl-minio_5.0.6_linux_arm64&response-content-type=application%2Foctet-stream [following]
...
$ chmod a+x kubectl-minio
$ sudo mv kubectl-minio /usr/local/bin
$ kubectl-minio version
v5.0.6
```

I haven't tested if a `kubectl-minio` binary for a different architecture
than the arch it is administerting will work; i.e. `kubectl-minio` for
amd64 running on a Ryzen NUC box while my k3s cluster is running on RPI4B
arm64 arch...

Now you can install with the following:

```sh
$ kubectl minio init
```

After a successfull installation you should see something like:

```sh
$ kubectl get all --namespace minio-operator
NAME                                  READY   STATUS    RESTARTS   AGE
pod/minio-operator-7dbf54467d-mxqv2   1/1     Running   0          7m33s
pod/console-756f85dc86-nkw6k          1/1     Running   0          7m32s
pod/minio-operator-7dbf54467d-ltgcj   1/1     Running   0          7m33s

NAME               TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)             AGE
service/operator   ClusterIP   10.43.212.107   <none>        4221/TCP            7m34s
service/sts        ClusterIP   10.43.50.13     <none>        4223/TCP            7m34s
service/console    ClusterIP   10.43.147.79    <none>        9090/TCP,9443/TCP   7m34s

NAME                             READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/console          1/1     1            1           7m33s
deployment.apps/minio-operator   2/2     2            2           7m34s

NAME                                        DESIRED   CURRENT   READY   AGE
replicaset.apps/console-756f85dc86          1         1         1       7m33s
replicaset.apps/minio-operator-7dbf54467d   2         2         2       7m34s
```

Note that the default installation does not include an `ingress`.  The
installation guide recommends connecting to the minio GUI console using
`kubectl minio proxy` or port forwarding from your local machine, but I
prefer to create an ingress so that the minio console can be reached
without manually creating a proxy or port forward.

### Make `console` service accessible from Tailscale

There are several ways to make a k8s service available from your
`tailscale` tailnet.

- (1) Install the `tailscale` operator in your k8s cluster
  + https://tailscale.com/kb/1236/kubernetes-operator/
  + https://github.com/tailscale/tailscale/blob/main/cmd/k8s-operator/manifests/operator.yaml
- (2) Define a `tailscale` sidecar in your k8s deployment
  + https://tailscale.com/kb/1185/kubernetes/#sample-sidecar
- (3) Define a `tailscale` proxy pod
  + https://tailscale.com/kb/1185/kubernetes/#sample-proxy
- (4) Use `traefik proxy v3.0` ingress with `console` as backend service
  + `k3s v1.27.3` uses `traefik` installed by `helm`
  + `helm.sh/chart=traefik-21.2.1` -> docker tag `v.2.9.9`
  + https://traefik.io/blog/exploring-the-tailscale-traefik-proxy-integration/

Here is info about the `console` service in ns `minio-operator`:

```
Name:              console
│ Namespace:         minio-operator
│ Labels:            app.kubernetes.io/instance=minio-operator
│                    app.kubernetes.io/name=operator
│                    name=console
│ Annotations:       operator.min.io/authors: MinIO, Inc.
│                    operator.min.io/license: AGPLv3
│                    operator.min.io/support: https://subnet.min.io
│ Selector:          app=console
│ Type:              ClusterIP
│ IP Family Policy:  SingleStack
│ IP Families:       IPv4
│ IP:                10.43.147.79
│ IPs:               10.43.147.79
│ Port:              http  9090/TCP
│ TargetPort:        9090/TCP
│ Endpoints:         10.42.0.40:9090
│ Port:              https  9443/TCP
│ TargetPort:        9443/TCP
│ Endpoints:         10.42.0.40:9443
│ Session Affinity:  None
│ Events:            <none>
```

Currently the `console` service is only available via TCP 9443 HTTPS on the
internal pod IP `10.42.0.40`. The service terminates HTTPS traffic and
forwards plain HTTP to `10.43.147.79` on TCP 9090.


### Use tailscale operator to expose k8s service

**(1) Edit your tailnet policy file file** (admin console)

https://login.tailscale.com/admin/acls/file

```hujson
"tagOwners": {
   "tag:k8s-operator": [],
   "tag:k8s": ["tag:k8s-operator"],
}
```

**(2) Create an OAuth Client** (admin console)

https://login.tailscale.com/admin/settings/oauth

Create the client with `Devices` write scope and the tag
`tag:k8s-operator`.

#### Use tailscale sidecar to expose k8s pod to tailnet





### Get TLS certs from LE via `tailscale` sidecar

https://github.com/tailscale/tailscale/issues/7588

> I took the `postStart` command from your example and applied it to the
> tailscale sidecar instead to call tailscale cert and share the generated
> cert with the main container over an emptyDir volume.

I think it should be possible to save the tailscale `.crt` and `.key`
files to 
