New Arch installation on LenovoS310 rev 2015.04
==================================================

# Summary
- Last Updated: 2019.03.22
- Date Created: 2015.11.12
- Updated by: gojun077@gmail.com

> **Note**: `install.txt` is included in the root of the liveUSB env,
> so it is not necessary to connect to Internet to refer to the Arch
> installation walkthrough

# Installation instructions


## UEFI boot from archiso
- 1. a message will show up that says 'Failed to Start Loader'
- 2. Select OK to use HashTool to enroll hash two files
    + `loader.efi`
    + `vmlinuz.efi`
- 3. Select `loader.efi` and confirm
- 4. Go to the parent dir, then into archiso
- 5. Select `vmlinuz.efi` and confirm
- 6. Exit the HashTool menu
- 7. In Boot Device Selection Menu -> Arch Linux archiso x86_64 UEFI CD

> at the cmd prompt, enter the following

```sh
od -An -t u1 /sys/firmware/efi/efivars/SecureBoot-XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
```
> where `XXXX...` is different from machine to machine. use TAB completion.
> If you get something like `6  0  0  0  1` where last integer is '1', you are in
> secure boot


- get internet connection (the wifi switch was turned off)
    + `wifi-menu`
- activate wired interface to install remotely via GNU Screen
- change address and iface name for your particular HW
    + `ip a add 192.168.10.99/24 broadcast 192.168.10.255 dev enp1s0`
- activate `sshd.socket` (instead of `sshd.service`)
    + `systemctl start sshd.socket`
- change root PW so you can remotely connect through `ssh` & `screen`
    + `passwd`
...
- find name of local disk
    + `lsblk`

> Note that the pink Lenovo S310 contains a 100GB SSD in addition to a
> 500GB HDD (magnetic disk). The 500GB drive appears as `/dev/sda`
> while the SSD appears as `/dev/sdb`.  You can change this in BIOS at
> the next boot

-  create partition table for new GPT partition (`gdisk`)

> press `o` which will erase all existing partition info and will not
> convert MBR to GPT. Also change gdisk type to `ef00` *EFI System
> Partition*. Note that *EFI System Partition* must be formatted as
> `FAT32` for UEFI systems. This partition will be mounted as `/boot`
> in Archlinux.

- `gdisk` commands in order:
    + `n` (new partition)
    + `1` partition number 1
    + `default`
    + `+512M`
    + `ef00` partition type for ESP
    + `n` (new partition)
    + `2` partition number 2
    + `default`
    + `default` size (use remaining disk)
    + `8E00` partition type for LVM

## create LUKS container for LVM volumes

> For my second Arch install after Ubuntu nuked my existing LUKS container,
> I first installed Ubuntu with UEFI using just regular partitions
> and then installed Arch second. This time I am using slightly
> more robust `cryptsetup luksFormat` options. References:

https://wiki.archlinux.org/index.php/Dm-crypt/Device_encryption#Cryptsetup_usage

```sh
cryptsetup -v --cipher aes-xts-plain64 --key-size 512 --hash sha512 \
  --iter-time 5000 --use-random luksFormat /dev/sdb5
```

> open LUKS container and use mountpoint `lvm`

`cryptsetup open --type luks /dev/sdb5 lvm`

> create PV in the open LUKS container

`pvcreate /dev/mapper/lvm`

> create VG in the PV

`vgcreate ARCH /dev/mapper/lvm`

> create all logical volumes in VG

```
lvcreate -L 8G ARCH -n swapvol
lvcreate -L 25G ARCH -n rootvol
lvcreate -L 20G ARCH -n varvol
lvcreate -l +100%FREE ARCH -n homevol
```

> create swap partition

`mkswap /dev/ARCH/swapvol`

> make file systems on ESP and LV's. EFI System Partition will be `/boot`
> Format ESP as `FAT32`. Note: `mkfs.fat` requires `dostools` package.

```sh
for i in {rootvol,varvol,homevol}; do
  mkfs.ext4 /dev/ARCH/$i
done
```

> create partitions on /dev/sda (during my 2nd install these
> already existed)

```
gdisk
o
...
mkfs.ext4 /dev/sda{1,2}
```

> create mount points and mount system partitions (GPT)

```sh
mount /dev/ARCH/rootvol /mnt
mkdir /mnt/{boot,var,home,MULTIMEDIA}
mount /dev/sdb1 /mnt/boot
mount /dev/ARCH/varvol /mnt/var
mount /dev/ARCH/homevol /mnt/home
#mount /dev/sda1 /mnt/MULTIMEDIA
mount /dev/sda2 /mnt/MULTIMEDIA
```

>  activate swap

`swapon /dev/ARCH/swapvol`

> edit mirrorlist with fast mirrors

`vim /etc/pacman.d/mirrorlist`

> build packages

`pacstrap /mnt base base-devel`

> generate fstab file

`genfstab -p -L /mnt >> /mnt/etc/fstab`

> *change root* into new system

`arch-chroot /mnt /bin/bash`

> After `chroot`ing into the new system, check `/etc/fstab` to make
> sure all partitions are there; Also verify `df -h` to make sure all
> system partitions are mounted.


> **Important WARNING about `chroot` after `pacstrap`**

> If you can only find `/` mounted after chrooting into the pacstrap
> system, the problem is that you created
> `/mnt/{boot,var,home,MULTIMEDIA}` *BEFORE* you mounted `/` (root
> volume) on `/mnt` with `mount /dev/ARCH/rootvol /mnt` This will
> cause the mountpoints `{boot,var,home,...}` to be non-existent
> within the chroot!!!

```sh
printf "%s\\n" "### Partition Sanity Check for pacstrap chroot ###"
df -h
cat /etc/fstab
```

> Install add'l packages (these packages should not require X11) For
> UEFI GPT don't install `grub`; Archlinux now uses `systemd bootctl`

```sh
pacman -Syyu --noconfirm screen mc vim cronie dialog wpa_supplicant \
  libnl git cmus mplayer fbida p7zip alsa-utils alsa-tools abs emacs \
  gdb shellcheck ipython2 ipython bitlbee ncdu android-tools \
  android-udev htop irssi dhclient openssh firewalld nfs-utils \
  vsftpd rsync docker intel-ucode auctex archey3 wget syslinux \
  gptfdisk traceroute darkhttpd dnsmasq dmidecode wireshark-cli nmap
```

> Note that `dnsmasq` already contains a `tftp` server, so you don't
> need to install `tftp` separately.

> Optional packages for a graphical environment

```sh
pacman -Syyu --noconfirm xfce4 xfce4-clipman-plugin xmonad lxdm \
  chromium firefox xarchiver meld gdmap deluge quodlibet gimp \
  inkscape simplescreenrecorder scrot motion xawtv gparted ibus \
  ibus-hangul ibus-qt pulseaudio pavucontrol gpa virtualbox \
  dkms-host-dkms virtualbox-guest-iso linux-headers \
  xf86-input-synaptics ttf-baekmuk wqy-bitmapfont wqy-zenhei \
  redshift zathura zathura-djvu jre8-openjdk
```

> set hostname

`echo someHostname > /etc/hostname`

> add pinkS310 to `/etc/hosts`; replace `localhost` with `pinkS310`
> output should be something like: `pinkS310.localdomain pinkS310`

```
vim /etc/hosts
:%s/localhost/pinkS310/g
```

> set time zone
`ln -s /usr/share/zoneinfo/Asia/Seoul /etc/localtime`

> set time and date (if you haven't done so already)

`date -s "XXXX-XX-XX YY:YY"`
`hwclock -w`

> enable locales in /etc/locale.gen (en_US)

`vim /etc/locale.gen`

> generate locales

`locale-gen`

> set locale preferences in /etc/locale.conf

`echo "LANG=$LANG" > /etc/locale.conf`

> add LUKS and LVM modules to kernel config `/etc/mkinitcpio.conf`

```
vim /etc/mkinitcpio.conf
encrypt lvm2
# As of 2016.12, the hooks should be
HOOKS="base systemd sd-encrypt autodetect modconf block sd-lvm2 filesystems keyboard fsck"
```

> **NOTE**: In `mkinitcpio.conf` if you use the `systemd` module, you
> will have to use slightly different `HOOKS`, namely, `sd-encrypt`
> and `sd-lvm2`.  If you use `sd-encrypt`, the `bootctl` menu options
> will be different.

> regenerate initramfs image

`mkinitcpio -p linux`


> INSTALL UEFI BOOTLOADER

> `bootctl` is part of `systemd` and the base install

`bootctl install`

> create `/boot/loader/entries/arch.conf`

> if using an Intel CPU, make sure pkg `intel-ucode` is installed
> and add `initrd /intel-ucode.img` followed by actual `initrd`.
> Note that the device UUID should be that of the raw crypto device

```
vim /boot/loader/entries/arch.conf
title Arch
linux /vmlinuz-linux
initrd /intel-ucode.img
initrd /initramfs-linux.img
options cryptdevice=UUID=f121bc44-c8c2-4881-b9d5-da4d1b49f3e4:ARCH root=/dev/mapper/ARCH-rootvol rw
```

> If you use kernel modules `systemd`, `sd-encrypt` and `sd-lvm` the
> options are different:

```
options luks.uuid=ba04cb93-328c-4f9a-a2a9-4a86cee0f592 luks.name=UUID=ba04cb93-328c-4f9a-a2a9-4a86cee0f592=luks root=UUID=a99f56ac-c34c-45b4-9595-6c2fed6b60e7 rw
```

> modify `/boot/loader/loader.conf` to select the *default* entry
> (without `.conf`)

```
timeout 10
default arch
```

### FINALIZE SETTINGS, REBOOT

> Change root passwd

`passwd`

> exit from chroot

`exit`

> unmount chroot partitions

`umount -R /mnt`

> reboot

`reboot`

### AFTER REBOOT
# create new user
useradd -m archjun
# create pw for new user
passwd archjun
# add user to group wheel
usermod -a -G wheel archjun
# edit /etc/sudoers to enable wheel
visudo
# enable sshd socket daemon
systemctl enable sshd.socket
systemctl start sshd.socket
# enable wired and wireless networks so you can continue setup from a
# remote machine (over GNU Screen)
# enable and start firewalld

wifi-menu
ip l set enp1s0 up
ip a add 192.168.10.99/24 broadcast 192.168.10.255 dev enp1s0

# Disable root SSH
# (not necessary if you git clone dotfiles repo and run script)
vim /etc/ssh/sshd_config
PermitRootLogin no

# Enable serial console access for regular user
# first command for /dev/ttyX
# second command for /dev/ttyUSBX
usermod -a -G tty archjun
usermod -a -G uucp archjun

##  download PKGBUILD's from AUR (download to ~/Downloads)
mkdir $HOME/Downloads
# Dropbox
wget https://aur.archlinux.org/cgit/aur.git/snapshot/dropbox.tar.gz
# dropbox-cli
wget https://aur.archlinux.org/cgit/aur.git/snapshot/dropbox-cli.tar.gz
# Spideroak One
wget https://aur.archlinux.org/cgit/aur.git/snapshot/spideroak-one.tar.gz
# smtp-cli
wget https://aur.archlinux.org/cgit/aur.git/snapshot/smtp-cli.tar.gz
# Rescuetime
wget https://aur.archlinux.org/cgit/aur.git/snapshot/rescuetime.tar.gz
# xprintidle (req'd by Rescuetime)
wget https://aur.archlinux.org/cgit/aur.git/snapshot/xprintidle.tar.gz
# ttf-monofur
wget https://aur.archlinux.org/cgit/aur.git/snapshot/ttf-monofur.tar.gz
# ttf-nanum (Naver)
wget https://aur.archlinux.org/cgit/aur.git/snapshot/ttf-nanum.tar.gz
# ttf-nanumgothic_coding
wget https://aur.archlinux.org/cgit/aur.git/snapshot/ttf-nanumgothic_coding.tar.gz
# wqy-microhei-kr-patched
wget https://aur.archlinux.org/cgit/aur.git/snapshot/wqy-microhei-kr-patched.tar.gz
# ttf-ms-fonts
wget https://aur.archlinux.org/cgit/aur.git/snapshot/ttf-ms-fonts.tar.gz
# mnemosyne
wget https://aur.archlinux.org/cgit/aur.git/snapshot/mnemosyne.tar.gz
# imagewriter (SUSE Studio)
wget https://aur.archlinux.org/cgit/aur.git/snapshot/imagewriter.tar.gz
# chromium pepper flash (Adobe Flash replacement)
wget https://aur.archlinux.org/cgit/aur.git/snapshot/chromium-pepper-flash.tar.gz

# extract AUR pkg's (extract to ~/Downloads/AUR/)
cd ~/Downloads || exit 2
mkdir -p $HOME/Downloads/AUR

for i in {dropbox.tar.gz,dropbox-cli.tar.gz,spideroak-one.tar.gz,smtp-cli.tar.gz,rescuetime.tar.gz,xprintidle.tar.gz,ttf-monofur.tar.gz,ttf-nanum.tar.gz,ttf-nanumgothic_coding.tar.gz,wqy-microhei-kr-patched.tar.gz,ttf-ms-fonts.tar.gz,mnemosyne.tar.gz,imagewriter.tar.gz,chromium-pepper-flash.tar.gz}; do
  tar -xvf $i -C $HOME/Downloads/AUR/
done

# build AUR packages


# setup Spideroak
# https://spideroak.com/faq/how-do-i-set-up-a-new-device-from-the-command-line
SpiderOakONE --setup=-
ln -s $HOME/"SpiderOak Hive" $HOME/SpiderOak_Hive
# setup Dropbox
# http://www.dropboxwiki.com/tips-and-tricks/install-dropbox-in-an-entirely-text-based-linux-environment
/opt/dropbox/dropboxd
This computer isn't linked to any Dropbox account...
Please visit https://www.dropbox.com/cli_link_nonce?nonce=xxxxxxxxxxxxxxxxxxxxx to link this device.


# setup ALSA - all channels are muted by default; go to MASTER and press 'm'
# in non-X11 env, stick with ALSA (do not install pulseaudio)
alsamixer

## Clone git repos containing misc system configs
# clone github repo 'dotfiles'
git clone https://github.com/gojun077/jun-dotfiles.git $HOME/dotfiles
# clone bitbucket repo 'bin'
git clone https://gojun077@bitbucket.org/gojun077/bin-scripts.git $HOME/bin
# run dotfiles symlink script
bash $HOME/dotfiles/create_symlinks.sh
# setup pomo2beeminder
mkdir $HOME/Documents
git clone https://github.com/gojun077/pomo2beeminder.git $HOME/Documents/pomo2beeminder
# clone growin_wo repo

# clone term_sessions repo


# setup cmus
# copy all the files from ~/.config/cmus/ from another machine
# to the new machine; o/w cmus sometimes refuses to even start
# https://wiki.archlinux.org/index.php/Cmus
# https://bbs.archlinux.org/viewtopic.php?id=203305 - extra settings for ALSA

scp * archjun@192.168.10.58:~/.config/cmus/

# setup bitlbee (refer to separate tut)
chown -R bitlbee:bitlbee /var/lib/bitlbee

## Setup Virtualbox
# enable dkms service
systemctl enable dkms
systemctl start dkms

## setup PXE
chown -R archjun:archjun /usr/local
mkdir /usr/local/tftpboot
git clone https://gojun077@bitbucket.org/gojun077/pxeboot.git /usr/local/tftpboot/pxe
