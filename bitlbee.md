Setting Up Bitlbee
======================
Bitlbee is a chat-to-IRC gateway that works well with irssi.

# Summary
- Last Updated: 2020.02.24
- gojun077@gmail.com

> This guide covers `bitlbee` setup on various environments such
> as vanilla Linux, Windows Subsystem for Linux (Win10), and MacOS.


# Install and Setup

## Ubuntu 16.04+
- `sudo apt update; sudo apt install bitlbee`
- Ubuntu version with libpurple enabled:
    + `sudo apt install bitlbee-libpurple`

> Warning; `bitlbee` doesn't have dependencies on X11/graphical
> desktop, but `bitlbee-libpurple` does (because of video and
> audio codes for `pidgin`)

- rename default config file
    + `sudo mv /etc/bitlbee/bitlbee.conf /etc/bitlbee/bitlbee.orig`

- create symlink from dotfiles repo `bitlbee` file to `/etc/bitlbee`
    + `sudo ln -s ~/dotfiles/bitlbee /etc/bitlbee/bitlbee.conf`
    + this is my personal conf for bitlbee including:
        + md5 hashed operator PW

- restart bitlbee systemd service (if it is running)
    + `sudo systemctl restart bitlbee`

## WSL1 Windows 10

> Assuming you are using Ubuntu 16.04+ in WSL, install the same
> packages as above. However, WSL1 does not support `systemd`,
> so you will **not** be running `bitlbee` in `ForkDaemon` mode.
> Instead, you should run it as a regular single-process `Daemon`.
> Therefore the config file you are using will be different in
> the config file symlink step.

> I also encountered a `Ping Timeout` issue when running bitlbee
> under WSL; According to `bitlbee.conf` comments on this feature:

```
## BitlBee can send PING requests to the client to check whether it's still
## alive. This is not very useful on local servers,
```

> So in `bitlbee-wsl` I disabled the timeout with `PingTimeOut = 0`


- create symlink from dotfiles repo
    + `sudo ln -s ~/dotfiles/bitlbee-wsl /etc/bitlbee/bitlbee.conf`

> You also need to make sure that `~/.irrsi/config` has been properly
> symlinked from the dotfiles repo and that `~/.irssi/certs/` PATH
> exists and contains `irssi.pem`

> When starting `bitlbee` in WSL, you will have to run it as `sudo`
> but after starting, it will change the process to the `bitlbee`
> user.

## MacOS

> `bitlbee` is available from `homebrew`

`brew install bitlbee`

After installation, you will see the following notes:

> To have `launchd` start `bitlbee` now and restart at login: `brew
> services start bitlbee` Or, if you don't want/need a background
> service you can just run: `bitlbee -D`

`bitlbee -D` simply runs the program in *Daemon* mode

> Note that installation via homebrew will create `bitlbee` configs
> in the path `/usr/local/etc/bitlbee`, **not** in the canonical path
> `/etc/bitlbee`.

> According to https://wiki.bitlbee.org/HowtoHangoutsMacOS
> about enabling Google Hangouts in `bitlbee` on MacOS, it states
> that bitlbee should be running in Fork mode, otherwise plugins
> like `libpurple` won't work. So in your `bitlbee.conf` for MacOS
> set `RunMode = ForkDaemon`.

> Create a symlink from the dotfiles repo

`sudo ln -s ~/dotfiles/bitlbee-mac /usr/local/etc/bitlbee/bitlbee.conf`


# Using bitlbee

## Log into bitlbee chat G/W from irssi
- make sure bitlbee systemd service is running
    + `systemctl status bitlbee`

- or if running bitlbee in simple Daemon mode,
    + `ps -ef | grep bitlbee`

If you have any connection issues and see the error

```
Irssi: Unable to connect server localhost port 6667 [Connection refused]
```

Try running `telnet` to make sure bitlbee is listening on 6667


```sh
# Happy path
junbuntu@APSEODESK515:~/.irssi$ telnet localhost 6667
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
:localhost NOTICE * :BitlBee-IRCd initialized, please go on
^C
quit

# Daemon not running / bitlbee.conf misconfiguration
junbuntu@APSEODESK515:~/.irssi$ telnet localhost 6667
Trying 127.0.0.1...
telnet: Unable to connect to remote host: Connection refused
```


- from irssi try to connect to local bitlbee server
    + `/connect localhost -p <bitlbee pw>`

- once connected you automatically will join `&bitlbee` channel
- or manually join with `/join &bitlbee`

## Add chat accts to bitlbee

### gmail
- `account add jabber gojun077@gmail.com <app-specific-PW>>`
    + You will see: *<@root> Account successfully added with tag gtalk*
- `account gtalk on`
- `account gtalk set nick_format %full_name`
- chat with individual users
  + `/msg userName`
- google groupchat
    + Requires `purple-hangouts` plugin for `libpurple`
    + this is b/c group chat uses *Hangouts* instead of XMPP

### slack
> Slack stopped supporting IRC and XMPP on 2018.05.15, which meant that
> `bitlbee` could no longer interface w/ Slack OOTB. It _is_ possible,
> however, to use `libpurple` and the plugin `slack-libpurple` to
> access Slack channels in bitlbee.

- https://www.theregister.co.uk/2018/03/09/slack_cuts_ties_to_irc_and_xmpp/

- https://github.com/dylex/slack-libpurple

#### Setting up `slack-libpurple`

> On Debian/Ubuntu, there aren't any binary packages for `slack-libpurple`
> so you will have to clone the above repo and then build the source
> yourself. You will need to have the development packages for
> libpurple installed. On Ubuntu `libpurple-dev`.


#### archived info on setting up slack (DEPRECATED)
- `account add jabber archjun@openstackkr.xmpp.slack.com <slack-xmpp-pw>`
    + Note that *slack-irc-pw* can be found from *.../account/gateways*:
    + **openstackkr.slack.com/account/gateways** in my case
    + `account jabber on`
- Add group chat channels
    + `chat add archjun@openstackkr.xmpp.slack.com general@conference.openstackkr.xmpp.slack.com`
    + `chat add archjun@openstackkr.xmpp.slack.com 2016-study-upstream@conference.openstackkr.xmpp.slack.com`
- join channels
    + `/join #general`
- chat with individual users
    + `/msg userName someText` (will open a session in 'localhostl)
    + for some reason, sending `/msg username` without text doesn't work

### twitter

```
account add twitter gojun077
account twitter set stream off
account twitter on
```

> A new irssi channel will be opened by twitter it will provide a URL
> you must open in a browser in browser, click *Authorize App* and
> record the PIN Enter this PIN into the *twitter_<username>* channel
> Finally a new irssi channel with all your tweets will be created

> In mid-2018, Twitter deprecated streams functionality through the
> twitter API, so you must explicitly tell Bitlbee to disable
> streams in twitter.

https://wiki.bitlbee.org/HowtoTwitter/StreamDeprecation

### discord

#### install necessary dependencies

> Since no binary packages are yet available, you must build
> from source.

```sh
sudo apt install libglib2.0-dev autotools-dev libtool \
  dh-autoreconf bitlbee-dev
```

#### build bitlbee-discord

```sh
cd /path/to/bitlbee-discord/source
./autogen.sh
./configure
make
sudo make install
```

#### add discord account

```
account add discord <email> <password>
account discord on

chat list discord
chat add discord !1 #mydiscordchannel
chan #mydiscordchannel set auto_join true
/join #mydiscordchannel
```

#### `Login error: captcha-required` Workaround

> 아래와 같은 에러를 받는다면:

```
12:24 <@root> discord - Login error: Login error: captcha-required
```

> Firefox 브라우저로 `discordapp.com`에 들어가서 *Login* 버튼 누르고
> email 주소를 입력하고 그 email 주소로 오는 Verification mail 클릭하세요.
> *Login* 한 번 다시 시도해야 할 수도 있습니다. 로긴이 성공적으로 되면 웹
> 브라우저 옵션에서 *Web Developer* -> *Storage Inspector* ->
> *Local Storage* -> http://discordapp.com -> *token* 확인해보면 긴
> 토큰 값이 나와요. 그 값을 double-click하고 복사한 다음 bitlbee cli에
> 아래와 같이 설정하세요:

```
account discord off
account discord set_token <긴 토큰 값>
account discord on
```

#### 관련 문서

https://github.com/sm00th/bitlbee-discord/
https://github.com/sm00th/bitlbee-discord/issues/150
https://github.com/sm00th/bitlbee-discord/issues/118


### telegram

> To use telegram with `bitlbee` and `libpurple`, you must have the
> `telegram-purple` plugin installed. On Ubuntu, you can add a PPA
> which provides this package

```sh
sudo add-apt-repository ppa:nilarimogard/webupd8
sudo apt-get update
sudo apt-get install telegram-purple
```

> To add your telegram account to bitblee:

```
account add telegram +countrycodePHONENUMBER
account telegram on
```

> There shouldn't be any spaces between the country code and your
> phone number.

> Upon your first login attempt Telegram will open a new window in
> `irssi` and ask you to enter a 5-digit login code that was sent to
> your Telegram client. Enter the code in the new in in the new
> window(tab) and you will be authenticated. After getting the code
> you can also quickly add it to the bitlbee profile for telegram
> by typing `account telegram set password <5-digit PW>`



### matrix.org

#### Installation from binary package

```sh
sudo apt install purple-matrix
```

> Note: the version must be at least `0.0.0+git20180325-1build1`
> otherwise you will get the following error when you try to connect
> to matrix

```
@root> matrix - Login error: Invalid response from homeserver
```

> Googling for this error suggests that it has something to do with
> response chunking from matrix that is not gracefully handled by
> older versions of `purple-matrix`.

#### Installation from source

> First install dependencies for building `purple-matrix` from
> source

```sh
sudo apt install libpurple-dev libjson-glib-dev libglib2.0-dev \
  libhttp-parser-dev libsqlite3-dev libolm-dev
```

#### Add matrix account to bitlbee
```
account add matrix @username:matrix.org mySuperSeekretPass
account matrix on
```

#### filtering `@root> matrix - Connected` msg's in &bitlbee channel

> `purple-matrix` periodically sends a notification to the bitlbee
> control channel that it is connected. Since the messages keep
> repeating, you may want to filter them.

```
/ignore -regexp -pattern matrix.*Connected &bitlbee
```

#### Join matrix.org channel

> The canonical name of matrix channels is of the form
> `!abcXYZ...:matrix.org`, with the uuid part in front of the
> colon taking up 18 characters. This is known as the Matrix
> `Internal room ID`, and you can view it from `riot.im`'s
> _Channel Settings_ -> _Advanced_ menu:

![Matrix Internal room ID](images/matrix_riot.im_internal_room_id.png)

> However in `irssi` when you want to join a matrix channel you
> cannot use the full canonical name; instead you must use the
> truncated name provided by `irssi`, which includes the 18 chars
> plus the colon plus the string `mat`

> So if I want to add the channel `!hEuEYSWKomxnWlSKqi:matrix.org`
> I would enter the following in `irssi`

```
/join #!hEuEYSWKomxnWlSKqi:mat
```


## Saving all your chat and SNS accts
- `register <pw-to-save-all-accts>`
- `save`

## Recall saved chat and SNS accts
- `identify <PW-registered-previously>`

## Set up `auto_reconnect` with delay

```
set auto_reconnect true
set auto_reconnect_delay 60
```


# References

- https://wiki.archlinux.org/index.php/Bitlbee#Telegram
    + setting up bitlbee to work with Telegram
- https://sourcedigit.com/20374-install-telegram-purple-ppa-pidgin-plugin-on-ubuntu-linux/
    + setting up telegram-purple on Ubuntu
- https://wiki.bitlbee.org/Commands#account_.3Caccount_id.3E_set
    + commands for editing accounts
- https://wiki.bitlbee.org/
    + List of services bitlbee can connect to
- https://www.bitlbee.org/user-guide.html
    + &bitlbee control channel commands
