Yabai tiling WM HOWTO
===========================

# Summary

- Created on: Nov 17 2023
- Created by: gopeterjun@naver.com
- Last Upated: Mar 8 2024

There are many well-known tiling Window Managers on Linux, such as `i3`,
`awesome`, `xmonad` etc for the old Xorg server. For the new Wayland
protocol that replaces XServer, `sway` is a solid re-implementation of
`i3`.

On MacOS, however, your choices are limited. The oldest tiling WM is
Amethyst, but I have found it to be buggy and not as good as the tiling
WM's on Linux. In Oct 2023, I started using
[yabai](https://github.com/koekeishiya/yabai/tree/master) and I now think
that it is the best tiling WM for MacOS. It is configured using plaintext
shell files and integrates tightly with a MacOS keyboard shortcut utility
called `skhd`, created by the `yabai` developer.

It can be configured to act just like `i3` and `sway` Window Managers and
it is very stable. The only downside is that enabling it fully on recent
versions of MacOS requires that you boot into Safe Mode and partially
disable some of the System Protections.

# Topics

## Installation

In 2023 installation from Homebrew is still the recommended method. It is
*possible* to install `yabai` via the `nix` package manager, but you have
to jump through additional hoops. Ironically, installing many packages from
`nix` on MacOS is actually better than installing from `brew`, like in the
case of `emacs`.

```sh
brew install koekeishiya/formulae/yabai
```

**Note**: you need to have a Github PAT setup or Git Credential Manager
(GCM) as a 2FA app setup on MacOS to be able to `git clone` via https for
the above homebrew recipe.

## Start yabai daemon

```sh
yabai --start-service
```

The old way `brew services start yabai` is now deprecated.

The first time you try to start the service daemon, you will be prompted to
manually give access to the service under *System Settings* -> *Privacy &
Security* -> *Accessibility* Toggle the slider to the right, so it turns
*green*.

Then re-invoke the command above. You will be prompted one more time
to allow `yabai` to record screens. The daemon will now be running, as
you can verify with `ps auxwww | grep yabai`

When yabai is acting weird (not properly sizing windows in Binary
Space Partition mode, for example), invoke

```sh
yabai --restart-service
```

## Update to latest release

```sh
# stop yabai
yabai --stop-service

# upgrade yabai
brew upgrade yabai

# start yabai
yabai --start-service
```

## Yabai config

`~/.yabairc`

**Reference**:

https://github.com/koekeishiya/yabai/issues/1738


## Disable System Protection (SIP)

https://github.com/koekeishiya/yabai/wiki/Disabling-System-Integrity-Protection

This is necessary for Yabai to have control of the window server.
First shutdown your Macbook and then turn it on while holding down the
power button until you see *Startup Options*. After booting into the the
Recovery console, select *Terminal*. The commands below are different
depending on your hardware (Intel vs Apple Silicon) and OS version


### Apple Silicon MacOS 13.x, 14.x

```sh
csrutil enable --without fs --without debug --without nvram
```

Reboot into the regular OS and open a terminal.

```sh
# For Apple Silicon
sudo nvram boot-args=-arm64e_preview_abi
```

This will allow the running of non-Apple signed arm64e binaries


### Intel

```sh
# If you're on Intel macOS 13.x.x, 12.x.x, or 11.x.x
# Requires Filesystem Protections and Debugging Restrictions to be disabled
# (workaround because --without debug does not work)
# (printed warning can be safely ignored)
csrutil disable --with kext --with dtrace --with nvram --with basesystem
```
Reboot into regular MacOS, open terminal and run `csrutil status` to
see SIP status.

## Troubleshooting

Sometimes the `.yabairc` config setting `autoraise` for
`focus_follows_mouse` will stop working. In these cases run the
following:

```sh
yabai --stop-service
yabai --start-service
```

Sometimes `--restart-service` doesn't do the trick and only an actual stop
then start resolves the issue. But I isn't `--restart-service` just a
convenience wrapper around stop, start?
