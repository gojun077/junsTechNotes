KVM and QEMU HOWTO
=====================

# Summary

- Created on: Jan 8 2024
- Created by: gopeterjun@naver.com
- Last Updated: Jan 9 2024

KVM (Kernel-based Virtual Machine) is a type 1 hypervisor, while QEMU
(Quick Emulator) is a type 2 hypervisor. Type 1 hypervisors directly access
host machine resources (bare metal). Type 2 hypervisors negotiate resource
allocation with the OS, so type 2 is slower than type 1.  KVM runs via a
kernel module and is built into Linux.

QEMU is a machine emulator that can emulate various hardware architectures
even if they are different from the host's architecture, so it is a tool
that is often used for system emulation.

KVM started life as as a fork of QEMU, `qemu-kvm`, but KVM was included in
the mainline Linux kernel starting with `2.6.20`. QEMU can run on top of
KVM for a speed boost and many of the utilities and tools built for QEMU
and KVM can be used for editing `qcow2` (QEMU copy-on-write) VM images.

This guide contains examples and notes of commands for administering KVM,
editing machine images, and working with cloud-native tooling around
virtualization.

# Topics

## QEMU Copy On Write - QCOW

QCOW (in the form of `qcow2` file format) is ubiquitous in the world of
virtual machines, especially in the public and private cloud. Despite the
rise of containers and Kubernetes, in the managed k8s services offered by
public clouds (Azure's AKS, AWS' EKS, GCP's GKE), the actual k8s nodes
hosting pods are just Virtual Machines. These VMs mostly run on the KVM
hypervisor (although AWS initially used Xen hypervisor in its early days
and still has some workloads running on Xen) and use QCOW VM images.

Hobbyists who have used VMWare Fusion Player or Virtualbox or even
`virt-manager` GUI frontend to `libvirt` are more familiar with installing
Linux onto VM's the old-fashioned way, i.e. by mounting an `.iso` file and
booting from `.iso` when a VM is first provisioned. Installation from
`.iso` can be automated via `kickstart` or `di` (Debian Installer).

The disadvantage of this old-school method of installing from `.iso` or
other installation images is that there is a lot of file duplication, not
to mention all the time it takes to install the OS from an `.iso` to a
virtual block device.  Wouldn't it be great if all the common files for
running the same OS could be read from a common file and avoid the onerous
OS install step? Well this desired de-duplications becomes possible if you
use QCOW.

Most cloud providers now use cloud images as a template from which to build
individual VM disk images. QCOW only allocates storage as needed, which is
why QCOW file sizes are just a fraction of those of raw disk images. But if
a single cloud image is used to build VM disk images, how can users of QCOW
customize VM disk images?

From [Wikipedia](https://en.wikipedia.org/wiki/Qcow):

> The `qcow` format also allows storing changes made to a read-only base
> image on a separate `qcow` file by using *copy on write*. This new `qcow`
> file contains the path to the base image to be able to refer back to it
> when required. When a particular piece of data has to be read from this
> new image, the content is retrieved from it if it is new and was stored
> there; if it is not, the data is fetched from the base image.

In this way, someone who runs 20 Ubuntu 22.04 VMs can save lots of space by
keeping one Ubuntu 22.04 base image file as a read-only file known as a
*backing file* and create new machine image files that refer to the backing
file but that store changes within the new file.

## Create qcow2 image from a backing file

We can create linked `qcow2` machine images from a backing image by
using the `qemu-img` tool:

```
qemu-img create -b BACKING_FILE -F BACKING_FMT -f TARGET_FMT \
  TARGET_FILENAME [SIZE]
```

Let's try a real example. You can download Ubuntu cloud images from
https://cloud-images.ubuntu.com/

In the example below I will `wget` a `qcow2`-formatted public cloud image
of Ubuntu 22.04 for the amd64 architecture that can run on public clouds,
Openstack, LXD, etc. I will then remove the write bit on the qcow file so
that it can be used as a read-only template, and create separate machine
images in `.qcow2` format that are linked to the read-only template.

For example:

```sh
# NOTE: the linux-kvm KVM optimized kernel did not boot for me
# just use the regular QCow2 UEFI/GPT Bootable disk image
$ wget https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img
--2024-01-09 00:43:20--  https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img
Resolving cloud-images.ubuntu.com (cloud-images.ubuntu.com)... 185.125.190.37, 185.125.190.40, 2620:2d:4000:1::17, ...
Connecting to cloud-images.ubuntu.com (cloud-images.ubuntu.com)|185.125.190.37|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 673710080 (642M) [application/octet-stream]
Saving to: ‘jammy-server-cloudimg-amd64.img’

jammy-server-cloudimg-amd64.i 100%[================================================>] 642.50M  10.6MB/s    in 63s

2024-01-09 00:44:24 (10.2 MB/s) - ‘jammy-server-cloudimg-amd64.img’ saved [673710080/673710080]

$ chmod -w jammy-server-cloudimg-amd64.img
$ qemu-img create -b jammy-server-cloudimg-amd64.img \
  -F qcow2 -f qcow2 \
  pj-ubuntu.qcow2 20G
Formatting 'pj-ubuntu', fmt=qcow2 cluster_size=65536 extended_l2=off compression_type=zlib size=21474836480 backing_file=jammy-server-cloudimg-amd64.img backing_fmt=qcow2 lazy_refcounts=off refcount_bits=16
$ file pj-ubuntu
pj-ubuntu: QEMU QCOW Image (v3), has backing file (path jammy-server-cloudimg-amd64.img, mtime Thu Jan  1 00:00:16 1970
), 21474836480 bytes (v3), has backing file (path jammy-server-cloudimg-amd64.img), 21474836480 bytes
```

Note that the backing image must be in the same directory as the
target image. For example, the following will fail:

```sh
$ sudo qemu-img create -b jammy-server-cloudimg-amd64.img \
  -F qcow2 -f qcow2 \
  /var/lib/libvirt/images/pj-ubuntu.qcow2 20G
[sudo] password for jundora:
qemu-img: /var/lib/libvirt/images/pj-ubuntu.qcow2: Could not open '/var/lib/libvirt/images/jammy-server-cloudimg-amd64.img': No such file or directory
Could not open backing image.
```

You can also create a dynamically-growing `qcow2` disk image by using the
option `-o preallocation=off`. Although this is a bit slower than using a
`qcow2` with a preallocated storage size, this option can save alot of
space, esp. if most of your VM's aren't actually the maximum space
allocated! If you use this option, the `disk-size` will simply define the
maximum size that the disk can grow to.

Note that OS images for public clouds do not have a default user or
password. This is normally handled by public cloud providers' metadata
service which interfaces with the `cloud-init` service that runs by default
inside these OS images. If you try to just boot one of these images on your
hypervisor, you will not be able to login!

The next section will show how we can customize our target images with
a custom iso that `cloud-init` can read on first boot.


## Create .iso containing YAML files for cloud-init

The `cloud-init` service running in virtual machine images for public
clouds looks for 2 required YAML config files, `user-data` and `meta-data`
at boot. Also `vendor-data` and `network-config` YAML files can be
provided, but these are optional.

**meta-data**:

```yaml
instance-id: noriteo
local-hostname: noriteo
```

**user-data**:

```yaml
#cloud-config

timezone: Asia/Seoul
users:
  - default
  - name: pj
    passwd: "generate PW hash in /etc/shadow format"
    lock_passwd: false
    shell: /bin/bash
    chpasswd: { expire: False }
    ssh_authorized_keys:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJhU17VuEc6XGVEoLSz+OpFZCfc8jVSqrOHgI1i9jfde gopeterjun@naver.com
    sudo: ["ALL=(ALL) NOPASSWD:ALL"]
    groups: sudo
```

Note: to generate a Linux user password hash you can use `mkpasswd`:

```sh
$ mkpasswd -m sha-512
Password:
...
```

If you don't have `mkpasswd` available (for example, you are on MacOS),
you can also generate a Linux-style SHA512 password hash with Python 3.
Just don't use the `crypt` module, which will be removed starting in
Python 3.13:

```sh
$ python3 -c 'import base64,hashlib,os; salt=os.urandom(16);password=b"mypw";hasher=hashlib.sha512();hasher.update(salt+password);shadow="$6$"+base64.b64encode(salt).decode("utf-8").strip("=")+"$"+base64.b64encode(hasher.digest()).decode("utf-8").strip("=");print(shadow)'
$6$74mJ16MwWG6WEXcaWLSh5A$G3XHBQy//cjZbsay3HrmFvxItCv0T4LMYYK4C+xWQNP38dbhDJG9hlObSzbtGF1rThdqY0j2OGyiDPXXi+/5mw
```

As you can see above, the plaintext password `mypw` has been converted into
the UNIX shadow password formatted-string starting with `$6$` for SHA512
hash digest.

**NOTE**: although the generated password is in the correct format,
for some reason when `cloud-init` inserts the password hash into
`/etc/shadow`, it is not removing the `!` character which means that
the user has no password.

Once you have created these two files, run `genisoimage` to create an
iso image containing `meta-data` and `user-data`. This `.iso` image
will be mounted in a CD-ROM device which you will add in the customization
screen in `virt-manager`.

```sh
$ genisoimage -output cidata.iso -V cidata -r -J user-data meta-data
I: -input-charset not specified, using utf-8 (detected in locale settings)
Total translation table size: 0
Total rockridge attributes bytes: 331
Total directory bytes: 0
Path table size(bytes): 10
Max brk space used 0
183 extents written (0 MB)
```

## Edit .qcow2 file using libguestfs-tools / guestfs

This package which is usually named `libguestfs-tools` on most distros,
but `guestfs` on Fedora, contains lots of tools that are helpful for
debugging and troubleshooting machine images.

First lets see the file systems inside of an image:

```sh
$ guestfish -a ubuntu.qcow2

Welcome to guestfish, the guest filesystem shell for
editing virtual machine filesystems and disk images.

Type: ‘help’ for help on commands
      ‘man’ to read the manual
      ‘quit’ to quit the shell

><fs> run
><fs> list-filesystems
/dev/sda1: ext4
/dev/sda14: unknown
/dev/sda15: vfat
><fs> exit
```

Now we can mount the ext4 fs we found above on `/mnt` so we can edit
files within the image:

```sh
sudo guestmount -a noriteo-ubuntu-22.04.qcow2 -m /dev/sda1 /mnt
```

I then ran `sudo vim /mnt/etc/shadow` and discovered that the entry
for user `pj` incorrectly had `!` set for the password, followed by the
password hash starting with `$6$`. Not sure why this is happening, but
I manually fixed this.

Now you must unmount the `.qcow2` image from `/mnt`:

```sh
sudo guestunmount /mnt
```

After manually fixing `/etc/shadow`, login is now working and
networking works, as it has been automatically set up by `cloud-init`

**POSTSCRIPT**: the reason why `!` was being inserted for my user in
`/etc/shadow` is that I didn't specify `lock_passwd: false`! This
value defaults to `true` if it is not specified.


## References

- https://sumit-ghosh.com/posts/create-vm-using-libvirt-cloud-images-cloud-init/
- https://blog.wikichoon.com/2020/09/virt-install-cloud-init.html
- https://guides.zadarastorage.com/cs-compute-guide/latest/modify-cloud-image-qcow.html
- https://documentation.ubuntu.com/lxd/en/stable-4.0/cloud-init/
- https://cloudinit.readthedocs.io/en/latest/reference/network-config-format-v1.html#network-config-v1
- https://askubuntu.com/questions/1365637/autoinstall-user-datas-users-section-does-not-work-at-all
