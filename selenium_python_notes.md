Using Selenium Webdriver with Python
======================================

# Summary

- Created on: Sep 26 2021
- Created by: gojun077@gmail.com
- Last Updated: Sep 30 2021

Selenium is a browser automation tool written in Java and open-sourced by
Thoughworks in the early 2000's. It officially supports client libraries in
Java, C#, Ruby, JavaScript, R and Python.

This guide covers Selenium usage in Python. All of the examples in this
guide will use Selenium `webdriver`, which can control web browsers in
headless mode. To use `chromium` in headless mode via `webdriver`, you need
to install the `chromedriver` package for your environment. To use
`firefox` in headless mode via `webdriver`, you must install the
`geckodriver` package.

# Topics

## Controlling Firefox with geckodriver

Depending on your Linux distribution package names may differ slightly. On
Ubuntu, you will need to install the following pkg's:

```sh
sudo apt install firefox firefox-geckodriver
```

Now let's use headless Firefox to take a screenshot of `google.com`

```python
from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions

options = FirefoxOptions()
options.add_argument("--headless")
browser = webdriver.Firefox(options=options)
browser.get("http://google.com")
browser.save_screenshot("screenshot.png")
browser.close()
```

References:

- https://stackoverflow.com/questions/46753393/how-to-make-firefox-headless-programmatically-in-selenium-with-python


## Controlling Chrome/Chromium with chromedriver


Depending on your Linux distribution package names may differ slightly. On
Ubuntu, you will need to install the following pkg's:

```sh
sudo apt install chromium chromium-chromedriver
```

Let's use headless Chrome to take a screenshot of my leetcode profile
page.

```python
from selenium import webdriver

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('window-size=1920,1080')
browser = webdriver.Chrome(options=options)
browser.get("https://leetcode.com/gojun077")
browser.save_screenshot("screenshot.png")
browser.close()
```
