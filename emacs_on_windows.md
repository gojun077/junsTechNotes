Setting up Windows-native Emacs
===================================

# Summary

- Created on: Feb 13 2022
= Created by: gojun077@gmail.com
- Last Updated: Feb 13 2022

Setting up Emacs in the non-POSIX environment that is MS Windows has
some differences with installing on Emacs on Linux or MacOS. The
biggest stumbling block I encountered was setting the paths to linters
and programming language binaries/interpreters.

# Topics

## Location of .emacs file

The default location for `.emacs` is `c:/Users/<username>/AppData/Roaming/`


## Fonts

See what fonts are available on your system:

- `M-x menu-set-font`

See info about actual font being used:

- `M-x describe-font`

## python

If you install packages via python `pip` or use the Anaconda Python
distribution for Windows, the `python*` executables and modules will not be
in the user's PATH. Therefore you must specify where the executables and
various modules can be found. Here is an example from my `.emacs` file for
MS Windows:

```elisp
(setq python-shell-interpreter "python3.exe")
(setq ansi-color-for-comint-mode t)
(setq python-check-command "c:/Users/pj/AppData/Local/Packages/PythonSoftwareFoundation.Python.3.9_qbz5n2kfra8p0/LocalCache/local-packages/Python39/Scripts/pyflakes3.exe")
(setq flycheck-python-pyflakes-executable "c:/Users/pj/AppData/Local/Packages/PythonSoftwareFoundation.Python.3.9_qbz5n2kfra8p0/LocalCache/local-packages/Python39/Scripts/pyflakes3.exe")
```

I didn't have to specify an absolute path for `python3.exe` above because
this binary was installed from the Microsoft App Store. However, I
installed the Anaconda Distribution of Python separately and it and its
pkg's aren't in the user's PATH. Likewise for packages installed via `pip`
like the `pyflakes` linter.


## flycheck

`flycheck` is a realtime linter for Emacs, replacing `flymake`. It has tons
of plugins available through ELPA and MELPA package repos for various
languages. Usually on Linux or MacOS the programming languages you
install through your package manager or homebrew are automatically set
up so that they are in your PATH. 


## magit

As awesome as `magit` is, as of Feb 2022 it is basically unusable on the
MS Windows-native port of Emacs. From what I've read on the Internet, the
problem stems from the general slowness of *git on windows* using mingw64.
When I tried to commit using `magit` inside of Emacs, the entire Win11 VM
became unresponsive and stuck on 100% CPU. I couldn't even kill the VM from
my hypervisor and had to kill the hypervisor process itself.

If you want to use `magit` on MS Windows, your best bet is to install
Windows Subsystem for Linux 2 (WSL2). The problem is that WSL2 is a
resource hog; it uses up to 50% of your total system memory (or 8GB,
whichever is lower). That is too heavy for a lightweight Win11 VM dev
envo.

