Jun's GNU Screen Tips and Tricks
==================================

# Summary

- Created on: 29 Feb 2020
- Created by: gojun077@gmail.com
- Last Updated: 11 Oct 2024


This guide is not intended to be comprehensive and is more of a place for
me to collect my notes about how I use `screen`.

# Snippets

## Reattach to specific tab in a detached screen session

> At work I run `irssi` and `bitlbee` inside a GNU Screen session
> running on WSL1 (Windows Subsystem for Linux). Although it runs,
> it's not 100% stable, and I sometimes encounter crashes in `irssi`
> when my twitter (through `bitlbee`) feed tries to output text
> encoded in a format that WSL doesn't like. Even if I kill the irssi
> and bitlbee processes, the tab where irssi used to run in becomes
> totally unresponsive. In such cases I close the entire WSL terminal
> window, launch a new WSL window, and reattach to my existing
> screen session but choose a different tab number

```sh
screen -r -p <tabNum>
```

## kill a tab from another tab inside a screen session

> Let's say you're in tab 1 but you want to kill tab 7. You can
> achieve this by entering the following:

```
C-a : at "7#" kill
```

> First press `Ctrl-a` to enter command mode, then type colon to enter
> a screen command line. From there, use the `at` command to specify
> tab 7 as the tab you want to run the `kill` command on.

## kill a specific tab from outside a running screen session

> This command is very useful in the case where you are stuck in an
> unresponsive tab. First close your terminal and open up a new
> one. Your GNU Screen session will still be running. To find your
> existing session's sessionID, run `screen -ls`. Now run the
> following command before re-attaching to your session:

```sh
screen -XS <sessID> -p <tabNum> kill
```

## send command to all windows

> You can send a `screen` command like `kill` to all tabs with

```
C-a : at "#" kill
```

> And you can send a regular shell command like `ls` to all tabs with

```
C-a : at "#" stuff "ls^M"
```

## send command to all windows whose name contains some string

> Assume that you have 5 windows named `bash0` to `bash4`. You can
> send an `echo 'hello world'` command to all these windows with:

```
C-a : at "bash#" stuff "echo 'hello world'^M"
```

## List all screen sessions on the host

```sh
$ screen -ls
There is a screen on:
	26658.utk	(Detached)
1 Socket in /tmp/uscreens/S-macjun.
```

## Transmit ON, OFF `xon`, `xoff`

- `C-a q` Send a `^Q` ASCII XON to the current window
- `C-a C-q` same as above
- `C-a s` Send a `^S` ASCII XOFF to the current window
- `C-a C-s` same as above

Note that `C-a q` send XON is redundant if flow control is set to
`off` or `auto`

- Toggle flow control mode
  - `C-a f`
  - `C-a C-f`
  - `:flow <fstate>`
    - where `<fstate>` can be `on`, `off`, `auto`

- Set flow control default setting for new windows
  - same as `:flow` except default setting is changed
  - initial setting is `auto`
  - `:defflow <fstate> [interrupt]`
    - where `<fstate>` can be `on`, `off`, `auto`
    - `interrupt` is optional
      - causes output accumulated from interrupted program to be flushed
      - use this if `C-c` does not interrupt the display fast enough

- https://www.gnu.org/software/screen/manual/html_node/XON_002fXOFF.html
- https://web.mit.edu/gnu/doc/html/screen_14.html#SEC86

## Kill a screen session from the outside

```sh
# kills the entire session, not just one window
screen -X -S sessName quit
```

## Copy and paste contents of buffer


- `C-a [` enter copy mode
- `C-b` navigate one screen *b*ack
- `C-f` navigate one screen *f*orward
- `SPACE` start selection within copy mode
- `SPACE` end selection within copy mode
- `C-a ]` paste selection (usually into new buffer)


## Remove Splits

`C-a Q` or `C-a :remove`

## Use screen to connect over serial console

If you have plugged in your serial console cable to the USB port (A or C)
on your computer and the other end into a network switch or computing
device (like the RPi SBC), first run `lsusb` to make sure the USB dongle is
being detected and then `dmesg -wH` to see the USB to tty interface
assigned to the UART.

For example:

```sh
$ lsusb
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 001 Device 007: ID 1a86:7523 QinHeng Electronics CH340 serial converter
Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 003 Device 002: ID 05e3:0608 Genesys Logic, Inc. Hub
Bus 003 Device 003: ID 8087:0029 Intel Corp. AX200 Bluetooth
Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
```

The USB to serial converter shows up as `1a86:7523`.

```sh
$ sudo dmesg -wH
...
[  +0.143065] usb 1-1: New USB device found, idVendor=1a86, idProduct=7523, bcdDevice= 2.64
[  +0.000007] usb 1-1: New USB device strings: Mfr=0, Product=2, SerialNumber=0
[  +0.000003] usb 1-1: Product: USB Serial
[  +0.020439] usbcore: registered new interface driver ch341
[  +0.000013] usbserial: USB Serial support registered for ch341-uart
[  +0.000010] ch341 1-1:1.0: ch341-uart converter detected
[  +0.012308] usb 1-1: ch341-uart converter now attached to ttyUSB0
```

`ttyUSB0` is the tty to USB interface assigned to our UART. Now we will
connect to this interface:

```sh
$ sudo screen /dev/ttyUSB0 115200
```

## References
- http://web.mit.edu/gnu/doc/html/screen_1.html
