sqlite3 notes
=================

# Summary

- Created on: May 9 2023
- Created by: gojun077@gmail.com
- Last Updated: May 9 2023

Python `pickle` and `shelve` are fine when you have small data that you
want to save as a `dict` datatype, but for anything more serious, you
really need to be using a database. `sqlite3` is great because it supports
`SQL` syntax but doesn't require a running daemon like `mysqld` or setting
up a production DB; `sqlite3` DB's are just single files and there are
libraries for `sqlite3` in most languages.

`sqlite3` supports `sql` operations such as setting indexes on columns
(which orders a column into a B-Tree) and primary key management with
`rowid` (64-bit signed int).

`sqlite` is ubiquitous, and is used as the default DB for Android and is
also used in mission-critical software throughout industry.


# Topics

## Open DB

You can open a DB directly on the commandline with:

```sh
sqlite3 /path/to/mydb.sqlite
```

Alternatively, you can just launch `sqlite3` and then open your DB with
sqlite command `.open FILENAME`.

## Show all tables in DB

```
sqlite> .tables
users
```

To access all rows in table `users` above, you can use regular SQL syntax:

```sql
SELECT * FROM users;
```


## Exit DB

```
sqlite> .exit
```


## Storing dates as ISO8601 `TEXT` datatype

https://www.sqlite.org/lang_datefunc.html

There is no separate `DATE` datatype in `sqlite3`, but you can store
datetime as ISO8601 compliant `TEXT`. A time string can be in any of the
following formats:

```
1. YYYY-MM-DD
2. YYYY-MM-DD HH:MM
3. YYYY-MM-DD HH:MM:SS
4. YYYY-MM-DD HH:MM:SS.SSS
5. YYYY-MM-DDTHH:MM
6. YYYY-MM-DDTHH:MM:SS
7. YYYY-MM-DDTHH:MM:SS.SSS
8. HH:MM
9. HH:MM:SS
10. HH:MM:SS.SSS
11. now
12. DDDDDDDDDD
```

In addition, formats 2 through 10 may be optionally followed by a timezone
indicator of the form `"[+-]HH:MM"` or just `"Z"`. The date and time
functions use `UTC` or `"zulu"` time internally, and so the `"Z"` suffix is
a no-op. Any non-zero `"HH:MM"` suffix is subtracted from the indicated
date and time in order to compute `zulu` time.

Therefore, the following strings are valid ISO8601 `TEXT` in `sqlite3`:

```
2020-04-28T12:00:00+09:00
2020-04-25T08:55:52.000Z
2020-04-01T06:00:00+00:00
2020-04-01T06:00:00Z
```

## Comparing ISO8601 TEXT dates

As long as your dates are in valid ISO8601 format, you can directly compare
them using comparison operators.

## UPDATE record using `?` parameters

Usually your SQL operations will need to use values from Python
variables. You shouldn’t assemble your query using Python’s string
operations because doing so is insecure; it makes your program vulnerable
to an SQL injection attack

Instead, use the DB-API’s parameter substitution. Put `?` as a placeholder
wherever you want to use a value, and then provide a tuple of values as the
second argument to the cursor’s execute() method.

If you don't provide a tuple to the substitute parameter, you will get an
error about `Incorrect number of bindings supplied`.

https://stackoverflow.com/questions/16856647/sqlite3-programmingerror-incorrect-number-of-bindings-supplied-the-current-sta

The solution is to make sure you are passing a tuple or singleton tuple
i.e. `(mytup, mytup1)` or `(mytup,)`, respectively.

## DESCRIBE the columns in a table

In `mysql`, the keyword `DESCRIBE` or `DESC` will provide the
type info for all columns in a table. In `sqlite3`, however, this
keyword does not exist. Instead, you must use the command

```
.schema <tablename>
```

## Dealing with db corruption issues

On 2020.09.22, I manually ran `cuervos.py` which writes to sqlite3
`fifacuervos.db` at the same time that a cron job was writing to the same
DB;

I got the error: `Error: database disk image is malformed` I then opened up
the file in sqlite3:

```
sqlite> PRAGMA integrity_check;
*** in database main ***
On tree page 2 cell 0: invalid page number 7
Error: database disk image is malformed
```

To recover from a corrupt sqlite3 DB, you are supposed to use the `.dump`
command to export the DB to raw `sql` statements and then recreate a new DB
from this dump. I used the following command I found from SO:

```sh
cat <( sqlite3 fifacuervos.db .dump | grep "^ROLLBACK" -v ) \
<( echo "COMMIT;" ) | sqlite3 "fix_fifacuervos.db"
```

After loading the new db, I no longer get the corruption error.
