Problems with Touchpad detection on Archlinux with ROG GL553VW
================================================================

# Snippets from dmesg -wH

```
[  +0.028601] ACPI Error: [USBC] Namespace lookup failure, AE_ALREADY_EXISTS (20170119/dswload-378)
[  +0.000008] ACPI Exception: AE_ALREADY_EXISTS, During name lookup/catalog (20170119/psobject-227)
[  +0.000059] ACPI Exception: AE_ALREADY_EXISTS, (SSDT:RV11Rtd3) while loading table (20170119/tbxfload-228)
[  +0.010339] ACPI Error: 1 table load failures, 10 successful (20170119/tbxfload-246)
...
[  +0.039450] i8042: PNP: No PS/2 controller found.
[  +0.000000] i8042: Probing ports directly.
...
[  +0.001098] input: ASUS ROG SICA as /devices/pci0000:00/0000:00:14.0/usb1/1-2/1-2:1.0/0003:0B05:181B.0001/input/input1
[  +0.000092] hid-generic 0003:0B05:181B.0001: input,hidraw0: USB HID v1.11 Mouse [ASUS ROG SICA] on usb-0000:00:14.0-2/input0
[  +0.002683] input: ASUS ROG SICA as /devices/pci0000:00/0000:00:14.0/usb1/1-2/1-2:1.1/0003:0B05:181B.0002/input/input2
...
[  +0.020845] hid-generic 0003:0B05:181B.0002: input,hiddev0,hidraw1: USB HID v1.11 Keyboard [ASUS ROG SICA] on usb-0000:00:14.0-2/input1
...
[  +0.215442] input: ITE Tech. Inc. ITE Device(8910) as /devices/pci0000:00/0000:00:14.0/usb1/1-11/1-11:1.0/0003:0B05:8176.0003/input/input3
...
[  +0.053301] hid-generic 0003:0B05:8176.0003: input,hiddev0,hidraw2: USB HID v1.10 Keyboard [ITE Tech. Inc. ITE Device(8910)] on usb-0000:00:14.0-11/input0
```

ITE Device is detected for both keyboard and touchpad
