Nextcloud HOWTO
=================

# Summary

- Created on: Oct 2 2023
- Created by: gopeterjun@naver.com
- Last Updated: Oct 2 2023

Nextcloud is an open source tool for filehosting. It provides functionality
similar to Dropbox or Google Drive. It is a fork of the Owncloud project.
This HOWTO contains my personal notes on setting up Nextcloud on a RPi3B
with 1GB DDR2 RAM on Debian 12 Bookworm. Instead of using `nginx` or
`apache2` webservers as recommended by the Nextcloud installation guide, I
am using Caddy 2 webserver. And instead of using an external domain name, I
am using a Tailnet internal domain only accessible via Tailscale. TLS certs
are provided by Tailscale through Let's Encrypt for the tailnet hostname.

# Topics

## Installation

### Debian 12 (Bookworm)

## Setup

### Caddy webserver config

`php-fpm` for caddy

- https://gist.github.com/Finkregh/b3ca58f4ad6f27d8d0ef7246ee8d7941
- https://caddy.community/t/help-getting-nextcloud-php-to-work-with-caddy/8992/3

## References

- https://docs.nextcloud.com/server/latest/admin_manual/installation/source_installation.html
- https://www.linuxtuto.com/how-to-install-nextcloud-on-debian-12/
- https://docs.nextcloud.com/server/latest/admin_manual/installation/system_requirements.html

