Ubuntu Linux Distro notes
==============================

# Summary

- Created on: Apr 20 2023
- Created by: gopeterjun@naver.com
- Last Updated: Apr 20 2023

I have great fondness for the Ubuntu Linux distribution. It was the first
Linux that I ever used successfully (back in 2003 I tried to install
Mandrake Linux on a Intel Celeron notebook, but that didn't work). In 2009
I installed Ubuntu Hardy Heron 8.04 on an ASUS Eee netbook and upgraded to
9.04 Jaunty Jackalope. Ubuntu has probably played the biggest role in
bringing desktop Linux to a wider audience although it is also
super-popular today as an OS image for Linux VM instances in the cloud and
is also quite popular as a base image for containers.

This markdown file contains tips and tricks regarding Ubuntu usage and
sysadmin tasks.

# Topics

## local network .deb package cache and proxy

https://wiki.debian.org/AptCacherNg

`apt-cacher-ng` creates a local cache of Debian/Ubuntu packages.
The server must open `3142/tcp` for clients to get package metadata

On the Ubuntu machines that will be clients of apt-cacher-ng, create a new
file named `00proxy` in `/etc/apt/apt.conf.d`. It should look like:

```
Acquire::http::Proxy "http://<apt-cacher-ng-server>:3142";
```

Note that https is not supported OOTB and these packages are not cached.

### gpg key errors for `apt-cacher-ng` clients

Machines set up to get packages from a local proxy may return the following
key error:

```
An error occurred during the signature verification. The repository is not updated and the previous index files will be used.
GPG error: http://ports.ubuntu.com/ubuntu-ports jammy-updates InRelease: The following signatures
were invalid: BADSIG 871920D1991BC93C Ubuntu Archive Automatic Signing Key (2018) <ftpmaster@ubuntu.com>
```

In this case, you will need to refresh the package signing keys on the machine
hosting the package cache with the following command:

```sh
$ sudo apt-key adv --refresh-keys --keyserver keyserver.ubuntu.com
Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).
Executing: /tmp/apt-key-gpghome.7f2Mwcmpp5/gpg.1.sh --refresh-keys --keyserver keyserver.ubuntu.com
gpg: refreshing 2 keys from hkp://keyserver.ubuntu.com
gpg: key 871920D1991BC93C: "Ubuntu Archive Automatic Signing Key (2018) <ftpmaster@ubuntu.com>" not changed
gpg: key D94AA3F0EFE21092: "Ubuntu CD Image Automatic Signing Key (2012) <cdimage@ubuntu.com>" not changed
gpg: Total number processed: 2
gpg:              unchanged: 2
$ sudo apt update
```

Now if you `apt update` on `apt-cacher-ng` client machines, you should
no longer see the package signing key error.

Reference:

- https://askubuntu.com/questions/131601/gpg-error-release-the-following-signatures-were-invalid-badsig
