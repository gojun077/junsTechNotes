PJ's kubectl cheatsheet
=========================

# Summary

Created on: Aug 24 2021
Created by: gopeterjun@naver.com
Last Updated: Feb 3 2024

This file contains `kubectl` snippets I've found useful as well
as notes about using `kubectl`.

Note, however, that for many common tasks, it is *MUCH* easier to use tools
like `k9s` or a GUI `k8s` toolkit like Mirantis Lens (proprietary) to get a
shell in a pod or container, view pod logs, edit the configs of a running
service, view secrets, etc.

On the other hand, there are some tasks, like copying a file into a
container running in a pod, that can only be accomplished using raw
`kubectl` commands


# Topics

## kubeconfig file

https://www.redhat.com/sysadmin/kubeconfig

> The `kubeconfig` file's default location for `kubectl` or `oc` is the
> `~/.kube` directory. Instead of using the full `kubeconfig` name, the
> file is just named `config`. The default location of the `kubeconfig`
> file is `~/.kube/config`. There are other ways to specify the
> `kubeconfig` location, such as the `KUBECONFIG` environment variable or
> the `kubectl --kubeconfig` parameter.

`kubeconfig` file is in YAML format and contains:

- cluster
  + `k8s` cluster
- user
  + credential used to interact with `k8s` API
- context
  + combination of `cluster` and `user`


### Specify path to kubeconfig file via KUBECONFIG shell env var

Although the default `kubeconfig` is stored in the file `~/.kube/config`,
some `k8s` distros like `k3s` use a different filename and path to
`kubeconfig`. You can specify the path to your `kubeconfig` in your
shell environment with the shell variable `KUBECONFIG`.

```sh
export KUBECONFIG=/path/to/kubeconfig/file/foo
```

Some tools like `helm` expect this shell var to exist.


## Get a shell in a k8s pod

```sh
$ kubectl get pods -n mynamespace
NAME                                              READY   STATUS             RESTARTS   AGE
logstash-64fbfdcf4f-cw22d                         1/1     Running            0          8m21s
logstash-64fbfdcf4f-kdrj9                         1/1     Running            0          7m
logstash-64fbfdcf4f-mj2nm                         1/1     Running            0          7m40s
...
```

Let's say I want to get a shell in one of the `logstash-64...` pods to
check that a certain k8s secret is mounted or not on the mount point
`/usr/share/logstash/ssl/`

```sh
$ kubectl exec -it logstash-64fbfdcf4f-cw22d bash -n mynamespace
kubectl exec [POD] [COMMAND] is DEPRECATED and will be removed in a future version. Use kubectl exec [POD] -- [COMMAND] instead.
logstash@logstash-64fbfdcf4f-cw22d:~$ ls -al /usr/share/logstash/ssl
total 72
drwxr-sr-x 2 root     root 4096 Mar 28 10:38 .
drwxrwsr-x 1 logstash root 4096 Mar 28 10:38 ..
-rw-r--r-- 1 root     root 1164 Mar 28 10:38 logstash.cert
-rw-r--r-- 1 root     root 2362 Mar 28 10:38 sa_genesis_key.json
...
```

## Get a shell in a container running in a pod

The `kubectl get pods` command only shows pod names, but how to show the
names of containers and the container images running in the pod?

You have to parse the output of `kubectl get pods` to get this info:

```sh
$ kubectl get pods -n mynamespace -o \
  jsonpath='{range .items[*]}{"\n"}{.metadata.name}{"\t"}{.metadata.namespace}{"\t"}{range .spec.containers[*]}{.name}{"=>"}{.image}{","}{end}{end}'|sort|column -t
```

https://kubernetes.io/docs/reference/kubectl/jsonpath/

> Kubectl supports JSONPath template. JSONPath template is composed of
> JSONPath expressions enclosed by curly braces {}. Kubectl uses JSONPath
> expressions to filter on specific fields in the JSON object and format
> the output.

Once you have the name of the container running in the name of the pod
you want, you can use `kubectl exec` to get a shell in the container
running on the pod. Assume the following:

- pod name: `10k-viewer-79fb5b8dd6-hf9kc`
- container name: `10k-viewer`
- ns name: `myns`


```sh
kubectl exec --stdin --tty 10k-viewer-79fb5b8dd6-hf9kc \
  -c 10k-viewer -n myns -- /bin/bash
```

Note the arguments to `kubectl exec`:

- `<pod name>`
- `-c` container name
- `-n` namespace name
- `--` delimiter to separate container cmd's from kubectl args
- `cmd` to execute in container

Also keep in mind that, depending on your container image, you might not
have `bash` available (i.e. Alpine Linux containers, etc). In that case,
invoke `/bin/sh`

## Get logs from container in pod

Note: to find the name of the container running in your pod, you will
need to use `kubectl describe pod podname`

```sh
kubectl logs my-pod -c my-container -n my-namespace
```

Here's an example from a real customer application

```sh
$ kubectl logs --kubeconfig=kubeconfig-solutions-tenk-sa.yaml \
  10k-viewer-68b7946bc4-jxcxh -c 10k-viewer -n solutions-tenk
warn: Microsoft.AspNetCore.DataProtection.Repositories.FileSystemXmlRepository[60]
      Storing keys in a directory '/root/.aspnet/DataProtection-Keys' that may not be persisted outside of the container. Protected data will be unavailable when container is destroyed.
warn: Microsoft.AspNetCore.DataProtection.KeyManagement.XmlKeyManager[35]
      No XML encryptor configured. Key {92e4c4dc-c5fb-48d5-83c7-e8b9ed047318} may be persisted to storage in unencrypted form.
warn: Microsoft.AspNetCore.Server.Kestrel[0]
      Overriding address(es) 'http://+:80'. Binding to endpoints defined in UseKestrel() instead.
info: Microsoft.Hosting.Lifetime[0]
      Now listening on: http://0.0.0.0:80
info: Microsoft.Hosting.Lifetime[0]
      Application started. Press Ctrl+C to shut down.
info: Microsoft.Hosting.Lifetime[0]
      Hosting environment: Production
info: Microsoft.Hosting.Lifetime[0]
      Content root path: /app
warn: Microsoft.AspNetCore.HttpsPolicy.HttpsRedirectionMiddleware[3]
      Failed to determine the https port for redirect.
Request:
Method: GET, RequestUri: 'https://vault.corp.mycorp.foo/v1/secret/sol/games/tenk-unity-viewer/prd/10K_API_TOKEN', Version: 1.1, Content: <null>, Headers:
{
  X-Vault-Token: <REDACTED>
  Accept: application/json
}

Response:
StatusCode: 403, ReasonPhrase: 'Forbidden', Version: 1.1, Content: System.Net.Http.HttpConnectionResponseContent, Headers:
{
  Date: Tue, 05 Oct 2021 00:40:50 GMT
  Connection: keep-alive
  Cache-Control: no-store
  Strict-Transport-Security: max-age=15724800; includeSubDomains
  Content-Type: application/json
  Content-Length: 33
}
{"errors":["permission denied"]}


Caught an exception! Expecting a 10k API Token!
Unable to get Bookings: Object reference not set to an instance of an object.
```

## Create new namespace

```
$ kubectl create namespace codecov-v5
namespace/codecov-v5 created
```
## Get list of pods in namespace

```sh
[peter.koh@peter ~]$ kubectl get pods -n qa-cqabots
NAME                                 READY   STATUS    RESTARTS   AGE
closeduplicates-5bcc9bff4-mwklz      1/1     Running   0          106m
reassignanswered-6bbbb97c6d-v98n9    1/1     Running   0          81m
resolvedassignbot-8556c658d4-bdfsx   1/1     Running   0          94m
spotlightassignbot-c87f44b94-m6dlc   1/1     Running   0          106m
```

## Get list of secrets in namespace

https://kubernetes.io/docs/concepts/configuration/secret/

```sh
kubectl get secrets --kubeconfig=mySA.json -n myNs
```

To get detailed info on the secrets in a namespace:

```sh
kubectl describe secret --kubeconfig=mySA.json -n myNs \
  <secretName>
```

If you don't specify `<secretName>`, info will be shown for all secrets.

Some secrets are stored in `base64`, but running this will show the `secret`
as a string.


## Create a k8s secret

### create secret from json file

```sh
kubectl create secret generic my-sa \
  --from-file=key.json=PATH-TO-KEY-FILE.json -n myns
```

Note that the name of your secret must be all lowercase alphanumeric and
can include a hyphen `-` but no underscores are allowed.


### Create secret from plaintext to use as k8s secret

k8s secrets must be encoded as base64. If you have some file, you
can convert it to base64 text as follows:

```sh
cat my-secret | base 64 | tr -d '\n'
```


## Show resources being used in namespace

```sh
kubectl --kubeconfig=myfile -n myns get all
```


## kubectl contexts

https://kubernetes.io/docs/reference/kubectl/cheatsheet/#kubectl-context-and-configuration


## copy a local file to a container in a pod

```sh
kubectl cp foo2 10k-viewer-7d9f7c96d5-dzjn8:/tmp/foo2 -c \
  10k-viewer -n myns
```

The above will copy the file `foo2` from the present working directory to
`/tmp/foo2` in container `10k-viewer` in pod `10k-viewer-7d9f7c96d5-dzjn8`
in the namespace `myns`.

## Filter kubectl output using jsonpath

Normally kubectl will give you verbose output nicely formatted with
field names, etc:

```sh
$ kubectl get pods -n solutions-tenk
NAME                          READY   STATUS    RESTARTS   AGE
10k-viewer-7d9f7c96d5-dzjn8   2/2     Running   0          12d
```

But if you are trying to parse this output to get a specific value, the
default output is too verbose. Let's say you just want to get the *name*
field from `get pods`. You can do that with the following:

```sh
$ kubectl get pods -n solutions-tenk -o=jsonpath='{.items..metadata.name}'
10k-viewer-7d9f7c96d5-dzjn8
```

## kubectl output options and parsing output

https://gist.github.com/so0k/42313dbb3b547a0f51a547bb968696ba
