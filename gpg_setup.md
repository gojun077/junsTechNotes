GPG Setup
===================

# Summary

- Created on 5 Feb 2020
- Created by: gojun077@gmail.com
- Last Updated: 25 Nov 2024

GNU Privacy Guard is the open source implementation of PGP cryptographic
keys. GPG keys are asymmetric, with separate private and public
keys. Messages encrypted with your public key can only be encrypted by your
private key. Nowadays in 2023 many more Pubkey algorithms are available
besides just RSA; In `gpg 2.3.8` for example, you can use EdDSA (ed25519)
elliptic curve algorithm to generate your public-private keypair for GPG.


# Topics

## Setting up GPG on MacOS

First you need to install `gpgtools` for MacOS:

https://www.gnupg.org/download/ Lists packages for various OS'es

https://releases.gpgtools.org/GPG_Suite-2019.2.dmg mac-specific binary

> Once the `gpgtools` suite is installed on Mac, a GUI window
> will pop up and you will be prompted to generate a keypair.
> Once this is complete, make sure you can see the keys which
> were created by entering the following command on the CLI:

`gpg --list-keys`

> You can also generate a keypair using the CLI with

`gpg --full-generate-key`

> which will start an interactive dialog on the command line for key
> generation. When you are done you will have a new secret key, public key,
> and revocation certificate.

## Setting up GPG on Ubuntu Linux

## Setting up gnupg2 on Fedora

`gpg` version 2 became the default on Fedora starting with Fedora 30. The
package name is `gnupg2` to differentiate it from v1 `gnupg`, and the
binary is named `gpg2`. It is installed by default on the Fedora 36 server
distro. However, to work properly you must also install the dependency
`pinentry` so you can securely type your password on the CLI.

```sh
sudo dnf update && sudo dnf install pinentry
```

If pinentry is not installed, you will get the following error when
doing anything that requires keyphrase input:

```
gpg: key <KEYID>: error sending to agent: No pinentry
gpg: error building skey array: No pinentry
```

You also should have a `gpg-agent.conf` file in your default
`~/.gnupg` directory. This contains settings for how long to cache
gpg passwords, etc:

```
default-cache-ttl 28800  # 8h (cache time after last gnuPG activity)
max-cache-ttl 28800  # 8h (cache time after entering PW)
```


## Setting up GPG on Windows Subsystem for Linux - Ubuntu

## Set proper ownership and permissions on `~/.gnupg/`

gpg2 might complain about permissions with:

```
gpg: WARNING: unsafe permissions on homedir '/home/jundora/.gnupg'
```

You can fix this with the following:

```sh
find ~/.gnupg -type f -exec chmod 600 {} \;
find ~/.gnupg -type d -exec chmod 700 {} \;
```

And of course the owner of `~/.gnupg` should be your local user.

https://gist.github.com/oseme-techguy/bae2e309c084d93b75a9b25f49718f85


## Changing Your GPG Passphrase

https://www.cyberciti.biz/faq/linux-unix-gpg-change-passphrase-command/

First, list the keys on your gpg keychain

```sh
$gpg --list-keys

~/.gnupg/pubring.kbx
------------------------
pub   4096R/9AABBCD8 2013-10-04
uid                  Home Nas Server (Home Nas Server Backup)
sub   4096R/149D60C7 2013-10-04
```

Now enter the ID for your pubkey after `--edit-key` option flag

```sh
$ gpg --edit-key 9AABBCD8
Secret key is available.


pub  4096R/9AABBCD8  created: 2013-10-04  expires: never       usage: SC
                     trust: ultimate      validity: ultimate
sub  4096R/149D60C7  created: 2013-10-04  expires: never       usage: E
[ultimate] (1). Home Nas Server (Home Nas Server Backup)


gpg>
```

Now enter `passwd` at the `gpg>` prompt and you will have to enter your old
password before being able to select a new password which you will have to
enter twice.

**Note**: this only changes the passphrase for your private GPG key that is
currently loaded into your gpg keyring. If you want to use this passphrase
for the same private key on other machines, you will first need to export
your GPG private key to armored ASCII and then import it into a keyring on
the remote machine. See the next section for details on exporting your
GPG private key.


## Export your GPG private key

https://unix.stackexchange.com/questions/481939/how-to-export-a-gpg-private-key-and-public-key-to-a-file

- 1 List the keys you have:
  + `gpg --list-secret-keys` or `gpg -K`

- 2 Export the key: (you can export to a file or to `stdout`)
  + `gpg --export-secret-key <KEY-ID> > my-key.asc` # to file
  + `gpg --armor export <KEY-ID>`  # to `stdout`
  + You will be prompted to enter the password locking the key

You can then copy your armored ASCII `.asc` file to another machine where
you can import it with `gpg`. Note that you *never* need to upload your
private GPG key to, say, Github for gpg key auth, although you could upload
your gpg Pubkey for the purposes of signing commits with your gpg key.

## Backup your private key to paper using 'paperkey'

If you already have an armored ASCII file of your gpg private key,
you can do the following to write your private key into a human-legible
hexadecimal format:

```sh
cat my-gpg-privkey.asc | paperkey > my-gpg-privkey.paperkey
```

Note that if you try to view your Armored ASCII privkey with a pager like
`less`, it will appear as gibberish and your system will interpret it as
a binary file. Your gpg pubkey, in contrast, is human readable and contains
data in the following header and footer:

```
-----BEGIN PGP PUBLIC KEY BLOCK-----
-----END PGP PUBLIC KEY BLOCK-----
```


## Import your GPG private key on a new machine

- Import your secret key file
  + `gpg --import my-key.asc`
  + You will be prompted to enter the password locking the key
  + you must have the `pinentry` package installed if you are on Linux

- List your keys `gpg --list-keys`

You should now be able to see your new keypair (the armored ASCII file
contains both the secret and public key info of your gpg key).


## GPG secret subkey (ssb)

When you run `gpg -K` (`gpg --list-secret-keys`), you may see several keys
in addition to the primary `sec` secret key. Key type `ssb` stands for
*Secret SuBkey* which are like normal keys except bound to a master
keypair. The subkey can be used for signing or encryption and can be
revoked independently of master keys. To toggle between keys, specify an
index starting from `0` for the master key. If you have 1 subkey, then its
index will be `1`.

```
gpg> key <n>
```

You should then see an asterisk next to the key type, i.e. `ssb*` if the
secret subkey is selected.

You can then use regular gpg shell commands to change expiration or change
the password on your subkey. Note that by default the subkey uses the same
password as the master key, because at original key creation an encryption
subkey is created automatically.

## Set the trust level of a gpg key

In the case of a private key which you own, you will want to set the
trust level to `ultimate`, the highest trust level.

- Edit your gpg key:
  + `gpg --edit-key <KEY-ID>`

- at the `gpg>` prompt, type `trust`
  + select `5` for `ultimate` trust level

## Export your GPG public key as Armored ASCII

```sh
gpg --output ~/my-export.key --armor --export <KEY-ID>
```

You can then copy-paste this when adding GPG signing keys to Github or
Gitlab (Bitbucket doesn't yet support signing commits with GPG).

## Send your public key to a public keyserver

```sh
gpg --send-keys --keyserver keys.opengpg.org <keyID/fingerprint>
```

## Revoke your GPG sec/pub keypair

https://superuser.com/questions/1526283/how-to-revoke-a-gpg-key-and-upload-in-gpg-server

Generate revocation certificate for a given key-ID:

`gpg --output revoke.asc --gen-revoke <key-ID>`

Import revocation cert to keyring:

`gpg --import revoke.asc`


Send revocation cert to a keyserver:

`gpg --keyserver keys.openpgp.org --send-keys key-ID`

Note that `keys.openpgp.org` uses `Sequoia-PGP`, a Rustlang pgp
implementation


## Set key expiration on gpg keypair

```
$ gpg -K  # list private keys
$ gpg --edit-key <KEY-ID>
...
Secret key is available
...
gpg> expire
Changing expiration time for the primary key.
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0) 10y
Key expires at 일  4/10 16:03:01 2033 KST
Is this correct? (y/N)
```

Note that any subkeys will follow the expiration of the master key, even
if the subkey has no expiration date set.


## Extend the expiration on an already expired key

https://superuser.com/questions/813421/can-you-extend-the-expiration-date-of-an-already-expired-gpg-key

As long as the gpg keys have not been revoked, you can use the method in
the previous section above to extend the expiration of the secret key.



## References
- https://www.howtogeek.com/427982/how-to-encrypt-and-decrypt-files-with-gpg-on-linux
