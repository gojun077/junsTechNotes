Notes on setting up SSH properly
===================================

# Summary

- Created on: 30 Dec 2021
- Created by: gojun077@gmail.com
- Last Updated: 29 Jul 2024

I've been using `ssh` for over a decade now but sometimes I find myself
having to backtrack and unlearn something I learned improperly or just
blindly cargo-culted from other engineers' `ssh` configs. This file
contains snippets, ssh client config recommendations, info on setting up
`ssh-agent` on various platforms (Linux, MacOS, etc) and for various Linux
environments (X11, Wayland, headless). Hopefully these notes to myself will
save me time by keeping me from googling for the same old ssh walkthroughs.

# Topics

## ssh client config

By default, your ssh client configs will be read from `~/.ssh/config`.
Alot of people nowadays store their ssh client configs on Github in dotfile
repos, but if you do so, you also should create some ssh client configs
that *aren't* stored in VCS that contain sensitive info like external IP's
of corporate/work servers, names of your ssh privkeys, etc.

My default ssh client config for MacOS can be found
[here](https://github.com/gojun077/jun-dotfiles/blob/main/ssh/ssh-config-macOS):

```
# Global Defaults
TCPKeepAlive=yes
ServerAliveInterval=20
ServerAliveCountMax=6
StrictHostKeyChecking=no
Compression=yes

# MacOS-specific settings
AddKeysToAgent yes
UseKeychain yes
IgnoreUnknown UseKeyChain

# Specify add'l ssh config files
Include config_secure
Include config_dashboard
Include config_work

# Per Host Options
Host bitbucket.org
    User gojun077
    Hostname bitbucket.org
    PreferredAuthentications publickey
    IdentitiesOnly yes
    IdentityFile ~/.ssh/archjun_rsa

Host github
    User gojun077
    Hostname github.com
    PreferredAuthentications publickey
    IdentitiesOnly yes
    IdentityFile ~/.ssh/archjun_rsa

Host gitlab
    User gojun077
    Hostname gitlab.com
    PreferredAuthentications publickey,keyboard-interactive
    IdentitiesOnly yes
    IdentityFile ~/.ssh/archjun_rsa
```

Not sure if I should move my `Host` declarations to `config_secure`
or not; My GH username is public anyways, of course, but there is really
no need to reveal the name of my RSA privkey.

Note that you can use the `Include` keyword in modern versions of `openssh`
allowing your ssh config to refer to other files. This is handy for storing
sensitive ssh client configs in separate files not stored in VCS.

## setting up ssh-agent

Before talking about `ssh-agent` I will go into some background on RSA
public key cryptography. Every time you try to `ssh` into some remote host
running an ssh daemon, you will either be prompted for user/pass or you
will have to present the private key half of your RSA keypair (with the
public key half residing on the remote machine). Most RSA private keys are
protected by passphrases, so you will first have to enter your passphrase
locally to decrypt your privkey and then hash part of it and compare this
with the pubkey hash. This works because in asymmetric encryption like
public key crypto, a private key can be used to derive a public key, but
not the other way around.

However, typing your RSA privkey passphrase every time you want to use your
SSH key gets old real fast, especially since SSH keys are commonly used not
only for logging in to remote servers but for authenticating with version
control services like Github, Gitlab, and Bitbucket. Every time you want to
push to a remote git repo, you will be prompted to enter your privkey
password. Here is where an `ssh-agent` comes in handy. If an `ssh-agent` is
running, you only have to type your privkey password once and the agent
will cache your password. Next time you want to use your SSH privkey,
`ssh-agent` will use your cached password. The quoted block below is from
some old post on the Internet, but this is something you _don't_ want to do
in 2024:

> The best practice is to launch `ssh-agent` in your `.bash_profile`,
> which contains settings for interactive, *login* shells. `.bashrc`, in
> contrast, contains settings for interactive, *non-login* shells, i.e. shell
> sessions launched after your have logged into your system.

I don't launch ssh-agent via `.bash_profile` anymore, but instead launch it
as a systemd user service.

If you launch `ssh-agent` in `.bashrc`, you will get a new `ssh-agent` with
every newly-launched terminal in your current session, which is unnecessary
and wasteful.

An example of `ssh-agent` systemd service file intended to run as user
service: https://github.com/gojun077/jun-dotfiles/blob/main/systemd/user/ssh-agent.service

```systemd
# see https://wiki.archlinux.org/title/SSH_keys#Start_ssh-agent_with_systemd_user
[Unit]
Description=SSH key agent service file for user

[Service]
Type=simple
Environment=SSH_AUTH_SOCK=%t/ssh-agent.socket
ExecStart=/usr/bin/ssh-agent -D -a $SSH_AUTH_SOCK

[Install]
WantedBy=default.target
```

**Note**: Circa July 2024 systemd user service files for `ssh-agent` are
provided by default on Fedora, and don't need to be created manually as
above, but on an Ubuntu 22.04 minimal install, no `ssh-agent` service
files exist.

Now the next time you are prompted for your ssh privkey password and enter
it, your `ssh-agent` will cache it and use it for all subsequent requests.
If you want to manually add an ssh privkey to `ssh-agent`, you can do so
with `ssh-add /path/to/ssh-privkey`. You can also view which ssh
keys are loaded in your `ssh-agent` with `ssh-agent -l`.

On MacOS only, you can *permanently* load an ssh privkey into the MacOS
keychain by using `ssh-add -K /path/to/ssh-privkey`.

### forwarding your local `ssh-agent` to remote machines

You can use ssh privkeys that are loaded in your local `ssh-agent` on
remote machines without physically copying over your ssh privkeys by using
the following option in your ssh client config:

```
ForwardAgent yes
```

The above setting can be global or it can be per-host. It is safer to only
forward `ssh-agent` on a case-by-case basis.

You should only forward your `ssh-agent` to remote servers that you
reasonably trust; any user with root access on the remote machine can
easily reuse your fowarded keys once they discover the PID of your
ssh-agent and use that access to `ssh-copy-id` their own ssh pubkeys to any
machines that you are connected to including your own local machine!

Note that if you forward your ssh-agent to a remote machine, you should
*not* have another `ssh-agent` running as your user on the remote and
you also should not have a separate ssh config on that remote that specifies
the path to ssh private keys being on the remote.

The link below shows an example of `ssh-agent` forwarding being mucked up
by another `ssh-agent` running remotely:

https://serverfault.com/questions/404447/why-is-ssh-agent-forwarding-not-working


## Generate Ed25519 (EdDSA) format SSH keys

RSA is still the most common format for SSH keys, but the Edwards curve
Digital Signature Algorithm (EdDSA) is quickly gaining popularity. It is
faster to generate and verify believed to be more secure, and has smaller
keys. You should prefer `ed25519` to `ECDSA` in 2024.

```sh
ssh-keygen -t ed25519 -C "foo@foo.org" -f foo-ng
```

## Testing ssh key auth with Github.com

https://docs.github.com/en/authentication/connecting-to-github-with-ssh/testing-your-ssh-connection

```sh
ssh -T git@github.com
```

If you are getting `git@github.com: Permission denied (publickey).` then
invoke `ssh -v` to see details for why ssh key auth is failing. You can
also try to specify the path to your private key when connecting by using
the `-i` option to ssh client. The most common reason is that the
permissions on your private key file are too open. Your private key should
have `600` permissions (owner r/w, no read for group and other), while your
public key shoudl have `644` permissions.


```sh
$ ssh -i ~/.ssh/archjun-newgen -T git@github.com
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@         WARNING: UNPROTECTED PRIVATE KEY FILE!          @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Permissions 0644 for '/Users/macjun/.ssh/archjun-newgen' are too open.
It is required that your private key files are NOT accessible by others.
This private key will be ignored.
```

Once you fix your private key permissions you should see something like:

```
Hi gojun077! You've successfully authenticated, but GitHub does not provide shell access.
```
