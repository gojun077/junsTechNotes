Installation notes for Raspberry Pi 4B
==========================================

# Summary

- Created on: 7 Apr 2023
- Created by: gojun077@gmail.com
- Last Updated: 19 Aug 2023

This guide contains notes applicable to all models of the Raspberry Pi 4B
(with 2GB, 4GB, or 8GB LPDDR4-3200 RAM).

# Topics

## Raspberry Pi 4B does not have a Real Time Clock (RTC)

https://raspberrypi-guide.github.io/electronics/add-real-time-clock

> Because the Raspberry Pi is designed as an ultra-low cost computer, it
> lacks the little coin-battery-powered ‘Real Time Clock’ (RTC) module that
> sits in your laptop or desktop computer. Instead, the Raspberry Pi
> updates the time automatically from the global ntp (nework time protocol)
> servers. This can be a problem for stand-alone projects with no network
> connection, as your Raspberry Pi will not be able to accurately keep the
> time. There are various inexpensive add-on RTC boards available that
> simply plug on top of the Raspberry Pi’s GPIO pins.

Because there is no RTC on the RPi SBC, commands like `hwclock` will fail.
In Fedora 38, systemd will set a taint flag `local-hwclock`:

https://github.com/systemd/systemd/blob/main/README

> Systemd will also warn when the cgroup support is unavailable in the
> kernel (taint flag 'cgroups-missing'), the system is using the old cgroup
> hierarchy (taint flag 'cgroupsv1'), the hardware clock is running in
> non-UTC mode (taint flag 'local-hwclock'), the kernel overflow UID or GID
> are not 65534 (taint flags 'overflowuid-not-65534' and
> 'overflowgid-not-65534'), the UID or GID range assigned to the running
> systemd instance covers less than 0…65534 (taint flags 'short-uid-range'
> and 'short-gid-range').

Note that the systemd *taint flag* is different from a tainted kernel:

https://docs.kernel.org/admin-guide/tainted-kernels.html

> The kernel will mark itself as ‘tainted’ when something occurs that might
> be relevant later when investigating problems. Don’t worry too much about
> this, most of the time it’s not a problem to run a tainted kernel; the
> information is mainly of interest once someone wants to investigate some
> problem, as its real cause might be the event that got the kernel
> tainted. That’s why bug reports from tainted kernels will often be
> ignored by developers, hence try to reproduce problems with an untainted
> kernel.

Cases that can cause a tainted kernel are installing a proprietary kernel
module, use of staging drivers, use of out-of-tree modules, MCE (machine
check exceptions), kernel Oops, etc. You can check the kernel's taint
status by searching through the kernel logs:

```sh
sudo journalctl -k | grep taint
```

You can also check the value of `/proc/sys/kernel/tainted`, 0 for
untainted, 1 for tainted.



## Enabling USB boot on RPi 4B

https://raspberrystreet.com/learn/how-to-boot-raspberrypi-from-usb-ssd

First, you need to be using a bootloader version `9-03-2020` or higher.
You can check for bootloader updates with `sudo rpi-eeprom-update`. If an
update is available, `sudo rpi-eeprom-update -a` and then reboot to apply
the update.

If you are running RPi OS (Debian), you can also use

```sh
vcgencmd bootloader_version
```

to verify the bootloader version.

```sh

```
