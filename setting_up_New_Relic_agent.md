Setting up New Relic Infrastructure Agent on Linux
======================================================

# Summary

- Created on: Aug 28 2021
- Created by: gojun077@gmail.com
- Last Updated: Aug 28 2021

This file contains notes on installing the New Relic Infrastructure
agent on Ubuntu Linux and Archlinux. Ubuntu is officially supported,
while Archlinux has a package available in the AUR.

For both distro's, using the guided install commands from New Relic failed.
This HOWTO explains how I was able to manually install the NR agent.

# Topics

## Generate a License Key for the New Relic Agent

From the New Relic Web Console, click on your user picture at the
upper-right and click on the menu item *API keys*. Make sure you
see a key of Type *INGEST - LICENSE*. Click on the elipsis `...` on the
right-most of the row containing your key and select *Copy key*.

Now on your Linux machine where you want to install the New Relic agent,
you must first create a new relic conf file containing your 40-char
license.

```sh
echo "license_key: YOUR_LICENSE_KEY" | sudo tee -a /etc/newrelic-infra.yml
```

## Add New Relic Package Repository, Install Package

### Ubuntu 20.04

New Relic has said that it will continue to support Ubuntu LTS versions.
First add the gpg key for the New Relic Ubuntu repo:

```sh
curl -s \
  https://download.newrelic.com/infrastructure_agent/gpg/newrelic-infra.gpg \
  | sudo apt-key add -
```

Next add the repo url and settings in `/etc/apt/sources.list.d/`

```sh
printf \
"deb https://download.newrelic.com/infrastructure_agent/linux/apt focal main" \
| sudo tee -a /etc/apt/sources.list.d/newrelic-infra.list
```

Now update your deb package list with `sudo apt update` and install the
package with your `sudo` user:

```sh
sudo apt-get install libcap2-bin
sudo NRIA_MODE="PRIVILEGED" apt-get install newrelic-infra
```

### Archlinux

Although Archlinux is not officially supported, there is a `PKGBUILD`
in the AUR which works fine.
