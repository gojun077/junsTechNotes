Fedora cockpit setup notes
==============================

# Summary

- Created on: May 11 2023
- Created by: gopeterjun@naver.com
- Last Updated: May 11 2023

Cockpit is system administration tool that is installed by default on
Fedora Linux. It allows users to do sysadmin tasks via the web browser.

# Topics

## Using HTTPS

https://cockpit-project.org/guide/latest/https


> Cockpit will load a certificate from the `/etc/cockpit/ws-certs.d`
> directory. It will use the last file with a .cert or .crt extension in
> alphabetical order. The file should contain one or more OpenSSL style
> BEGIN CERTIFICATE blocks for the server certificate and the intermediate
> certificate authorities.

> The file should contain one or more OpenSSL style BEGIN CERTIFICATE
> blocks for the server certificate and the intermediate certificate
> authorities.

> The private key must be contained in a separate file with the same name
> as the certificate, but with a .key suffix instead. The key must not be
> encrypted.

I am using Let's Encrypt TLS certs issued by `tailscale cert`. Tailscale
periodically gets new certs from LE every 60 days, so I enabled HTTPS
by creating symlinks from the certs in `/var/lib/tailscale/certs/` to
`/etc/cockpit/ws-certs.d/`


## using nginx as reverse proxy to terminate HTTPS/TLS

https://cockpit-project.org/guide/latest/cockpit.conf.5

> Cockpit can be configured via `/etc/cockpit/cockpit.conf`. This file is
> not required and may need to be created manually. The file has a INI file
> syntax and thus contains key / value pairs, grouped into topical groups.

> **Note**: The port that cockpit listens on cannot be changed in this
> file. To change the port change the systemd `cockpit.socket` file.

TODO

- https://gist.github.com/jmtsantos/c3287e023a1be4958f5847331fcbe1b7
- https://unkwn1.dev/post/proxy-cockpit-with-ssl-howto/
