Github Pages HOWTO
=====================

# Summary

- Created on: May 29 2023
- Created by: gopeterjun@naver.com
- Last Updated: May 29 2023

https://docs.github.com/en/pages/getting-started-with-github-pages/creating-a-github-pages-site

# Topics

## Create GH Pages repo

On the `github.com` webpage, click on *Create New Repo* `+` button at the
top-right of the page. For the repo name, type your username followed by
`.github.io`. Also, make sure that you set the repo to *public*, as private
Github pages require a paid subscription.

## Use custom domain with GH Pages

### Namecheap DNS

First make sure that you are using *Basic DNS* in the Namecheap *Manage
DNS* tab. If you are using external DNS services from AWS, GCP, Oracle
Cloud, etc, first make sure that you have deleted your DNS zone from the
cloud provider and then move the Namecheap DNS from *Custom DNS* to *Basic
DNS*.

From the *Manage DNS* tab, click on *Advanced DNS*. and add a CNAME record
for your github username (`gojun077`) followed by `.github.io`. This
presupposes that you have already created a public repository in your
Github account that is named `gojun077.github.io`.

Once you have added the single `CNAME` record and the 4 `A` records from
the Github and Namecheap walkthroughs (see links below), go to Github and
go into the settings for the public repo you just created, and click on
*Pages* from the left-hand menu. In the *Custom domain* section, add your
domain name and click *Save*. Also click on *Verify DNS* to make sure
your Namecheap DNS settings can be detected by Github. Also try navigating
to your custom domain; you should see your Github pages site.

Finally, access your personal settings in your Github account (not the
settings for your Github Pages repo) and select *Pages* from the left-hand
menu.

Click on the button, *Add a domain*, and you will be presented with data which
you should copy-paste as a `TXT` record into Namecheap DNS. Once you have
created the `TXT` record, click on the *Verify* button. When the DNS changes
have propagated you should see the message, *Successfully verified ...*

One nice thing about Github Pages is that it works seamlessly with Let's
Encrypt and can generate TLS certs for your custom domain on your behalf once
your DNS records are properly configured.


**References**:

- https://docs.github.com/en/pages/configuring-a-custom-domain-for-your-github-pages-site/about-custom-domains-and-github-pages
- https://docs.github.com/en/pages/configuring-a-custom-domain-for-your-github-pages-site/managing-a-custom-domain-for-your-github-pages-site
- https://gist.github.com/plembo/84f80c920bb5ac6f19e53fe6f8db1ff7
- https://www.namecheap.com/support/knowledgebase/article.aspx/9645/2208/how-do-i-link-my-domain-to-github-pages/
