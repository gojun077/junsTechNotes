// go-ssh-concurr-session.go
// Created on: 2020.09.16
// Created by: gojun077@gmail.com
// Last Updated: 2020.09.16
// Contributors: gojun077@gmail.com

/*
This program reads a file containing a list of hosts and will loop
through the list of hosts and connect concurrently to multiple
hosts via ssh and execute remote commands within an interactive session.

This program takes the following arguments:
- name of file containing a newline-delimited list of IP's/hosts
- name of login user
- path to user's ssh private keyfile
- path to json file containing ssh privkey PW
*/

package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"golang.org/x/crypto/ssh"
	"io"
	"io/ioutil"
	"os"
	"sync"
	"time"
)

// json file must have key-keyval of form "password": "s3kr3t"
type Config struct {
	PWkey string `json:"password"`
}

func getKeySigner(privateKeyFile string, pw string) ssh.Signer {
	privateKeyData, err := ioutil.ReadFile(privateKeyFile)
	if err != nil {
		fmt.Printf("Error loading private key file! %v\n", err)
	}

	privateKey, err := ssh.ParsePrivateKeyWithPassphrase(
		privateKeyData, []byte(pw))
	if err != nil {
		fmt.Printf("Error parsing private key! %v\n", err)
	}
	return privateKey
}

func main() {
	start := time.Now()
	// argument flags BEGIN
	myfile := flag.String("f", "",
		"name of file containing hostnames/ip's, one per line")
	myuser := flag.String("u", "", "ssh login user name")
	mykey := flag.String("k", "", "path to ssh private key file")
	mypass := flag.String("p", "",
		"path to json file containing ssh privkey PW")
	myport := flag.String("P", "22", "TCP port used by sshd")
	flag.Parse()
	// argument flags END

	if len(os.Args) < 5 {
		flag.Usage()
		os.Exit(1)
	}

	myfileval := *myfile
	fmt.Printf("My file name is: %s\n", myfileval)
	myuserval := *myuser
	fmt.Printf("myuser is: %s\n", myuserval)
	mykeyval := *mykey
	fmt.Printf("keyfile is: %s\n", mykeyval)
	mypassval := *mypass
	fmt.Printf("key pw file is: %s\n", mypassval)
	myportval := *myport

	mycmd := `python
import os
import socket
statvfs = os.statvfs("/")
root_partsize=statvfs.f_frsize*statvfs.f_blocks
root_partfree=statvfs.f_frsize*statvfs.f_bavail
root_partpctused=(root_partsize-root_partfree)/float(root_partsize)
localip = socket.gethostbyname(socket.gethostname())
hname = socket.getfqdn()
print("%s %s root part size: %d, root part avail: %d, used ratio: %0.4f"
     %(hname, localip, root_partsize, root_partfree, root_partpctused))
exit()
sudo su -
whoami
`

	f, err := os.Open(*myfile)
	if err == nil {
		defer f.Close()
	}

	jsonFile, err := os.Open(mypassval)
	if err != nil {
		fmt.Printf("Error opening json file! %v\n", mypassval)
	}
	defer jsonFile.Close()

	var pwcfg Config
	jdecode := json.NewDecoder(jsonFile)
	if err != nil {
		fmt.Printf("Error decoding json! %v\n", err)
	}
	jdecode.Decode(&pwcfg)
	pw := pwcfg.PWkey

	privateKey := getKeySigner(mykeyval, pw)

	config := &ssh.ClientConfig{
		User:            myuserval,
		Auth:            []ssh.AuthMethod{ssh.PublicKeys(privateKey)},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	fscanner := bufio.NewScanner(f)
	hostL := []string{}
	for fscanner.Scan() {
		host := fscanner.Text()
		hostL = append(hostL, host)
	}
	var wg sync.WaitGroup
	for _, host := range hostL {
		new_host := host
		wg.Add(1)
		go func() {
			defer wg.Done()
			fmt.Printf("Connecting to %s...\n", new_host)
			client, err := ssh.Dial("tcp", new_host+":"+myportval, config)
			if err != nil {
				fmt.Printf("Error dialing server! %v\n", err)
			}
			// Multiple sessions per client are allowed
			session, err := client.NewSession()
			if err != nil {
				fmt.Printf("Failed to create session! %v\n", err)
			}
			defer session.Close()

			// Get pseudo-terminal
			err = session.RequestPty(
				"vt100", // or "linux", "xterm"
				40,      // Height
				80,      // Width
				ssh.TerminalModes{
					ssh.ECHO:          0,     // 0 disable, 1 enable
					ssh.TTY_OP_ISPEED: 14400, // input speed = 14.4kbaud
					ssh.TTY_OP_OSPEED: 14400, // output speed = 14.4kbaud
				},
			)

			if err != nil {
				fmt.Printf("Error requesting psuedo-terminal! %v\n", err)
			}

			// create stdin buffer into which to send commands to interactive
			// ssh session
			stdin, err := session.StdinPipe()
			if err != nil {
				fmt.Printf("Error setting up stdin for sess! %v\n", err)
			}
			//go io.Copy(stdin, os.Stdin)

			stdout, err := session.StdoutPipe()
			if err != nil {
				fmt.Printf("Error setting up stdout for sess! %v\n", err)
			}
			go io.Copy(os.Stdout, stdout)

			stderr, err := session.StderrPipe()
			if err != nil {
				fmt.Printf("Error setting up stderr for sess! %v\n", err)
			}
			go io.Copy(os.Stderr, stderr)

			// create interactive ssh shell session
			err = session.Shell()
			if err != nil {
				fmt.Printf("Error creating interactive ssh session! %v\n", err)
			}
			// send raw string commands to Shell()
			stdin.Write([]byte(mycmd))
			// HACK - sleep for 1 sec to allow remote cmd's time to finish
			time.Sleep(1 * time.Second)
		}()
	}
	wg.Wait()

	elapsed := time.Since(start)
	fmt.Printf("\nTotal execution time: %v\n", elapsed)
}
