sway WM for Wayland desktop HOWTO
=========================================

# Summary

- Created on: Sep 25 2021
- Created by: gojun077@gmail.com
- Last Updated: Jun 29 2024

This guide is for setting up `sway` BSP tiling window manager (WM) for the
Wayland display server which replaces X11/Xorg. Starting from Ubuntu 21.04,
Fedora 36, etc, Wayland became the default (2021) display server. `sway`
was started by Drew DeVault of SourceHut fame and is inspired by the `i3`
BSP tiling WM which unfortunately only works on X11/Xorg.

# Topics


## List of installation packages

```sh
sudo dnf update
sudo dnf install clipman imv sway waybar wev wl-clipboard wofi xlsclients
```

The package names are basically the same between Fedora and Ubuntu when
it comes to sway packages

## Sway Hotkeys

- `Mod + s` - stacking mode
- `Mod + f` - full-screen mode
- `Mod + w` - tabbed window mode
- `Mod + shift + SPC` floating mode (doesn't work for me)
- `Mod + e` - standard BSP mode
- `Mod + d` - launcher/menu program
- `Mod + Shift + c` - reload Sway config
- `Mod + <left/right arrow>` - move focus to left or right window
- `Mod + Shift <left/right>` - move focused window to left or right
- `Mod + n` - jump to workspace `n` (0-9)
- `Mod + Shift + n` - move focused window to workspace `n`


## Run sway desktop on a headless machine for remote access

Install the `wayvnc` package, which will allow you to access your sway
session with any vnc client.

On MacOS I use the open-source TigerVNC `1.12.0`. It has builds for
Linux, MacOS and Windows.


## Firewall rules

wayvnc listens for TCP connections on port 5000, so you need to make sure
this port is open if you are running a software Firewall locally.

### add firewall rule to firewalld

Fedora uses `firewalld` which is running by default. The default zone is
`FedoraServer` and your any active network interfaces from install time
will be in this zone.

```sh
$ sudo su
# firewall-cmd --list-all
FedoraServer (active)
  target: default
  icmp-block-inversion: no
  interfaces: wlp3s0
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
# firewall-cmd --get-default-zone
FedoraServer
# firewall-cmd --get-active-zones
FedoraServer
  interfaces: wlp3s0
# firewall-cmd --add-port 5900/tcp --permanent
success
# firewall-cmd --reload
success
# firewall-cmd --zone=Trusted --change-interface=tailscale0 \
  --permanent
success
# firewall-cmd --zone=FedoraServer --change-interface=enp2s0 \
  --permanent
success
# firewall-cmd --reload
success
# firewall-cmd --list-all
FedoraServer (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp2s0 tailscale0 wlp3s0
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 5900/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

`vnc` needs tcp 5900 to be open so that clients can connect. All rules in
`firewalld` are added dynamically and are not saved unless added to the
`permanent` rule set with flag `--permanent`. To apply rules added to the
permanent set, reload `firewalld` with `firewall-cmd --reload`.

The `tailscale0` virtual network interface for Tailscale can be
added to the zone `Trusted`, allowing all traffic from that iface.

### Launch wayvnc on server

```sh
export WLR_BACKENDS=headless
export WLR_LIBINPUT_NO_DEVICES=1
export WAYLAND_DISPLAY=wayland-1
export XDG_RUNTIME_DIR=/run/user/1000
export XDG_SESSION_TYPE=wayland
sway &
wayvnc -p 0.0.0.0
date +'%F %H:%M:%S'
```

After executing the above on the server, I had to press `<ENTER>` to
scroll past some error messages before `sway` started running properly
on Fedora 36.


### Launch sway + wayvnc on rpi

Run the following as the regular user (`ubuntu`):

```sh
export WLR_BACKENDS=headless
export WLR_LIBINPUT_NO_DEVICES=1
export WAYLAND_DISPLAY=wayland-1
export XDG_RUNTIME_DIR=/run/user/1000
export XDG_SESSION_TYPE=wayland
GTK_IM_MODULE=fcitx
QT_IM_MODULE=fcitx
sway &
sleep 1;
wayvnc -p 0.0.0.0
```

Note that the snippet above doesn't work when executed within a script
because the env vars are local to the sub-shell session. You will have to
execute the above within a terminal multiplexer session like `tmux` or GNU
`screen`.

I got the above snippet from the following reddit thread:

https://www.reddit.com/r/swaywm/comments/my67f6/sway_16_and_headless/

It is best practice to limit the IP's from which you will accept
connections. Instead of allowing VNC connections from everywhere, you could
for instance, only accept connections from your local subnet, i.e.
`192.168.0.0/24`.

### Connect to sway session with VNC client

Simply specify the IP of your RPI4 in TigerVNC or some other VNC client and
you will be greeted with the Sway desktop. Note that VNC does not support
audio. In my Wayland session with SwayWM on RPI4, I couldn't get `firefox`
or `chromium-browser` working. It seems that there are still some Broadcom
GPU issues for the RPI4 on Wayland. However on amd64 Ryzen 5600G with
integrated Radeon 7 GPU, web browsers and rendering work great OOTB in
`sway`. The lightweight `luakit` browser works great on the RPI4 in SwayWM,
though.

I must say, however, that VNC client performance with `wayvnc` server is
blazingly fast compared to a regular X11 `xfce4` session with
`x2goserver` on a RPI4B with 8GB RAM. The latter is unbearably slow...

## Check if an app is running in XWayland or Wayland

Linux GUI apps which run on XWindows/X11 can also run on Wayland inside an
`XWayland` window. But when apps run in XWayland, you will often see
graphical artifacts, screen tearing, blurriness, etc. When these problems
occur on Wayland, the first thing to check is whether the app is running in
XWayland compatibility mode for apps that don't support Wayland.

```sh
$ xlsclients
asahilinux  Keybase
```

The above output reveals that `Keybase` is running on XWayland.

## Workarounds to force apps to run in native Wayland mode

### Electron apps

Add the option `--ozone-platform=wayland` when invoking the binary exec.

This works for VSCode and Keybase.

### chromium-browser

The most sure-fire way I have found is to add the option `--ozone-platform=wayland`
when invoking the binary executable.

## 3rd-party apps

These apps aren't available from the default repos for Linux distributions
and must be either manually compiled and built for your architecture (amd64, aarch64, arm64)
or manually downloaded and installed.

### Compiling redshift-wayland fork

Refer to the Archlinux `PKGBUILD` from
https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=redshift-wayland-git
to get an idea of the build dependencies and the commands for building
and compiling the `redshift-gtk` binary that will work under Wayland. The
only package I was missing was `intltool`.


```sh
$ git clone -b wayland https://github.com/minus7/redshift.git \
    redshift-wayland
$ cd redshift-wayland
$ ./bootstrap
$ ./configure --prefix=/usr \
    --enable-drm  \
    --enable-vidmode  \
    --enable-geoclue2  \
    --with-systemduserunitdir=/usr/lib/systemd/user  \
    --enable-wayland
# make sure you remove the option '--enable-xrandr'
$ sudo make install  # install binary into /usr/bin/
```

Now you can launch redshift-gtk with something like `redshift-gtk -l
37.5667:126.9781` (`-l latitude:longitude`) and it will work under
Wayland.

### Keybase

See my separate guide on Keybase regarding Wayland setup; after launching
the Keybase Electron GUI in Wayland, you may find that `keybase-gui` and
other systemd services are not working properly. See the separate guide for
details on how to work around this.

## References

- https://www.fosskers.ca/en/blog/wayland
  + guide to setting up sway on Archlinux and Ubuntu
- https://github.com/Alexays/Waybar
  + swaybar replacement
- https://github.com/minus7/redshift.git
  + fork of `redshift` for Wayland
  + use branch `wayland`
