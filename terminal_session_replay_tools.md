Tools for recording and playing back terminal sessions
===========================================================

# Summary

# Topics

## script and scriptreplay

`script` for recording terminal sessions

`scriptreplay` for playing them back

It is always a good idea to specify a timing log with `--timing`. Note that
this option only exists in GNU `script`, not the BSD `script` version!

```
```

## asciicinema

## ...

## References

- https://www.redhat.com/sysadmin/playback-scriptreplay
