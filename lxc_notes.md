LXC HOWTO
=============

# Summary

- Created on : Jan 10 2024
- Created by: gopeterjun@naver.com
- Last Updated: Jan 10 2024

`lxc` is an omnibus command similar to `docker` that manages containers,
container networks, network profiles, and other lxc/lxd settings. It is
integrated tightly with the container hypervisor `lxd`. Although `lxd`
settings are stored in 
