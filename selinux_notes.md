SElinux notes
=================

# Summary

- Created on: May 9 2023
- Created by: gopeterjun@naver.com
- Last Updated: 18 Dec 2024

Security Enhanced Linux (`selinux`) is a Mandatory Access Control (MAC)
scheme that runs on top of Linux's default Discretionary Access Control (DAC)
security model. It uses a collection of rules to define a *baseline* security
context, with OOTB rules for most popular applications. It is possible to
write your own rules as well as customize existing rules. SElinux is mostly
used on RHEL-based Linux distributions.

# Topics

## List selinux context labels by network port

```sh
sudo semanage port -l
```

Normally this command is used together with `grep`:

```sh
$ sudo semanage port -l | grep http_port_t
http_port_t                    tcp      80, 81, 443, 488, 8008, 8009, 8443, 9000
$ sudo semanage port -l | grep 8080
http_cache_port_t              tcp      8080, 8118, 8123, 10001-10010
$ sudo semanage port -l | grep 9090
websm_port_t                   tcp      9090
websm_port_t                   udp      9090
```

## Edit selinux context label `http_port_t` for webserver

By default, TCP 80, 81, 443, 488, 8008, 8009, 8443, 9000 can be bound by
webservers. However `selinux` will prevent a web server using the
`http_port_t` context from binding to a port number that is not included in
the `http_port_t` label. Suppose you wish to bind nginx to `TCP 8086`
as a reverse proxy in front of influxdb.

If you try to bind to 8086 without first editing the selinux security
context, you will see the following in `/var/log/audit/audit.log`:

```
type=AVC msg=audit(1683626628.789:1237): avc:  denied  { name_bind } for  pid=96838 comm="nginx" src=8086 scontext=system_u:system_r:httpd_t:s0 tcontext=system_u:object_r:unreserved_port_t:s0 tclass=tcp_socket permissive=0
```

To avoid seeing this message, add TCP 8086 to `http_port_t`:

```sh
$ sudo semanage port -a -t http_port_t -p tcp 8086
$ sudo semanage port -l | grep http_port_t
http_port_t                    tcp      8086, 80, 81, 443, 488, 8008, 8009, 8443, 9000
```

Now `tcp 8086` has been added to context label `http_port_t`

But you are not done yet; On the backend, influxdb will also be listening
for regular HTTP traffic after TLS has been terminated by the reverse
proxy.  Let's assume `influxdb` is listening on `tcp 9086`. By default,
selinux does not allow `nginx` to act as a proxy. If you look at
`/var/log/audit/audit.log`, you will see the following:

```
type=AVC msg=audit(1683628561.487:1300): avc:  denied  { name_connect } for  pid=101182 comm="nginx" dest=9086 scontext=system_u:system_r:httpd_t:s0 tcontext=system_u:object_r:unreserved_port_t:s0 tclass=tcp_socket permissive=0
```

This is hard to parse for a human. To get an explanation of this, let's use
`setroubleshoot` with `journalctl`:

```sh
sudo journalctl -t setroubleshoot --since=19:00
```

We now get the following English-language explanations:

```
May 09 19:03:52 argonaut setroubleshoot[96841]:
SELinux is preventing nginx from name_bind access on the tcp_socket port 9086.
For complete SELinux messages run: sealert -l ...

*****  Plugin bind_ports (85.9 confidence) suggests   ****************
If you want to allow nginx to connect to network port 9086
Then you need to modify the port type
Do
# semanage port -a -t PORT_TYPE -p tcp 9086
    where PORT_TYPE is one of the following: dns_port_t, dnssec_port_t, kerberos_port_t, ocsp_port_t,

*****  Plugin catchall_boolean (7.33 confidence) suggests   *********
If you want to allow httpd to can network connect
Then you must tell SElinux about this by enabling the 'httpd_can_network_connect' boolean.
Do
setsebool -P httpd_can_network_connect 1
...
```

This can be resolved by enabling the selinux booleans either
`httpd_can_network_connect` or `httpd_can_network_relay` which will permit
nginx to act as a proxy:

```sh
sudo setsebool -P httpd_can_network_connect 1
```

Here is another example of using nginx as a reverse proxy in front of
Gradio (used to serve local ML models on the web). Gradio normally listens
on TCP 7860 for regular HTTP traffic. We will setup `nginx` to terminate
HTTPS connections coming in on port 7960 and forward plain HTTP
to 7860. Before we make any changes to selinux, `/var/log/audit/audit.log`
will show the following:

```
type=AVC msg=audit(1696552548.756:311): avc:  denied  { name_bind } for  pid=13332 comm="nginx" src=7960 scontext
=system_u:system_r:httpd_t:s0 tcontext=system_u:object_r:unreserved_port_t:s0 tclass=tcp_socket permissive=0
```

We need to add both TCP 7960 for nginx and TCP 7860 for gradio to the
Selinux label `http_port_t`

```sh
sudo semanage port -a -t http_port_t -p tcp 7960
sudo semanage port -a -t http_port_t -p tcp 7860
sudo semanage port -l | grep http_port_t
http_port_t                    tcp      7960, 7860, 8081, 7908, 7907, 8086, 80, 81, 443, 488, 8008, 8009, 8443, 9000
```

## Enable haproxy to listen on port

I was trying to use haproxy to listen on TCP/6443, but got the following
selinux denials:

```sh
aureport -a

AVC Report
===============================================================
# date time comm subj syscall class permission obj result event
===============================================================
1. 12/18/2024 21:06:49 haproxy system_u:system_r:haproxy_t:s0 0 tcp_socket name_bind system_u:object_r:unreserved_port_t:s0 denied 499
2. 12/18/2024 21:10:12 haproxy system_u:system_r:haproxy_t:s0 0 tcp_socket name_bind system_u:object_r:unreserved_port_t:s0 denied 508
3. 12/18/2024 21:10:30 keepalived system_u:system_r:keepalived_t:s0 0 file setattr system_u:object_r:bin_t:s0 denied 513
4. 12/18/2024 21:10:50 haproxy system_u:system_r:haproxy_t:s0 0 tcp_socket name_bind system_u:object_r:unreserved_port_t:s0 denied 518
5. 12/18/2024 21:13:41 haproxy system_u:system_r:haproxy_t:s0 0 tcp_socket name_bind system_u:object_r:unreserved_port_t:s0 denied 524
6. 12/18/2024 21:20:00 haproxy system_u:system_r:haproxy_t:s0 0 tcp_socket name_bind system_u:object_r:unreserved_port_t:s0 denied 530
```

The denial was made because `haproxy_t` context type does not have the
permissions to bind to TCP/6443.

We can add the necessary permissions as follows:

```sh
sudo semanage port -a -t http_port_t -p tcp 6443
```

*References*

- https://unix.stackexchange.com/questions/363878/which-selinux-policies-apply-to-haproxy

**References**:

- https://www.nginx.com/blog/using-nginx-plus-with-selinux/
- https://access.redhat.com/articles/2191331
  + troubleshooting SElinux
