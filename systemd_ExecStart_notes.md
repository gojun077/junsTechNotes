systemd service file HOWTO
============================

# Summary

- Created on: Mar 29 2022
- Created by: gojun077@gmail.com
- Last Updated: Mar 29 2022

Systemd `.service` and `.timer` files enable much more robust and
sophisticated recurring task behavior compared to legacy cronjobs, but
`systemd` is much more finicky that `cron`. Commands that run well
in an interactive shell environment will also generally work without
modifications in a `crontab` file, but the same is not true for
running scripts or shell commands in `ExecStart=` inside a systemd
`.service` file.

This guide walks through some gotchas and points to be aware of when
trying to run custom scripts, commands, binaries, etc as systemd
services that may also be triggered by timers.

# Topics

## systemd service types

There are various service types in systemd. See this
[link](https://wiki.archlinux.org/title/systemd#Service_types) for details.

To use arguments with your systemd service, however, the service type must
be `oneshot` or `simple`.

Quoted from the above link:

> `Type=simple` (default): systemd considers the service to be started up
> immediately. The process must not fork. Do not use this type if other
> services need to be ordered on this service, unless it is socket
> activated.

> `Type=oneshot`: this is useful for scripts that do a single job and then
> exit. You may want to set RemainAfterExit=yes as well so that systemd
> still considers the service as active after the process has exited.


## Use arguments in ExecStart

To pass arguments in `ExecStart=` of your systemd service file, a surefire
method is to define your arguments in a separate config file and then refer
to the args in your service file.

Create `/etc/systemd/system/foo.conf`

```
ARG1=-o
ARG2=--verbose
```

And in `/etc/systemd/system/foo.service` you can use `ARG1`, `ARG2`

```
[Service]
Type=oneshot
EnvironmentFile=/etc/systemd/system/foo.conf
ExecStart = /usr/bin/prog $ARG1 $ARG2
```


https://superuser.com/questions/728951/systemd-giving-my-service-multiple-arguments

Another workaround which I have successfully used in the past is to call
a script from `ExecStart` that can handle arguments. For example, if you
have a python script that can handle command line arguments via `argparse`,
write a shell wrapper `foo.sh` that executes your python script and
passes args to it:

```sh
#!/bin/bash
/usr/local/bin/foo.py arg1 --mode test
```

This avoids the strict limitations imposed on commands passed inside
`ExecStart=` in `.service` files.

## Use environment variables in service file

Environment variables like `PYTHONPATH` and others will be unavailable
if you execute a subshell through a systemd service. You can define env
vars by creating a `.conf` in `/etc/systemd/system` that has the
same `basename` as the `.service` file.

Let's assume that I have `match0_asset_pair_process_alarm_count.service`
with the following content:

```
[Unit]
Description=Run 'match0_asset_pair_process_alarm_count.sh'
After=network.target

[Service]
Type=oneshot
User=ec2-user
Group=ec2-user
ExecStart="/home/ec2-user/bin/match0_asset_pair_process_alarm_count.sh"
Restart=no
StandardOutput=syslog
StandardError=syslog
KillSignal=SIGKILL

[Install]
WantedBy=multi-user.target
```

This `.service` file is triggered by a `.timer` file with the same
`basename` that runs on some regular interval (hourly, daily, etc) with
a random jitter. The shell script called by the `.service` file runs a
Python script that requires `PYTHONPATH` to be set. We can set this env
var in `match0_asset_pair_process_alarm_count.service.d_10-python.conf`
which contains the following:

```
[Service]
Environment=PYTHONPATH=${PYTHONPATH}:/usr/local/bin:/usr/lib64/python37.zip:/usr/lib64/python3.7:/usr/lib64/python3.7/lib-dynload:/usr/local/lib64/python3.7/site-packages:/usr/local/lib/python3.7/site-packages:/usr/lib64/python3.7/site-packages:/usr/lib/python3.7/site-packages
```


