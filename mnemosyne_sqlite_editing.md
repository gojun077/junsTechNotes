How to Edit Mnemosyne Card Schedule with sqlite
=================================================

# Summary
- Last Updated: 2018.07.15
- Updated by: gojun077@gmail.com

> Mnemosyne is a Spaced Repetition Software program similar to
> SuperMemo and Anki. Users create "index cards"/"memory cards" and
> the software decides when to present these cards to the user
> depending on the *correctness* score earned when the card was last
> presented and the time that has elapsed since the card was last
> shown to the user. In mnemosyne *correctness* ranges from 1~5, with
> 5 denoting perfect mastery and 1 total ignorance. In practice, cards
> which the user knows well will be presented less frequently at later
> dates in the future, while cards graded with lower *correctness* scores
> will appear more frequently.

> One issue with all SRS programs is that if the user misses their
> daily scheduled reviews, the number of scheduled cards will begin
> to pile up. Then when the user resumes their daily reviews they
> will be met with a mountain of cards that must be reviewed before
> any new cards will be shown. This is very disheartening and can
> derail even the most motivated mnemosyne user. However, it is
> possible to manually edit mnemosyne's `default.db` which uses
> `sqlite` using either the `sqlite3` cli or the GUI
> *DB Broswer for SQLite*.

# Editing Mnemosyne `default.db`

- launch GUI `sqlitebrowser`
- or launch cli `sqlite3`
- open mnemosyne `default.db`
- select table `cards`
- look at COLUMN `next_rep`
    + 10 digit number is in UNIX Epoch time format
- convert epoch time to human readable time
    + bash: `date -d @1748563200`
        + `2025. 05. 30. (금) 09:00:00 KST`
    + python:

    ```python
    >>> import datetime
    >>> datetime.datetime.fromtimestamp(1748563200).strftime('%c')
    'Fri May 30 09:00:00 2025'
    ```

## Change scheduled review date

> For a given card, if the UNIX Epoch time value in the COLUMN
> `next_rep` is in the past, that card will appear in the *scheduled*
> list the next time mnemosyne is started. If you don't want the
> card to appear in this list, you must change the value of `next_rep`
> to a date in the future (using UNIX epoch time format).

### method for sqlitebrowser
- first click the `next_rep` COLUMN to sort ascending or descending

> Once you have found a run of time values in the past, group
> select them with *<SHIFT> + leftclick*. Then right-click the
> highlighted group and select *Paste*


# Notes

## sqlite3 CLI
- exit sqlite3
    + `.exit`
- open sqlite db file
    + `.open FILENAME`
    + or launch sqlite3 followed by sqlite DB filename
        + `sqlite3 my.db`
