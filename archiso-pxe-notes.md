Archiso PXE Notes
=================
Unlike installation iso's from mainstream Linux distro's, the
Archlinux iso names its initrd file differently. When booting
Arch you must separately load Intel microcode before the initial
ramdisk image.

# Important Paths on iso
- /arch/boot/x86_64
  contains the linux kernel and initrd image for 64-bit
  + kernel: `vmlinuz`
  + initrd: `archiso.img`
- /arch/boot
  contains intel microcode and memtest
  + `intel_ucode.img`
  + `memtest`

# Arch-specific Kernel boot params for PXE
These boot params can be used for both UEFI and Legacy BIOS PXE
although where they are inserted will differ. For examples,
refer to `/usr/local/tftpboot/pxelinux/grub.cfg` (UEFI) and
`/usr/local/tftpboot/pxelinux/pxelinux.cfg/default` (BIOS)

- `archisobaseddir=arch`
  Specifies the root directory of the installation iso

- `archiso_http_srv=http://192.168.95.97:8080/`
  Specifies installation file location over http
  (you can also use `nbd` and `nfs` instead of *http*)

- `ip=:::::eth0:dhcp`
  Tell the Arch kernel to bring up the network iface (on the
  machine to be installed with Arch) and get an IP address via
  DHCP. For predictability, the network iface in the Arch
  chroot environment is always named `eth0`
