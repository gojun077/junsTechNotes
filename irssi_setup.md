irssi IRC Client Setup
========================

# Summary
- Created: 2019
- Last Updated: Sep 20 2021
- Updated by: gojun077@gmail.com

> Notes to help me remember how to setup irssi on a new machine.

# Topics

## Package Install

### Ubuntu
- `sudo apt update; apt install irssi`

### Archlinux

- `sudo pacman -S irssi`

# irssi setup

## irssi config
- First run irssi one time to generate `~/.irssi/config`
    + `irssi`
- exit with `/quit`
- rename the default irssi config file
    + `mv ~/.irssi/config ~/.irssi/config.old`
- Create symlink from dotfiles repo file irssi-config to target folder
    + `ln -s ~/dotfiles/irssi-config ~/.irssi`
- Create subfolder for irssi.pem key
    + `mkdir ~/irssi/certs`
- Create symlink for `irssi.pem` keyfile into `~/.irssi`
    + `ln -s ~/SpiderOak\ Hive/keys/irssi.pem ~/.irssi/certs`
- This keyfile was used to register the nick `archjun` on Freenode


## Generate SASL SSL cert

https://libera.chat/guides/certfp



## Test installation and config
- `irssi`
- `/connect freenode`
- You should be logged in automatically as `archjun`
- Your nick should also be automatically verified

## Automatically log channel to file

> Add the following to your `~/.irssi/config` file:

```
/LOG OPEN -autoopen -targets #IRISnetTechUpdates ~/irclogs/irisTechUpdates
```

## Automatically login to a chatnet

### bitlbee

```
  bitlbee = {
    type = "IRC";
    nick = "mynick";
    username = "myusername";
    realname = "my realname";
    autosendcmd = "say identify password";
  };
```



# Appendix

## Use a self-signed cert for passwordless freenode login

- Generate cert (both priv and pub into same file)

    ```bash
    openssl req -x509 -new -newkey rsa:4096 -sha256 -days 1000 -nodes \
        -out irssi.pem -keyout mykey.pem
    ```

- In the *freenode* section of `~/.irssi/config` setup SSL

    ```
    use_ssl = "yes";
    ssl_cert = "~/.irssi/irssi.pem";
    ssl_pass = "<irssi.pem_password>";
    ssl_verify = "yes";
    ```

- Launch irssi and tell freenode to use pem keyfile for auth

```
/server add -auto -ssl -ssl_cert ~/.irssi/irssi.pem -network freenode chat.freenode.net 6697
```

- Get fingerprint of pem file

    ```
    cd ~/.irrsi
    openssl x509 -in irssi.pem -outform der | sha1sum -b | cut -d' ' -f1
    ```

- Add your fingerprint to NickServ
    + `/msg NickServ CERT ADD <pem fingerprint>`


- References
    + https://freenode.net/kb/answer/certfp
    + https://pthree.org/2010/02/01/oftc-ssl-nickserv-and-irssi/
