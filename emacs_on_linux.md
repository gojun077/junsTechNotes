Emacs on Linux - HOWTO
========================

# Summary

- Created on: Jul 16 2023
- Created by: gopeterjun@naver.com
- Last Updated: Jul 16 2023

This HOWTO contains my personal notes, tips/tricks, list of hotkeys,
etc. for setting up and using Emacs on Linux. In July 2023, the most
recent stable version of Emacs is `v28.2` and it works on both X11 and
Wayland desktops, although native Wayland support isn't yet available
for `v28.x`. Emacs will be native Wayland compatible starting with
`v29.x`

# Topics

## IME language switcher hotkey

Many IME's for desktop Linux like `iBus` and `fcitx5` use the
`C-SPC` keychord as the hotkey for switching between input
languages.
