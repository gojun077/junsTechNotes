List of packages explicitly installed by me on Fedora 36
==========================================================

# Summary

- Created on: July 7 2022
- Created by: gojun077@gmail.com
- Last Updated: July 7 2022

This list of packages does not include automatically-installed
dependencies, only manually-installed packages.

