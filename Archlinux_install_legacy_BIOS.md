Archlinux Install guide for Legacy BIOS firmware machines
=============================================================

# Summary

- Created On: Aug 26 2021
- Created by: gojun077@gmail.com
- Last Updated: Sep 4 2021

Installation notes for installing Archlinux iso 202108 on x64
architecture. It's been 5 years since I last did a vanilla install of
Archlinux on an Intel machine although I installed Archlinux AArch64 on a
RPI 3B in 2019.

The machines I used this time are:

(1) Acer Aspire One D257 with Intel Atom N570 4-core @1.66GHz CPU, 2GB DDR3
RAM, and 320 GB spinning rust HDD, Intel Centrino Wireless-N 100

(2) Dell XPS m1330 with Intel Core2Duo T8300 @2.40 GHz, 2 cores, 4GB DDR2
RAM, and 128 GB Samsung SSD (830 Series), Intel Wireless 3945ABG

Some things that changed in the Archlinux install over the past 5 years:

- `wifi-menu` replaced by `iwctl`
- `sudo` package is no longer included by default
- minor changes to packages, `mkinitcpio` syntax, etc.

# Pre-Installation Steps from Live USB / ISO

All the commands below are provided by default by the installation
media and you won't be required to install anything from `pacman`
package manager (yet).

## Connect to wifi for Internet connection

Archlinux now uses the `iwctl` cli wrapper to the `iwd` daemon written by
Intel. It is lightweight and doesn't depend on `wpa_supplicant`.

```
iwctl  # you will be presented with an interactive prompt
[iwd]# device list
[iwd]# station wlan0 scan
[iwd]# station wlan0 get-networks
[iwd]# station wlan0 connect mySSID
password: ********
[iwd]#station wlan0 show
[iwd]#known-networks list
...
quit
```

## Update system clock

`timedatectl set-ntp true`

## Setup Local Storage

First we need to find the name of the disk(s)/storage media that are
present in the machine.

```sh
$ lsblk
NAME               MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINTS
sda                  8:0    0 298.1G  0 disk
...
```

If the disk is empty, you should only see the device name and no
partitions. If some partitions exist, that's OK, as we will delete
them with `fdisk`.

### Create new partitions / Delete old partitions with `fdisk`

Because this machine is quite old, it doesn't use UEFI firmware; instead it
uses legacy BIOS firmware. Therefore we will write a MBR partition table to
the disk. Recall that MBR has quite a few limitations -- limited max
partition size, only up to 4 primary partitions, etc. But this is not a big
issue because we can create a LVM partition and create a Volume Group within
that can hold many Logical Volumes.

```sh
fdisk /dev/sda  # use the device name found in 'lsblk'
```

list of commands you will use:

- `m` help menu
- `d` delete partition
- `l` list all available partition types
- `n` create a new partition
- `p` print the partition table
- `t` change a partition type
- `v` verify the partition table
- `q` quit without saving changes
- `w` write table to disk and exit

First `p` print the existing partition table to see what's what. `d` to
delete partitions which are identified by numbers 1-9. When no partitions
exist, start creating new partitions with `n`. Use the default offset
blocks (just press ENTER) for the start of the partition, and when you
are prompted for the ending block, just enter a size like +1G for 1 Gigabyte.
All partitions are by default created as type 83 default Linux partition.

For our purposes create 2 partitions, both primary ones. The first
partition will be used as `sda1` (although you will not name it explicity)
and make it 1GB. Next create another partition and let it occupy all the
space remaining. Press `t` to change the partition type to `8e` which is
for *LVM*. Finally press `w` to write your changes to the partition table.

### Create LUKS container

Now we will use `cryptsetup` to create a LUKS (Linux Unified Key
Setup) encrypted container, open it, and create LVM resources inside.


```sh
cryptsetup luksFormat /dev/sda2
# you will be prompted to enter a password twice
# this will be the key needed to decrypt the container

# open LUKS container with mountpoint 'lvm'
cryptsetup open --type luks /dev/sda2 lvm
# type the LUKS container password
```

### Create LVM Resources

First create a Physical Volume (PV) in the open LUKS container

`pvcreate /dev/mapper/lvm`

Next create a Volume Group (VG) named *ARCH* in the PV

`vgcreate ARCH /dev/mapper/lvm`

Now create Logical Volumes (LV) in the VG. Note that we will not create a
LV for the `/boot` partition, as the boot partition has been created as
a primary partition and will reside outside of the LVM partition.

```sh
lvcreate -L 4G ARCH -n swapvol
lvcreate -L 30G ARCH -n rootvol
lvcreate -L 25G ARCH -n varvol
lvcreate -l +100%FREE ARCH -n homevol
```

### Create Swap, Create File Systems

```sh
mkswap /dev/ARCH/swapvol
mkfs.ext4 /dev/sda1  # format /boot part as ext4
for i in {rootvol,varvol,homevol}; do
  mkfs.ext4 /dev/ARCH/$i
done
```

At this point, if you run `lsblk -f` you will see something like:

```
$ lsblk -f
NAME               FSTYPE      FSVER    LABEL UUID                                   FSAVAIL FSUSE% MOUNTPOINTS
loop               squashfs    4.0                                                             0   100% /run/archiso/airootfs
sda
├─sda1             ext4        1.0            834e450e-35f9-4840-a300-9dd4fa16b089    xxx.xM     9% /boot
└─sda2             crypto_LUKS 2              a889e62c-d4c1-4e38-880f-63334ed10393
  └─ARCH           LVM2_member LVM2 001       m97L84-0oOR-Ki0o-XIfh-pcw3-RDpJ-Dld23M
    ├─ARCH-swapvol swap        1              573f8d8d-c529-477a-964e-a8a8163afa7a                  [SWAP]
    ├─ARCH-rootvol ext4        1.0            51d68eb4-1e1e-408d-8dfd-eda72700e35b     xx.xG     4% /
    ├─ARCH-varvol  ext4        1.0            2cf2292b-3d85-4d92-95f8-d148cb263591     xx.xG     1% /var
    └─ARCH-homevol ext4        1.0            b2bf8e32-2c96-45cc-8d25-9efe83db826d    xx.xG     0% /home
sdb
└─sdb1             vfat        FAT32    ARCH_202108 16F8-E9B2                         1.1G    39% /run/archiso/bootmnt
```

### Mount partitions into mount points that will be used in chroot

This step is the most confusing for people who have never installed
Archlinux before from the CLI without any GUI installer.

We will use `/mnt` as `/`, so all the partitions we have created
so far will be mounted under `/mnt` with the exception of our
LVM rootvol LV, which will be mounted directly on `/mnt`

```sh
# first mount the root partition on /mnt
mount /dev/ARCH/rootvol /mnt
# Now create mount points in chroot dir
mkdir /mnt/{/boot,var,home}
# mount partitions on chroot sub-dirs
mount /dev/sda1 /mnt/boot
mount /dev/ARCH/varvol /mnt/var
mount /dev/ARCH/homevol /mnt/home
```


## Last few steps before entering chroot environment

Activate the swap partition

`swapon /dev/ARCH/swapvol`

Make sure that the mirrors in the pacman mirrorlist are located close
to you or are fast mirrors.

`vim /etc/pacman.d/mirrorlist`

Bootstrap a minimum Archlinux environment into the chroot dir

`pacstrap /mnt base linux linux-firmware`

Generate fstab and write it into the chroot dir

`genfstab -U /mnt >> /mnt/etc/fstab`


# chroot environment

Starting in this section, we will use `pacman` to install packages into
the chroot environment. These packages will remain after rebooting. Also
the bootloader and other settings made in the chroot will be persistent.

Enter the chroot

`arch-chroot /mnt`


## Install packages needed for bootloader and initramfs

```sh
pacman -Syyu --noconfirm grub lvm2 sudo intel-ucode openssh ethtool \
  dhclient git iwd vim screen dmidecode which
```

## Timezone and Localization Settings

```sh
ln -sf /usr/share/zoneinfo/Asia/Seoul /etc/localtime
hwclock --systohc
```

Uncomment the line containing `en_US.UTF-8 UTF-8` in `/etc/locale.gen`
and then run `locale-gen`

Check `$LANG`

```sh
$ echo $LANG
en_US.UTF-8
```

Create `locale.conf`

```sh
echo "LANG=$LANG" > /etc/locale.conf
```


## Network Config

Create `/etc/hostname`

```sh
echo "myhost" > /etc/hostname
```

Add matching entries to `/etc/hosts`

```
127.0.0.1     localhost
::1            localhost
127.0.1.1      myhost
```

## Initial Ramdisk FS setup

Since we're using LVM and LUKS, we need a new `initramfs` which is
generated in Archlinux using `mkinitcpio`

Edit `/etc/mkinitcpio.conf` and in `HOOKS` make sure you have the
following:

```
HOOKS=(base udev autodetect keyboard keymap consolefont modconf block encrypt lvm2 filesystems fsck)
```

Most importantly, `encrypt` and `lvm2` must be in the order indicated
above, otherwise at boot-time the LUKS container cannot be opened and the
LVM partition won't work.

After finishing your edits, regenerated the `initramfs` image
`mkinitcpio -P`

## Setup GRUB bootloader

Earlier we used `pacman` to install the `grub` package, but all the
bootloader files have not been written to disk yet.

If you look at `/boot` before running `grub-install`, you will notice that
it only contains `lost+found` sub-dir, `vmlinuz-linux`, `intel-ucode.img`,
`initramfs-linux.img` and `initramfs-linux-fallback.img`.

To generate the grub boot files and the create the `grub` sub-dir in
`/boot`, run `grub-install --recheck /dev/sda`

Next we need to add the LUKS cryptdevice info to `/etc/default/grub`
so the bootloader can open the LUKS container and find the LVM root volume
hiding inside. But first we need the UUID of the LUKS container. You can
find this with `lsblk -f`. You can see that `sda2` has `FSTYPE`
`crypto_LUKS`, and in the fields to the right, you can see the UUID.
Let's write this info to a file so we can open it in `vim` along with
the default grub.

```sh
lsblk -f > /tmp/lsblk_f.txt
vim /tmp/lsblk_f.txt /etc/default/grub
```

In `vim` use the `v` modal(?) to activate copy mode, select the UUID text
and press `y` to yank to the clipboard. Then enter command mode with `:`
and type `n<ENTER>` to go to the next file (`grub`).

On the line `GRUB_CMDLINE_LINUX_DEFAULT=`, enter the following

`"cryptdevice=UUID=<longUUID>:ARCH root=/dev/mapper/ARCH-rootvol"`

To paste in the long UUID, simply press `p` at the cursor position
where you want to insert the clipboard text.

Now we need to generate `grub.cfg` in `/boot/grub/` and it will
detect `intel-ucode` and make `/boot/intel-ucode.img` the first initrd
in the bootloader as well as read our settings from `/etc/default/grub`.

```sh
grub-mkconfig -o /boot/grub/grub.cfg
```


## Final steps before rebooting into new installation on disk

Change root passwd with `passwd`

Exit from chroot with `exit` or `<Ctrl-d>`

Unmount the chroot partitions `umount -R /mnt`

Reboot with `reboot`


# Post-install steps

## enable NTP time sync

`timedatectl set-ntp true`

## Setup wireless network connection

Enable and start the `iwd` systemd service and launch `iwctl`

```sh
systemctl enable iwd
systemctl start iwd
iwctl
```

After connecting to a SSID, you will have to manually request a DHCP
lease:

```sh
dhclient wlan0
ip a  # see the IP you have been assigned
```

You should now have an IP address assigned to your wireless iface.


## Enable and start sshd

```sh
systemctl enable sshd
systemctl start sshd
```

## Setup non-root user

```sh
useradd -m archjun
passwd archjun  # enter PW twice
usermod -a -G wheel archjun
```


Run `visudo` and uncomment the line containing `wheel` in `/etc/sudoers`;
note that you must only edit this file using `visudo`!

```sh
ln -s /usr/bin/vim /usr/bin/vi
visudo
# uncomment %wheel ALL=(ALL) ALL
```

Finally switch to the non-root user account

```sh
su - archjun
```

## Make permanent network settings

We will use `systemd-networkd` to tell the machine what network
interfaces to bring up at boot and how to connect to the Internet.

First let's create an entry for the wireless interface in
`/etc/systemd/network`. I'll name it `20-wlan0.network` and
it should contain the following:

```
[Network]
Description=Settings for netbook wireless iface
DHCP=ipv4

[Match]
Name=wlan0

[DHCPv4]
RouteMetric=20
```

Since we're using `iwd` and `iwctl` to control the wireless iface,
we also need to create `/etc/iwd/main.cfg`:

```
[General]
use_default_interface=true
EnableNetworkConfiguration=true
```

Now enable and start `systemd-networkd`

```sh
systemctl enable systemd-networkd
systemctl start systemd-networkd
```

Now the wireless interface will come up on boot and will also request
a DHCP lease from your wireless router.

## Setup temperature sensors

Install the following:

```sh
pacman -S lm_sensors i2c-tools rrdtool
```

and then as root execute `sensors-detect` and answer `yes` to the
prompts of whether to scan various interfaces.

After running this tool, `lm_sensors.service` will be enabled and
started.

To enable logging of sensor data (temperature, mostly) to `journalctl`,
you need to enable `sensord.service` which is included as part of the
`lm_sensors` package, although it is not enabled by default:

```sh
sudo systemctl enable sensord
sudo systemctl start sensord
```

Note that the `rrdtool` package must be installed (Round-Robin
Database) in order for `sensord` to start.

Now you can see temperature, fan speed and other sensor data with
`journalctl -usensord`


## Enable TRIM for Solid-State Disks (SSD)

https://wiki.archlinux.org/title/Solid_state_drive

Check if your hardware supports TRIM / wear-leveling

`lsblk --discard` and check that *non-zero* values exist for `DISC-GRAN`
or `DISC-MAX`.

The `util-linux` package contains `fstrim.timer` which runs
`fstrim.service` every week.

`ExecStart` in `fstrim.service` runs the following command:

```sh
/usr/bin/fstrim --listed-in /etc/fstab:/proc/self/mountinfo \
  --verbose --quiet-unsupported
```


## Install Graphics Drivers

Older machines with legacy BIOS firmware also have old graphics cards.
In the case of old Nvidia cards (before 2010), you are better off using
the `nouveau` open-source driver.

```sh
pacman -Syu xf86-video-nouveau
```

https://wiki.archlinux.org/title/Nouveau

https://wiki.archlinux.org/title/Dell_XPS_M1330_/_M1530

> Most units were shipped with an Nvidia 8400M-GS card (G86M). It is no
> longer properly supported by the proprietary NVIDIA driver. You have two
> options:

> - Use kernel mode setting (works out of the box)
> - Install xf86-video-nouveau (small improvement)

> Performance will not be great. Expect frequent freezes under moderate
> loads.


## Install i3 tiling Window Manager

```sh
pacman -Syu xorg-server xorg-xinit i3-wm ttf-dejavu dmenu xterm \
  i3status
```

You can now launch i3 with `startx`. After launching one time and
exiting with `Super+Shift+e` you can find an auto-generated
config in `~/.config/i3/config`

You may also want to install another terminal of your choice and edit
the i3 config, otherwise `Super-<ENTER>` will always launch `xterm`.

Here's an example of specifying the default terminal to be `alacritty`

```
bindsym $mod+Return exec alacritty
```

Alacritty is available from the community repo.

The base i3 config will be saved to `~/.config/i3/config` where you
can define hotkeys and keybindings. The default status bar is `i3status`
and you can choose what status items will be listed there by copying
`/etc/i3status.conf` to `~/.config/i3status/config`, although first
you have to create the `i3status` folder in `~/.config`:

```sh
mkdir ~/.config/i3status
cp /etc/i3status.conf ~/.config/i3status/config
```


## Map Function Keys

On some notebooks I've used, the function keys *just work* without further
tweaking, but in the case of the Dell XPS m1330, the `Fn` keys for
increasing and decreasing screen brightness and toggling the display
on/off were not working OOTB.

- https://wiki.archlinux.org/title/Xbindkeys
- https://wiki.archlinux.org/title/Backlight#Backlight_utilities

```sh
pacman -S acpilight xbindkeys
touch ~/.xbindkeysrc
xbindkeys --key  # gives you the keycode upon Fn+key press
```


## Setup dotfiles


## Install browser

On the netbooks and ancient Core2Duo machines where I've installed
Archlinux, recent versions of Chromium and even Firefox are much too
heavy and cause freeze-like unresponsiveness and overheating. For example,
my DELL XPS m1300 has only 256MB  of VRAM while my Acer Aspire netbook only
has 8MB of VRAM. In the case of the Aspire, I've given up on installing a
Desktop Environment on that netbook entirely; it runs great without X11
and is still useful as a wireless connection forwarder for other
machines that lack wifi.

I've found `opera` browser v78.0.xxxx runs without making my core2Duo
unusable, so that's what I'm using for the time being.


## Install utilities

YMMV

```sh
pacman -S emacs ipython nmap bind redshift pipewire \
  pipewire-alsa pipewire-pulse pcmanfm cmus ffmpeg alsa-utils \
  alsa-tools wget man-db
```

The package `bind` contains the `dig` DNS utility, btw.


## How to Install pkgs from Arch User Repository (AUR)

https://wiki.archlinux.org/title/Arch_User_Repository

```sh
pacman -S --needed base-devel
```

Then from `aur.archlinux.org/packages` search for a package by name. and
click on the link. From the package page, at the far-right under *Package
Actions* copy the link for `Download snapshot` and use `wget` or curl to
download `<pkgName>.tar.gz` to your machine. `tar xvf` the tarball, cd
into the newly-created directory and you should see `PKGBUILD` and
`.SRCINFO` files. Open up `PKGBUILD` to make sure there are no malicious
commands inside. If it looks fine, run the command

```sh
# do not run as root
makepkg -is  # -i install, -s syncdeps
```

If you are using a machine with multiple cores, edit `/etc/makepkg.conf`
and uncomment the line `#MAKEFLAGS="-j2"`. This setting specifies the
number of processes to use when running `make`. For a modern multicore
machine, `-j4` should speed up your build by 2x or 3x. Adjust this setting
as appropriate.

This will build a binary package from the recipe in the `PKGBUILD` that can
be installed by `pacman`. The binary package will be in `tar.zst` format,
compressed with Facebook's Zstandard compression algo. It features near-realtime
extraction or archives and provides good compression similar to `.7z`

Now install the binary package with `pacman -U <mybinary>.tar.zst`

# Links

- https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#LVM_on_LUKS
- https://wiki.archlinux.org/title/general_recommendations
- https://wiki.archlinux.org/title/Xinit
- https://wiki.archlinux.org/title/PipeWire
- 
