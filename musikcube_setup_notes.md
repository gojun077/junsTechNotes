Musikcube notes
===================

# Summary

- Created on: 21 Feb 2023
- Created by: gojun077@gmail.com
- Last Updated: 14 Dec 2024

[musikcube](https://github.com/clangen/musikcube) is an open source
streaming client and server written in C++ with a `ncurses` UI. It can also
be used as a music client and an Android client `musikdroid` is available
through the upstream project's releases page. Although it is terminal
focused and heavily uses hotkeys, some mouse control is possible.

# Topics

## Download and Install latest release binaries

https://github.com/clangen/musikcube/releases

Binaries are available for MacOS, Windows, and various Linux distros. I
downloaded the `.rpm` package and it installs fine on Fedora 37:

```sh
$ wget https://github.com/clangen/musikcube/releases/download/0.99.5/musikcube_linux_0.99.5_amd64.rpm
...
$ sudo dnf install musikcube_linux_0.99.5_amd64.rpm
```

Packages for PC can act as both server and client. An Android client
`musikdroid` is also available for download via the Github releases page

**NOTE**: `aarch64` / `arm64` pre-built binaries on the Github releases
page are only available for MacOS, not Linux. There are, however, pre-
built binaries for RPi armv6 and armv8, but these binaries are only 32-bit.

## Open ports in firewall with `firewall-cmd`

```sh
# metadata port
$ sudo firewall-cmd --add-port 7905/tcp --zone home
success
# audio port
$ sudo firewall-cmd --add-port 7906/tcp --zone home
success
$ sudo firewall-cmd --runtime-to-permanent --zone home
success
```

A websocket server runs on port `7905`, used for metadata retrieval.
An http server runs on port `7906` and is used to serve audio data to clients.


## nginx reverse proxy for SSL termination in front of musikcube

https://github.com/clangen/musikcube/wiki/ssl-server-setup

Here is my sample `/etc/nginx/conf.d/musikcube.conf` which I
have customized using the Mozilla TLS/SSL [web server config
generator](https://ssl-config.mozilla.org/):

```nginx
# websocket metadata server
server {
    listen 7907 ssl;
    listen [::]:7907 ssl;
    server_name argonaut.finch-blues.ts.net;

    location / {
        proxy_pass http://127.0.0.1:7905;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
    }

    ssl_certificate /var/lib/tailscale/certs/argonaut.finch-blues.ts.net.crt;
    ssl_certificate_key /var/lib/tailscale/certs/argonaut.finch-blues.ts.net.key;
    ssl_session_timeout 10m;
    ssl_session_cache shared:MozSSL:10m;  # about 40000 sessions
    ssl_session_tickets off;

    # curl https://ssl-config.mozilla.org/ffdhe2048.txt > /path/to/dhparam
    ssl_dhparam /etc/nginx/ssl-dhparams.txt;

    # intermediate configuration
    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
    ssl_prefer_server_ciphers off;

    # HSTS (ngx_http_headers_module is required) (63072000 seconds)
    add_header Strict-Transport-Security "max-age=63072000" always;

    # OCSP stapling
    ssl_stapling on;
    ssl_stapling_verify on;

    # verify chain of trust of OCSP response using Root CA and Intermediate certs
    ssl_trusted_certificate /var/lib/tailscale/certs/lets-encrypt-x3-cross-signed.pem;

    # replace with the IP address of your resolver
    resolver 192.168.21.1;
}

# audio file server
server {
    listen 7908 ssl;
    server_name argonaut.finch-blues.ts.net;

    location / {
        proxy_pass http://127.0.0.1:7906;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    ssl_certificate /var/lib/tailscale/certs/argonaut.finch-blues.ts.net.crt;
    ssl_certificate_key /var/lib/tailscale/certs/argonaut.finch-blues.ts.net.key;
    ssl_session_timeout 10m;
    ssl_session_cache shared:MozSSL:10m;  # about 40000 sessions
    ssl_session_tickets off;

    # curl https://ssl-config.mozilla.org/ffdhe2048.txt > /path/to/dhparam
    ssl_dhparam /etc/nginx/ssl-dhparams.txt;

    # intermediate configuration
    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
    ssl_prefer_server_ciphers off;

    # HSTS (ngx_http_headers_module is required) (63072000 seconds)
    add_header Strict-Transport-Security "max-age=63072000" always;

    # OCSP stapling
    ssl_stapling on;
    ssl_stapling_verify on;

    # verify chain of trust of OCSP response using Root CA and Intermediate certs
    ssl_trusted_certificate /var/lib/tailscale/certs/lets-encrypt-x3-cross-signed.pem;

    # replace with the IP address of your resolver
    resolver 192.168.21.1;
}
```

The difference from the plain vanilla settings is that now SSL ports will
be `TCP 7907` and `7908` and nginx reverse proxy will forward plain HTTP
traffic to `TCP 7905` and `7906`, respectively, for metadata and audio.

This will require that you add TCP 7907 and 7908 to `firewalld`. My home
network is in `--zone home`, so I could add these ports as follows:

```sh
# metadata port
$ sudo firewall-cmd --add-port 7905/tcp --zone home
success
# audio port
$ sudo firewall-cmd --add-port 7906/tcp --zone home
success
$ sudo firewall-cmd --runtime-to-permanent
success
```

Finally, you need to update the SElinux context label `http_port_t`
so that `nginx` can bind to TCP 7907 and 7908:

```sh
$ sudo semanage port -a -t http_port_t -p tcp 7907
$ sudo semanage port -a -t http_port_t -p tcp 7908
$ sudo semanage port -l | grep http_port_t
http_port_t                    tcp      8081, 7908, 7907, 8086, 80, 81, 443, 488, 8008, 8009, 8443, 9000
```

**Note**: Since we are using Tailscale TLS certs issued by Let's Encrypt,
it is important to use the FQDN when inputting remote server info in `musikcube`
clients like `musikcube` itself or `musikdroid` for Android.

In the case of `musikdroid` ssl certificate validation will fail unless you
use the FQDN `argonaut.finch-blues.ts.net`; I had problems when just using
`argonaut`, which still resolves via Tailscale. Also note that `musikdroid`
will fail to connect to your `musikcube` server when SSL is enabled if your
certs expire. In nginx `error.log` you will something like the following if
you have expired certs:

```
023/07/14 10:27:44 [error] 2258#2258: OCSP response not successful (6: unauthorized) while requesting certificate status, responder: r3.o.lencr.org, peer: 23.216.159.51:80, certificate: "/var/lib/tailscale/certs/<FQDN>.crt"
```

## MacOS + Alacritty + musikcube issues

Because of how MacOS handles keyboard input, the `alacritty` terminal which
I am currently using cannot pass some `ctrl-` and `meta-` key chords
properly on MacOS. It is possible to whitelist these key chords in
`alacritty.yml` so that they won't be intercepted by MacOS and instead sent
to the terminal.

In MacOS, you can configure your terminal emulator to treat your left
alt/option key as `Meta`.


## Firewall rules

### firewalld

### ufw

## Keyboard shortcuts

https://github.com/clangen/musikcube/wiki/user-guide

- main view switching
  + `~` switch to console view
  + `a` switch to library view
  + `s` switch to settings view
- playback
  + `j` previous track
  + `l` next track
  + `u` seek back
  + `o` seek forward
  + `C-e` configure equalizer
  + `C-p` pause/resume globally
  + `C-x` stop (unload streams, free resources)
- library
  + `b` browse view
  + `n` play queue
  + `f` show album/artist/genre search
  + `t` show track search
  + `1` browse by artist
  + `2` browse by album
  + `3` browse by genre
  + `5` browse by playlist
    + `M-n` create new empty playlist
    + `M-s` save currently selected playlist
    + `M-DOWN(UP)` move selected track down(up)
    + `C-DOWN(UP)` move selected track down(up) on MacOS
  + `d` browse by directory
  + `M-ENTER` show context menu for selected item
  + `SPACE` pause/resume

## Building from source

`clangen` offers binaries for most platforms and architectures from the
Github repo's releases page, but ARM64 Linux builds are conspicuously absent

If you are using the free/libre `ffmpeg` libraries available from the
default Fedora repos, the packages you need are as follows:

```sh
sudo dnf install gcc-c++ make cmake libogg-devel libvorbis-devel \
  ncurses-devel zlib-devel alsa-lib-devel pulseaudio-libs-devel \
  libcurl-devel libmicrohttpd-devel lame-devel libev-devel \
  taglib-devel openssl-devel libopenmpt-devel asio-devel \
  ffmpeg-free-devel
```

If you are using the proprietary `ffmpeg` codecs offered via `rpmfusion`,
you can replace `ffmpeg-free-devel` with `ffmpeg-devel`.

https://github.com/clangen/musikcube/wiki/building

```sh
cmake -G "Unix Makefiles" .
make
sudo make install
```

## Troubleshooting

I have noticed that mouse support and some keychords don't respond well
when running `musikcube` within a GNU Screen multiplexer session. But when
running `musikcube` within a `tmux` multiplexer session everything just
works.
