Working with the Trello API
==================================

# Summary

- Created on: Oct 26 2023
- Created by: gopeterjun@naver.com
- Last Updated: Oct 26 2023

Notes, tips, tricks for working with the Trello API.

# Topics

## Get API Key and Token

First go to the Power-Ups admin screen https://trello.com/power-ups/admin

Click the blue *New* button at the upper-right.

You will see a web form titled *New Power-Up or Integration*. Here enter
the name of your Power-Up, which Workspace the Power-Up will act on, an
iframe connector url (this is only used if your Power Up is offered
publicly but you still have to enter some URL; use the URL for your
personal site, for example), and other info.

You will then be taken to a *Basic Information* screen where you select
your Power Up Categories, email, and support contact email info. From the
left-hand menu, click on *API key*. You can paste in an existing API key or
generate a new one for your app. For the purposes of making a personal app,
from this page, you can click on the *Token* link next to the *API key*
field. The token link will take you to `https://trello.com/1/`
with the following endpoint:

```
authorize?expiration=never&scope=read,write,account&response_type=token&key=<API-key>
```

You will then see a token on the screen which will only be shown once.
This token will have full rights on your account.

## Get list of boards

The [API
Introduction](https://developer.atlassian.com/cloud/trello/guides/rest-api/api-introduction/)
shows all examples using `curl` but in my examples below I will translate
these to `python requests` format.

First in your shell session, export your Trello API Key and Token
as shell env vars as follows:

```sh
export apikey="YourTrelloAPIKey"
export apitoken="LongToken"
```

The first example below will return all the boards in your account.

```sh
curl https://api.trello.com/1/members/me/boards?fields=name,url&key={apikey}&token={apitoken}
```

Let's do the same thing in Python. Start a Python REPL session in the
same shell session where you exported `apikey` and `apitoken`.

```python
import os
import python

apikey = os.getenv("apikey")
apitoken = os.getenv("apitoken")

url = "https://api.trello.com/1/members/me/boards"
sess = requests.Session()
params = {"key": apikey, "token": apitoken}
resp_sess = sess.get(url, params=params)
resp_sess.raise_for_status()
data = resp_sess.json() # returns a ListOfDict
```

This can return lots of boards if you've been using Trello for a long
time. In my case, I had 76 boards, but most of them are closed.

Below is a description of commonly used keys in the dictionary for Trello
boards, but the following list is *not* comprehensive. Refer to the [Boards
API
reference](https://developer.atlassian.com/cloud/trello/rest/#api-boards-id-get)
for more details.

- `id`: Board ID (unique)
- `nodeid`: contains semantic board address including workspace
- `name`: human-readable board name
- `desc`: board description
- `closed`: whether the board is closed or not
- `idOrganization`: relevant for Trello for Teams(?)
- `idEnterprise`: relevant for Trello for Teams(?)
- `limits`: a dict containing sub-keys with usage limits
  + `attachments`
    + `perBoard`: 36000 max, 28800 warn
    + `perCard`: 1000 max, 800 warn
  + `cards`
    + `openPerBoard`: 5000 max, 4000 warn
    + `openPerList`: 5000 max, 4000 warn
    + `totalPerBoard`: 2000000 max, 1600000 warn
    + `totalPerList`: 1000000 max, 800000 warn
  + `checklists`
    + `perBoard`: 1800000 max, 1440000 warn
    + `perCard`: 500 max, 400 warn
  + `checkItems`:
    + `perChecklist`: 200 max, 160 warn
  + `labels`
    + `perBoard`: 1000 max, 800 warn
  + `lists`
    + `openPerBoard`: 500 max, 400 warn
    + `totalPerBoard`: 3000 max, 2400 warn
  + `stickers`
    + `perCard`: 70 max, 56 warn
    + `reactions`
      + `perAction`: 900 max, 720 warn
      + `uniquePerAction`: 17 max, 14 warn
- `url`: https link to board
- `prefs`: dict containing board preferences
  + `permissionLevel`: `public` or `private`
  + `comments`: `members` or ?
  + `calendarFeedEnabled`: `True` or `False`
- `shortLink`: short code for board can be used instead of name in url
  + `https://trello.com/b/<shortLink>`
- `labelNames`: dict containing mapping between label colors and names
- `dateLastActivity`: ISO 8601 date string of last activity
- `dateLastView`: ISO 8601 date string of last board view
- `shortUrl`: https link to board using `shortLink`
  + `https://trello.,com/b/<shortLink>`

Note that when making a Trello board
[GET](https://developer.atlassian.com/cloud/trello/rest/api-group-boards/#api-boards-id-get-request)
request only the following query parameters are supported:

- actions
- boardStars
- cards
- card_pluginData
- checklists
- customFields
- fields
- labels
- lists
- members
- memberships
- pluginData
- organization
- organization_pluginData
- myPrefs
- tags

It is not possible to query the REST API for boards that are not closed,
although you could do this filtering yourself once you have retrieved
a list of all boards and parsing the `closed` field within each dict
representing a Trello board.

The REST API has a `/1/search?query="searchTerm"` endpoint that
can search all actions, boards, cards, members, and organizations. You
can't specify which fields to search, but you *can* specify the fields
you want to return.


## Find a specific board from the ListOfDicts of all boards

Continuing from the example above,



## Get Filtered Cards on a Board

https://developer.atlassian.com/cloud/trello/rest/api-group-boards/#api-boards-id-cards-filter-get

```python
import requests


id = "myboardid"
# can be one of "all|closed|none|open|visible"
filter = "closed"
url = f"https://api.trello.com/1/boards/{id}/cards/{filter}"

params = {"key": apikey,
           "token": apitoken}

sess = requests.session()
resp_sess = sess.get(url, params=params)
resp_sess.raise_for_status()
data = resp_sess.json()

```
